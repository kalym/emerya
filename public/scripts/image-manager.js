;(function($) {

$.fn.imageManager = function(params) {
    return this.each(function(){
        var imageManager = this;
        if ($(imageManager).data('initialized')) {
            return;
        } else {
            $(imageManager).data('initialized', 1);
        }

        var param = $.extend({
          images : [],
          'upload-button' : '/public/images/admin/upload234.png',
          'upload-button-width' : 234,
          'upload-action' : '/upload/product',
          'placeholder' : '/public/images/img234x234.png',
          'limit' : 5,
          'prefix' : '_gallery_',
          'title' : '����������',
          'img-preview' : 'preview',
          'img-map' : []
        }, params);

        var $a = $('<a></a>');
        $a.append(
            $('<img />').attr('src', '/public/images/admin/add.png')
        ).attr('href', 'javascript:;').
        click(function(){
            var empty = {};
            for (var $i in param['img-map'])
                empty[param['img-map'][$i]]='';
            addImgBlock(empty);
            return false;
        });


        $(imageManager).empty().append(
            $('<h2></h2>').append(param.title + ' ').append($a)
        )

        var counter = 0;
        var id = 0;

        var getImgBlock = function(images) {
            counter++;
            id++;
            var $div = $('<div></div>');
            var $imgBlock = $div.clone().
            css({
                'float':'left',
                paddingTop: '0.5em',
                paddingRight : '0.5em',
                position : 'relative'
            }).append(
               $('<img />').attr('src', '/public/images/admin/delete.png').
               css({
                    position : 'absolute',
                    top : 0,
                    left : param['upload-button-width'] - 10,
                    cursor : 'pointer'
               }).click(function(){
                   counter--;
                   if (counter==(param['limit']-1)) $a.show();
                   $(this).parent().remove();
                   return false;
               })
            ).append(
                $('<img />').attr('src', images[param['img-preview']] || param['placeholder']).
                    addClass('imgorig')
            ).append('<br />').append(
                $div.clone().attr('id', 'fileUpload-' + param['prefix'] + id)
            );
            for (var $k in images) { 
                $imgBlock.append(
                    $('<input />').attr({type:'hidden', name:param['prefix'] + $k + '[]', value : images[$k]})
                )
            }
                
            if (counter == param['limit']) $a.hide();    
                
            return $imgBlock;
            
        }

        var addImgBlock = function(images) {
            var imgBlock = getImgBlock(images);
            $(imageManager).append(imgBlock);

            $("#fileUpload-"+ param['prefix'] + id).fileUpload({
		'uploader': '/public/scripts/uploadify/uploader.swf',
		'cancelImg': '/public/scripts/uploadify/cancel.png',
		'script': param['upload-action'],
		'multi': false,
                'buttonImg' : param['upload-button'],
                'width': param['upload-button-width'],
                'height' : 31,
                'wmode'  : 'transparent',
                'auto' : true,
                'onComplete': function (event, queueID, fileObj, response, data) {
                    res = eval('(' + response + ')');
                    if (!res.errorCode) {
                        
                        $(".imgorig", imgBlock).attr('src', res[param['img-preview']]);
                        for (var $i in param['img-map']) {
                            $("input[name^=" + param['prefix'] + param['img-map'][$i] + "]", imgBlock).attr('value', res[param['img-map'][$i]]);
                        }
                    }
                    return true
                },
                'onError': function (a, b, c, d) {
                    if (d.status == 404)
                        alert('Could not find upload script. Use a path relative to: '+'<?= getcwd() ?>');
                    else if (d.type === "HTTP")
                        alert('error '+d.type+": "+d.status);
                    else if (d.type ==="File Size")
                        alert(c.name+' '+d.type+' Limit: '+Math.round(d.sizeLimit/1024)+'KB');
                    else
                        alert('error '+d.type+": "+d.text);
                }
            });
        }

        $.each(param.images, function(){
            addImgBlock(this);
        });

    })
}

})(jQuery);