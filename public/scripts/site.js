function ajaxSubmit(form) {
    var f_name = $(form).attr('id');
        $("#" + f_name).closest('div').find(".result").removeClass('ok error').empty().html('<img src="/public/images/loading.gif">');
        v = new Object();
        $.each( form, function() {
            v[this.name] = this.value;
        });
        v['_form_name_'] = f_name;

        $.ajax({
            'type' : 'POST',
            'url'  : form.action,
            'data' : v,
            'dataType' : 'json',
            'success' : showRequestResult
         });
         return false;
}    
    
function showRequestResult(res) {
    var f_name = res._form_name_;
    if (!res.errorCode) {
        $("#" + f_name).closest('div').find(".result").empty().removeClass('ok error').addClass('ok').html(res.msg);
        switch (res.action) {
            case 'refresh' :
                setTimeout( "location.reload()", 1000);
                break;
            case 'redirect' :
                setTimeout( function(){
                    location.href = res.url;
                }, 1000);
                break;
            case 'reset' :
                $("#" + f_name).get(0).reset();
                setTimeout(function(){
                  $("#" + f_name).closest('div').find(".result").empty().removeClass('ok error');
                }, 1000);           
            case 'close' :
                //setTimeout( "$.fancybox.close()", 1000);
                setTimeout( "$('.fader').click()", 1000);
                break;
            case 'hide-form' : 
                $("#" + f_name).closest('.form').remove();
                break;
            case 'cb' :
                console.log(res.cb);
                window[res.cb](res);
                setTimeout( "$('.fader').click()", 1000);
            default:
                break;
        }
    } else {
        $("#" + f_name).closest('div').find(".result").removeClass('ok error').addClass('error').html(res.msg);
    }
}