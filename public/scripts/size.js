
;(function($) {
$.fn.size = function(param) {
    return this.each(function(){
        var size = this;
        if ($(size).data('initialized')) {
            return;
        } else {
            $(size).data('initialized', 1);
        }

        var param = $.extend({
            'size-small' : '100%',
            'size-normal' : '115%',
            'size-big' : '130%'
        }, param)
        
        init();
        
        function drawSizeBlock() {
            var $content = $('<div>������ ������: <a href="javascript:;" class="size-small">�</a> \
                <a href="javascript:;" class="size-normal">�</a> \
                <a href="javascript:;" class="size-big">�</a></div>');
            $(size).empty().append($content);
            
            $content.find('a.size-small').click(function() {
                $(this).parent().find('a').removeClass('size-active');
                $(this).addClass('size-active');
                $('.article-content').css({
                    'font-size' : param['size-small'],
                    'line-height' : 'normal'
                });
                setCookie('sizeActive', 'size-small');
            })
            
            $content.find('a.size-normal').click(function() {
                $(this).parent().find('a').removeClass('size-active');
                $(this).addClass('size-active');
                $('.article-content').css({
                    'font-size':param['size-normal'],
                    'line-height' : 'normal'
                });
                setCookie('sizeActive', 'size-normal');
            })
            
            $content.find('a.size-big').click(function() {
                $(this).parent().find('a').removeClass('size-active');
                $(this).addClass('size-active');
                $('.article-content').css({
                    'font-size':param['size-big'],
                    'line-height' : 'normal'
                });
                setCookie('sizeActive', 'size-big');
            })
        }
        
        function init() {
            drawSizeBlock();
            var sizeActive = getCookie('sizeActive') || 'size-small';
            $(size).find('.' + sizeActive).addClass('size-active');
            $('.article-content').css({
                    'font-size':param[sizeActive],
                    'line-height' : 'normal'
                });
        }
        
        function setCookie(name, value) {
              var valueEscaped = escape(value);
              var expiresDate = new Date();
              expiresDate.setTime(expiresDate.getTime() + 365 * 24 * 60 * 60 * 1000); // 1 year
              var expires = expiresDate.toGMTString();
              var newCookie = name + "=" + valueEscaped + "; path=/; expires=" + expires;
              if (valueEscaped.length <= 4000) document.cookie = newCookie + ";";
        }


        function getCookie(name) {
              var prefix = name + "=";
              var cookieStartIndex = document.cookie.indexOf(prefix);
              if (cookieStartIndex == -1) return null;
              var cookieEndIndex = document.cookie.indexOf(";", cookieStartIndex + prefix.length);
              if (cookieEndIndex == -1) cookieEndIndex = document.cookie.length;
              return unescape(document.cookie.substring(cookieStartIndex + prefix.length, cookieEndIndex));
        }
        
    })
}
})(jQuery);
