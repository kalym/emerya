;(function($) {

$.fn.imageManager = function(param) {
    return this.each(function(){
        var imageManager = this;
        var maxPhoto = 5;
        if ($(imageManager).data('initialized')) {
            return;
        } else {
            $(imageManager).data('initialized', 1);
        }

        param = $.extend({
          images : []
        }, param)

        var $a = $('<a></a>');
        $a.append(
            $('<img />').attr('src', '/public/images/admin/add.png')
        ).attr('href', 'javascript:;').
        click(function(){
            addImgBlock('', '', '');
            return false;
        });


        $(imageManager).empty().append(
            $('<h2></h2>').append('�������������� ���������� ').append($a)
        )

        var counter = 0;
        var id = 0;

        var getImgBlock = function(preview, orig) {
            counter++;
            id++;
            var $div = $('<div></div>');
            var $imgBlock = $div.clone().
            css({
                'float':'left',
                paddingTop: '0.5em',
                paddingRight : '0.5em',
                position : 'relative'
            }).append(
               $('<img />').attr('src', '/public/images/admin/delete.png').
               css({
                    position : 'absolute',
                    top : 0,
                    left : 85,
                    cursor : 'pointer'
               }).click(function(){
                   counter--;
                   if (counter==maxPhoto-1) $a.show();
                   $(this).parent().remove();
                   return false;
               })
            ).append(
                $('<img />').attr('src', (preview ? preview : '/public/images/photo0.png')).
                    addClass('imgPreview')
            ).append('<br />').append(
                $div.clone().attr('id', 'fileUpload-' + id)
            ).append(
                $('<input />').attr({type:'hidden', name:'img_preview[]', value : preview})
            ).append(
                $('<input />').attr({type:'hidden', name:'img_orig[]', value : orig})
            )  
                
            if (counter == maxPhoto) $a.hide();    
                
            return $imgBlock;
            
        }

        var addImgBlock = function(preview, orig) {

            preview = preview || '';
            orig = orig || '';

            var imgBlock = getImgBlock(preview, orig);
            $(imageManager).append(imgBlock);

            $("#fileUpload-"+id).fileUpload({
		'uploader': '/public/scripts/uploadify/uploader.swf',
		'cancelImg': '/public/scripts/uploadify/cancel.png',
		'script': '/upload/img-gallery',
		'multi': false,
                'buttonImg' : '/public/images/button-small.png',
                'width': 96,
                'height' : 31,
                'wmode'  : 'transparent',
                'auto' : true,
                'onComplete': function (event, queueID, fileObj, response, data) {
                    res = eval('(' + response + ')');
                    if (!res.errorCode) {
                        $(".imgPreview", imgBlock).attr('src', res.preview);
                        $("input[name^=img_preview]", imgBlock).attr('value', res.preview);
                        $("input[name^=img_orig]", imgBlock).attr('value', res.orig);
                    }
                    return true
                },
                'onError': function (a, b, c, d) {
                    if (d.status == 404)
                        alert('Could not find upload script. Use a path relative to: '+'<?= getcwd() ?>');
                    else if (d.type === "HTTP")
                        alert('error '+d.type+": "+d.status);
                    else if (d.type ==="File Size")
                        alert(c.name+' '+d.type+' Limit: '+Math.round(d.sizeLimit/1024)+'KB');
                    else
                        alert('error '+d.type+": "+d.text);
                }
            });
        }

        $.each(param.images, function(){
            addImgBlock(this[0], this[1]);
        });

    })
}

})(jQuery);