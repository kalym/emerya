;(function($) {

$.fn.videoManager = function(param) {
    return this.each(function(){
        var videoManager = this;
        var maxVideo = 5;
        if ($(videoManager).data('initialized')) {
            return;
        } else {
            $(videoManager).data('initialized', 1);
        }

        param = $.extend({
          items : []
        }, param)

        var $a = $('<a></a>');
        $a.append(
            $('<img />').attr('src', '/public/images/admin/add.png')
        ).attr('href', 'javascript:;').
        click(function(){
            addVideoBlock('', '', '');
            return false;
        });


        $(videoManager).empty().append(
            $('<h2></h2>').append('�������������� ����� ').append($a)
        )

        var counter = 0;
        var id = 100;

        var getVideoBlock = function(thumbnail, preview, video) {
            counter++;
            id++;
            var $div = $('<div></div>');
            var $div_video = $('<div style="width:300px; overflow:hidden"></div>').
                append('<br /><strong>���� � ����� (Youtube)</strong><br />').
                append(
                    $('<input />').attr({type:'text', name:'video_video[]', style: 'width:300px', value: video})
                );
            var $previewBlock = $div.clone().
            css({
                paddingTop: '0.5em',
                paddingRight : '0.5em'
            }).append(
                $('<img />').attr('src', (thumbnail ? thumbnail : '/public/images/video0.png')).
                    addClass('imgPreview')
            ).append('<br />').append(
                $div.clone().attr('id', 'fileUpload-' + id)
            ).append(
                $('<input />').attr({type:'hidden', name:'video_thumbnail[]', value : thumbnail})
            ).append(
                $('<input />').attr({type:'hidden', name:'video_preview[]', value : preview})
            )
            
            var $videoBlock = $('<div style="float:left; margin:0 0.5em 0.5em 0; overflow:hidden; border:1px solid #ccc; padding:0.5em"></div>').
                append(
                   $('<img />').attr('src', '/public/images/admin/delete.png').
                   css({
                        'float' : 'right',
                        cursor : 'pointer'
                   }).click(function(){
                       counter--;
                       if (counter==maxVideo-1) $a.show();
                       $(this).parent().remove();
                       return false;
                   })
                ).
                append($previewBlock).
                append($div_video);
                
            if (counter == maxVideo) $a.hide();    
                
            return $videoBlock;
            
        }

        var addVideoBlock = function(thumbnail, preview, video) {

            thumbnail = thumbnail || '';
            preview = preview || '';
            video = video || '';

            var videoBlock = getVideoBlock(thumbnail, preview, video);
            $(videoManager).append(videoBlock);

            $("#fileUpload-"+id).fileUpload({
		'uploader': '/public/scripts/uploadify/uploader.swf',
		'cancelImg': '/public/scripts/uploadify/cancel.png',
		'script': '/upload/video-gallery',
		'multi': false,
                'buttonImg' : '/public/images/button-xsmall.png',
                'width': 80,
                'height' : 31,
                'wmode'  : 'transparent',
                'auto' : true,
                'onComplete': function (event, queueID, fileObj, response, data) {
                    res = eval('(' + response + ')');
                    if (!res.errorCode) {
                        $(".imgPreview", videoBlock).attr('src', res.thumbnail);
                        $("input[name^=video_thumbnail]", videoBlock).attr('value', res.thumbnail);
                        $("input[name^=video_preview]", videoBlock).attr('value', res.preview);
                    }
                    return true
                },
                'onError': function (a, b, c, d) {
                    if (d.status == 404)
                        alert('Could not find upload script. Use a path relative to: '+'<?= getcwd() ?>');
                    else if (d.type === "HTTP")
                        alert('error '+d.type+": "+d.status);
                    else if (d.type ==="File Size")
                        alert(c.name+' '+d.type+' Limit: '+Math.round(d.sizeLimit/1024)+'KB');
                    else
                        alert('error '+d.type+": "+d.text);
                }
            });
        }

        $.each(param.items, function(){
            addVideoBlock(this[0], this[1], this[2]);
        });

    })
}

})(jQuery);