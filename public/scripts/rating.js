
;(function($) {
$.fn.rating = function(param) {
    return this.each(function(){
        var rating = this;
        if ($(rating).data('initialized')) {
            return;
        } else {
            $(rating).data('initialized', 1);
        }

        param = $.extend({
          autoStart : true
        }, param)
        
        var params = eval('(' + $(rating).attr('rel') + ')');

        drawRatingBlock(params);

        function drawRatingBlock(params) {

            var aLeft = $('<a></a>').
                    addClass('plus').
                    attr('href', 'javascript:;').
                    attr('title', '+').click(function(){
                        $(rating).find('div').empty().append(
                            $('<img />').attr('src', '/public/images/loading_r.gif').css('marginTop', '4px')
                        );
                        v = {
                            rate : 1,
                            id: params.id
                        };

                        $.ajax({
                            'type' : 'POST',
                            'url'  : '/articles/rate/',
                            'data' : v,
                            'dataType' : 'json',
                            'success' : drawRatingBlock
                        })
                    })

            var aRight = $('<a></a>').
                    addClass('minus').
                    attr('href', 'javascript:;').
                    attr('title', '-').click(function(){
                        $(rating).find('div').empty().append(
                            $('<img />').attr('src', '/public/images/loading_r.gif').css('marginTop', '4px')
                        );
                        v = {
                            rate : -1,
                            id: params.id
                        };

                        $.ajax({
                            'type' : 'POST',
                            'url'  : '/articles/rate/',
                            'data' : v,
                            'dataType' : 'json',
                            'success' : drawRatingBlock
                        })
                    })

            $(rating).empty();
            $(rating).append(
                $('<div class="num"></div>').append(params.rate)
            ).append(
                aLeft
            ).append(
                aRight
            );

            if (params.error) {
                alert(params.error);
            }
        }
        
    })
}
})(jQuery);