/* headerDecor */
function headerDecor(){
	if ( $('.header-decor').length ) {
		var main = $('.main');
		var header_decor = $('.header-decor');
		var header_h = $('.header').height();
		var content_top = $('.content-top').height();
		var main_w_half = main.width()/2;
		header_decor.css({'border-left-width':main_w_half,'border-right-width':main_w_half});
		
		function initHeaderScroll(){
			var win_top = $(window).scrollTop();
			var progress = win_top/content_top;
			$('.header,.content-top-main').css('opacity',1-progress);
			$('.header').css('top',((header_h*1.7)*progress));
			$('.content-top-main').css('top',-((header_h/2)*progress));	
		}
		initHeaderScroll();
	
		// scroll
		$(window).scroll(function(){
			initHeaderScroll();
		});
	
		// resize
		$(window).resize(function(){
			content_top = $('.content-top').height();
			main_w_half = main.width()/2;
			var main_left = main.offset().left;
			header_decor.css({'border-left-width':main_w_half,'border-right-width':main_w_half});
			if ( header_decor.hasClass('fixed') )
				header_decor.css('left',main_left);
			initHeaderScroll();
		});
	}
}
/* headerDecor end */

/* faces */
function faces(){
	if ( $('.faces-photos-list').length ) {
		var facesInterval;
		var faces_length = $('.faces-photos-list > li:visible').length;
		var old_rand;
		$(window).resize(function(){
			faces_length = $('.faces-photos-list > li:visible').length;
		});

		// facesIntervalFn
		function facesIntervalFn(){
			facesInterval = setInterval(function(){
				var rand = parseInt(Math.round(Math.random()*(faces_length-1)));
				while( rand == old_rand ){
					var rand = parseInt(Math.round(Math.random()*(faces_length-1)));
				}
				toggleFaces($('.faces-photos-list > li:eq('+rand+') .face-photo'));
			},3000);	
		}
		facesIntervalFn();
		
		// faces hover
		$('.faces').hover(
			function(){
				clearInterval(facesInterval);
			},
			function(){
				facesIntervalFn();
			}
		);
		
		// toggleFaces
		function toggleFaces(obj){
			var cur = obj.parents('li:eq(0)');
			var index = cur.index();
			old_rand = index;
			cur.addClass('current').siblings('.current').removeClass('current');
			cur.parents('.faces:eq(0)').find('.faces-info-list > li:eq('+index+')').addClass('current').siblings('.current').removeClass('current');
		}
		
		// face-hover
		$('.faces-photos-list .face-photo').hover(
			function(){
				toggleFaces($(this));
			},
			function(){}
		);
	}
}
/* faces end */

/* sideMenu */
function sideMenu(){
	// js-side-link
	if ( $('.js-side-link').length ){
		$('.js-side-link').on('click',function(e){
			var cur = $(this);
			var cur_li = cur.parents('li:eq(0)');
			if ( cur_li.hasClass('active') ){
				cur_li.removeClass('active');
				$('.side-menu-overlay').fadeOut(500,function(){
					$(this).remove();
				});
				$('.side-submenu-menus-wrap,.side-submenu-login,.side-submenu-remember-password,.side-menu-buy-wrap,.side-submenu-profile-wrap').removeClass('active');
				$('.side-submenu-registration').addClass('active');
			} else {
				$('.side-menu-overlay').fadeOut(500,function(){
					$(this).remove();
				});
				cur_li.addClass('active').siblings('.active').removeClass('active');
				$('.side-submenu-menus-wrap,.side-submenu-login,.side-submenu-remember-password,.side-menu-buy-wrap,.side-submenu-profile-wrap').removeClass('active');
				$('.side-submenu-registration').addClass('active');
				$('.side-menu-wrap').after('<div class="side-menu-overlay"></div>');
				$('.side-menu-overlay').fadeIn(500);
			}
			e.preventDefault();
		});
		
		// overlay click
		$('body').on('click','.side-menu-overlay',function(){
			$('.side-menu-list > li.active .js-side-link').trigger('click');
		});
	}
	
	// admin-question	
	if ( $('.admin-question-open').length ) {
		$('.admin-question-open').on('click',function(e){
			$('.side-submenu-menus-wrap').addClass('active');
			e.preventDefault();
		});
		$('.admin-question-close').on('click',function(e){
			$('.side-submenu-menus-wrap').removeClass('active');
			e.preventDefault();
		});
	}
	
	// side-pay-open
	if ( $('.side-pay-open').length ) {
		$('.side-pay-open').on('click',function(e){
			$('.side-menu-buy-wrap').addClass('active');
			e.preventDefault();
		});
	}
	
	// side-pay-promo-activate
	if ( $('.side-pay-promo-activate').length ) {
		$('.side-pay-promo-activate').on('click',function(e){
			alert('Промокод Активирован');
			e.preventDefault();
		});
	}
	
	// side-login-open
	if ( $('.side-login-open').length ) {
		$('.side-login-open').on('click',function(e){
			$('.side-submenu-login').addClass('active').siblings('.active').removeClass('active');
			e.preventDefault();
		});
	}
	
	// side-registration-open
	if ( $('.side-registration-open').length ) {
		$('.side-registration-open').on('click',function(e){
			$('.side-submenu-registration').addClass('active').siblings('.active').removeClass('active');
			e.preventDefault();
		});
	}
	
	// remember-password-link
	if ( $('.remember-password-link').length ) {
		$('.remember-password-link').on('click',function(e){
			$('.side-submenu-remember-password').addClass('active').siblings('.active').removeClass('active');
			e.preventDefault();
		});
	}
	
	// profile-edit-open
	if ( $('.profile-edit-open').length ) {
		$('.profile-edit-open').on('click',function(e){
			$('.side-submenu-profile-wrap').addClass('active').siblings('.active').removeClass('active');
			e.preventDefault();
		});
	}
}
/* sideMenu end */

/* payments */
function payments(){
	if ( $('.payment-item').length ) {
		$('.payment-item').on('click',function(){
			var cur = $(this);
			if ( !cur.hasClass('active') ) {
				cur.addClass('active');
				cur.parents('li:eq(0)').siblings().find('.payment-item.active').removeClass('active');
			}
		});
	}
}
/* payments end */

/* listedGallery */
function listedGallery(){
	if ( $('.tile-list').length ) {
		var indent = 0.2; // процент высоты картинки, который должен быть виден, чтобы картинка появилась
		var min_duration = 0.1;  // регулирует процент отличия в скорости появления картинок (default: 0.4 / 0.7)
		var max_duration = 0.9;
		var win_h = $(window).height();
		var scroll_top = $(window).scrollTop();
		var win_bottom = win_h + scroll_top;
		var elem_h = $('.tile-list > li:first').height();
		var visible_indent = elem_h*indent;
		$('.tile-list > li:not(.active)').each(function(){
			var elem = $(this);
			var elem_top = elem.offset().top;
			var randDuration = ( Math.random() * ( max_duration - min_duration ) + min_duration ) + 's';
			if ( (elem_top+visible_indent) < win_bottom )
				elem.css('transition-duration',randDuration).addClass('active');
		});
	}
}
/* listedGallery end */

/* siteToTop */
function siteToTop(){
	
}
/* siteToTop end */

/* showRightFixedLinks */
function showRightFixedLinks(){
	
}
/* showRightFixedLinks end */

/* fixedLinks */
function fixedLinks(){
	if ( $('.right-fixed-links.bottom').length && $('.subscribe').length ) {
		var links_bottom = $('.right-fixed-links.bottom')
		var subscribe = $('.subscribe');
		
		function linksPos(){
			var scroll_top = $(window).scrollTop();
			var scroll_bottom = scroll_top + $(window).height();
			var subscribe_top = subscribe.offset().top;
			var subscribe_height = parseInt(subscribe.innerHeight());
			var subscribe_bottom = subscribe_top + subscribe_height;
			var subsscribe_middle = subscribe_top + (subscribe_height/2);
			if ( scroll_bottom >= subscribe_bottom )
				links_bottom.addClass('absolute').css('top',subscribe_bottom);	
			else
				links_bottom.removeClass('absolute').removeAttr('style');
		}
		linksPos();
		$(window).scroll(function(){
			linksPos();
		});
		$(window).resize(function(){
			linksPos();
		});
	}
	
	// totop-link
	if ( $('.fixed-totop-link').length ) {
		$('.fixed-totop-link').on('click',function(e){
			$('html,body').animate({scrollTop:0},500);
			e.preventDefault();
		});
	}
	
	// show links
	if ( $('.right-fixed-links.hidden').length ) {
		var indent = 200;
		function showLinks(){
			if ( $(window).scrollTop() > indent )
				$('.right-fixed-links').removeClass('hidden');
		}
		showLinks();
		$(window).scroll(function(){
			showLinks();
		});
	}
}
/* fixedLinks end */

/* popups */
function popups(){
	if ( $('.js-popup-link').length ) {
		// open
		$('.js-popup-link').on('click',function(e){
			var cur = $(this);
			if ( cur.hasClass('active') )
				$('.popup-overlay').trigger('click');
			else {
				cur.addClass('active');
				var cur_attr = cur.attr('href');
				var popup = $(cur_attr);
				popup.addClass('active').fadeIn().siblings('.popup-wrap.active').fadeOut();
				if ( popup.hasClass('popup-search') ){
					$('html,body').animate({scrollTop:0},500);
					$('.popup-search-input').trigger('focus');	
				}
			}
			e.preventDefault();
		});
		
		// close
		$('.popup-overlay,.js-popup-close').on('click',function(e){
			$('.js-popup-link.active').removeClass('active');
			$('.popup-wrap.active').removeClass('active').fadeOut();
		});
	}
}
/* popups end */

/* showMore */
function showMore(){
	// js-more-news
	if ( $('.js-news-more').length ) {
		$('.js-news-more').on('click',function(e){
			var cur = $(this);
			var parent = cur.parents('.news');
			parent.find('.news-item:hidden').fadeIn(500);
			cur.parents('.news-buttons').remove();
			e.preventDefault();
		});
	}

	// js-reservations-more
	if ( $('.js-reservations-more').length ) {
		$('.js-reservations-more').on('click',function(e){
			var cur = $(this);
			var parent = cur.parents('.reservations');
			parent.find('.reservation:hidden').fadeIn(500);
			cur.parents('.reservations-buttons').remove();
			e.preventDefault();
		});
	}

	// js-unique-views-more
	if ( $('.js-unique-views-more').length ) {
		$('.js-unique-views-more').on('click',function(e){
			var cur = $(this);
			var parent = cur.parents('.unique-views');
			parent.find('.unique-view:hidden').fadeIn(500);
			cur.parents('.unique-views-buttons').remove();
			e.preventDefault();
		});
	}
	
	// js-mypublications-more
	if ( $('.js-mypublications-more').length ) {
		$('.js-mypublications-more').on('click',function(e){
			var cur = $(this);
			var parent = cur.parents('.mypublications');
			parent.find('article:hidden').fadeIn(500);
			cur.parents('.mypublications-buttons').remove();
			e.preventDefault();
		});
	}
	
	// js-messages-more
	if ( $('.js-messages-more').length ) {
		$('.js-messages-more').on('click',function(e){
			var cur = $(this);
			var parent = cur.parents('.messages');
			parent.find('.message:hidden').fadeIn(500);
			cur.parents('.messages-buttons').remove();
			e.preventDefault();
		});
	}

	// js-publications-more
	if ( $('.js-publications-more').length ) {
		$('.js-publications-more').on('click',function(e){
			var cur = $(this);
			var parent = cur.parents('.publications');
			parent.find('article:hidden').css({'opacity':0,'display':'inline-block'}).animate({'opacity':1},500);
	
			cur.parents('.publications-buttons').remove();
			e.preventDefault();
		});
	}
	
	// js-experts-more
	if ( $('.js-experts-more').length ) {
		$('.js-experts-more').on('click',function(e){
			var cur = $(this);
			var parent = cur.parents('.experts');
			parent.find('.experts-list > li:hidden').fadeIn(500);
			cur.parents('.experts-buttons').remove();
			e.preventDefault();
		});
	}
	
	// js-podcasts-more
	if ( $('.js-podcasts-more').length ) {
		$('.js-podcasts-more').on('click',function(e){
			var cur = $(this);
			var parent = cur.parents('.podcasts');
			parent.find('article:hidden').fadeIn(500);
			cur.parents('.podcasts-buttons').remove();
			e.preventDefault();
		});
	}
	
	// js-search-more
	$('body').on('click','.js-search-more',function(e){
		var cur = $(this);
		var parent = cur.parents('.popup-search-results');
		parent.find('.popup-search-results-section > article:hidden').css({'opacity':0,'display':'inline-block'}).animate({'opacity':1},500);
		cur.parents('.popup-search-results-buttons').remove();
		e.preventDefault();
	});
}
/* showMore end */

/* buttonsFn */
function buttonsFn(){
	// publications-add-desc-close
	if ( $('.publications-add-desc-close') ) {
		$('.publications-add-desc-close').on('click',function(e){
			$('.publications-add-desc').fadeOut(function(){
				$('.publications-add-desc-close').remove();
			});
			e.preventDefault();
		});
	}
	
	// js-add-message
	if ( $('.js-add-message') ) {
		$('.js-add-message').on('click',function(e){
			var cur = $(this);
			if ( $('.user-page').length ) {
				if ( cur.parents('.user-actions').length )
					cur.parents('.user-actions').next('.add-message').slideDown();
				else
					cur.next('.add-message').slideDown();
			} else
				$('.add-message').slideDown();
			if ( cur.hasClass('btn-gray') )
				cur.remove();
			e.preventDefault();
		});
	}
	
	// add-tofav
	if ( $('.fav-holder:not(.disabled)').length ) {
		$('.fav-holder:not(.disabled) .fav-link').on('click',function(){
			var cur = $(this);
			var parent = cur.parents('.fav-holder');
			var number = parent.find('.fav-value');
			if ( parent.hasClass('active') )
				number.html(parseInt(number.text())-1);
			else
				number.html(parseInt(number.text())+1);
			parent.toggleClass('active');
		});
	}
	
	// js-all-categories
	if ( $('.js-all-categories').length ) {
		$('.js-all-categories').on('click',function(e){
			var cur = $(this);
			$('.js-side-link.side-menu-categories').trigger('click');
			e.preventDefault();
		});
	}
	
	// side-submenu-categories-list
	if ( $('.side-submenu-categories-list').length ) {
		$('.side-submenu-categories-list a').on('click',function(e){
			var cur = $(this);
			var cur_text = cur.text();
			$('.js-all-categories span').html(cur_text);
			var parent = cur.parents('li:eq(0)');
			parent.addClass('current').append('<span class="green-checked"></span>');
			$('.side-submenu-categories-list li.current').not(parent).removeClass('current').find('.green-checked').remove();
			$('.js-side-link.side-menu-categories').trigger('click');
			e.preventDefault();
		});
	}
	
	// full-subscribe-item
	if ( $('.full-subscribe-item').length ) {
		$('.full-subscribe-item').on('click',function(e){
			$('.js-side-link.side-menu-buy').trigger('click');
			e.preventDefault();
		});
	}
}
/* buttonsFn end */

/* refreshSliderUsers */
function refreshSliderUsers(){
	if ( $('.refresh-slider-users').length ) {
		var list = $('.refresh-slider-users .refresh-slider-list');
		var slides_count = list.children(':visible').length;
		$(window).resize(function(){
			slides_count = list.children(':visible').length;
		});
		
		var hidden_list = $('.refresh-slider-users .refresh-hidden-list');
		var hidden_list_length = hidden_list.children().length;
		var hidden_count = slides_count;
		
		var refreshInterval;
		var old_random;
		
		function refreshAnimate(refresh_type){ // 0 - случайная анимация, 1 - по порядку
			var interval_delay = 1500;
			if ( refresh_type == 1 ){
				var random = 0;
				interval_delay = 100;
			}
			
			refreshInterval = setInterval(function(){
				if ( refresh_type == 0 ){
					while( random == old_random ){
						random = Math.round(Math.random()*(slides_count-1));
					}
					old_random = random;
				}
				var cur_li = list.children().eq(random);
				var cur_elem = cur_li.children().eq(0).addClass('old');
				var hidden_elem = hidden_list.children().eq(hidden_count).children().clone().addClass('new').appendTo(cur_li);
				setTimeout(function(){
					hidden_elem.removeClass('new');
					cur_elem.addClass('old');
				},100);
				setTimeout(function(){
					cur_elem.remove();
				},500);
				hidden_count++;
				if ( refresh_type == 1 )
					random++;
				if ( refresh_type == 1 && random == slides_count ){
					clearInterval(refreshInterval);
					$('.refresh-slider-users').removeClass('blocked');
				}
				if ( hidden_count == hidden_list_length )
					hidden_count = 0;
			},interval_delay);
		}
		refreshAnimate(0);
		
		// hover
		$('.refresh-slider').hover(
			function(){
				clearInterval(refreshInterval);
			},
			function(){
				refreshAnimate(0);
			}
		);
		
		// refresh-slider-link
		$('.refresh-slider-users .refresh-slider-link').on('click',function(e){
			var cur = $(this);
			var parent = cur.parents('.refresh-slider');
			if ( !parent.hasClass('blocked') ) {
				parent.addClass('blocked');
				clearInterval(refreshInterval);
				refreshAnimate(1);
			}
			e.preventDefault();
		});
	}
}
/* refreshSliderUsers end */

/* refreshSliderArticles */
function refreshSliderArticles(){
	if ( $('.refresh-slider-articles').length ) {
		var list = $('.refresh-slider-articles .refresh-slider-list');
		var slides_count = list.children(':visible').length;
		$(window).resize(function(){
			slides_count = list.children(':visible').length;
		});
		
		var hidden_list = $('.refresh-slider-articles .refresh-hidden-list');
		var hidden_list_length = hidden_list.children().length;
		var hidden_count = slides_count;
		
		var refreshInterval;
		var old_random;
		
		function refreshAnimate(refresh_type){ // 0 - случайная анимация, 1 - по порядку
			var interval_delay = 1500;
			if ( refresh_type == 1 ){
				var random = 0;
				interval_delay = 100;
			}
			
			refreshInterval = setInterval(function(){
				if ( refresh_type == 0 ){
					while( random == old_random ){
						random = Math.round(Math.random()*(slides_count-1));
					}
					old_random = random;
				}
				var cur_li = list.children().eq(random);
				var cur_elem = cur_li.children().eq(0).addClass('old');
				var hidden_elem = hidden_list.children().eq(hidden_count).children().clone().addClass('new').appendTo(cur_li);
				setTimeout(function(){
					hidden_elem.removeClass('new');
					cur_elem.addClass('old');
				},100);
				setTimeout(function(){
					cur_elem.remove();
				},500);
				hidden_count++;
				if ( refresh_type == 1 )
					random++;
				if ( refresh_type == 1 && random == slides_count ){
					clearInterval(refreshInterval);
					$('.refresh-slider-articles').removeClass('blocked');
				}
				if ( hidden_count == hidden_list_length )
					hidden_count = 0;
			},interval_delay);
		}
		refreshAnimate(0);
		
		// hover
		$('.refresh-slider').hover(
			function(){
				clearInterval(refreshInterval);
			},
			function(){
				refreshAnimate(0);
			}
		);
		
		// refresh-slider-link
		$('.refresh-slider-articles .refresh-slider-link').on('click',function(e){
			var cur = $(this);
			var parent = cur.parents('.refresh-slider');
			if ( !parent.hasClass('blocked') ) {
				parent.addClass('blocked');
				clearInterval(refreshInterval);
				refreshAnimate(1);
			}
			e.preventDefault();
		});
	}
}
/* refreshSliderArticles end */

/* publicationsSlider */
function publicationsSlider(){
	if ( $('.publications-slider').length ) {
		// init
		if ( !$('.resolution-large,.resolution-medium,.resolution-small').is(':visible') ) {
			var slides = 4;
			var margin = 17;
			var moveSlides = 4;
		}
		if ( $('.resolution-large').is(':visible') ) {
			var slides = 3;
			var margin = 20;
			var moveSlides = 3;
		}
		if ( $('.resolution-medium').is(':visible') ) {
			var slides = 2;
			var margin = 49;
			var moveSlides = 2;
		}
		if ( $('.resolution-small').is(':visible') ) {
			var slides = 2;
			var margin = 27;
			var moveSlides = 2;
		}
		publicationsSlider = $('.publications-slider .slider-list').bxSlider({
			prevSelector:$('.publications-slider-prev'),
			nextSelector:$('.publications-slider-next'),
			prevText:'',
			nextText:'',
			pager:false,
			minSlides: slides,
			maxSlides: slides,
			slideWidth: 9999,
			slideMargin: margin,
			moveSlides: moveSlides,
			useCSS:false
		});
		
		// resize
		$(window).resize(function(){
			if ( !$('.resolution-large,.resolution-medium,.resolution-small').is(':visible') ) {
				var slides = 4;
				var margin = 17;
				var moveSlides = 4;
			}
			if ( $('.resolution-large').is(':visible') ) {
				var slides = 3;
				var margin = 20;
				var moveSlides = 3;
			}
			if ( $('.resolution-medium').is(':visible') ) {
				var slides = 2;
				var margin = 49;
				var moveSlides = 2;
			}
			if ( $('.resolution-small').is(':visible') ) {
				var slides = 2;
				var margin = 27;
				var moveSlides = 2;
			}
			
			publicationsSlider.reloadSlider({
				prevSelector:$('.publications-slider-prev'),
				nextSelector:$('.publications-slider-next'),
				prevText:'',
				nextText:'',
				pager:false,
				minSlides: slides,
				maxSlides: slides,
				slideWidth: 9999,
				slideMargin: margin,
				moveSlides: moveSlides,
				useCSS:false
			});
		});
	}
}
/* publicationsSlider end */

/* booksSlider */
function booksSlider(){
	if ( $('.books-slider').length ){
		// init
		if ( !$('.resolution-large,.resolution-medium,.resolution-small').is(':visible') ) {
			var nav_item = 5;
		}
		if ( $('.resolution-large').is(':visible') ) {
			var nav_item = 4;
		}
		if ( $('.resolution-medium').is(':visible') ) {
			var nav_item = 3;
		}
		if ( $('.resolution-small').is(':visible') ) {
			var nav_item = 2;
		}
		$('.books-slider').sliderkit({
			shownavitems:nav_item,
			auto:false,
			circular:true,
			panelfx:'sliding' // fading
		});
		
		var myStandardTabs = $(".books-slider").data("sliderkit");
		
		$(window).resize(function(){
			if ( !$('.resolution-large,.resolution-medium,.resolution-small').is(':visible') ) {
				myStandardTabs.options.shownavitems = 5;
				myStandardTabs._buildNav();
			}
			if ( $('.resolution-large').is(':visible') ) {
				myStandardTabs.options.shownavitems = 4;
				myStandardTabs._buildNav();
			}
			if ( $('.resolution-medium').is(':visible') ) {
				myStandardTabs.options.shownavitems = 3;
				myStandardTabs._buildNav();
			}
			if ( $('.resolution-small').is(':visible') ) {
				myStandardTabs.options.shownavitems = 2;
				myStandardTabs._buildNav();
			}
		});
	}
}
/* booksSlider end */

/* tabs */
function tabs(){
	if ( $('.tabs-wrap').length ) {
		$('.tab-controls-list a').on('click',function(e){
			var cur = $(this);
			var cur_li = cur.parents('li:eq(0)');
			if ( !cur_li.hasClass('current') ){
				cur_li.addClass('current').siblings().removeClass('current');
				var cur_li_index = cur_li.index();
				var parent = cur.parents('.tabs-wrap:eq(0)');
				var content = parent.find('.tabs:eq(0)');
				var cur_tab = content.children('.tab:eq('+cur_li_index+')');
				content.children('.tab:eq('+cur_li_index+')').fadeIn().siblings().fadeOut(0);
				
				// reload sliders
				if ( cur_tab.find('.dealers-slider').length ){
					var cur_slider_index = cur_tab.find('.dealers-slider .slider-list').attr('data-slider');
					dealersSliders[cur_slider_index].reloadSlider();
				}
				
				// reload selects
				if ( cur_tab.find('select').length ){
					customSelectRefresh(cur_tab.find('select'));
					customSelectRefreshPlaceholder(cur_tab.find('select'));	
				}
			}
			e.preventDefault();
		});
	}
}
/* tabs end */

/* mobileMoves */
function mobileMoves(){
	// index/category pages
	if ( $('.main-page').length || $('.category-page').length ) {
		var cur_section = $('.publications:not(.bottom) .publications-section');
		var banner = cur_section.find('.publication-banner:not(.size2)');
		var banner2 = cur_section.find('.publication-banner.size2');
		if ( !$('.resolution-large,.resolution-medium,.resolution-small').is(':visible') ) {
			banner2.insertAfter(cur_section.children().eq(3));
			banner.insertAfter(cur_section.children().eq(9));
		}
		if ( $('.resolution-large').is(':visible') ) {
			banner2.insertAfter(cur_section.children().eq(1));
			banner.insertAfter(cur_section.children().eq(6));
		}
	}
	
	// user page
	if ( $('.user-page').length ) {
		var cur_section = $('.publications:not(.bottom) .publications-section');
		var banner2 = cur_section.find('.publication-banner.size2');
		if ( !$('.resolution-large,.resolution-medium,.resolution-small').is(':visible') ) {
			banner2.insertAfter(cur_section.children().eq(3));
		}
		if ( $('.resolution-large').is(':visible') ) {
			banner2.insertAfter(cur_section.children().eq(1));
		}
	}
}
/* mobileMoves end */

/* textFirstLetter */
function textFirstLetter(){
	if ( $('.publication-info-text').length ) {
		$('.publication-info-text textarea').on('keyup',function(){
			var cur = $(this);
			var cur_value = cur.val();
			var cur_value_first = cur_value[0];
			if ( !cur_value_first )
				cur_value_first = '';
			cur.parents('.publication-info-bottom').find('.letter').html(cur_value_first);
		});
	}
	
	if ( $('.medium-editor-textarea').length ) {
		setInterval(function(){
			var cur_value_first = $('.medium-editor-textarea p').text()[0];
			$('.publication-info-bottom').find('.letter').html(cur_value_first);
		},100);
	}
}
/* textFirstLetter end */

/* autoResizeTextarea */
function autoResizeTextarea(){
	if ( $('.autoresize-textarea').length )
		$('.autoresize-textarea').autoResize({
			animate:false,
			limit:9999,
			extraSpace:0
		});
}
/* autoResizeTextarea end */

/* publicationsSectionHeight */
function publicationsSectionHeight(){
	if ( $('.publications-section').length ) {
		setTimeout(function(){
			var section = $('.publications-section');
			var article = $('.publications-section > article:not(.publication-banner):eq(0)');
			article.removeAttr('style');
			var article_h = article.innerHeight();
			section.children('article').css('height',article_h);
		},1);
	}
}
/* publicationsSectionHeight end */

/* parallax */
function parallax(){
	if ( $('.content-top').length ){
		var content_top = $('.content-top');
		var content_top_bottom = content_top.offset().top + content_top.height();
		var win_top = $(window).scrollTop();
		
		function initParalax(){
			win_top = $(window).scrollTop();
			var percent = win_top/content_top_bottom;
			var bg_percent = 50-(50*percent);
			$('.content-top').css('background-position','50% '+bg_percent+'%');
		}
		initParalax();
		
		// scroll
		$(window).scroll(function(){
			initParalax();
		});
		
		// resize
		$(window).resize(function(){
			content_top_bottom = content_top.offset().top + content_top.height();
			initParalax();
		});
	}
}
/* parallax end */

$(document).ready(function(){
	if ( $(window).width() < 640 || $(window).height() < 640 )
		$('meta[name="viewport"]').attr('content','width=640, user-scalable=yes');
	parallax();
	headerDecor();
	faces();
	sideMenu();
	payments();
	listedGallery();
	siteToTop();
	showRightFixedLinks();
	popups();
	showMore();
	buttonsFn();
	refreshSliderUsers();
	refreshSliderArticles();
	publicationsSlider();
	booksSlider();
	mobileMoves();
	textFirstLetter();
	autoResizeTextarea();
	fixedLinks();
});
$(window).load(function(){
	publicationsSectionHeight();
});
$(window).resize(function(){
	listedGallery();
	mobileMoves();
	publicationsSectionHeight();
});
$(window).scroll(function(){
	listedGallery();
});