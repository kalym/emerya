$(document).on('click', '.js-book-button', function (e) {
    var cur = $(this);
    var parent = cur.parents('.reservation');
    var url = cur.attr('data-href');
    $.ajax({
        'type': 'POST',
        'url': url,
        'dataType': 'json',
        'success': function (response) {
            if (response.error) {
                console.log(response);
                $.growl.error({
                    title: '<svg class="icon-danger"><use xlink:href="#icon-danger"></use></svg>',
                    close: '<svg class="icon-close"><use xlink:href="#icon-close"></use></svg>',
                    message: response.error
                });
            } else {
                parent.after(response.html);
                parent.remove();
            }
        }
    });

    e.preventDefault();
});

$(document).on('click', '.user-image-edit', function (e) {
    var input = $(document).find('#avatar-input');
    var submit = $(document).find('#avatar-submit');
    input.trigger('click');

    $(document).on('change', '#avatar-input', function (e) {
        submit.trigger('click');
    });

    e.preventDefault();
});

$(document).on('submit', ".ajax-form, .ajax-form-file", function (e) {
    e.preventDefault();
    if ($(this).hasClass('ajax-form-file')) {
        return ajaxSubmitFile(this);
    } else {
        return ajaxSubmit(this);
    }
});

function ajaxSubmitFile(form) {
    var f_name = $(form).attr('id');
    $("#" + f_name).closest('div').find(".ajax-form-result").removeClass('ajax-form-result-ok ajax-form-result-error').empty();
    v = {};
    $.each(form, function () {
        if (this.name == 'content') {
            console.log(this.value);
        }
        v[this.name] = this.value;
    });
    v['_form_name_'] = f_name;

    var formData = new FormData(form);

    $.ajax({
        type: 'POST',
        url: form.action,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (res) {
            if (!res.errorCode) {
                if (res.msg) {
                    $.growl.notice({
                        title: '<svg class="icon-checked"><use xlink:href="#icon-checked"></use></svg>',
                        close: '<svg class="icon-close"><use xlink:href="#icon-close"></use></svg>',
                        message: res.msg
                    });
                }
                if (f_name == 'form-avatar') {
                    $('#avatar-output').attr('src', res.avatar);
                    setTimeout("location.reload()", 1000);
                }
            } else {
                $.growl.error({
                    title: '<svg class="icon-danger"><use xlink:href="#icon-danger"></use></svg>',
                    close: '<svg class="icon-close"><use xlink:href="#icon-close"></use></svg>',
                    message: res.msg
                });
            }
        },
        error: function (data) {
            $.growl.error({
                title: '<svg class="icon-danger"><use xlink:href="#icon-danger"></use></svg>',
                close: '<svg class="icon-close"><use xlink:href="#icon-close"></use></svg>',
                message: '������!'
            });
        }
    });
}

function ajaxSubmit(form) {
    var f_name = $(form).attr('id');
    $("#" + f_name).closest('div').find(".ajax-form-result").removeClass('ajax-form-result-ok ajax-form-result-error').empty();
    v = {};
    $.each(form, function () {
        if (this.name == 'content') {
            console.log(this.value);
        }
        v[this.name] = this.value;
    });
    v['_form_name_'] = f_name;

    $.ajax({
        'type': 'POST',
        'url': form.action,
        'data': v,
        'dataType': 'json',
        'success': showRequestResult
    });
    return false;
}

function showRequestResult(res) {
    var f_name = res._form_name_;
    if (!res.errorCode) {
        if (res.msg) {
            $.growl.notice({
                title: '<svg class="icon-checked"><use xlink:href="#icon-checked"></use></svg>',
                close: '<svg class="icon-close"><use xlink:href="#icon-close"></use></svg>',
                message: res.msg
            });
        }
        // $("#" + f_name).closest('div').find(".ajax-form-result").removeClass('ajax-form-result-ok ajax-form-result-error').empty().addClass('ajax-form-result-ok').html(res.msg);
        switch (res.action) {
            case 'refresh' :
                setTimeout("location.reload()", 1000);
                break;
            case 'redirect' :
                setTimeout(function () {
                    location.href = res.url;
                }, 3000);
                break;
            case 'reset' :
                $("#" + f_name).get(0).reset();
                setTimeout(function () {
                    $("#" + f_name).closest('div').find(".ajax-form-result").empty().removeClass('ajax-form-result-ok ajax-form-result-error');
                }, 3000);
            case 'close' :
                //setTimeout( "$.fancybox.close()", 1000);
                setTimeout("$('.fader').click()", 1000);
                break;
            case 'hide-form' :
                $("#" + f_name).closest('.form').remove();
                break;
            case 'comment-form' :
                $("#" + f_name)[0].reset();
                if (res.replace_id) {
                    $('#' + res.replace_id).html(res.replace_content);
                } else {
                    if ($(".messages-section article:last").length) {
                        $(".messages-section article:last").after(res.last_comment);
                    } else {
                        $(".messages-section").append(res.last_comment);
                    }
                }
                break;
            case 'cb' :
                window[res.cb](res);
                setTimeout("$('.fader').click()", 1000);
            default:
                break;
        }
    } else {
        $.growl.error({
            title: '<svg class="icon-danger"><use xlink:href="#icon-danger"></use></svg>',
            close: '<svg class="icon-close"><use xlink:href="#icon-close"></use></svg>',
            message: res.msg
        });
        // $("#" + f_name).closest('div').find(".ajax-form-result").removeClass('ajax-form-result-ok ajax-form-result-error').addClass('ajax-form-result-error').html(res.msg);
    }
}