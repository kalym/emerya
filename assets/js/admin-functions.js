function strip_tags(str) {
    return str.replace(/<\/?[^>]+>/gi, '');
}


/* autoResizeTextarea */
function autoResizeTextarea() {
    if ($('.autoresize-textarea').length)
        $('.autoresize-textarea').autoResize({
            animate: false,
            limit: 9999,
            extraSpace: 0
        });
    $('.autoresize-textarea').keydown();
}
/* autoResizeTextarea end */

/* textFirstLetter */
function textFirstLetter() {
    if ($('.publication-info-text').length) {
        $('.publication-info-text textarea').on('keyup', function () {
            var cur = $(this);
            var cur_value = strip_tags(cur.val());
            var cur_value_first = cur_value[0];
            console.log(cur_value);
            if (!cur_value_first)
                cur_value_first = '';
            cur.parents('.publication-info-bottom').find('.letter').html(cur_value_first);
        });
    }

    if ($('.medium-editor-textarea').length) {
        setInterval(function () {
            var cur_value_first = strip_tags($('.medium-editor-textarea').text())[0];
            $('.publication-info-bottom').find('.letter').html(cur_value_first);
            if (!cur_value_first)
                $('.publication-info-bottom').find('.letter').html('');
        }, 100);
    }
}
/* textFirstLetter end */

// upload full_image
$(document).on('click', '.big-image .inner', function (e) {
    var input = $(document).find('#js-article-image-full');
    input.trigger('click');

    $(document).on('change', '#js-article-image-full', function (e) {
        var cur = $(this)[0];
        var resultBlock = $('.big-image');
        if (cur.files && cur.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                resultBlock.css({
                    'background-image': "url(" + e.target.result + ")"
                });
            }

            if ((/\.(png|jpeg|jpg|gif)$/i).test(cur.files[0].name)) {
                resultBlock.addClass('active');
                reader.readAsDataURL(cur.files[0]);
            }
        }
    });
});

// upload full_image end

// upload images
$(document).on('click', '.publication-info-add-image', function (e) {
    $(document).on('change', '#js-article-image', function (e) {
        var cur = $(this)[0];
        var resultBlock = $('.js-image-result');
        if (cur.files && cur.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                resultBlock.css({
                    'background-image': "url(" + e.target.result + ")"
                });
            }

            if ((/\.(png|jpeg|jpg|gif)$/i).test(cur.files[0].name)) {
                resultBlock.addClass('active');
                reader.readAsDataURL(cur.files[0]);
            }
        }
    });
});

$(document).on('submit', '.ajax-form-file', function (e) {
    e.preventDefault();
    ajaxSubmitFile(this);
});

function ajaxSubmitFile(form) {
    v = {};
    $.each(form, function () {
        if (this.name == 'content') {
            console.log(this.value);
        }
        v[this.name] = this.value;
    });

    var formData = new FormData(form);

    $.ajax({
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            // �������� �������� �� ������
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = (evt.loaded / evt.total) * 100;
                    $('#image-preloader').css('width', percentComplete + '%');
                    console.log(percentComplete);
                }
            }, false);
            return xhr;
        },
        type: 'POST',
        url: form.action,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (res) {
            if (!res.errorCode) {
                $('#image-preloader').css('width', '0%');
                $(".js-image-result").css('background-image', 'url(' + res.image + ')');
                $("form input[name=image]").prop('value', res.image);
                $("form input[name=preview]").prop('value', res.preview);
                $("form input[name=preview2]").prop('value', res.preview2);
                $("form input[name=original]").prop('value', res.original);
                $("form input[name=widget_img]").prop('value', res.widget_img);
            }
            else {
                alert(res.msg);
            }
        },
        error: function (data) {
            alert('������ ��� �������� �����������!');
        }
    })
    ;
}
// upload images end

// upload podcast
$(document).on('click', '.podcast-load', function (e) {
    var input = $(document).find('#upload-podcast');
    var submit = $(document).find('#upload-podcast-submit');
    input.trigger('click');

    $(document).on('change', '#upload-podcast', function (e) {
        submit.trigger('click');
    });
    e.preventDefault();
});

$(document).on('submit', '.ajax-form-podcast', function (e) {
    e.preventDefault();
    ajaxSubmitFilePodcast(this);
});

function ajaxSubmitFilePodcast(form) {
    v = {};
    $.each(form, function () {
        if (this.name == 'content') {
            console.log(this.value);
        }
        v[this.name] = this.value;
    });

    var formData = new FormData(form);

    $.ajax({
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            // �������� �������� �� ������
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = (evt.loaded / evt.total) * 100;
                    $('#big-preloader').css('width', percentComplete + '%');
                    console.log(percentComplete);
                }
            }, false);
            return xhr;
        },
        type: 'POST',
        url: form.action,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (res) {
            if (!res.errorCode) {
                $('#big-preloader').css('width', '0%');
                $("form input[name=podcast]").prop('value', res.url);
                $('.audio-holder').html($('<audio controls="control" preload="none" type="audio/mp3" src="' + res.url + '"></audio><a href="#" class="remove-podcast">�������</a>'));
                $('audio').mediaelementplayer();
            }
            else {
                alert(res.msg);
            }
        },
        error: function (data) {
            alert('������ ��� �������� �����������!');
        }
    })
    ;
}

$(document).on('click', '.remove-podcast', function (e) {
    e.preventDefault();
    $("form input[name=podcast]").prop('value', '');
    $('.audio-holder').html($(''));
})
// upload podcast end

$(document).ready(function () {
    textFirstLetter();
    autoResizeTextarea();
});