/**
 * purejs-mousetip - A Pure Javascript Solution to Tooltips That Follow Your Mouse
 * @version 0.0.1
 * @author Joel Eisner <jeisner93@gmail.com>
 * @link http://joeleisner.com/purejs-mousetip
 * @license MIT
 */

!function() {
    function e(e) {
        e.addEventListener("mouseenter", function(t) {
            var s = document.createElement("span"), o = e.getAttribute("mousetip-msg"), i = document.createTextNode(o), n = e.getAttribute("mousetip-css-zindex"), u = e.getAttribute("mousetip-css-position"), r = e.getAttribute("mousetip-css-padding"), d = e.getAttribute("mousetip-css-borderradius"), a = e.getAttribute("mousetip-css-background"), c = e.getAttribute("mousetip-css-color");
            s.id = "mousetip", s.style.zIndex = n || "9999999999999999", s.style.position = u || "fixed", s.style.padding = r || "5px 10px 8px", s.style.borderRadius = d || "4px", s.style.background = a || "rgba(0,0,0,0.75)", s.style.color = c || "#fff", s.appendChild(i), document.body.appendChild(s)
        })
    }
    function t(e) {
        e.addEventListener("mouseleave", function(e) {
            var t = document.getElementById("mousetip");
            t.parentNode.removeChild(t)
        })
    }
    function s(e) {
        e.addEventListener("mousemove", function(t) {
            var s = t.clientX, o = t.clientY, i = document.getElementById("mousetip"), n = e.getAttribute("mousetip-pos");
            if (null !== n) {
                var u, r, d = n.split(" ");
                if (2 == d.length) {
                    switch (d[0]) {
                        case"bottom":
                            u = 15;
                            break;
                        case"center":
                            u = 0 - i.offsetHeight / 2;
                            break;
                        case"top":
                            u =- 15 - i.offsetHeight;
                            break;
                        default:
                            u = 15
                    }
                    switch (d[1]) {
                        case"right":
                            r = 15;
                            break;
                        case"center":
                            r = 0 - i.offsetWidth / 2;
                            break;
                        case"left":
                            r =- 15 - i.offsetWidth;
                            break;
                        default:
                            r = 15
                    }
                } else
                    u = 15, r = 15;
                i.style.top = o + u + "px", i.style.left = s + r + "px"
            } else
                i.style.top = o + 15 + "px", i.style.left = s + 15 + "px"
        })
    }
    for (var o = document.querySelectorAll("[mousetip]"), i = 0; i < o.length; i++) {
        var n = o[i];
        e(n), t(n), s(n)
    }
}();

