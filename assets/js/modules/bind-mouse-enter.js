// Bind Mouse Enter Function
this.bindMouseEnter = function(element, settings) {
    // Bind a 'mouseenter' event listener to the element
    element.addEventListener('mouseenter', function(event) {
        // When the user enters the element, create the mouse-tip element with a message defined from the elements 'mousetip-msg' attribute, ...
        var mouseTip = document.createElement('span'),
            elMsg = element.getAttribute(settings.selector + '-msg'),
            mouseTipMsg = document.createTextNode(elMsg),
            // ... attempt to grab any styling attributes, ...
            mouseTipZIndex = element.getAttribute(settings.selector + '-css-zindex'),
            mouseTipPosition = element.getAttribute(settings.selector + '-css-position'),
            mouseTipPadding = element.getAttribute(settings.selector + '-css-padding'),
            mouseTipBorderRadius = element.getAttribute(settings.selector + '-css-borderradius'),
            mouseTipBackground = element.getAttribute(settings.selector + '-css-background'),
            mouseTipColor = element.getAttribute(settings.selector + '-css-color');
        // ... give the mouse-tip an id for easier DOM selection, ...
        mouseTip.id = settings.selector;
        // ... set the default styles of the mouse-tip (use styling attributes if applicable), ...
        mouseTip.style.zIndex = mouseTipZIndex || settings.cssZIndex;
        mouseTip.style.position = mouseTipPosition || settings.cssPosition;
        mouseTip.style.padding = mouseTipPadding || settings.cssPadding;
        mouseTip.style.borderRadius = mouseTipBorderRadius || settings.cssBorderRadius;
        mouseTip.style.background = mouseTipBackground || settings.cssBackground;
        mouseTip.style.color = mouseTipColor || settings.cssColor;
        // ... append the message to the mouse-tip, ...
        mouseTip.appendChild(mouseTipMsg);
        // ... and append the mouse-tip to the document body
        document.body.appendChild(mouseTip);
    });
};
