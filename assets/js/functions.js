/* headerDecor */
function headerDecor() {
    if ($('.header-decor').length) {
        var main = $('.main');
        var header_decor = $('.header-decor');
        var header_h = $('.header').height();
        var content_top = $('.content-top').height();
        var main_w_half = main.width() / 2;
        header_decor.css({'border-left-width': main_w_half, 'border-right-width': main_w_half});

        function initHeaderScroll() {
            var win_top = $(window).scrollTop();
            var progress = win_top / content_top;
            $('.header,.content-top-main').css('opacity', 1 - progress);
            $('.header').css('top', ((header_h * 1.7) * progress));
            $('.content-top-main').css('top', -((header_h / 2) * progress));
        }

        initHeaderScroll();

        // scroll
        $(window).scroll(function () {
            initHeaderScroll();
        });

        // resize
        $(window).resize(function () {
            content_top = $('.content-top').height();
            main_w_half = main.width() / 2;
            var main_left = main.offset().left;
            header_decor.css({'border-left-width': main_w_half, 'border-right-width': main_w_half});
            if (header_decor.hasClass('fixed'))
                header_decor.css('left', main_left);
            initHeaderScroll();
        });
    }
}
/* headerDecor end */

/* faces */
function faces() {
    if ($('.faces-photos-list').length) {
        var facesInterval;
        var faces_length = $('.faces-photos-list > li:visible').length;
        var old_rand;
        $(window).resize(function () {
            faces_length = $('.faces-photos-list > li:visible').length;
        });

        // facesIntervalFn
        function facesIntervalFn() {
            facesInterval = setInterval(function () {
                var rand = parseInt(Math.round(Math.random() * (faces_length - 1)));
                while (rand == old_rand) {
                    var rand = parseInt(Math.round(Math.random() * (faces_length - 1)));
                }
                toggleFaces($('.faces-photos-list > li:eq(' + rand + ') .face-photo'));
            }, 3000);
        }

        facesIntervalFn();

        // faces hover
        $('.faces').hover(
            function () {
                clearInterval(facesInterval);
            },
            function () {
                facesIntervalFn();
            }
        );

        // toggleFaces
        function toggleFaces(obj) {
            var cur = obj.parents('li:eq(0)');
            var index = cur.index();
            old_rand = index;
            cur.addClass('current').siblings('.current').removeClass('current');
            cur.parents('.faces:eq(0)').find('.faces-info-list > li:eq(' + index + ')').addClass('current').siblings('.current').removeClass('current');
        }

        // face-hover
        $('.faces-photos-list .face-photo').hover(
            function () {
                toggleFaces($(this));
            },
            function () {
            }
        );
    }
}
/* faces end */

/* sideMenu */
function sideMenu() {
    // js-side-link
    if ($('.js-side-link').length) {
        $('.js-side-link').on('click', function (e) {
            var cur = $(this);
            var cur_li = cur.parents('li:eq(0)');

            var popupOverlay = $(document).find('.popup-overlay');
            if (popupOverlay.length) {
                popupOverlay.trigger('click');
            }


            if (cur_li.hasClass('active')) {
                cur_li.removeClass('active');
                cur_li.removeClass('select-cat');
                $('.side-menu-overlay').fadeOut(500, function () {
                    $(this).remove();
                });
                $('.side-submenu-menus-wrap,.side-submenu-registration,.side-submenu-remember-password,.side-menu-buy-wrap,.side-submenu-profile-wrap,.side-submenu-signup-wrap,.side-submenu-sign-wrap,.side-submenu-remember-wrap').removeClass('active');
                $('.side-submenu-menus-wrap,.side-submenu-registration,.side-submenu-remember-password,.side-menu-buy-wrap,.side-submenu-profile-wrap').removeClass('select-cat');
                $('.side-submenu-login').addClass('active');
            } else {
                if (!cur_li.hasClass('cart')) {
                    $('.side-menu-overlay').fadeOut(500, function () {
                        $(this).remove();
                    });
                    cur_li.addClass('active').siblings('.active').removeClass('active');
                    cur_li.addClass('active').siblings('.active').removeClass('select-cat');

                    $('.side-submenu-menus-wrap,.side-submenu-registration,.side-submenu-remember-password,.side-menu-buy-wrap,.side-submenu-profile-wrap,.side-submenu-signup-wrap,.side-submenu-sign-wrap,.side-submenu-remember-wrap').removeClass('active');
                    $('.side-submenu-menus-wrap,.side-submenu-registration,.side-submenu-remember-password,.side-menu-buy-wrap,.side-submenu-profile-wrap').removeClass('select-cat');

                    $('.side-submenu-login').addClass('active');
                    $('.side-menu-wrap').after('<div class="side-menu-overlay"></div>');
                    $('.side-menu-overlay').fadeIn(500);
                }
            }
            e.preventDefault();
        });

        // overlay click
        $('body').on('click', '.side-menu-overlay', function () {
            $('.side-menu-list > li.active .js-side-link').trigger('click');
        });
    }

    if ($('.side-menu-link').length) {
        $(document).on('click', '.side-menu-link', function () {
            $('.side-submenu-signup-wrap').removeClass('active');
            $('.side-submenu-remember-wrap').removeClass('active');
            $('.side-submenu-sign-wrap').addClass('active');
        });
    }

    // js-side-link
    if ($('.js-side-select-cat').length) {
        $('.js-side-select-cat').on('click', function (e) {
            $('.side-menu-menus').click();
            $('.side-menu-list li:first').addClass('select-cat').siblings('.active').removeClass('active');

            e.preventDefault();
        })
    }

    if ($('.js-select-category').length) {
        $('.js-select-category').on('click', function (e) {
            var cur = $(this);
            var categoryId = cur.attr('data-category-id');
            var categoryName = cur.attr('data-category-name');

            $('#cat_name').html(categoryName);
            $('#cat_output').attr('value', categoryId);
            $('.side-menu-menus').click();
            e.preventDefault();
        })
    }

    // admin-question
    if ($('.admin-question-open').length) {
        $('.admin-question-open').on('click', function (e) {
            $('.side-submenu-menus-wrap').addClass('active');
            e.preventDefault();
        });
        $('.admin-question-close').on('click', function (e) {
            $('.side-submenu-menus-wrap').removeClass('active');
            e.preventDefault();
        });
    }

    // side-pay-open
    if ($('.side-pay-open').length) {
        $('.side-pay-open').on('click', function (e) {
            $('.side-menu-buy-wrap').addClass('active');
            e.preventDefault();
        });
    }

    // side-pay-promo-activate
    if ($('.side-pay-promo-activate').length) {
        $('.side-pay-promo-activate').on('click', function (e) {
            alert('�������� �����������');
            e.preventDefault();
        });
    }

    // side-login-open
    if ($('.side-login-open').length) {
        $('.side-login-open').on('click', function (e) {
            $('.side-submenu-login').addClass('active').siblings('.active').removeClass('active');
            e.preventDefault();
        });
    }

    // side-registration-open
    if ($('.side-registration-open').length) {
        $('.side-registration-open').on('click', function (e) {
            $('.side-submenu-sign-wrap').removeClass('active');
            $('.side-submenu-remember-wrap').removeClass('active');
            $('.side-submenu-signup-wrap').addClass('active');
            e.preventDefault();
        });
    }

    // side-registration-open
    if ($('.side-remember-open').length) {
        $('.side-remember-open').on('click', function (e) {
            $('.side-submenu-sign-wrap').removeClass('active');
            $('.side-submenu-signup-wrap').removeClass('active');
            $('.side-submenu-remember-wrap').addClass('active');
            e.preventDefault();
        });
    }

    // remember-password-link
    if ($('.remember-password-link').length) {
        $('.remember-password-link').on('click', function (e) {
            $('.side-submenu-sign-wrap').removeClass('active');
            $('.side-submenu-signup-wrap').removeClass('active');
            $('.side-submenu-remember-wrap').addClass('active');

            e.preventDefault();
        });
    }

    // profile-edit-open
    if ($('.profile-edit-open').length) {
        $('.profile-edit-open').on('click', function (e) {
            $('.side-submenu-profile-wrap').addClass('active').siblings('.active').removeClass('active');
            e.preventDefault();
        });
    }
}
/* sideMenu end */

/* payments */
function payments() {
    if ($('.payment-item').length) {
        $('.payment-item').on('click', function () {
            var cur = $(this);
            if (!cur.hasClass('active')) {
                cur.addClass('active');
                cur.parents('li:eq(0)').siblings().find('.payment-item.active').removeClass('active');
            }
        });
    }
}
/* payments end */

/* listedGallery */
function listedGallery() {
    if ($('.tile-list').length) {
        var indent = 0.2; // ������� ������ ��������, ������� ������ ���� �����, ����� �������� ���������
        var min_duration = 0.1;  // ���������� ������� ������� � �������� ��������� �������� (default: 0.4 / 0.7)
        var max_duration = 0.9;
        var win_h = $(window).height();
        var scroll_top = $(window).scrollTop();
        var win_bottom = win_h + scroll_top;
        var elem_h = $('.tile-list > li:first').height();
        var visible_indent = elem_h * indent;

        var element_for_reload = $('.reload-element');

        if (element_for_reload.length) {
            var bottom_of_object, bottom_of_window;
            bottom_of_object = element_for_reload.offset().top - elem_h;
            bottom_of_window = $(window).scrollTop() + $(window).height();
            if (bottom_of_window > bottom_of_object) {
                if (!element_for_reload.hasClass('ajax-loading-element')) {
                    element_for_reload.addClass('ajax-loading-element');
                    var parent = element_for_reload.parents('.tile');
                    var renderContainer = parent.find('.tile-list');
                    showMoreRequest(element_for_reload, renderContainer, 'list-buttons', 'li');
                }
            } else {
                element_for_reload.removeClass('ajax-loading-element');
            }
        }

        $('.tile-list > li:not(.active)').each(function () {
            var elem = $(this);
            var elem_top = elem.offset().top;
            var randDuration = ( Math.random() * ( max_duration - min_duration ) + min_duration ) + 's';
            if ((elem_top + visible_indent - 20) < win_bottom) {
                elem.css('transition-duration', randDuration).addClass('active');
            }
        });
    }
}
/* listedGallery end */

/* siteToTop */
function siteToTop() {

}
/* siteToTop end */

/* showRightFixedLinks */
function showRightFixedLinks() {

}
/* showRightFixedLinks end */

/* fixedLinks */
function fixedLinks() {
    if ($('.right-fixed-links.bottom').length && $('.subscribe').length) {
        var links_bottom = $('.right-fixed-links.bottom')
        var subscribe = $('.subscribe');

        function linksPos() {
            var scroll_top = $(window).scrollTop();
            var scroll_bottom = scroll_top + $(window).height();
            var subscribe_top = subscribe.offset().top;
            var subscribe_height = parseInt(subscribe.innerHeight());
            var subscribe_bottom = subscribe_top + subscribe_height;
            var subsscribe_middle = subscribe_top + (subscribe_height / 2);
            if (scroll_bottom >= subscribe_bottom)
                links_bottom.addClass('absolute').css('top', subscribe_bottom);
            else
                links_bottom.removeClass('absolute').removeAttr('style');
        }

        linksPos();
        $(window).scroll(function () {
            linksPos();
        });
        $(window).resize(function () {
            linksPos();
        });
    }

    // totop-link
    if ($('.fixed-totop-link').length) {
        $('.fixed-totop-link').on('click', function (e) {
            $('html,body').animate({scrollTop: 0}, 500);
            e.preventDefault();
        });
    }

    // show links
    if ($('.right-fixed-links.hidden').length) {
        var indent = 200;

        function showLinks() {
            if ($(window).scrollTop() > indent)
                $('.right-fixed-links').removeClass('hidden');
        }

        showLinks();
        $(window).scroll(function () {
            showLinks();
        });
    }

    if ($('.relative-box').length) {
        var indent = $('.publication-info').offset().top;

        function showRelativeBox() {
            if ($(window).scrollTop() > indent && $(window).scrollTop() < $('.publication-info-buttons .btn-gray').offset().top + $('.publication-info-buttons .btn-gray').height()) {
                $('.relative-box').removeClass('hidden');
            } else {
                $('.relative-box').addClass('hidden');
            }
        }

        showRelativeBox();
        $(window).scroll(function () {
            showRelativeBox();
        });
    }

    if ($('.fixed-change-buttons').length) {
        var indent = $('.publication-info-title').offset().top;

        function showFixedChangeButtons() {
            if ($(window).scrollTop() > indent && $(window).scrollTop() < $('.subscribe').offset().top) {
                $('.fixed-change-buttons .top').removeClass('hidden');
            } else {
                $('.fixed-change-buttons .top').addClass('hidden');
            }
        }

        showFixedChangeButtons();
        $(window).scroll(function () {
            showFixedChangeButtons();
        });
    }

    if ($('.fixed-soc-buttons a.fb').length) {
        var indent = $('.publication-article-wrap').offset().top;
        var indentVK = indent + 20;
        var indentTW = indentVK + 20;
        var duration = 200;

        function tpl(item, state) {
            if (state) {
                $($(item + ':eq(0)')).fadeIn(duration, function nextItem() {
                    $(this).next().fadeIn(duration, nextItem);
                });
            } else {
                $($(item + ':eq(2)')).fadeOut(duration, function prevItem() {
                    $(this).prev().fadeOut(duration, prevItem);
                });
            }
        }


        function socialLinks() {
            tpl('.fixed-soc-buttons a', $(window).scrollTop() > indent && $(window).scrollTop() < $('.messages-section').offset().top + $('.messages-section').height() - 300);
        }

        socialLinks();
        $(window).scroll(function () {
            socialLinks();
        });
    }

    if ($('.like-change').length) {
        if ($('.like-change').hasClass('js-question-link') && !$('.like-change').hasClass('js-user-question')) {
            var indent = $('.content').offset().top;

            function showQuestionLink() {
                if ($(window).scrollTop() > indent && $(window).scrollTop() < $('.news-buttons').offset().top + $('.news-buttons').height()) {
                    $('.like-change .top').removeClass('hidden');
                } else {
                    $('.like-change .top').addClass('hidden');
                }
            }

            showQuestionLink();
            $(window).scroll(function () {
                showQuestionLink();
            });
        } else if ($('.like-change').hasClass('js-author-recommend-link')) {
            var indent = $('.user-statistics').offset().top;

            function showAuthorLink() {
                if ($(window).scrollTop() > indent && $(window).scrollTop() < $('.publications.bottom').offset().top + $('.publications.bottom').height()) {
                    $('.like-change .top').removeClass('hidden');
                } else {
                    $('.like-change .top').addClass('hidden');
                }
            }

            showAuthorLink();
            $(window).scroll(function () {
                showAuthorLink();
            });
        } else {
            if ($('.publication-article-wrap').length) {
                var indent = $('.publication-article-wrap').offset().top;

                function showLikeLinks() {
                    if ($(window).scrollTop() > indent && $(window).scrollTop() < $('.messages-section').offset().top + $('.messages-section').height()) {
                        $('.like-change .top').removeClass('hidden');
                    } else {
                        $('.like-change .top').addClass('hidden');
                    }
                }

                showLikeLinks();
                $(window).scroll(function () {
                    showLikeLinks();
                });
            }
        }
    }
}
/* fixedLinks end */

/* popups */
function popups() {
    if ($('.js-popup-link').length) {
        // open
        $('.js-popup-link').on('click', function (e) {
            var shadowBg = $(document).find('.side-menu-overlay');
            if (shadowBg.length) {
                shadowBg.trigger('click');
            }

            var cur = $(this);
            if (cur.hasClass('active')) {
                $('.popup-overlay').trigger('click');
            } else {
                $('body').addClass('hidden-box');
                cur.addClass('active');
                var cur_attr = cur.attr('href');
                var popup = $(cur_attr);
                popup.addClass('active').fadeIn().siblings('.popup-wrap.active').fadeOut();
                if (popup.hasClass('popup-search')) {
                    $('html,body').animate({scrollTop: 0}, 500);
                    $('.popup-search-input').trigger('focus');
                }
            }
            e.preventDefault();
        });

        // close
        $('.popup-overlay,.js-popup-close').on('click', function (e) {
            $('body').removeClass('hidden-box');
            $('.search-output').html(' ');
            $('.popup-search-input').val('');
            $('.js-popup-link.active').removeClass('active');
            $('.popup-wrap.active').removeClass('active').fadeOut();
        });
    }
}
/* popups end */

function showImage() {
    if ($('#js-article-image').length) {
        $('#js-article-image').change(function () {
            var cur = $(this)[0];
            var resultBlock = $('.js-image-result');
            if (cur.files && cur.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    resultBlock.css({
                        'background-image': "url(" + e.target.result + ")"
                    });
                }

                // if (Math.ceil(cur.files[0].size/1024) > 200) {
                //     cur.value = '';
                //     resultBlock.removeClass('active');
                //     resultBlock.css({
                //         'background-image': "none"
                //     });
                // } else {
                if ((/\.(png|jpeg|jpg|gif)$/i).test(cur.files[0].name)) {
                    resultBlock.addClass('active');
                    reader.readAsDataURL(cur.files[0]);
                } else {
                    cur.value = '';
                    resultBlock.removeClass('active');
                    resultBlock.css({
                        'background-image': "none"
                    });
                }
                // }
            }
        });
    }
}

/* showMore */
function showMore() {
    // js-more-comments
    if ($('.js-comments-more').length) {
        $('body').on('click', '.js-comments-more', function (e) {
            var cur = $(this);
            cur.blur();
            var parent = cur.parents('.messages');

            if (cur.hasClass('ajax-loading')) {
                e.preventDefault();
                return;
            }
            cur.addClass('ajax-loading');
            var renderContainer = parent.find('.messages-section');

            showMoreRequest(cur, renderContainer, 'messages-buttons', 'article')

            e.preventDefault();
        });
    }
    // js-more-news
    if ($('.js-news-more').length) {
        $('body').on('click', '.js-news-more', function (e) {
            var cur = $(this);
            cur.blur();
            var parent = cur.parents('.news');

            if (cur.hasClass('ajax-loading')) {
                e.preventDefault();
                return;
            }
            cur.addClass('ajax-loading');
            var renderContainer = parent.find('.news-section');

            showMoreRequest(cur, renderContainer, 'news-buttons', 'article')

            e.preventDefault();
        });
    }

    // js-reservations-more
    if ($('.js-reservations-more').length) {
        $('body').on('click', '.js-reservations-more', function (e) {
            var cur = $(this);
            cur.blur();
            var parent = cur.parents('.reservations');

            if (cur.hasClass('ajax-loading')) {
                e.preventDefault();
                return;
            }
            cur.addClass('ajax-loading');
            var renderContainer = parent.find('.reservations-section');

            showMoreRequest(cur, renderContainer, 'eservations-buttons', 'article')

            e.preventDefault();
        });
    }

    // js-unique-views-more
    if ($('.js-unique-views-more').length) {
        $('body').on('click', '.js-unique-views-more', function (e) {
            var cur = $(this);
            cur.blur();
            var parent = cur.parents('.unique-views');

            if (cur.hasClass('ajax-loading')) {
                e.preventDefault();
                return;
            }
            cur.addClass('ajax-loading');
            var renderContainer = parent.find('.unique-views-section');

            showMoreRequest(cur, renderContainer, 'unique-views-buttons', 'article')

            e.preventDefault();
        });
    }

    // js-mypublications-more
    if ($('.js-mypublications-more').length) {
        $('body').on('click', '.js-mypublications-more', function (e) {
            var cur = $(this);
            cur.blur();
            var parent = cur.parents('.mypublications');

            if (cur.hasClass('ajax-loading')) {
                e.preventDefault();
                return;
            }
            cur.addClass('ajax-loading');
            var renderContainer = parent.find('.mypublications-section');

            showMoreRequest(cur, renderContainer, 'mypublications-buttons', 'article')

            e.preventDefault();
        });
    }

    // js-messages-more
    if ($('.js-messages-more').length) {
        $('body').on('click', '.js-messages-more', function (e) {
            var cur = $(this);
            cur.blur();
            var parent = cur.parents('.messages');
            parent.find('.message:hidden').fadeIn(500);
            cur.parents('.messages-buttons').remove();
            e.preventDefault();
        });
    }

    // js-publications-more
    if ($('.js-publications-more').length) {
        $('body').on('click', '.js-publications-more', function (e) {
            var cur = $(this);
            cur.blur();

            if (cur.hasClass('ajax-loading')) {
                e.preventDefault();
                return;
            }
            cur.addClass('ajax-loading');
            var renderContainer = cur.parents('.publications').find('.publications-section');

            showMoreRequest(cur, renderContainer, 'publications-buttons', 'article')

            e.preventDefault();
        });
    }

    // js-experts-more
    if ($('.js-experts-more').length) {
        $('body').on('click', '.js-experts-more', function (e) {
            var cur = $(this);
            cur.blur();

            if (cur.hasClass('ajax-loading')) {
                e.preventDefault();
                return;
            }
            cur.addClass('ajax-loading');

            var parent = cur.parents('.experts');
            var renderContainer = parent.find('.experts-list');
            showMoreRequest(cur, renderContainer, 'experts-buttons', 'li')

            e.preventDefault();
        });
    }

    // js-podcasts-more
    if ($('.js-podcasts-more').length) {
        $('body').on('click', '.js-podcasts-more', function (e) {
            var cur = $(this);
            cur.blur();
            var parent = cur.parents('.podcasts');

            if (cur.hasClass('ajax-loading')) {
                e.preventDefault();
                return;
            }
            cur.addClass('ajax-loading');
            var renderContainer = parent.find('.podcasts-section');

            showMoreRequest(cur, renderContainer, 'podcasts-buttons', 'article')

            e.preventDefault();
        });
    }

    // js-search-more
    $('body').on('click', '.js-search-more', function (e) {
        var cur = $(this);
        cur.blur();
        var parent = cur.parents('.popup-search-results');

        if (cur.hasClass('ajax-loading')) {
            e.preventDefault();
            return;
        }
        cur.addClass('ajax-loading');
        var renderContainer = parent.find('.popup-search-results-section');

        showMoreRequest(cur, renderContainer, 'popup-search-results-buttons', 'article')

        e.preventDefault();
    });
}
/* showMore end */

/* showMoreRequest */
function showMoreRequest(cur, renderContainer, buttons_class, container) {
    var nextPage = parseInt(cur.attr('data-current-page')) + 1;
    var maxPages = parseInt(cur.attr('data-total-pages'));
    var nextItemsUrl = cur.attr('href') + nextPage;

    if (cur.attr('href') != '#') {
        $.ajax({
            'type': 'GET',
            'url': nextItemsUrl,
            'success': function (response) {
                if (renderContainer.parent().hasClass('notepad-publications')) {
                    renderContainer.parent().attr('data-current-page', nextPage);
                }

                if (nextPage >= maxPages) {
                    cur.parents('.' + buttons_class).remove();
                    var parent = renderContainer.parents().find('.js-add-message');
                    parent.removeClass('not-visible');
                } else {
                    cur.removeClass('ajax-loading');
                    cur.attr('data-current-page', nextPage);
                    cur.parents('.' + buttons_class).append(cur.clone());
                    cur.remove();
                }

                renderContainer.append(response);
                // renderContainer.find(container + ':hidden').css({
                //     'opacity': 0,
                //     'display': 'inline-block'
                // }).animate({'opacity': 1}, 500);
            }
        });
    }
}
/* showMoreRequest end */

function changeRateAndNotepad(url, el) {
    $.ajax({
        'type': 'GET',
        'url': url,
        'success': function (response) {
            if (!response.errorCode) {
                if (el) {
                    var mousetip = $(document).find('#mousetip');
                    if ($(el).hasClass('fixed-like-link')) {
                        if (response.msg) {
                            $.growl.notice({
                                title: '<svg class="icon-checked"><use xlink:href="#icon-checked"></use></svg>',
                                close: '<svg class="icon-close"><use xlink:href="#icon-close"></use></svg>',
                                message: response.msg
                            });
                        }
                        $(el).remove();
                        if (mousetip.length) {
                            $.each(mousetip, function (index, el) {
                                $(el).remove();
                            })
                        }
                    } else if ($(el).hasClass('notepad-plus')) {
                        if (response.msg) {
                            $.growl.notice({
                                title: '<svg class="icon-checked"><use xlink:href="#icon-checked"></use></svg>',
                                close: '<svg class="icon-close"><use xlink:href="#icon-close"></use></svg>',
                                message: response.msg
                            });
                        }
                        $(el).remove()
                    } else if ($(el).hasClass('fixed-author-recommend-link')) {
                        var parent = $(el).parents('.like-change');
                        console.log(response);
                        if (parseInt(response.isRecommend) == 1) {
                            $(el).attr('mousetip-msg', '������� ������������');
                            if (mousetip.length) {
                                $.each(mousetip, function (index, el) {
                                    $(el).html('������� ������������');
                                })
                            }
                            parent.addClass('active');
                            $.growl.notice({
                                title: '<svg class="icon-checked"><use xlink:href="#icon-checked"></use></svg>',
                                close: '<svg class="icon-close"><use xlink:href="#icon-close"></use></svg>',
                                message: '����� �������� � ���� ������������'
                            });
                        } else {
                            $(el).attr('mousetip-msg', '������������� ������');
                            if (mousetip.length) {
                                $.each(mousetip, function (index, el) {
                                    $(el).html('������������� ������');
                                })
                            }
                            parent.removeClass('active');
                            $.growl.warning({
                                title: '<svg class="icon-checked"><use xlink:href="#icon-checked"></use></svg>',
                                close: '<svg class="icon-close"><use xlink:href="#icon-close"></use></svg>',
                                message: '����� ������ �� ����� ������������'
                            });
                        }
                    } else {
                        if (response.msg) {
                            $.growl.warning({
                                title: '<svg class="icon-close"><use xlink:href="#icon-close"></use></svg>',
                                close: '<svg class="icon-close"><use xlink:href="#icon-close"></use></svg>',
                                message: response.msg
                            });
                        }

                        $(el).parents('article').remove();
                        var publicationSections = $(document).find('.mypublications');
                        var currentPage = publicationSections.attr('data-current-page');
                        var updateSectionUrl = publicationSections.attr('data-update-url');

                        $.ajax({
                            'type': 'GET',
                            'url': updateSectionUrl + currentPage,
                            'success': function (response) {
                                publicationSections.html(response);
                            }
                        });
                    }
                }
            } else {
                if (response.msg) {
                    $.growl.error({
                        title: '<svg class="icon-danger"><use xlink:href="#icon-danger"></use></svg>',
                        close: '<svg class="icon-close"><use xlink:href="#icon-close"></use></svg>',
                        message: response.msg
                    });
                }
            }
        }
    });
}

/* buttonsFn */
function buttonsFn() {
    if ($('.chat-user-item').length) {
        $(document).on('click', '.chat-user-item', function (e) {
            var cur = $(this);
            var url = cur.attr('data-url');
            $.ajax({
                'type': 'GET',
                'url': url,
                'success': function (response) {
                    $('.messages-section').html(response);
                    cur.addClass('active').siblings('.active').removeClass('active');
                    console.log($('.chat-right-box')[0].scrollHeight);
                    $('.chat-right-box').scrollTop($('.chat-right-box')[0].scrollHeight);
                }
            })
        })
    }

    if ($('.chat-user-item:first').length) {
        $('.chat-user-item:first').trigger('click');
    }

    if ($('.search-icon').length) {
        $(document).on('click', '.search-icon', function (e) {
            var parent = $(this).parents('.search-form');
            var input = parent.find('input[type="text"]');
            input.focus();
        })
    }

    if ($('.js-isPremium').length) {
        $(document).on('click', '.js-isPremium', function (e) {
            var mousetip = $(document).find('#mousetip');
            if ($(this).parents('.relative-box').find('input[type="checkbox"]')[0].checked) {
                $(this).attr('mousetip-msg', '������������ ������ ��� ��������');
                mousetip.html('������������ ������ ��� ��������');
            } else {
                $(this).attr('mousetip-msg', '������ ��������� ������ ��� ��������');
                mousetip.html('������ ��������� ������ ��� ��������');
            }
        });
    }

    if ($('.js-like-change').length) {
        $(document).on('click', '.js-like-change', function (e) {
            var cur = $(this);
            var url = cur.attr('data-href');

            if (cur.hasClass('fixed-like-link')) {
                changeRateAndNotepad(url, cur);
            }

            if (cur.hasClass('notepad-plus')) {
                changeRateAndNotepad(url, cur);
            }

            if (cur.hasClass('notepad-minus')) {
                changeRateAndNotepad(url, cur);
            }

            if (cur.hasClass('fixed-question')) {
                $('.js-side-link.quest').trigger('click');
            }

            if (cur.hasClass('fixed-author-recommend-link')) {
                changeRateAndNotepad(url, cur);
            }

            e.preventDefault();
        });
    }

    if ($('.fixed-star-link')) {
        $(document).on('click', '.fixed-star-link', function (e) {
            var cur = $(this);
            $('html,body').animate({scrollTop: 0}, 500);
            if (cur.hasClass('js-loading')) {
                e.preventDefault();
            } else {
                var isList = cur.hasClass('active');
                var url;
                if (isList) {
                    url = cur.attr('data-href-list');
                    cur.removeClass('active');
                    cur.attr('mousetip-msg', '����������');
                } else {
                    url = cur.attr('data-href-popular');
                    cur.addClass('active');
                    cur.attr('mousetip-msg', '�����');
                }
                cur.blur();
                cur.addClass('js-loading');
                $.ajax({
                    'type': 'GET',
                    'url': url,
                    'success': function (response) {
                        cur.removeClass('js-loading');
                        $('.tile').html(response);
                        listedGallery();
                    }
                });
                e.preventDefault();
            }
        });
    }

    // publications-add-desc-close
    if ($('.publications-add-desc-close')) {
        $('.publications-add-desc-close').on('click', function (e) {
            $('.publications-add-desc').fadeOut(function () {
                $('.publications-add-desc-close').remove();
            });
            e.preventDefault();
        });
    }

    // js-add-message
    if ($('.js-add-message')) {
        $('.js-add-message').on('click', function (e) {
            var cur = $(this);
            if ($('.user-page').length) {
                if (cur.parents('.user-actions').length)
                    cur.parents('.user-actions').next('.add-message').slideDown();
                else
                    cur.next('.add-message').slideDown();
            } else
                $('.add-message').slideDown();
            if (cur.hasClass('btn-gray'))
                cur.remove();
            e.preventDefault();
        });
    }

    // add-tofav
    if ($('.fav-holder:not(.disabled)').length) {
        $(document).on('click', '.fav-holder:not(.disabled) .fav-link', function () {
            var cur = $(this);
            var url = cur.attr('data-href');
            var parent = cur.parents('.fav-holder');
            var number = parent.find('.fav-value');

            $.ajax({
                'type': 'GET',
                'url': url,
                'success': function (response) {
                    console.log(response);
                }
            });

            if (parent.hasClass('active')) {
                number.html(parseInt(number.text()) - 1);
            } else {
                number.html(parseInt(number.text()) + 1);
            }
            parent.toggleClass('active');
        });
    }

    // js-all-categories
    if ($('.js-all-categories').length) {
        $('.js-all-categories').on('click', function (e) {
            var cur = $(this);
            $('.js-side-link.quest').trigger('click');
            e.preventDefault();
        });
    }

    // side-submenu-categories-list
    if ($('.side-submenu-categories-list').length) {
        $('.side-submenu-categories-list a').on('click', function (e) {
            var cur = $(this);
            var cur_text = cur.text();
            $('.js-all-categories span').html(cur_text);
            var parent = cur.parents('li:eq(0)');
            parent.addClass('current').append('<span class="green-checked"></span>');
            $('.side-submenu-categories-list li.current').not(parent).removeClass('current').find('.green-checked').remove();
            $('.js-side-link.side-menu-categories').trigger('click');
            // e.preventDefault();
        });
    }

    // full-subscribe-item
    if ($('.full-subscribe-item').length) {
        $('.full-subscribe-item').on('click', function (e) {
            // $('.js-side-link.side-menu-buy').trigger('click');
            $('.side-menu-overlay').fadeOut(500, function () {
                $(this).remove();
            });
            $(document).find('li.cart').addClass('active');
            $('.side-menu-wrap').after('<div class="side-menu-overlay"></div>');
            $('.side-menu-overlay').fadeIn(500);

            e.preventDefault();
        });
    }
}
/* buttonsFn end */

/* refreshSliderUsers */
function refreshSliderUsers() {
    if ($('.refresh-slider-users-active').length) {
        var list = $('.refresh-slider-users .refresh-slider-list');
        var slides_count = list.children(':visible').length;
        $(window).resize(function () {
            slides_count = list.children(':visible').length;
        });

        var hidden_list = $('.refresh-slider-users .refresh-hidden-list');
        var hidden_list_length = hidden_list.children().length;
        var hidden_count = slides_count;

        var refreshInterval;
        var old_random;

        function refreshAnimate(refresh_type) { // 0 - ��������� ��������, 1 - �� �������
            var interval_delay = 1500;
            if (refresh_type == 1) {
                var random = 0;
                interval_delay = 100;
            }

            refreshInterval = setInterval(function () {
                if (refresh_type == 0) {
                    while (random == old_random) {
                        random = Math.round(Math.random() * (slides_count - 1));
                    }
                    old_random = random;
                }
                var cur_li = list.children().eq(random);
                var cur_elem = cur_li.children().eq(0).addClass('old');
                var hidden_elem = hidden_list.children().eq(hidden_count).children().clone().addClass('new').appendTo(cur_li);
                setTimeout(function () {
                    hidden_elem.removeClass('new');
                    cur_elem.addClass('old');
                }, 100);
                setTimeout(function () {
                    cur_elem.remove();
                }, 500);
                hidden_count++;
                if (refresh_type == 1)
                    random++;
                if (refresh_type == 1 && random == slides_count) {
                    clearInterval(refreshInterval);
                    $('.refresh-slider-users').removeClass('blocked');
                }
                if (hidden_count == hidden_list_length)
                    hidden_count = 0;
            }, interval_delay);
        }

        refreshAnimate(0);

        // hover
        $('.refresh-slider').hover(
            function () {
                clearInterval(refreshInterval);
            },
            function () {
                refreshAnimate(0);
            }
        );

        // refresh-slider-link
        $('.refresh-slider-users-active .refresh-slider-link').on('click', function (e) {
            var cur = $(this);
            var parent = cur.parents('.refresh-slider');
            if (!parent.hasClass('blocked')) {
                parent.addClass('blocked');
                clearInterval(refreshInterval);
                refreshAnimate(1);
            }
            e.preventDefault();
        });
    }
}
/* refreshSliderUsers end */

/* refreshSliderArticles */
function refreshSliderArticles() {
    if ($('.refresh-slider-articles').length) {
        var list = $('.refresh-slider-articles .refresh-slider-list');
        var slides_count = list.children(':visible').length;
        $(window).resize(function () {
            slides_count = list.children(':visible').length;
        });

        var hidden_list = $('.refresh-slider-articles .refresh-hidden-list');
        var hidden_list_length = hidden_list.children().length;
        var hidden_count = slides_count;

        var refreshInterval;
        var old_random;

        function refreshAnimate(refresh_type) { // 0 - ��������� ��������, 1 - �� �������
            var interval_delay = 1500;
            if (refresh_type == 1) {
                var random = 0;
                interval_delay = 100;
            }

            refreshInterval = setInterval(function () {
                if (refresh_type == 0) {
                    while (random == old_random) {
                        random = Math.round(Math.random() * (slides_count - 1));
                    }
                    old_random = random;
                }
                var cur_li = list.children().eq(random);
                var cur_elem = cur_li.children().eq(0).addClass('old');
                var hidden_elem = hidden_list.children().eq(hidden_count).children().clone().addClass('new').appendTo(cur_li);
                setTimeout(function () {
                    hidden_elem.removeClass('new');
                    cur_elem.addClass('old');
                }, 100);
                setTimeout(function () {
                    cur_elem.remove();
                }, 500);
                hidden_count++;
                if (refresh_type == 1)
                    random++;
                if (refresh_type == 1 && random == slides_count) {
                    clearInterval(refreshInterval);
                    $('.refresh-slider-articles').removeClass('blocked');
                }
                if (hidden_count == hidden_list_length)
                    hidden_count = 0;
            }, interval_delay);
        }

        refreshAnimate(0);

        // hover
        $('.refresh-slider').hover(
            function () {
                clearInterval(refreshInterval);
            },
            function () {
                refreshAnimate(0);
            }
        );

        // refresh-slider-link
        $('.refresh-slider-articles .refresh-slider-link').on('click', function (e) {
            var cur = $(this);
            var parent = cur.parents('.refresh-slider');
            if (!parent.hasClass('blocked')) {
                parent.addClass('blocked');
                clearInterval(refreshInterval);
                refreshAnimate(1);
            }
            e.preventDefault();
        });
    }
}
/* refreshSliderArticles end */

/* publicationsSlider */
function publicationsSlider() {
    if ($('.publications-slider.pub-slider-active').length) {
        // init
        if (!$('.resolution-large,.resolution-medium,.resolution-small').is(':visible')) {
            var slides = 4;
            var margin = 17;
            var moveSlides = 4;
        }
        if ($('.resolution-large').is(':visible')) {
            var slides = 3;
            var margin = 20;
            var moveSlides = 3;
        }
        if ($('.resolution-medium').is(':visible')) {
            var slides = 2;
            var margin = 49;
            var moveSlides = 2;
        }
        if ($('.resolution-small').is(':visible')) {
            var slides = 2;
            var margin = 27;
            var moveSlides = 2;
        }
        publicationsSlider = $('.publications-slider .slider-list').bxSlider({
            prevSelector: $('.publications-slider-prev'),
            nextSelector: $('.publications-slider-next'),
            prevText: '',
            nextText: '',
            pager: false,
            minSlides: slides,
            maxSlides: slides,
            slideWidth: 9999,
            slideMargin: margin,
            moveSlides: moveSlides,
            useCSS: false
        });

        // resize
        $(window).resize(function () {
            if (!$('.resolution-large,.resolution-medium,.resolution-small').is(':visible')) {
                var slides = 4;
                var margin = 17;
                var moveSlides = 4;
            }
            if ($('.resolution-large').is(':visible')) {
                var slides = 3;
                var margin = 20;
                var moveSlides = 3;
            }
            if ($('.resolution-medium').is(':visible')) {
                var slides = 2;
                var margin = 49;
                var moveSlides = 2;
            }
            if ($('.resolution-small').is(':visible')) {
                var slides = 2;
                var margin = 27;
                var moveSlides = 2;
            }

            publicationsSlider.reloadSlider({
                prevSelector: $('.publications-slider-prev'),
                nextSelector: $('.publications-slider-next'),
                prevText: '',
                nextText: '',
                pager: false,
                minSlides: slides,
                maxSlides: slides,
                slideWidth: 9999,
                slideMargin: margin,
                moveSlides: moveSlides,
                useCSS: false
            });
        });
    }
}
/* publicationsSlider end */

/* booksSlider */
function booksSlider() {
    if ($('.books-slider').length) {
        // init
        if (!$('.resolution-large,.resolution-medium,.resolution-small').is(':visible')) {
            var nav_item = 5;
        }
        if ($('.resolution-large').is(':visible')) {
            var nav_item = 4;
        }
        if ($('.resolution-medium').is(':visible')) {
            var nav_item = 3;
        }
        if ($('.resolution-small').is(':visible')) {
            var nav_item = 2;
        }
        $('.books-slider').sliderkit({
            shownavitems: nav_item,
            auto: false,
            circular: true,
            panelfx: 'sliding' // fading
        });

        var myStandardTabs = $(".books-slider").data("sliderkit");

        $(window).resize(function () {
            if (!$('.resolution-large,.resolution-medium,.resolution-small').is(':visible')) {
                myStandardTabs.options.shownavitems = 5;
                myStandardTabs._buildNav();
            }
            if ($('.resolution-large').is(':visible')) {
                myStandardTabs.options.shownavitems = 4;
                myStandardTabs._buildNav();
            }
            if ($('.resolution-medium').is(':visible')) {
                myStandardTabs.options.shownavitems = 3;
                myStandardTabs._buildNav();
            }
            if ($('.resolution-small').is(':visible')) {
                myStandardTabs.options.shownavitems = 2;
                myStandardTabs._buildNav();
            }
        });
    }
}
/* booksSlider end */

/* tabs */
function tabs() {
    if ($('.tabs-wrap').length) {
        $('.tab-controls-list a').on('click', function (e) {
            var cur = $(this);
            var cur_li = cur.parents('li:eq(0)');
            if (!cur_li.hasClass('current')) {
                cur_li.addClass('current').siblings().removeClass('current');
                var cur_li_index = cur_li.index();
                var parent = cur.parents('.tabs-wrap:eq(0)');
                var content = parent.find('.tabs:eq(0)');
                var cur_tab = content.children('.tab:eq(' + cur_li_index + ')');
                content.children('.tab:eq(' + cur_li_index + ')').fadeIn().siblings().fadeOut(0);

                // reload sliders
                if (cur_tab.find('.dealers-slider').length) {
                    var cur_slider_index = cur_tab.find('.dealers-slider .slider-list').attr('data-slider');
                    dealersSliders[cur_slider_index].reloadSlider();
                }

                // reload selects
                if (cur_tab.find('select').length) {
                    customSelectRefresh(cur_tab.find('select'));
                    customSelectRefreshPlaceholder(cur_tab.find('select'));
                }
            }
            e.preventDefault();
        });
    }
}
/* tabs end */

/* mobileMoves */
function mobileMoves() {
    // index/category pages
    if ($('.main-page').length || $('.category-page').length) {
        var cur_section = $('.publications:not(.bottom) .publications-section');
        var banner = cur_section.find('.publication-banner:not(.size2)');
        var banner2 = cur_section.find('.publication-banner.size2');
        if (!$('.resolution-large,.resolution-medium,.resolution-small').is(':visible')) {
            banner2.insertAfter(cur_section.children().eq(3));
            banner.insertAfter(cur_section.children().eq(9));
        }
        if ($('.resolution-large').is(':visible')) {
            banner2.insertAfter(cur_section.children().eq(1));
            banner.insertAfter(cur_section.children().eq(6));
        }
    }

    // user page
    if ($('.user-page').length) {
        var cur_section = $('.publications:not(.bottom) .publications-section');
        var banner2 = cur_section.find('.publication-banner.size2');
        if (!$('.resolution-large,.resolution-medium,.resolution-small').is(':visible')) {
            banner2.insertAfter(cur_section.children().eq(3));
        }
        if ($('.resolution-large').is(':visible')) {
            banner2.insertAfter(cur_section.children().eq(1));
        }
    }
}
/* mobileMoves end */

/* textFirstLetter */
function textFirstLetter() {
    if ($('.publication-info-text').length) {
        $('.publication-info-text textarea').on('keyup', function () {
            var cur = $(this);
            var cur_value = cur.val();
            var cur_value_first = cur_value[0];
            if (!cur_value_first)
                cur_value_first = '';
            cur.parents('.publication-info-bottom').find('.letter').html(cur_value_first);
        });
    }

    if ($('.medium-editor-textarea').length) {
        setInterval(function () {
            var cur_value_first = $('.medium-editor-textarea p').text()[0];
            $('.publication-info-bottom').find('.letter').html(cur_value_first);
            if (!cur_value_first)
                $('.publication-info-bottom').find('.letter').html('');
        }, 100);
    }
}
/* textFirstLetter end */

/* autoResizeTextarea */
function autoResizeTextarea() {
    if ($('.autoresize-textarea').length)
        $('.autoresize-textarea').autoResize({
            animate: false,
            limit: 9999,
            extraSpace: 0
        });

    $('.autoresize-textarea').keydown();
}
/* autoResizeTextarea end */

/* publicationsSectionHeight */
function publicationsSectionHeight() {
    if ($('.publications-section').length) {
        setTimeout(function () {
            var section = $('.publications-section');
            var article = $('.publications-section > article:not(.publication-banner):eq(0)');
            article.removeAttr('style');
            var article_h = article.innerHeight();
            section.children('article').css('height', article_h);
        }, 1);
    }
}
/* publicationsSectionHeight end */

/* searchInput */

function searchInput() {
    if ($('.popup-search-input').length) {
        $('.popup-search-input').donetyping(function () {
            var value = encodeURIwin125($(this).val());
            var url = $(this).attr('data-url') + value;

            if ($(this).val().length > 2) {
                $.ajax({
                    'type': 'GET',
                    'url': url,
                    'success': function (response) {
                        if ($('.search-output').length) {
                            $('.search-output').html(response);
                        }
                    }
                });
            } else {
                $('.search-output').html(' ');
            }
        }, 300);
    }
}

/* searchInput end */

/* parallax */
function parallax() {
    if ($('.content-top').length) {
        var content_top = $('.content-top');
        var content_top_bottom = content_top.offset().top + content_top.height();
        var win_top = $(window).scrollTop();

        function initParalax() {
            win_top = $(window).scrollTop();
            var percent = win_top / content_top_bottom;
            var bg_percent = 50 - (50 * percent);
            $('.content-top').css('background-position', '50% ' + bg_percent + '%');
        }

        initParalax();

        // scroll
        $(window).scroll(function () {
            initParalax();
        });

        // resize
        $(window).resize(function () {
            content_top_bottom = content_top.offset().top + content_top.height();
            initParalax();
        });
    }
}
/* parallax end */

/* article placeholder */

function articlePlaceholder() {
    if ($('.js-profile-placeholder').length) {
        $.each($('.js-profile-placeholder'), function (key, el) {
            var cur = $(this);
            var r = cur.find('.real');
            var p = cur.find('.placeholder-article-block');

            var rLength = r.length;
            var maxACount = 5;
            var resolutions = [
                {
                    'resolution': {
                        'width': 1280,
                        'count': 4
                    }
                },
                {
                    'resolution': {
                        'width': 1000,
                        'count': 3
                    }
                },
                {
                    'resolution': {
                        'width': 768,
                        'count': 2
                    }
                }
            ];

            resolutions.forEach(function (item, i, arr) {
                if ($(window).width() < item.resolution.width) {
                    maxACount = item.resolution.count;
                }
            });

            var placeholdersCount = 5;
            if (rLength > 0) {
                placeholdersCount = rLength % maxACount == 0 ? 0 : maxACount - (rLength % maxACount);
            } else {
                resolutions.forEach(function (item, i, arr) {
                    if ($(window).width() < item.resolution.width) {
                        placeholdersCount = item.resolution.count;
                    }
                });
            }

            p.each(function (index, el) {
                $(el).addClass('hidden-article')
            });

            p.each(function (index, el) {
                if (index < placeholdersCount) {
                    $(el).removeClass('hidden-article')
                }
            });
        })
    }
}

function resizeFullScreen() {
    var b = $(window).height();
    $('.full-screen').css('height', b + 'px');
}

function resizeSearchArea() {
    var b = $(window).height();
    $('.popup-inner').css('height', b + 'px');
}

/* article placeholder end */

/* audio */

function audio() {
    $(document).on('click', '.item-state-play', function (e) {
        var currentButton = $(this);
        var parent = currentButton.parents('.podcast');

        var player = $(document).find('#main-player');

        if (currentButton.hasClass('paused')) {
            currentButton.removeClass('paused');
            currentAudioPlayer(player, currentButton);
        } else {
            if (!parent.hasClass('current')) {
                $.each($(document).find('.podcast'), function (index, el) {
                    $(el).removeClass('current');
                    $(el).find('.item-state-play').removeClass('paused');
                    $(el).find('.podcast-video-line-uploaded').css({width: 0});
                    $(el).find('.podcast-video-line-current').css({width: 0});
                })
                parent.addClass('current');

                var audio = document.createElement("audio");
                audio.src = currentButton.attr('data-url');
                audio.id = 'main-player';
                $('.player-holder').html(audio);
                player = $(document).find('#main-player')[0];
            }
            currentButton.addClass('paused');

            currentAudioPlayer(player, currentButton);
        }


        e.preventDefault();
    })
}

function currentAudioPlayer(pl, clickedObject) {
    var player = $(pl);
    var cashLine = $(clickedObject).parents('.podcast').find('.podcast-video-line-uploaded');
    var progressLine = $(clickedObject).parents('.podcast').find('.podcast-video-line-current');

    if (player.length) {
        var cur = player[0];

        if (cur.paused == false) {
            cur.pause();
        } else {
            cur.play();
        }

        $(cur).on('progress', function () {
            var loadedPercentage = this.buffered.end(0) / this.duration;
            cashLine.css({width: loadedPercentage * 100 + '%'});
        });

        $(cur).on('timeupdate', function () {
            var playPercentage = (this.currentTime / this.duration) * 100;
            progressLine.css({width: playPercentage + '%'});
        });

        $(cur).on('ended', function () {
            // cashLine.css({width: '0'});
            progressLine.css({width: '0'});
            clickedObject.removeClass('paused');
        });
    }
}

/* audio end*/

$(document).ready(function () {
    if ($(window).width() < 640 || $(window).height() < 640)
        $('meta[name="viewport"]').attr('content', 'width=640, user-scalable=yes');
    parallax();
    audio();
    headerDecor();
    faces();
    sideMenu();
    payments();
    listedGallery();
    siteToTop();
    showRightFixedLinks();
    popups();
    showImage();
    showMore();
    searchInput();
    buttonsFn();
    refreshSliderUsers();
    refreshSliderArticles();
    publicationsSlider();
    booksSlider();
    mobileMoves();
    textFirstLetter();
    autoResizeTextarea();
    fixedLinks();
    articlePlaceholder();
    resizeFullScreen();
    resizeSearchArea();
});
$(window).load(function () {
    publicationsSectionHeight();
});
$(window).resize(function () {
    listedGallery();
    mobileMoves();
    publicationsSectionHeight();
    articlePlaceholder();
    resizeFullScreen();
    resizeSearchArea();
});
$(window).scroll(function () {
    listedGallery();
});

(function () {
    var transAnsiAjaxSys = [];
    var arr = [0x402, 0x403, 0x201A, 0x453, 0x201E, 0x2026, 0x2020, 0x2021, 0x20AC, 0x2030, 0x409, 0x2039,
        0x40A, 0x40C, 0x40B, 0x40F, 0x452, 0x2018, 0x2019, 0x201C, 0x201D, 0x2022, 0x2013, 0x2014, '0', 0x2122, 0x459, 0x203A, 0x45A,
        0x45C, 0x45B, 0x45F, 0x0A0, 0x40E, 0x45E, 0x408, 0x0A4, 0x490, 0x0A6, 0x0A7, 0x401, 0x0A9, 0x404, 0x0AB,
        0x0AC, 0x0AD, 0x0AE, 0x407, 0x0B0, 0x0B1, 0x406, 0x456, 0x491, 0x0B5, 0x0B6, 0x0B7, 0x451, 0x2116, 0x454,
        0x0BB, 0x458, 0x405, 0x455, 0x457, 0x410, 0x411, 0x412, 0x413, 0x414, 0x415, 0x416, 0x417, 0x418, 0x419,
        0x41A, 0x41B, 0x41C, 0x41D, 0x41E, 0x41F, 0x420, 0x421, 0x422, 0x423, 0x424, 0x425, 0x426, 0x427, 0x428,
        0x429, 0x42A, 0x42B, 0x42C, 0x42D, 0x42E, 0x42F, 0x430, 0x431, 0x432, 0x433, 0x434, 0x435, 0x436, 0x437,
        0x438, 0x439, 0x43A, 0x43B, 0x43C, 0x43D, 0x43E, 0x43F, 0x440, 0x441, 0x442, 0x443, 0x444, 0x445, 0x446,
        0x447, 0x448, 0x449, 0x44A, 0x44B, 0x44C, 0x44D, 0x44E, 0x44F
    ];
    var arLng = arr.length;
    for (var i = 0; i < arLng; i++)transAnsiAjaxSys[arr[i]] = i + 128;
    encodeURIwin125 = function (str) {
        var ret = [];
        for (var i = 0; i < str.length; i++) {
            var n = str.charCodeAt(i);
            if (typeof transAnsiAjaxSys[n] != 'undefined')
                n = transAnsiAjaxSys[n];
            if (n <= 0xFF)
                ret.push(n);
        }
        return escape(String.fromCharCode.apply(null, ret)).replace(/\+/mg, '%2B');
    }
}());

(function ($) {
    $.fn.extend({
        donetyping: function (callback, timeout) {
            timeout = timeout || 1e3; // 1 second default timeout
            var timeoutReference,
                doneTyping = function (el) {
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
            return this.each(function (i, el) {
                var $el = $(el);
                $el.is(':input') && $el.on('keyup keypress paste', function (e) {
                    if (e.type == 'keyup' && e.keyCode != 8) return;
                    if (timeoutReference) clearTimeout(timeoutReference);
                    timeoutReference = setTimeout(function () {
                        doneTyping(el);
                    }, timeout);
                }).on('blur', function () {
                    doneTyping(el);
                });
            });
        }
    });
})(jQuery);
