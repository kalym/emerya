<?php

class MyBootstrap {
    
    function bootstrap() {
        HookManager::getInstance()->bind(HookManager::EVENT_NEW_COMMENT, array($this, 'notifyNewComment'));
        HookManager::getInstance()->bind(HookManager::EVENT_APPROVE_COMMENT, array($this, 'notifyNewComment'));
        HookManager::getInstance()->bind(HookManager::EVENT_NEW_JOB, array($this, 'notifyNewJob'));
        HookManager::getInstance()->bind(HookManager::EVENT_NEW_POST, array($this, 'notifyNewPost'));
        HookManager::getInstance()->bind(HookManager::EVENT_APPROVE_POST, array($this, 'notifyNewPost'));
        HookManager::getInstance()->bind(HookManager::EVENT_NEW_QNA, array($this, 'notifyNewQna'));
        HookManager::getInstance()->bind(HookManager::EVENT_APPROVE_QNA, array($this, 'notifyNewQna'));
        HookManager::getInstance()->bind(HookManager::EVENT_PURCHASE, array($this, 'notifyPurchase'));
        HookManager::getInstance()->bind(HookManager::EVENT_NEW_ARTICLE, array($this, 'notifyNewArticle'));
        HookManager::getInstance()->bind(HookManager::EVENT_NEW_PHOTO, array($this, 'notifyNewPhoto'));
        HookManager::getInstance()->bind(HookManager::EVENT_LOGIN, array($this, 'logLogin'));
    }
    
    function logLogin($user)
    {
        $anStatLoginTable = new StatLoginTable;
        $anStatLoginTable->log($user, $_SERVER['REMOTE_ADDR'], date('Y-m-d H:i:s'));
    }
    
    function notifyNewPhoto($photo)
    {
        $url = '/users/photo/id/' . $photo->user_id;
        
        Zend_Db_Table_Abstract::getDefaultAdapter()->query("
                    INSERT INTO feed (datetime, user_id, content, type, url, is_new, author_id, content_id) 
                    (SELECT :datetime, who_id, :content, :type, :url, 1, :author_id, :content_id FROM recommend
                    WHERE whom_id=:user_id)", array (
                        ':datetime' => date('Y-m-d H:i:s'),
                        ':content' => '���������� ������ ������',
                        ':type' => FeedTable::TYPE_PHOTO,
                        ':url' => $url,
                        ':user_id' => $photo->user_id,
                        ':author_id' => $photo->user_id,
                        ':content_id' => $photo->photo_id
                    ));
    }
    
    function notifyNewArticle($article)
    {
        $url = '/' . $article->alias . '.html';
        
        Zend_Db_Table_Abstract::getDefaultAdapter()->query("
                    INSERT INTO feed (datetime, user_id, content, type, url, is_new, author_id, content_id) 
                    (SELECT :datetime, who_id, :content, :type, :url, 1, :author_id, :content_id FROM recommend
                    WHERE whom_id=:user_id)", array (
                        ':datetime' => date('Y-m-d H:i:s'),
                        ':content' => $article->name,
                        ':type' => FeedTable::TYPE_ARTICLE,
                        ':url' => $url,
                        ':user_id' => $article->user_id,
                        ':author_id' => $article->user_id,
                        ':content_id' => $article->article_id
                    ));
    }
    
    function notifyPurchase($article, $payment)
    {
        Zend_Db_Table_Abstract::getDefaultAdapter()->query("
                    INSERT INTO feed (datetime, user_id, content, type, url, is_new) 
                    VALUES (:datetime, :user_id, :content, :type, :url, 1)", array (
                        ':datetime' => date('Y-m-d H:i:s'),
                        ':user_id' => $article->user_id,
                        ':content' => $article->name,
                        ':type' => FeedTable::TYPE_PURCHASE,
                        ':url' => ''
                    ));
    }
    
    function notifyNewQna($qna)
    {
        if (!$qna->is_approved) return;
        
        $catQnaTable = new CatQnATable;
        $cat = $catQnaTable->find($qna->cat_id)->current();
        
        $url = '/question/' . $cat->alias . '/' . $qna->alias . '.html';
        
        Zend_Db_Table_Abstract::getDefaultAdapter()->query("
                    INSERT INTO feed (datetime, user_id, content, type, url, is_new, author_id, content_id) 
                    (SELECT :datetime, user_id, :content, :type, :url, 1, :author_id, :content_id FROM users
                    WHERE type=:user_type AND user_id!=:user_id)", array (
                        ':datetime' => date('Y-m-d H:i:s'),
                        ':content' => $qna->name,
                        ':type' => FeedTable::TYPE_QNA,
                        ':url' => $url,
                        ':user_type' => User::TYPE_WRITER,
                        ':user_id' => $qna->user_id,
                        ':author_id' => $qna->user_id,
                        ':content_id' => $qna->qna_id
                    ));
    }
    
    function notifyNewPost($post)
    {
        if (!$post->is_approved) return;
        
        $url = '/blogs' . ($post->is_video ? '/video' : ($post->is_photo ? '/photo' : ''))   . '/' . $post->alias . '.html';
        
        Zend_Db_Table_Abstract::getDefaultAdapter()->query("
                    INSERT INTO feed (datetime, user_id, content, type, url, is_new, author_id, content_id) 
                    (SELECT :datetime, user_id, :content, :type, :url, 1, :author_id, :content_id FROM users
                    WHERE type=:user_type AND user_id!=:user_id)", array (
                        ':datetime' => date('Y-m-d H:i:s'),
                        ':content' => $post->name,
                        ':type' => FeedTable::TYPE_POST,
                        ':url' => $url,
                        ':user_type' => User::TYPE_WRITER,
                        ':user_id' => $post->user_id,
                        ':author_id' => $post->user_id,
                        ':content_id' => $post->post_id
                    ));
    }
    
    function notifyNewJob($job)
    {
        $url = '/job/' . $job->alias . '.html';
        
        Zend_Db_Table_Abstract::getDefaultAdapter()->query("
                    INSERT INTO feed (datetime, user_id, content, type, url, is_new, author_id) 
                    (SELECT :datetime, user_id, :content, :type, :url, 1, :author_id FROM users
                    WHERE type=:user_type)", array (
                        ':datetime' => $job->datetime,
                        ':content' => $job->name,
                        ':type' => FeedTable::TYPE_JOB,
                        ':url' => $url,
                        ':user_type' => User::TYPE_WRITER,
                        ':author_id' => $job->user_id
                    ));
    }
    
    function notifyNewComment($item, $comment, $url)
    {
        if ($item->user_id == $comment->user_id) return;
        if (!$comment->is_approved) return; 

        switch ($comment->typology) {
            case CommentsTable::TYPOLOGY_ARTICLE :
                Zend_Db_Table_Abstract::getDefaultAdapter()->query("
                    INSERT INTO feed (datetime, user_id, content, type, url, is_new, author_id, content_id) 
                    VALUES(:datetime, :user_id, :content, :type, :url, 1, :author_id, :content_id)", array (
                        ':datetime' => $comment->datetime,
                        ':content' => $comment->content,
                        ':type' => FeedTable::TYPE_COMMENT_ARTICLE,
                        ':url' => $url,
                        ':user_id' => $item->user_id,
                        ':author_id' => ($comment->is_anonimus ? 0 : $comment->user_id),
                        ':content_id' => $comment->comment_id
                    ));
//                Zend_Db_Table_Abstract::getDefaultAdapter()->query("
//                    INSERT INTO feed (datetime, user_id, content, type, url, is_new, content_id) 
//                    (SELECT :datetime, user_id, :content, :type, :url, 1, :author_id, :content_id FROM comment_subscriptions
//                    WHERE typology_id=:typology_id 
//                    AND typology=:typology 
//                    AND user_id!=:user_id)", array (
//                        ':datetime' => $comment->datetime,
//                        ':content' => $comment->content,
//                        ':type' => FeedTable::TYPE_COMMENT_ARTICLE,
//                        ':url' => $url,
//                        ':typology_id' => $comment->typology_id,
//                        ':typology' => $comment->typology,
//                        ':user_id' => $comment->user_id,
//                        ':author_id' => ($comment->is_anonimus ? 0 : $comment->user_id),
//                        ':content_id' => $comment->comment_id
//                    ));
                break;
            case CommentsTable::TYPOLOGY_QNA :
                Zend_Db_Table_Abstract::getDefaultAdapter()->query("
                    INSERT INTO feed (datetime, user_id, content, type, url, is_new, author_id, content_id) 
                    VALUES(:datetime, :user_id, :content, :type, :url, 1, :author_id, :content_id)", array (
                        ':datetime' => $comment->datetime,
                        ':content' => $comment->content,
                        ':type' => FeedTable::TYPE_COMMENT_QNA,
                        ':url' => $url,
                        ':user_id' => $item->user_id,
                        ':author_id' => ($comment->is_anonimus ? 0 : $comment->user_id),
                        ':content_id' => $comment->comment_id
                    ));
                break;
            case CommentsTable::TYPOLOGY_POST:
                Zend_Db_Table_Abstract::getDefaultAdapter()->query("
                    INSERT INTO feed (datetime, user_id, content, type, url, is_new, author_id, content_id) 
                    VALUES(:datetime, :user_id, :content, :type, :url, 1, :author_id, :content_id)", array (
                        ':datetime' => $comment->datetime,
                        ':content' => $comment->content,
                        ':type' => FeedTable::TYPE_COMMENT_POST,
                        ':url' => $url,
                        ':user_id' => $item->user_id,
                        ':author_id' => ($comment->is_anonimus ? 0 : $comment->user_id),
                        ':content_id' => $comment->comment_id
                    ));
                break;
        }
    }
}


