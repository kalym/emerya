<?php
function runCron() {
    $time = strtotime(Site_Settings::get('cron_last_run', 0));
    if (time()-$time > 24 * 3600) {
        ini_set('memory_limit', '256M');
        ignore_user_abort(true);

        $settingsTable = new SettingsTable();
        $st = $settingsTable->fetchAll(
                $settingsTable->select()->where('name=?', 'cron_last_run')
                )->current();
        $st->value = date('Y-m-d H:i:s');
        $st->save();

        Zend_Db_Table_Abstract::getDefaultAdapter()->query(
            sprintf("DELETE FROM feed WHERE is_new = 0 AND datetime<'%s'",
                    date('Y-m-d H:i:s', time()-3600*24*7))
        );

        Zend_Db_Table_Abstract::getDefaultAdapter()->query(
            sprintf("DELETE FROM article_view WHERE expire < '%s'",
                    date('Y-m-d H:i:s', time()-3600*24*Site_Settings::get('ppv_truncate')))
        );

        $anArticlesTable = new ArticlesView;
        $select = $anArticlesTable->select()
                ->where('stat_expire<?', date('Y-m-d H:i:s'))
                ->where('stat_stop=0');

        $anAVT = new ArticleViewTable;

        foreach ($anArticlesTable->fetchAll($anArticlesTable->select()
                ->where('stat_expire<?', date('Y-m-d H:i:s'))
                ->where('stat_stop=0')) as $article) {

            $anAVT->stop($article, true);
        }

        //set up sitemap.xml
        $xml = new SimpleXMLElement(<<<CUT
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
</urlset>
CUT
                );
        $anArticlesTable = new ArticlesTable;
        $view = new Zend_View();
        $res = $anArticlesTable->select()->order('date DESC')->query(PDO::FETCH_OBJ);
        foreach ($res as $a) {
            $url = $xml->addChild('url', '');
            $url->loc = 'http://www.topauthor.ru' . $view->url(array($a->alias), 'article', true);
            $url->lastmod = date('Y-m-d', strtotime($a->date));
            $url->priority = '0.8';
        }
        $xml->asXML(dirname(__FILE__) . '/sitemap1.xml');
        //qna
        $xml = new SimpleXMLElement(<<<CUT
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
</urlset>
CUT
                );
        $anQnaTable = new QnaView();
        $view = new Zend_View();
        $res = $anQnaTable->select()->where('is_approved=1')->order('datetime DESC')->query(PDO::FETCH_ASSOC);
        foreach ($res as $a) {
            $url = $xml->addChild('url', '');
            $url->loc = 'http://www.topauthor.ru' . $view->url($a, 'qna', true);
            $url->lastmod = date('Y-m-d', strtotime($a['datetime']));
            $url->priority = '0.8';
        }
        $xml->asXML(dirname(__FILE__) . '/sitemap2.xml');
        //blogs
        $xml = new SimpleXMLElement(<<<CUT
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
</urlset>
CUT
                );
        $anPostTable = new PostsView;
        $view = new Zend_View();
        $res = $anPostTable->select()->where('is_approved=1')->order('date DESC')->query(PDO::FETCH_ASSOC);
        foreach ($res as $a) {
            $url = $xml->addChild('url', '');
            $url->loc = 'http://www.topauthor.ru' . $view->url($a, 'post', true);
            $url->lastmod = date('Y-m-d', strtotime($a['date']));
            $url->priority = '0.8';
        }
        $xml->asXML(dirname(__FILE__) . '/sitemap3.xml');
        //users
        $xml = new SimpleXMLElement(<<<CUT
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
</urlset>
CUT
                );
        $anUserTable = new UsersTable();
        $view = new Zend_View();
        $res = $anUserTable->select()->order('last_activity DESC')->query(PDO::FETCH_ASSOC);
        foreach ($res as $a) {
            $url = $xml->addChild('url', '');
            $url->loc = 'http://www.topauthor.ru' . $view->url(array($a['login']), 'user', true);
            $url->lastmod = date('Y-m-d', strtotime($a['last_activity']));
            $url->priority = '0.8';
        }
        $xml->asXML(dirname(__FILE__) . '/sitemap4.xml');
    }
}