<?php
define('ROOT_DIR', dirname(__FILE__));
$expires = 60*60;
header("Pragma: public");
header("Cache-Control: public, maxage=".$expires);
header("Content-Type: application/x-javascript");
header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expires) . ' GMT');
ini_set('display_errors', false);
$js = array(
    'public/scripts/jquery-latest.js',
    'public/scripts/jquery.main.js',
    'public/scripts/site.js',
    'public/scripts/rating.js',
    'public/scripts/size.js',
    'public/scripts/jquery/jquery.ocupload.js',
    'public/scripts/fancybox/jquery.fancybox-1.3.4.pack.js'
    
);
foreach ($js as $f)
{
    print "\n\n/* * * * *  $f * * * * */\n";
    include ROOT_DIR . "/" . $f;
}
