<?php

ini_set('eaccelerator.enable', 0);
//phpinfo(); exit;
//define('DEBUG', true);
// || $_SERVER['REMOTE_ADDR']=='188.187.16.73'
if ($_SERVER['REMOTE_ADDR']=='188.232.153.27') {
  define('DEBUG', true);
}

include_once 'cron.php';

function checkBan()
{
    if (!isset($_SESSION['is_ban'])) {
        $anBanTable = new BanTable;
        do {
            if ($anBanTable->fetchRow($anBanTable
                    ->select()
                    ->where('value=?', $_SERVER['REMOTE_ADDR'])
                    ->where('type=?', 'ip'))) {
                $_SESSION['is_ban'] = true;
                break;
            }

            $host = parse_url(@$_SERVER['HTTP_REFERER'], PHP_URL_HOST);

            if ($host && $anBanTable->fetchRow($anBanTable
                    ->select()
                    ->where('value=?', $host)
                    ->where('type=?', 'referrer'))) {
                $_SESSION['is_ban'] = true;
                break;
            }
            
            $_SESSION['is_ban'] = false;
        
        } while (false);
    }
    
    if ($_SESSION['is_ban'])
        die('� ������� ��������');
}

ini_set('display_errors', defined('DEBUG') ? 1: 0);
$loc = setlocale(LC_ALL, 'ru_RU.CP1251');

error_reporting(E_ALL | E_STRICT);
date_default_timezone_set('Europe/Moscow');
set_include_path('.'.PATH_SEPARATOR . './library'
    .PATH_SEPARATOR.'./application/models/'
    .PATH_SEPARATOR.get_include_path());

include "Zend/Loader.php";
function __autoload($className) {
    Zend_Loader::loadClass($className);
}

Zend_Filter::setDefaultNamespaces(array('Site_Filter', 'Zend_Filter'));
//Zend_Locale::setDefault('ru_RU.CP1251');

$config = new Zend_Config_Ini('./application/config.ini', 'general');

$registry = Zend_Registry::getInstance();
$registry->set('config', $config);
$registry->set('root_dir', dirname(__FILE__));

// setup database
$db = Zend_Db::factory($config->db->adapter, $config->db->config->toArray());
Zend_Db_Table::setDefaultAdapter($db);

checkBan();

$sTable = new SettingsTable;
$settings = array();
$sRowSet = $sTable->fetchAll();
      foreach ($sRowSet as $sRow) {
          $settings[$sRow->name] = $sRow->value;
      }
$registry->set('settings', $settings);

Zend_Controller_Action_HelperBroker::addPrefix('Site_Controller_Action_Helper');

$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
$viewRenderer->initView(); // make sure the view object is initialized
$viewRenderer->view->addHelperPath('Site/View/Helper', 'Site_View_Helper'); // set a helper path


// setup controller
$frontController = Zend_Controller_Front::getInstance();
$frontController->throwExceptions(defined('DEBUG'));
$frontController->setControllerDirectory(array(
    'default' => './application/default/controllers',
    'admin' => './application/admin/controllers'
));
Zend_Layout::startMvc();
Zend_Layout::getMvcInstance()->setLayout('layout');

include_once(dirname('__FILE__'). '/bootstrap.php');
$bootstrap = new MyBootstrap();
$bootstrap->bootstrap();

if (!Zend_Auth::getInstance()->hasIdentity() && isset($_COOKIE['auth'])) {
    $authAdapter = new Site_Auth_Adapter_Cookie($_COOKIE['auth']);
    Zend_Auth::getInstance()->authenticate($authAdapter);
    if (Zend_Auth::getInstance()->hasIdentity()) {
        HookManager::getInstance()->call(HookManager::EVENT_LOGIN, 
                array('user'=>Zend_Auth::getInstance()->getIdentity()));
    }
}

//Handel last activity time
if (Zend_Auth::getInstance()->hasIdentity()) {
    $anUsersTable = new UsersTable;
    $user = $anUsersTable->find(
               Zend_Auth::getInstance()->getIdentity()->user_id
            )->current();
    if ($user) {
        $user->last_activity = date('Y-m-d H:i:s');
        $user->last_ip = $_SERVER['REMOTE_ADDR'];
        $user->save();
        
        $adapter = new Site_Auth_Adapter_Null($user->login, '');
        Zend_Auth::getInstance()->authenticate($adapter);
    }
}

$dispatcher = $frontController->getDispatcher();
$request = $frontController->getRequest();

$router = $frontController->getRouter();
$router->addRoute('default', new Zend_Controller_Router_Route_Module(array(), $dispatcher, $request));
$route = new Site_Controller_Router_Route_Article();
$router->addRoute('article', $route);
$route = new Site_Controller_Router_Route_User();
$router->addRoute('user', $route);
$route = new Site_Controller_Router_Route_CatArticle();
$router->addRoute('cat-article', $route);
$route = new Site_Controller_Router_Route_ShopCat();
$router->addRoute('shop-cat', $route);
$route = new Site_Controller_Router_Route_Page();
$router->addRoute('page', $route);
$route = new Site_Controller_Router_Route_Job();
$router->addRoute('job', $route);
$route = new Site_Controller_Router_Route_Post();
$router->addRoute('post', $route);
$route = new Site_Controller_Router_Route_Qna();
$router->addRoute('qna', $route);
$route = new Site_Controller_Router_Route_CatQna();
$router->addRoute('cat-qna', $route);

$route = new Zend_Controller_Router_Route('articles/:p', array(
    'module' => 'default',
    'controller' => 'index',
    'action' => 'index',
    'p' => 1
), array(
    'p' => '\d+'
));
$router->addRoute('article-index', $route);

$route = new Zend_Controller_Router_Route('draft/:p', array(
    'module' => 'default',
    'controller' => 'draft',
    'action' => 'index',
    'p' => 1
), array(
    'p' => '\d+'
));
$router->addRoute('draft-index', $route);

$route = new Zend_Controller_Router_Route('popular/:p', array(
    'module' => 'default',
    'controller' => 'popular',
    'action' => 'index',
    'p' => 1
), array(
    'p' => '\d+'
));
$router->addRoute('popular-index', $route);

$route = new Zend_Controller_Router_Route('users/:p', array(
    'module' => 'default',
    'controller' => 'users',
    'action' => 'index',
    'p' => 1
), array(
    'p' => '\d+'
));
$router->addRoute('user-index', $route);

$route = new Zend_Controller_Router_Route('podcasts/:p', array(
    'module' => 'default',
    'controller' => 'podcasts',
    'action' => 'index',
    'p' => 1
), array(
    'p' => '\d+'
));
$router->addRoute('podcast-index', $route);

$route = new Zend_Controller_Router_Route('blogs/:p', array(
    'module' => 'default',
    'controller' => 'blogs',
    'action' => 'index',
    'p' => 1
), array(
    'p' => '\d+'
));
$router->addRoute('blog-index', $route);

$route = new Zend_Controller_Router_Route('blogs/video/:p', array(
    'module' => 'default',
    'controller' => 'blogs',
    'action' => 'video',
    'p' => 1
), array(
    'p' => '\d+'
));
$router->addRoute('blog-video', $route);

$route = new Zend_Controller_Router_Route('blogs/photo/:p', array(
    'module' => 'default',
    'controller' => 'blogs',
    'action' => 'photo',
    'p' => 1
), array(
    'p' => '\d+'
));
$router->addRoute('blog-photo', $route);

$route = new Zend_Controller_Router_Route('shop/:p', array(
    'module' => 'default',
    'controller' => 'shop',
    'action' => 'index',
    'p' => 1
), array(
    'p' => '\d+'
));
$router->addRoute('shop-index', $route);

$route = new Zend_Controller_Router_Route('question/:p', array(
    'module' => 'default',
    'controller' => 'question',
    'action' => 'index',
    'p' => 1
), array(
    'p' => '\d+'
));
$router->addRoute('question-index', $route);

$route = new Zend_Controller_Router_Route('expert/:p', array(
    'module' => 'default',
    'controller' => 'expert',
    'action' => 'index',
    'p' => 1
), array(
    'p' => '\d+'
));
$router->addRoute('expert-index', $route);

$route = new Site_Controller_Router_Route_NotFound();
$router->addRoute('not-found', $route);

$route = new Zend_Controller_Router_Route('w/get/:type/:user_id/:cat_id', array(
   'module' => 'default',
   'controller' => 'w',
   'action' => 'get',
   'cat_id' => null
));
$router->addRoute('widget', $route);

$frontController->setBaseUrl('/');
$frontController->registerPlugin(new Site_Controller_Plugin_Auth);
$frontController->registerPlugin(new Site_Controller_Plugin_NotFound);

// run!
runCron();

$frontController->dispatch();
/*
if ($_SERVER['REMOTE_ADDR']=='95.84.8.119') {
$profiler = $db->getProfiler();
$totalTime    = $profiler->getTotalElapsedSecs();
$queryCount   = $profiler->getTotalNumQueries();
$longestTime  = 0;
$longestQuery = null;

foreach ($profiler->getQueryProfiles() as $query) {
    if ($query->getElapsedSecs() > $longestTime) {
        $longestTime  = $query->getElapsedSecs();
        $longestQuery = $query->getQuery();
    }
}

echo 'Executed ' . $queryCount . ' queries in ' . $totalTime .
     ' seconds' . "\n";
echo 'Average query length: ' . $totalTime / $queryCount .
     ' seconds' . "\n";
echo 'Queries per second: ' . $queryCount / $totalTime . "\n";
echo 'Longest query length: ' . $longestTime . "\n";
echo "Longest query: \n" . $longestQuery . "\n";

}
*/
