$.addObject = function (a, b) {
    $.fn[a] = function (c) {
        var d = this.data(a);
        if (typeof c == "string") {
            var e = d[c];
            if (typeof e == "function") {
                e = e.apply(d, Array.prototype.slice.call(arguments, 1));
                return e == d ? this : e
            }
            if (arguments.length == 1) return e;
            d[c] = arguments[1];
            return this
        }
        if (d) return d;
        this.data(a, new b(this.selector, c));
        return this
    }
};
var Delegate = {
    create: function (a, b) {
        return function () {
            return b.apply(a, arguments)
        }
    }
},
com = {};
com.nodebeat = {};
com.nodebeat.SlideShow = function (a, b) {
    this.defaults = {
        delay: 4E3,
        threshold: 60
    };
    this.options = $.extend(this.defaults, b);
    this.element = $(a);
    this.items = $("li", this.element);
    this.numItems = this.items.length;
    this.dragOrigin = {
        x: 0,
        y: 0
    };
    this.itemIndex = 0;
    this.viewportWidth = this.element.width();
    this.viewportHeight = this.element.height();
    this._handleNavigation = Delegate.create(this, this.handleNavigation);
    this._handleTouchStart = Delegate.create(this, this.handleTouchStart);
    this._handleTouchMove = Delegate.create(this, this.handleTouchMove);
    this._handleTouchEnd = Delegate.create(this, this.handleTouchEnd);
    this._autoProgress = Delegate.create(this, this.autoProgress);
    this.initContainer();
    this.initNavigation();
    this.initTouch();
    this.startTimer()
};
com.nodebeat.SlideShow.prototype = {
    initContainer: function () {
        this.container = $("<div>");
        this.container.addClass("slideshowContainer");
        this.container.append(this.items);
        this.element.prepend(this.container)
    },
    initNavigation: function () {
        var a;
        this.navigation = $("<ul>").addClass("navigation");
        for (var b = 0; b < this.numItems; ++b) {
            a = $("<a>");
            a.attr("href", "#");
            a.attr("rel", "slide" + b);
            a.bind("click touchend", this._handleNavigation);
            this.navigation.append($("<li>").append(a))
        }
        this.navigation.css({
            left: this.element.width() * 0.5 - (this.numItems + 1) * 4.5
        });
        this.element.append(this.navigation)
    },
    initTouch: function () {
        this.element.bind("mousedown touchstart", this._handleTouchStart)
    },
    snapToNearest: function () {
        var a = this.itemIndex * -this.viewportWidth,
            b = this.container.position().left,
            c = this.options.threshold;
        if (b < a - c) this.gotoItem(this.itemIndex + 1);
        else b > a + c ? this.gotoItem(this.itemIndex - 1) : this.gotoItem(this.itemIndex)
    },
    startTimer: function () {
        this.timerInterval = setTimeout(this._autoProgress, this.options.delay)
    },
    autoProgress: function () {
        if (++this.itemIndex > this.numItems - 1) this.itemIndex = 0;
        this.gotoItem(this.itemIndex);
        clearTimeout(this.timerInterval);
        this.timerInterval = setTimeout(this._autoProgress, this.options.delay)
    },
    gotoItem: function (a) {
        this.itemIndex = a = Math.max(0, Math.min(a, this.numItems - 1));
        this.container.addClass("animated");
        this.container.css({
            left: this.itemIndex * -this.viewportWidth
        });
        $("a", this.navigation).each(function () {
            parseInt($(this).attr("rel").match(/\d$/)[0], 10) === a ? $(this).addClass("active") : $(this).removeClass("active")
        })
    },
    gotoNext: function () {
        this.gotoItem(this.itemIndex + 1)
    },
    gotoPrev: function () {
        this.gotoItem(this.itemIndex - 1)
    },
    handleNavigation: function (a) {
        var b = $(a.target).attr("rel");
        this.gotoItem(parseInt(b.match(/\d$/)[0], 10));
        a.preventDefault()
    },
    handleTouchStart: function (a) {
        var b, c;
        if (a.type === "touchstart") {
            b = a.originalEvent.targetTouches[0].pageX;
            c = a.originalEvent.targetTouches[0].pageY
        } else {
            b = a.pageX;
            c = a.pageY
        }
        this.dragOrigin.x = b - this.container.position().left;
        this.dragOrigin.y = c - this.container.position().top;
        $(window).bind("touchmove mousemove", this._handleTouchMove);
        $(window).bind("touchend mouseup", this._handleTouchEnd);
        this.container.removeClass("animated");
        clearTimeout(this.timerInterval);
        a.preventDefault()
    },
    handleTouchMove: function (a) {
        this.container.css({
            left: (a.type === "touchmove" ? a.originalEvent.targetTouches[0].pageX : a.pageX) - this.dragOrigin.x
        })
    },
    handleTouchEnd: function () {
        $(window).unbind("touchmove mousemove", this._handleTouchMove);
        $(window).unbind("touchend mouseup", this._handleTouchEnd);
        this.snapToNearest();
        this.startTimer()
    }
};
$.addObject("slideshow", com.nodebeat.SlideShow);

com.nodebeat.Main = function () {
    this.isIOS = !! /ip(hone|od|ad)/gi.test(navigator.userAgent)
};
com.nodebeat.Main.prototype = {
    init: function () {
        $("#iPad ul.slideshow").slideshow({
            delay: 4500
        });
        $("#iPhone ul.slideshow").slideshow({
            delay: 4700
        });
       
    }
};

$.fn.simplebox = function(options) { return new Simplebox(this, options); };

function Simplebox(context, options) { this.init(context, options); };

Simplebox.prototype = {
	options:{},
	init: function (context, options){
		this.options = $.extend({
			duration: 300,
			linkClose: 'a.close, a.btn-close',
			divFader: 'fader',
			faderColor: 'white',
			opacity: 0.7,
			wrapper: '#wrapper',
			linkPopap: '.link-submit',
			video: false
		}, options || {});
		this.btn = $(context);
		this.select = $(this.options.wrapper).find('select');
		this.initFader();
		this.btnEvent(this, this.btn);
	},
	btnEvent: function($this, el){
		el.click(function(){
			if ($(this).attr('href')) $this.toPrepare($(this).attr('href'));
			else $this.toPrepare($(this).attr('title'));
			return false;
		});
	},
	calcWinWidth: function(){
		this.winWidth = $('body').width();
		if ($(this.options.wrapper).width() > this.winWidth) this.winWidth = $(this.options.wrapper).width();
	},
	toPrepare: function(obj){
		if (this.options.video){
			var temp = '<div class="modal-video"> <iframe src="'+obj+'" width="640" height="360" frameborder="0"></iframe> <span class="close"></span> </div>';
			$('body').append(temp);
			obj = 'div.modal-video';
		}
		this.popup = $(obj);
		this.btnClose = this.popup.find(this.options.linkClose);
		this.submitBtn = this.popup.find(this.options.linkPopap);
		
		if ($.browser.msie) this.select.css({visibility: 'hidden'});
		this.calcWinWidth();
		this.winHeight = $(window).height();
		this.winScroll = $(window).scrollTop();
		
		this.popupTop = this.winScroll + (this.winHeight/2) - this.popup.outerHeight(true)/2;
		if (this.popupTop < 0) this.popupTop = 0;
		this.faderHeight = $(this.options.wrapper).outerHeight();
		if ($(window).height() > this.faderHeight) this.faderHeight = $(window).height();
		
		this.popup.css({
			top: this.popupTop,
			left: this.winWidth/2 - this.popup.outerWidth(true)/2
		}).hide();
		this.fader.css({
			width: this.winWidth,
			height: this.faderHeight
		});
		this.initAnimate(this);
		this.initCloseEvent(this, this.btnClose, true);
		this.initCloseEvent(this, this.submitBtn, false);
		this.initCloseEvent(this, this.fader, true);
	},
	initCloseEvent: function($this, el, flag){
		el.click(function(){
			$this.popup.fadeOut($this.options.duration, function(){
				if ($this.options.video){
					$this.popup.remove();
				}
				else{
					$this.popup.css({left: '-9999px'}).show();
				}
				if ($.browser.msie) $this.select.css({visibility: 'visible'});
				$this.submitBtn.unbind('click');
				$this.fader.unbind('click');
				$this.btnClose.unbind('click');
				$(window).unbind('resize');
				if (flag) $this.fader.fadeOut($this.options.duration);
				else {
					if ($this.submitBtn.attr('href')) $this.toPrepare($this.submitBtn.attr('href'));
					else $this.toPrepare($this.submitBtn.attr('title'));
				}
			});
			return false;
		});
	},
	initAnimate:function ($this){
		$this.fader.fadeIn($this.options.duration, function(){
			$this.popup.fadeIn($this.options.duration);
		});
		$(window).resize(function(){
			$this.calcWinWidth();
			$this.popup.animate({
				left: $this.winWidth/2 - $this.popup.outerWidth(true)/2
			}, {queue:false, duration: $this.options.duration});
			$this.fader.css({width: $this.winWidth});
		});
	},
	initFader: function(){
		if ($('div.'+this.options.divFader).length > 0) this.fader = $('div.'+this.options.divFader);
		else{
			this.fader = $('<div class="'+this.options.divFader+'"></div>');
			$('body').append(this.fader);
			this.fader.css({
				position: 'absolute',
				zIndex: 999,
				left:0,
				top:0,
				background: this.options.faderColor,
				opacity: this.options.opacity
			}).hide();
		}
	}
}

jQuery.fn.customCheckbox = function(_options){
	var _options = jQuery.extend({
		checkboxStructure: '<div></div>',
		checkboxDisabled: 'disabled',
		checkboxDefault: 'checkboxArea',
		checkboxChecked: 'checkboxAreaChecked'
	}, _options);
	return this.each(function(){
		var checkbox = jQuery(this);
		if(!checkbox.hasClass('outtaHere') && checkbox.is(':checkbox')){
			var replaced = jQuery(_options.checkboxStructure);
			this._replaced = replaced;
			if(checkbox.is(':disabled')) replaced.addClass(_options.checkboxDisabled);
			else if (checkbox.is(':checked')) {
				replaced.addClass(_options.checkboxChecked);
				checkbox.parent().addClass('ready');
			}
			else 
				replaced.addClass(_options.checkboxDefault);
				checkbox.parent().addClass('ready');
			
			replaced.click(function(){
				if(checkbox.is(':checked')) checkbox.removeAttr('checked');
				else checkbox.attr('checked', 'checked');
				changeCheckbox(checkbox);
			});
			checkbox.click(function(){
				changeCheckbox(checkbox);
			});
			replaced.insertBefore(checkbox);
			checkbox.addClass('outtaHere');
		}
	});
	function changeCheckbox(_this){
		_this.parent().toggleClass('ready');
		if(_this.is(':checked')) _this.get(0)._replaced.removeClass().addClass(_options.checkboxChecked);
		else _this.get(0)._replaced.removeClass().addClass(_options.checkboxDefault);
	}
}

$(document).ready(function () {
	$('.checkbox').customCheckbox();
	$('.video-link').simplebox({
		wrapper: '.promo',
		linkClose: 'span.close',
		video: true
	});
	$('.link-popup').simplebox({
		wrapper: '#container'
	});
    (new com.nodebeat.Main).init();
});