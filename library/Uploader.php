<?php


class Uploader 
{
    const ERROR_UNKNOWN_FILE_TYPE = '����������� ��� �����';
    const ERROR_FILE_UPLOAD       = '������ ��� �������� �����';
    const ERROR_FILE_SAVE         = '������ ���������� �����';
    
    protected $root       = null;
    protected $input_name = null;
    protected $file       = null;
    protected $type       = null;
    protected $size       = null;
    
    function __construct($root, $input_name = 'Filedata')
    {
        $this->root       = $root;
        $this->input_name = $input_name;
        $this->file       = @$_FILES[$this->input_name];
        $this->size       = $this->file['size'];
    }
    
    /**
     * Upload file 
     *
     * @param string $folder folder where upload should be stored
     * @return string $url   
     */                   
    function upload($folder = 'other')
    {
        $file = @$_FILES[$this->input_name];
        
        if (!is_array($file)) {
            return null;
        }        
        
        if ($file['error'] || !is_uploaded_file($file['tmp_name'])) {
            throw new Exception(self::ERROR_FILE_UPLOAD);
        }

       
        $url = $this->move($folder);
        return $url;
    }
    
    protected function move($folder) 
    {
        $folder = $this->getFolder() . "/{$folder}/"; 
        if (!is_dir($this->root . $folder)) {
            mkdir($this->root . $folder);
        }
        
        $url  =  $folder . $this->generateFileName() . '.' . $this->getExt($this->file['name']);
        $path = $this->root . $url;
        if (!@move_uploaded_file($this->file['tmp_name'], $path)) {
            throw new Exception(self::ERROR_FILE_SAVE);
        }  
        return $url;  
    }
    
    protected function getFolder() 
    {
        $folder = '/uploads/' . date("Y-m");
        if (!is_dir($this->root . $folder)) {
            mkdir($this->root . $folder);
        }
        return $folder;
    }
    
    protected function generateFileName() 
    {
        $symbols  = "abcdefghijklmnopqrstuvwxyz";
        $symbols .= "0123456789";
        $max = strlen($symbols);
        $name='';
        for ($i=0; $i<9; $i++) {
            $name .= $symbols[rand(0, $max-1)];
        }
        $name = md5( $name . microtime() );
        return $name;
    }
    
    function getSize() 
    {
        return $this->size;
    }
    
    function getType() 
    {
        return $this->type;
    }
    
    function getExt($filename) {
        $arr = explode('.', $filename);
        return array_pop($arr);
    }

}