<?php

class Form_Comment extends Zend_Form
{

    function initDefaults()
    {
        $this->getElement('form')->setValue('comment-add');
    }

    function init()
    {
        $this->setMethod(Zend_Form::METHOD_POST)
            ->setAttrib('encode', Zend_Form::ENCTYPE_MULTIPART)
            ->setAttrib('id', 'comment-add');

        $this->addElement('textarea', 'content', array(
            'required' => true,
            'filters' => array('StripTags'),
            'label' => '��� �����������'
        ));

        $content = $this->getElement('content');
        $content->setAttrib('rows', 8);

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('����������, ������� ����� �����������');
        $content->addValidator($validator);

        $validator = new Site_Validate_NotContainEmailAddress();
        $validator->setMessage('������ ��������� ������ ����������� ����� � ������������');
        $content->addValidator($validator);


        $this->addElement('checkbox', 'is_subscribed', array(
            'label' => '��������� � ����� ������������ �� �����',
            'id' => 'comment-is_subscribed'
        ));

        $this->addElement('hidden', 'form');

        $this->addElement('text', 'video', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '����� (Youtube)'
        ));

        $this->initDefaults();
    }

}

class Form_Subscribe extends Zend_Form
{

    function initDefaults()
    {
        $this->getElement('form')->setValue('comment-subscribe');
    }

    function init()
    {
        $this->setMethod(Zend_Form::METHOD_POST)
            ->setAttrib('encode', Zend_Form::ENCTYPE_MULTIPART)
            ->setAttrib('id', 'comment-subscribe');

        $this->addElement('hidden', 'form');
        $this->initDefaults();
    }

}

class CommentsProcessor
{

    public $typology = null;
    public $typology_id = null;
    protected $formComment = null;
    protected $formSubscribe = null;
    protected $request = null;
    public $page = 1;

    /** @var CommentsProcessorStrategy_Interface */
    protected $strategy = null;

    public function __construct($typology_id, $typology = CommentsTable::TYPOLOGY_ARTICLE)
    {
        $this->typology = $typology;
        $this->typology_id = $typology_id;
        switch ($this->getTypology()) {
            case CommentsTable::TYPOLOGY_ARTICLE:
                $this->strategy = new CommentsProcessorStrategy_Article($typology_id);
                break;
            case CommentsTable::TYPOLOGY_NEWS:
                $this->strategy = new CommentsProcessorStrategy_News($typology_id);
                break;
            case CommentsTable::TYPOLOGY_POST:
                $this->strategy = new CommentsProcessorStrategy_Post($typology_id);
                break;
            case CommentsTable::TYPOLOGY_QNA:
                $this->strategy = new CommentsProcessorStrategy_Qna($typology_id);
                break;
            case CommentsTable::TYPOLOGY_USER:
                $this->strategy = new CommentsProcessorStrategy_User($typology_id);
                break;
            case CommentsTable::TYPOLOGY_JOB:
                $this->strategy = new CommentsProcessorStrategy_Job($typology_id);
                break;
        }
    }

    public function run(Zend_Controller_Request_Abstract $request)
    {
        $this->request = $request;

        $formComment = $this->getFormComment();
        $formSubscribe = $this->getFormSubscribe();


        if ($request->isPost()
            && $this->canComment(Zend_Auth::getInstance()->getIdentity())
        ) {

            switch ($request->getParam('form')) {
                case 'comment-add' :
                    $this->addComment($formComment);
                    break;
                case 'comment-subscribe' :
                    $this->subscribeForComment($formSubscribe);
                    break;
            }
        }
    }

    public function showSubscribeForm()
    {
        return $this->strategy->showSubscribeForm();
    }

    public function allowSubscribe()
    {
        return $this->strategy->allowSubscribe();
    }

    public function allowRate()
    {
        return $this->strategy->allowRate();
    }

    public function getTitle()
    {
        return $this->strategy->getTitle($this->getComments());
    }

    public function getFormComment()
    {
        if (is_null($this->formComment)) {
            $this->formComment = new Form_Comment;
        }
        return $this->formComment;
    }

    public function getFormSubscribe()
    {
        if (is_null($this->formSubscribe)) {
            $this->formSubscribe = new Form_Subscribe;
        }
        return $this->formSubscribe;
    }

    public function getComments()
    {
        $anCommentsView = new CommentsView();

        if ($this->typology == CommentsTable::TYPOLOGY_QNA) {
            $select = $anCommentsView->select()
                ->from('comments_view')
                ->setIntegrityCheck(false)
                ->joinLeft('rate', "comments_view.comment_id=rate.comment_id AND rate.rate=1", array(
                    'cnt' => new Zend_Db_Expr('COUNT(rate.rate_id)'),
                ))
                ->where('comments_view.typology_id=?', $this->getTypologyId())
                ->where('comments_view.typology=?', $this->getTypology())
                ->where('comments_view.is_approved=1')
                ->group(array('comments_view.comment_id'))
                ->order('cnt DESC');
        } else {
            $select = $anCommentsView->select()
                ->where('typology_id=?', $this->getTypologyId())
                ->where('typology=?', $this->getTypology())
                ->where('is_approved=1')
                ->order('datetime ASC');
//        $select = $this->strategy->applyLimit($select);
//            $select = $this->strategy->applyOrder($select);
        }

        $comments = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $comments->setCurrentPageNumber(
            $this->page
        );
        $comments->setItemCountPerPage(5);

        return $comments;
    }

    public function getCommentTypologyName()
    {
        return $this->strategy->getName();
    }

    public function getCommentTypologyTypeName()
    {
        return $this->strategy->getTypeName();
    }

    public function getItem()
    {
        return $this->strategy->getItem();
    }

    public function canComment($user)
    {
        return $this->strategy->canComment($user);
    }

    public function getMsg()
    {
        return $this->strategy->getMsg();
    }

    protected function getTypologyItem()
    {
        return $this->strategy->getItem();
    }

    /**
     *
     * @return Zend_Controller_Request_Abstract
     */
    protected function getRequest()
    {
        return $this->request;
    }

    protected function subscribeForComment(Form_Subscribe $form)
    {
        if ($form->isValid($_POST)) {
            $data['email'] = Zend_Auth::getInstance()->getIdentity()->email;
            $data['typology'] = $this->getTypology();
            $data['typology_id'] = $this->getTypologyId();
            $anCSTable = new CSTable;
            $anCSTable->replace($data);
            $form->reset();
            $form->initDefaults();
        }
    }

    protected function getTypologyId()
    {
        return $this->typology_id;
    }

    protected function getTypology()
    {
        return $this->typology;
    }

    public function getImages()
    {
        $images = array();
        if ($this->getRequest()->isPost()) {
            $p = $this->getRequest()->getPost();
//            foreach ($p['img_small'] as $k => $v) {
//                if ($v) {
//                    $images_item = array(
//                        'small' => $v,
//                        'big' => $p['img_big'][$k],
//                        'preview' => $p['img_preview'][$k]
//                    );
//                    $images[] = $images_item;
//                }
//            }
        }
        return $images;
    }

    public function isModerate()
    {
        return Zend_Auth::getInstance()->hasIdentity() && !Zend_Auth::getInstance()->getIdentity()->isWriter();
    }

    protected function addComment(Form_Comment $form)
    {

        if ($form->isValid($_POST)) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $anCommentsTable = new CommentsTable;
            $comment = $anCommentsTable->fetchNew();
            $comment->typology_id = $this->getTypologyId();
            $comment->typology = $this->getTypology();
            $comment->user_id = $user->user_id;

            if ($comment->typology == CommentsTable::TYPOLOGY_QNA) {
                $comment->content = iconv('UTF-8', 'windows-1251', $form->getValue('content'));
            } else {
                $comment->content = $form->getValue('content');
            }

            $comment->datetime = date('Y-m-d H:i:s');
            $comment->gallery = serialize($this->getImages());
            $comment->video = $form->getValue('video');
            $comment->is_smile = Zend_Auth::getInstance()->getIdentity()->smile_until > date('Y-m-d h:i:s') ? 1 : 0;
            $comment->is_anonimus = $user->isAnonimus() ? 1 : 0;
            $comment->is_approved = $this->isModerate() ? 0 : 1;

            if (!$user->haveAprovedArticles()) {
                $comment->is_approved = 0;
            }

            $comment->save();
            if ((int)$form->getValue('is_subscribed') && $this->allowSubscribe()) {
                $data['email'] = Zend_Auth::getInstance()->getIdentity()->email;
                $data['user_id'] = Zend_Auth::getInstance()->getIdentity()->user_id;
                $data['typology_id'] = $this->getTypologyId();
                $data['typology'] = $this->getTypology();
                $anCSTable = new CSTable;
                $anCSTable->replace($data);
            }

            if ($comment->is_approved)
                $this->processApprovedComment($comment);

            $form->reset();
            $form->initDefaults();
            $router = Zend_Controller_Front::getInstance()->getRouter();

            $url = call_user_func_array(array($router, 'assemble'), $comment->getUrlParams());
            $url = $url . ($this->isModerate() ? '#comments-form' : '#comment-' . $comment->comment_id);
            HookManager::getInstance()->call(HookManager::EVENT_NEW_COMMENT, array(
                'item' => $this->getItem(),
                'comment' => $comment,
                'url' => $url
            ));


            $anCommentsView = new CommentsView();

            $selectedComment = $anCommentsView->fetchRow(
                $anCommentsView->select()
                    ->where('typology_id=?', $this->getTypologyId())
                    ->where('typology=?', $this->getTypology())
                    ->where('is_approved=1')
                    ->where('comment_id=?', $comment->comment_id)
            );

            $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
            $viewRenderer->initView();
            $view = $viewRenderer->view;
            $view->comment = $selectedComment;

            $resp = array();
            $resp['errorCode'] = 0;

            if (!$user->haveAprovedArticles()) {
                $resp['msg'] = iconv('windows-1251', 'UTF-8', '��� ����������� �������� �� ����� ����� �������� �����������');
                $resp['action'] = 'comment-form';
                $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');
            } else {
                $resp['msg'] = iconv('windows-1251', 'UTF-8', '��� ����������� ������� ��������');
                $resp['action'] = 'comment-form';
                $resp['last_comment'] = iconv('windows-1251', 'UTF-8', $view->render('widgets/_comment.phtml'));
                $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');
            }

            $resp = json_encode($resp);
            echo $resp;
            exit;
        } else {
            $resp = array();
            $resp['errorCode'] = 1;
            $resp['msg'] = iconv('windows-1251', 'UTF-8', '��������� ���� "���� ���������"');

            $resp = json_encode($resp);
            echo $resp;

            exit;
        }
    }

    function processApprovedComment($comment)
    {
        $this->updateRaiting($comment);
        $this->broadcastComment($comment);
    }

    protected function updateRaiting($comment)
    {
        $this->strategy->updateRaiting($comment);
    }

    protected function broadcastComment($comment)
    {
        $anCSTable = new CSTable;
        $subscribers = $anCSTable->fetchAll(
            $anCSTable->select()
                ->where('typology=?', $comment->typology)
                ->where('typology_id=?', $comment->typology_id)
        );


        $registry = Zend_Registry::getInstance();
        $settings = $registry->get('settings');
        $config = $registry->get('config');

        $mail = new Site_Mail('comment');
        $mail->setComment($comment);
        $mail->setUri('http://' . $config->site->domain . $this->getTypologyUrl());

        foreach ($subscribers as $sub) {
            $mail->send($sub->email);
        }
    }

    protected function getTypologyUrl()
    {
        return $this->strategy->getUrl();
    }

}

interface CommentsProcessorStrategy_Interface
{

    public function __construct($typology_id);

    public function getUrl();

    public function updateRaiting($comment);

    public function getItem();

    public function getName();

    public function getTypeName();

    public function showSubscribeForm();

    public function allowSubscribe();

    public function allowRate();

    public function canComment($user);

    public function getMsg();
}

abstract class CommentsProcessorStrategy_Abstract implements CommentsProcessorStrategy_Interface
{

    protected $typology_id;
    protected $typology;
    protected $msg = null;

    const MSG_NOT_AUTHENTIFICATED = '��� ���������� <a href="#" rel="310" class="side-login-open" onclick="$(\'.side-menu-sign\').click()">����� �� ����</a> ��� <a href="#" rel="310" class="side-registration-open" onclick="$(\'.side-menu-sign\').click()">������������������</a>, ����� ��������� �����������';
    const MSG_IGNORED = '������������ ������� ������������ ����������� �� ���';
    const MSG_HAS_NOT_APPROVED_ARTICLE = '� ��� ��� �������� �������������� ������, �� ������� ������ ����������� ������ ����� ��������� ����� �� ����� ������';

    public function __construct($typology_id)
    {
        $this->typology_id = $typology_id;
        $this->typology = $this->find($typology_id);
    }

    public function applyLimit(Zend_Db_Select $select)
    {
        return $select;
    }

    public function applyOrder(Zend_Db_Select $select)
    {
        return $select->order('datetime ASC');
    }

    public function getTitle($comments)
    {
        return count($comments) ?
            sprintf('����������� � %s: %s', $this->getTypeName(), $this->getName()) :
            sprintf('������������ � %s %s ���� ���, <span style="color:red">�������� ���� ����� �������!</span>', $this->getTypeName(), $this->getName());
    }

    public function updateRaiting($comment)
    {
        ;
    }

    public function showSubscribeForm()
    {
        return false;
    }

    public function allowSubscribe()
    {
        return true;
    }

    public function allowRate()
    {
        return false;
    }

    public function getItem()
    {
        return $this->typology;
    }

    public function getName()
    {
        return $this->getItem()->name;
    }

    protected function find($typology_id)
    {
        $table = $this->createTable();
        return $table->find($typology_id)->current();
    }

    abstract protected function createTable();

    public function canComment($user)
    {
        if (is_null($user)) {
            $this->msg = self::MSG_NOT_AUTHENTIFICATED;
            return false;
        }

        $anIgnorTable = new IgnorTable;
        if ($anIgnorTable->isIgnored($this->getItem()->user_id, $user->user_id)) {
            $this->msg = self::MSG_IGNORED;
            return false;
        }

//        if ($user->isWriter() && !$user->haveAprovedArticles()) {
//            $this->msg = self::MSG_HAS_NOT_APPROVED_ARTICLE;
//            return false;
//        }

        return true;
    }

    function getMsg()
    {
        return $this->msg;
    }

}

class CommentsProcessorStrategy_Article extends CommentsProcessorStrategy_Abstract
{

    protected function createTable()
    {
        return new ArticlesTable();
    }

    public function showSubscribeForm()
    {
        return true;
    }

    public function updateRaiting($comment)
    {
        $article = $this->getItem();
        $anCommentsTable = new CommentsTable();
        $sameUserComments = $anCommentsTable->fetchAll(
            $anCommentsTable->select()
                ->where('user_id=?', $comment->user_id)
                ->where('typology=?', $comment->typology)
                ->where('typology_id=?', $comment->typology_id)
        );

        if ($sameUserComments->count() == 1
            && $article->user_id != $comment->user_id
        ) {

            $anUsersTable = new UsersTable();
            $user = $anUsersTable->find($article->user_id)->current();
            $user->rating += Config::get('rating_comment_for_article', 1);
            $user->save();
        }
    }

    public function getTypeName()
    {
        return '������';
    }

    public function getUrl()
    {
        $view = new Zend_View();
        return $view->url(array(
            $this->getItem()->alias), 'article', true);
    }

}

class CommentsProcessorStrategy_News extends CommentsProcessorStrategy_Abstract
{

    protected function createTable()
    {
        return new NewsTable();
    }

    public function getTypeName()
    {
        return '�������';
    }

    public function getUrl()
    {
        $view = new Zend_View();
        return $view->url(array(
            'controller' => 'news',
            'action' => 'item',
            'id' => $this->getItem()->news_id), 'default', true);
    }

    public function canComment($user)
    {
        if (is_null($user)) {
            $this->msg = self::MSG_NOT_AUTHENTIFICATED;
            return false;
        }

        if ($user->isWriter() && !$user->haveAprovedArticles()) {
            $this->msg = self::MSG_IGNORED;
            return false;
        }

        return true;
    }

}

class CommentsProcessorStrategy_Post extends CommentsProcessorStrategy_Abstract
{

    protected function createTable()
    {
        return new PostsTable();
    }

    public function getTypeName()
    {
        return '�������';
    }

    public function getUrl()
    {
        $view = new Zend_View();
        return $view->url($this->getItem()->toArray(), 'post', true);
    }

}

class CommentsProcessorStrategy_Qna extends CommentsProcessorStrategy_Abstract
{

    protected function createTable()
    {
        return new QnaTable();
    }

    public function getTypeName()
    {
        return '�������';
    }

    public function getUrl()
    {
        $view = new Zend_View();
        $anCatQnaTable = new CatQnATable;
        $cat = $anCatQnaTable->find($this->getItem()->cat_id)->current();
        return $view->url(array(
            'c_alias' => $cat->alias,
            'alias' => $this->getItem()->alias), 'qna', true);
    }

    public function getTitle($comments)
    {
        return count($comments) ?
            sprintf('������ � %s:', $this->getTypeName()) :
            sprintf('������� � %s ���� ���, <span style="color:red">�������� ���� ����� �������!</span>', $this->getTypeName());
    }

    public function allowSubscribe()
    {
        return false;
    }

    public function allowRate()
    {
        return true;
    }

}

class CommentsProcessorStrategy_User extends CommentsProcessorStrategy_Abstract
{

    protected function createTable()
    {
        return new UsersTable();
    }

    public function applyLimit(Zend_Db_Select $select)
    {
        return $select->limit(1000);
    }

    public function applyOrder(Zend_Db_Select $select)
    {
        return $select->order('datetime DESC');
    }

    public function allowSubscribe()
    {
        return false;
    }

    public function getTypeName()
    {
        return '������������';
    }

    public function getTitle($comments)
    {
        $view = new Zend_View();
        return sprintf('<a href="%s" class="all">�������� ��� %d</a> ����� %s', $view->url(array(
            'controller' => 'users',
            'action' => 'wall',
            'id' => $this->typology_id), 'default', true), $this->getCommentsCount(), $this->getItem()->getTypeTitle2()
        );
    }

    protected function getCommentsCount()
    {
        $anCommentsTable = new CommentsTable;

        $select = $anCommentsTable->select()
            ->from('comments', array('cnt' => new Zend_Db_Expr('COUNT(comment_id)')))
            ->where('typology_id=?', $this->typology_id)
            ->where('typology=?', CommentsTable::TYPOLOGY_USER);

        $res = $anCommentsTable->fetchRow(
            $select
        );
        return $res->cnt;
    }

    public function getName()
    {
        return $this->getItem()->name_f . ' ' . $this->getItem()->name_l;
    }

    public function getUrl()
    {
        $view = new Zend_View();
        return $view->url(array($this->getItem()->login), 'user', true);
    }

}

class CommentsProcessorStrategy_Job extends CommentsProcessorStrategy_Abstract
{

    const MSG_JOB_NOT_ALLOWED = '������ � ����� �������� �� �������� � �������';
    const MSG_JOB_ONLY_WRITERS = '������ ������ ����� �������� �� ����� �������';
    const MSG_JOB_IS_CLOSED = '������ ��������';

    protected function createTable()
    {
        return new JobTable();
    }

    public function getTypeName()
    {
        return '������';
    }

    public function getUrl()
    {
        $view = new Zend_View();
        return $view->url(array($this->getItem()->alias), 'job', true);
    }

    public function allowSubscribe()
    {
        return false;
    }

    public function allowRate()
    {
        return false;
    }

    public function canComment($user)
    {
        if (is_null($user)) {
            $this->msg = self::MSG_NOT_AUTHENTIFICATED;
            return false;
        }

        $anIgnorTable = new IgnorTable;
        if ($anIgnorTable->isIgnored($this->getItem()->user_id, $user->user_id)) {
            $this->msg = self::MSG_IGNORED;
            return false;
        }

        if (!$user->isWriter() && $this->getItem()->user_id != $user->user_id) {
            $this->msg = self::MSG_JOB_ONLY_WRITERS;
            return false;
        }

        if ($user->isWriter() && !in_array($user->getStatus(), $this->getItem()->getAllowed())) {
            $this->msg = self::MSG_JOB_NOT_ALLOWED;
            return false;
        }

        if ($this->getItem()->is_closed) {
            $this->msg = self::MSG_JOB_IS_CLOSED;
            return false;
        }

        return true;
    }

}
