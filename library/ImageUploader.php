<?


class ImageUploader
{
    const ERROR_UNKNOWN_FILE_TYPE = '����������� ��� �����';
    const ERROR_FILE_UPLOAD = '������ ��� �������� �����';
    const ERROR_FILE_SAVE = '������ ���������� �����';
    const WATERMARK_URL = '/public/img/watermark.png';

    const RESIZE_TYPE_CROP = 1;
    const RESIZE_TYPE_GIZMO = 2;
    const RESIZE_TYPE_FIT = 3;
    const RESIZE_TYPE_MATCH_WIDTH = 4;
    const RESIZE_TYPE_MATCH_HEIGHT = 5;
    const RESIZE_CENTER = 6;

    protected $root = null;
    protected $input_name = null;
    protected $file = null;
    protected $type = null;
    protected $sizes = array();
    protected $size = null;

    function __construct($root, $input_name = 'Filedata')
    {
        $this->root = $root;
        $this->input_name = $input_name;
        $this->file = $_FILES[$this->input_name];
        $this->size = $this->file['size'];
    }

    function addWatermark($url)
    {
        $wm = $this->getHandlerFromPath($this->root . self::WATERMARK_URL);
        $wm_height = imagesy($wm);
        $wm_width = imagesx($wm);

        $handle = $this->getHandlerFromPath($this->root . $url);
        $height = imagesy($handle);
        $width = imagesx($handle);

        imagecopy($handle, $wm, $width - $wm_width - 35, $height - $wm_height - 15, 0, 0, $wm_width, $wm_height);

        $filename = $this->saveToFile($handle, $this->root . $url);
        return substr($filename, strlen($this->root));
    }

    function saveToFile($handle, $filename)
    {
        $parts = explode('.', $filename);
        $ext = array_pop($parts);
        switch ($ext) {
            case 'png' :
                imagepng($handle, $filename);
                break;
            case 'jpg' :
            case 'jpeg':
            default:
                $filename = substr($filename, 0, strlen($filename) - strlen($ext)) . 'jpeg';
                imagejpeg($handle, $filename);
                break;
        }

        return $filename;
    }

    public function resize($path, $sizes)
    {
        $img = $this->getHandlerFromPath($path);
        $urls = $this->bulkResize($img, $sizes);
        return $urls;
    }

    protected function bulkResize($img, $sizes)
    {
        $urls = array();

        foreach ($sizes as $size) {
            if (isset($size['height'])) {
                $width = $size['width'];
                $height = $size['height'];
                $type = $size['type'];
            } else {
                $width = $size[0];
                $height = $size[1];
                $type = self::RESIZE_TYPE_CROP;
            }
            $img_t = $this->_resize($img, $width, $height, $type);
            $urls[] = $this->save($img_t);
        }

        return $urls;
    }

    /**
     * Upload file with resizing
     *
     * @param array
     * @reurn array $urls
     */
    function upload($sizes = array(array(105, 82), array(416, 262)), $original = false)
    {
        $file = $_FILES[$this->input_name];

        if (!is_array($file)) {
            return array();
        }
        $result = array();

        if (is_array($file['tmp_name'])) {
            foreach ($file['tmp_name'] as $k => $v) {
                if ($file['error'][$k] || !is_uploaded_file($file['tmp_name'][$k])) {
                    throw new Exception(self::ERROR_FILE_UPLOAD);
                }
                $result[] = $this->_uploadSingle($file['tmp_name'][$k], $sizes, $original);
            }
        } else {
            if ($file['error'] || !is_uploaded_file($file['tmp_name'])) {
                throw new Exception(self::ERROR_FILE_UPLOAD);
            }
            $result = $this->_uploadSingle($file['tmp_name'], $sizes, $original);
        }

        return $result;
    }

    protected function _uploadSingle($name, $sizes = array(array(105, 82), array(416, 262)), $original = false)
    {
        $img = $this->getHandlerFromPath($name);

        $height = imagesy($img);
        $width = imagesx($img);

        $this->sizes['height'] = $height;
        $this->sizes['width'] = $width;

        $urls = array();


        foreach ($sizes as $size) {
            if (isset($size['height'])) {
                $width = $size['width'];
                $height = $size['height'];
                $type = $size['type'];
            } else {
                $width = $size[0];
                $height = $size[1];
                $type = self::RESIZE_TYPE_CROP;
            }
            $img_t = $this->_resize($img, $width, $height, $type);
            $urls[] = $this->save($img_t);
        }
        if ($original) {
            $urls[] = $this->move($name);
        }

        imagedestroy($img); //clean up
        return $urls;
    }

    protected function _resize($handle, $n_width, $n_height, $type = self::RESIZE_TYPE_CROP)
    {
        $height = imagesy($handle);
        $width = imagesx($handle);

        switch ($type) {
            case self::RESIZE_TYPE_GIZMO :
                $q = ($n_width / $width < $n_height / $height) ? $n_width / $width : $n_height / $height;
                break;
            case self::RESIZE_TYPE_MATCH_WIDTH :
                $q = $n_width / $width;
                break;
            case self::RESIZE_TYPE_MATCH_HEIGHT :
                $q = $n_height / $height;
                break;
            case self::RESIZE_TYPE_CROP :
            default :
                $q = ($n_width / $width > $n_height / $height) ? $n_width / $width : $n_height / $height;
        }

        $nn_width = $width * $q;
        $nn_height = $height * $q;


        $dist_x = $dist_y = 0;
        $src_x = $src_y = 0;
        $orig_width = $width;
        $orig_height = $height;

        if ($type == self::RESIZE_TYPE_GIZMO || $type == self::RESIZE_TYPE_CROP) {
            if ($nn_width < $n_width) {
                $dist_x = floor(($n_width - $nn_width) / 2);
            }
            if ($nn_width > $n_width) {
                $dist_x = -1 * floor(($nn_width - $n_width) / 2);
            }
            if ($nn_height < $n_height) {
                $dist_y = floor(($n_height - $nn_height) / 2);
            }
            if ($nn_height > $n_height) {
                $dist_y = -1 * floor(($nn_height - $n_height) / 2);
            }

            $result_handle = imagecreatetruecolor($n_width, $n_height);
        } elseif ($type == self::RESIZE_CENTER) {
            $src_y = floor($height / 2) - floor($n_height / 2);
            $src_x = floor($height / 2) - floor($n_width / 2);
            $orig_width = $nn_width = $n_width;
            $orig_height = $nn_height = $n_height;
            $result_handle = imagecreatetruecolor($n_width, $n_height);
        } else {
            $result_handle = imagecreatetruecolor($nn_width, $nn_height);
        }
        imagefill($result_handle, 0, 0, 0xFFFFFF);
        imagecopyresampled($result_handle, $handle, $dist_x, $dist_y, $src_x, $src_y, $nn_width, $nn_height, $orig_width, $orig_height);
        return $result_handle;
    }

    protected function save($handle)
    {

        $height = imagesy($handle);
        $width = imagesx($handle);

        $folder = $this->getFolder() . '/' . $width . '_' . $height . '/';
        if (!is_dir($this->root . $folder)) {
            mkdir($this->root . $folder);
        }

        $url = $folder . $this->generateFileName() . '.jpeg';
        $path = $this->root . $url;
        if (!imagejpeg($handle, $path)) {
            throw new Exception(self::ERROR_FILE_SAVE);
        }
        imagedestroy($handle);
        return $url;
    }

    protected function move($name = null)
    {
        $folder = $this->getFolder() . '/original/';
        if (!is_dir($this->root . $folder)) {
            mkdir($this->root . $folder);
        }

        $url = $folder . $this->generateFileName() . '.' . $this->type;
        $path = $this->root . $url;
        if (!@move_uploaded_file(($name ? $name : $this->file['tmp_name']), $path)) {
            throw new Exception(self::ERROR_FILE_SAVE);
        }
        return $url;
    }

    protected function getFolder()
    {
        $folder = '/uploads/' . date("Y-m");
        if (!is_dir($this->root . $folder)) {
            mkdir($this->root . $folder);
        }
        return $folder;
    }

    protected function generateFileName()
    {
        $symbols = "abcdefghijklmnopqrstuvwxyz";
        $symbols .= "0123456789";
        $max = strlen($symbols);
        $name = '';
        for ($i = 0; $i < 9; $i++) {
            $name .= $symbols[rand(0, $max - 1)];
        }
        $name = md5($name . microtime());
        return $name;
    }

    protected function getHandlerFromPath($path)
    {
        $functions = array(
            'jpeg' => 'imagecreatefromjpeg',
            'png' => 'imagecreatefrompng',
            'gif' => 'imagecreatefromgif'
        );

        $handle = false;
        foreach ($functions as $type => $fn) {
            $handle = @call_user_func($fn, $path);

            if ($handle) {
                $this->type = $type;
                break;
            }
        }

        if (!$handle) {
            throw new Exception(self::ERROR_UNKNOWN_FILE_TYPE);
        }

        return $handle;
    }

    function getSizes()
    {
        return $this->sizes;
    }

    function getSize()
    {
        return $this->size;
    }

    function getType()
    {
        return $this->type;
    }

}