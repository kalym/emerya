<?php

class YMap {
    const SERVICE_URL = 'http://geocode-maps.yandex.ru/1.x/';
    const SESSION_CACH_NAME = '_yandex_geo';
    private static $encoder = null;
    private static $_instance = null;

    /**
     *
     * @return YMap
     */
    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    private function getServiceKey() {
        return Zend_Registry::get('config')->yandex_api_key;
    }

    public function getCoordinate($addr) {
        if (!isset($_SESSION[self::SESSION_CACH_NAME][base64_encode($addr)])) {
            $vars = array(
                'geocode' => $addr,
                'format' => 'json',
                'key' => $this->getServiceKey()
            );
            $resp = file_get_contents(self::SERVICE_URL . '?' . http_build_query($vars));
            $res = $this->decode($resp);

            $_SESSION[self::SESSION_CACH_NAME][base64_encode($addr)] = explode(' ', $res->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
            
//            if ($res->Status->code == 200) {
//                $_SESSION[self::SESSION_CACH_NAME][base64_encode($addr)] = array(
//                    $res->Placemark[0]->Point->coordinates[1], $res->Placemark[0]->Point->coordinates[0]
//                );
//            } else {
//                $_SESSION[self::SESSION_CACH_NAME][base64_encode($addr)] = false;
//            }
        }

        return $_SESSION[self::SESSION_CACH_NAME][base64_encode($addr)];
    }

    public static function getAddrLine($user) {

        $addr = $user->country . ' ' . $user->city;

        return iconv('windows-1251', 'utf-8', $addr);
    }
    
    protected function encode($vars) {
        return $this->getEncoder()->encode($vars);
    }
    
    protected function decode($str) {
        return $this->getEncoder()->decode($str, Zend_Json::TYPE_OBJECT);
    }

    private function getEncoder() {
        if (is_null(self::$encoder)) {
            self::$encoder = new Zend_Json();
        }
        return self::$encoder;
    }

}