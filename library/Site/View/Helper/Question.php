<?php

class Site_View_Helper_Question extends Zend_View_Helper_Abstract
{
    public function question($active_cat_id = null)
    {
        $anCatsTable = new CatQnATable();

        $cats = $anCatsTable->fetchAll(
            $anCatsTable->select()->order('name ASC')
        );
        
        if (Zend_Controller_Front::getInstance()->getRequest()->getParam('cat')) {
            $active_cat_id = Zend_Controller_Front::getInstance()->getRequest()->getParam('cat')->cat_id;
        }

        return $this->view->partial('widgets/question.phtml',
            array('cats' => $cats, 'selected' => $active_cat_id)
        );
    }
}