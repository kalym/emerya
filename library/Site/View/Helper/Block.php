<?php
class Site_View_Helper_Block extends Zend_View_Helper_Abstract
{
    public function block($id)
    {
        $settings = Zend_Registry::get('settings');
        if ($settings[$id]) {
            return sprintf('<div class="block2">%s</div>', $settings[$id]); 
        }
    }

}