<?php

class Site_View_Helper_TruncateString extends Zend_View_Helper_Abstract
{
    function truncateString($string, $length)
    {
        $string = strip_tags($string);

        $lengthOld = strlen($string);
        $string = substr($string, 0, $length);
        $string = rtrim($string, "!,.-");
        $str = ($lengthOld > $length) ? "�" : ' ';

        return $string . $str;
    }
}
