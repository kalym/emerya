<?php

class Site_View_Helper_PopularArticle extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function popularArticle()
    {
        $anPopularView = new PopularView();
        $articles = $anPopularView->fetchAll(
            $select = $anPopularView->select()
                ->order('next_sunday DESC')
                ->limit(1)
        );

        $article = $articles->current();

        return $article ? $this->view->partial('popular.inc.phtml',
            array('item'=>$article,
                'title' => '���������� �� ������')
        ) : '';

    }

}

