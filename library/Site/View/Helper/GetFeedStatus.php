<?php

class Site_View_Helper_GetFeedStatus extends Zend_View_Helper_Abstract
{
    public function getFeedStatus($type)
    {
        $status = '';
        switch ($type) {
            case FeedTable::TYPE_COMMENT_ARTICLE :
                $status = '<span class="news-item-status-new-article">����������� � ����� ������</span>';
                break;
            case FeedTable::TYPE_COMMENT_POST :
                $status = '<span class="news-item-status-new-article">����������� �� ��� ���� � �����</span>';
                break;
            case FeedTable::TYPE_COMMENT_QNA :
                $status = '<span class="news-item-status-new-answer">����� ����� �� ��� ������ � ������� ������� � ������</span>';
                break;
            case FeedTable::TYPE_POST :
                $status = '<span class="news-item-status-new-article">����� ���� � �����</span>';
                break;
            case FeedTable::TYPE_QNA :
                $status = '<span class="news-item-status-new-question">����� ������ � ������� ������� � ������</span>';
                break;
            case FeedTable::TYPE_JOB :
                $status = '<span class="news-item-status-new-article">����� ����������� � ������� ����� �������</span>';
                break;
            case FeedTable::TYPE_PURCHASE :
                $status = '<span class="news-item-status-new-article">������� ����� ������</span>';
                break;
            case FeedTable::TYPE_ADMIN :
                $status = '<span class="news-item-status-new-article">��������� �� ��������������</span>';
                break;
            case FeedTable::TYPE_ARTICLE:
                $status = '<span class="news-item-status-new-article">����� ������ �� ������, �������� �� ������������</span>';
                break;
            case FeedTable::TYPE_PAYOUT:
                $status = '<span class="news-item-status-paid">����������� ������� �������</span>';
                break;
            case FeedTable::TYPE_PHOTO:
                $status = '<span class="news-item-status-new-article">����� ���������� � ������� ������, �������� �� ������������</span>';
                break;
        }
        
        return $status;
    }

}