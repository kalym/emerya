<?

Zend_Loader::loadClass('Zend_View_Helper_Abstract');

class Site_View_Helper_Iif extends Zend_View_Helper_Abstract
{
    public function iif($statement, $then, $else='')
    {
        if ($statement) {
            return $then;
        } else {
            return $else;
        }
    }
}
