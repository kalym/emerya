<?php

class Site_View_Helper_ExpandLinks extends Zend_View_Helper_Abstract
{
    public function expandLinks($content, $links)
    {
        if (empty($links)) return $content;

        $anArticleTable = new ArticlesTable;
        $articles = $anArticleTable->fetchAll($anArticleTable->select()->where('article_id IN (?)',
            array_values($links)));
        $aliases = array();
        foreach ($articles as $article) {
            $aliases[$article->article_id] = $article->alias;
        }

        foreach ($links as $word => $article_id) {
            //we want to replace only first instance in content
            $content = explode($word, $content, 2);

            $content = implode(sprintf('<a href="%s">%s</a>',
                $this->view->url(array($aliases[$article_id]), 'article', true),
                $this->view->escape($word)), $content);
        }

        return $content;
    }
}
