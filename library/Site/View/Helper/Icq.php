<?

Zend_Loader::loadClass('Zend_View_Helper_Abstract');

class Site_View_Helper_Icq extends Zend_View_Helper_Abstract
{
    public function icq($email)
    {
        return $email ? sprintf('<a href="http://www.icq.com/whitepages/cmd.php?uin=%s&action=message">%s</a>', $email, $email) : '&ndash;';
    }

}