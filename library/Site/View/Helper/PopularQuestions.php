<?php

class Site_View_Helper_PopularQuestions extends Zend_View_Helper_Abstract
{
    public function popularQuestions($cat = null)
    {
        $anQnaTable = new QnaView;

        $select = $anQnaTable->select()
            ->from('qna_view as q')
            ->setIntegrityCheck(false)
            ->joinLeft('comments as c', 'q.qna_id=c.typology_id AND c.typology=\'qna\'', array(
                'cnt' => new Zend_Db_Expr('COUNT(c.comment_id)'),
            ))
            ->group(array('c.typology_id'));

        if ($cat) {
            $select = $select->where('q.cat_id=?', $cat);
        }

        $select = $select
            ->where('q.is_approved=1')
            ->order('cnt DESC')
            ->limit(24);

        $questions = $anQnaTable->fetchAll(
            $select
        );

        return $this->view->partial('widgets/popularQuestions.phtml',
            array('item' => $questions)
        );
    }
}