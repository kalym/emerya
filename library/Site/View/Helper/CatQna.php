<?php

class Site_View_Helper_CatQna extends Zend_View_Helper_Abstract
{
    public function catQna($active_cat_id = null)
    {

        $anCatsTable = new CatQnATable();

        $cats = $anCatsTable->fetchAll(
            $anCatsTable->select()->order('name ASC')
        );

        return $this->view->partial('widgets/cat-qna.phtml',
            array('cats' => $cats, 'selected'=>$active_cat_id)
        );
    }

}
