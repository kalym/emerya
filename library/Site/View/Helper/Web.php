<?

Zend_Loader::loadClass('Zend_View_Helper_Abstract');

class Site_View_Helper_Web extends Zend_View_Helper_Abstract
{
    public function web($www)
    {
        $www = preg_replace('|^http://|', '', $www);
        return $www ? sprintf('<a href="http://%s">%s</a>', $www, $www) : '&ndash;';
    }

}