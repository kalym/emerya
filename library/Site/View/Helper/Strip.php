<?

Zend_Loader::loadClass('Zend_View_Helper_Abstract');

class Site_View_Helper_Strip extends Zend_View_Helper_Abstract
{
    public function strip($str, $length=120, $max_line = -1)
    {
        $f = false;
        
        $str = strip_tags($str);
        if ($max_line > 0) {
           $str = explode("\n", $str);
           $f = count($str) > $max_line; 
           $str = implode("\n", array_slice($str, 0, $max_line));
        }
        
        if (strlen($str)>$length) {
            $newStr = wordwrap($str, $length, "|***|");
            list($str) = explode("|***|", $newStr);
            $str = htmlspecialchars($str) . '&hellip;';
        } elseif ($f) {
            $str .= '&hellip;';
        }
                    
        return $str;
    }
}