<?php

class Site_View_Helper_FirstLetter extends Zend_View_Helper_Abstract
{
    function firstLetter($string)
    {
        $string = trim(strip_tags($string));
        $string = preg_replace('/[^\w+]/', '', $string);
        $firstLetter = $string[0];
        return $firstLetter;
    }
}
