<?php

class Site_View_Helper_ReadersChoice extends Zend_View_Helper_Abstract
{
    public function readersChoice()
    {
        $anNotepadTable = new NotepadTable;

        $articles = $anNotepadTable->fetchAll(
            $anNotepadTable->select()
                ->from('notepad')
                ->joinLeft('articles_view', 'articles_view.article_id=notepad.article_id')
                ->setIntegrityCheck(false)
                ->order('notepad_id DESC')
                ->group(array('notepad.article_id'))
                ->limit(40)
        );

        return $this->view->partial('widgets/readers-choice.phtml',
            array('articles' => $articles)
        );
    }
}
