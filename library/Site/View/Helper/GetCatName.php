<?php
class Site_View_Helper_GetCatName extends Zend_View_Helper_Abstract
{
    public function getCatName($cat_id)
    {
        if ($cat_id > 0) {
            $anCatsTable = new CatsTable();

            $currentCat = $anCatsTable->fetchRow(
                $anCatsTable->select()->where('cat_id=?', $cat_id)
            );

            return $this->view->getFirst($currentCat->name, '�������� �������');
        }

        return '�������� �������';
    }
}