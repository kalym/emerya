<?php

class Site_View_Helper_GetArticlePlaceholder extends Zend_View_Helper_Abstract
{
    public function getArticlePlaceholder($articlesCount)
    {
        $cycleNum = 0;
        if ($articlesCount < 10) {
            $cycleNum = 5;
        }


        return $this->view->partial('article-placeholder.inc.phtml', array(
            'cycleNum' => $cycleNum
        ));
    }
}