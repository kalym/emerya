<?php

class Site_View_Helper_WidgetBestVideo extends Zend_View_Helper_Abstract {

    public function widgetBestVideo($widget=false) {
//        $frontendOptions = array(
//            'lifetime' => 3600 * 24 * 1, // cache lifetime of 1 days
//                //'automatic_serialization' => true
//        );
//
//        $backendOptions = array(
//            'cache_dir' => Zend_Registry::getInstance()->get('root_dir') . '/tmp/' // Directory where to put the cache files
//        );
//
//        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
//        if (!($content = $cache->load('widget_widgetBestArticles_' . $cat->cat_id))) {
        $anPostsView = new PostsView();
        $select = $anPostsView->select()
                ->where('is_approved=1')
                ->where('is_video=1')
                ->where('clicks<?', 10000)
                ->order('clicks DESC')
                ->limit(16);

        $items = array();
        foreach ($anPostsView->fetchAll($select) as $item) {
            $items[] = array(
                'preview' => $item->preview ? $item->preview : '/public/images/video-default.gif',
                'name' => $item->name,
                'url' => $this->view->url($item->toArray(), 'post', true)
            );
        }

        $content = $this->view->partial('widgets/widget-best.phtml', array(
            'overlay' => '/public/images/pvideo.png',
            'widget' => $widget,
            'domain' => 'http://' . Zend_Registry::get('config')->site->domain,
            'params' => http_build_query(array(
                'type' => 'video',
            )),
            'items' => $items,
            'title' => '���������� �����',
            'name' => 'TopAuthor.ru | ���������� �����'
                )
        );
//            $cache->save($content, 'widget_widgetBestArticles_'. $cat->cat_id);
//        }
        return $content;
    }

}
