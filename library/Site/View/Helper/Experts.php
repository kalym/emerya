<?php

class Site_View_Helper_Experts extends Zend_View_Helper_Abstract {

    public function experts() {
        $frontendOptions = array(
            'lifetime' => 3600 * 24 * 2, // cache lifetime of 2 days
                //'automatic_serialization' => true
        );

        $backendOptions = array(
            'cache_dir' => Zend_Registry::getInstance()->get('root_dir') . '/tmp/' // Directory where to put the cache files
        );

        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
        if (!($content = $cache->load('widget_experts'))) {
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());

            $select = $select
                            ->from('users')
                            ->columns(array('users.*', 'cnt' => new Zend_Db_Expr('COUNT(comment_id)')))
                            ->joinLeft('comments', "users.user_id=comments.user_id AND comments.typology = 'qna'")
                            ->where('is_locked=0')
                            ->group('users.user_id')->order('cnt DESC')->limit(30);

            $items = $stats['discussion'] = new Zend_Db_Table_Rowset(array('data' => $select->query()->fetchAll()));

            $content = $this->view->partial('widgets/experts.phtml', array('items' => $items)
            );
            $cache->save($content, 'widget_experts');
        }
        return $content;
    }

}
