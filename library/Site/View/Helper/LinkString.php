<?php

class Site_View_Helper_LinkString extends Zend_View_Helper_Abstract
{
    function linkString($s)
    {
        return preg_replace('/https?:\/\/[\w\-\.!~#?&=+\*\'"(),\/]+/', '<a href="$0" target="_blank">$0</a>', $s);
    }
}