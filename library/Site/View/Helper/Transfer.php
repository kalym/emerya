<?php

class Site_View_Helper_Transfer extends Zend_View_Helper_Abstract
{
    public function transfer($content)
    {
        return $this->removeAnyTags($this->replace_br($content));
    }

    public function replace_br($data)
    {
        return str_replace("\n", "</p>\n<p>", '<div>' . $data . '</div>');
    }

    public function removeAnyTags($str, $repto = NULL)
    {
        $pattern = "/<p[^>]*><\\/p[^>]*>/";

        return preg_replace($pattern, '', $str);
    }
}