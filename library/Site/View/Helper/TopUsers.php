<?php

class Site_View_Helper_TopUsers extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function topUsers()
    {
        $anUsersTable = new UsersTable();
        $users = $anUsersTable->fetchAll(
                $anUsersTable->select()
                    ->where('is_locked=0')
                    ->order('rating DESC')
                    ->where('type=?', User::TYPE_WRITER)
                    ->limit(10)
                );

        return $this->view->partial('widgets/topUsers.phtml',
            array('users'=>$users)
        );

    }

}
