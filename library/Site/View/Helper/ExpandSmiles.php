<?php

class Site_View_Helper_ExpandSmiles extends Zend_View_Helper_Abstract
{
    public function expandSmiles($content, $replace)
    {
        return $replace ?
            preg_replace('/\((0[0-9]{2})\)/i', '<img src="/public/images/smile/$1.bmp" />', $content) :
            $content;
    }

}
