<?

class Site_View_Helper_HasAccess extends Zend_View_Helper_Abstract
{
    public function hasAccess($recourceId)
    {
        return Site_Auth_Admin::getInstance()->getIdentity()->hasAccess($recourceId);
    }
}
