<?php

class Site_View_Helper_News extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function news()
    {
        $anNewsTable = new NewsTable();
        $news = $anNewsTable->fetchAll(
            $anNewsTable->select()->order('date DESC')->limit(Site_Settings::get('home_news_count', 4))
        );

        return $this->view->partial('widgets/news.phtml',
            array('news'=>$news)
        );

    }

}
