<?php

class Site_View_Helper_ActiveUsers extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function activeUsers()
    {
        $anUsersTable = new UsersTable();

        $users = $anUsersTable->fetchAll(
            $anUsersTable->select()
                ->where('type=?', User::TYPE_WRITER)
                ->order('last_activity DESC')
                ->limit(Site_Settings::get('sidebar_active_users_count', 3))
        );

        return $this->view->partial('widgets/activeUsers.phtml',
            array('items'=>$users)
        );

    }

}
