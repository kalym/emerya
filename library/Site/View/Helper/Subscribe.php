<?php

class Site_View_Helper_Subscribe extends Zend_View_Helper_Abstract
{
    public function subscribe()
    {
        return $this->view->partial('widgets/subscribe.phtml');
    }
}