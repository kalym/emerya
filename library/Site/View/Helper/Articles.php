<?php
class Site_View_Helper_Articles extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function articles()
    {
        $anArticlesTable = new ArticlesTable();
        $articles = $anArticlesTable->fetchAll(
            $anArticlesTable->select()->order('date DESC')->limit(Site_Settings::get('home_articles_count', 8))
        );

        return $this->view->partial('widgets/articles.phtml',
            array('articles'=>$articles)
        );

    }

}