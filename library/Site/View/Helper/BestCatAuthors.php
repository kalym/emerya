<?php

class Site_View_Helper_BestCatAuthors extends Zend_View_Helper_Abstract {

    public function bestCatAuthors($cat) {
        $frontendOptions = array(
            'lifetime' => 3600 * 24 * 2, // cache lifetime of 2 days
                //'automatic_serialization' => true
        );

        $backendOptions = array(
            'cache_dir' => Zend_Registry::getInstance()->get('root_dir') . '/tmp/' // Directory where to put the cache files
        );

        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
//        if (!($content = $cache->load('widget_bestCatAuthors_' . $cat->cat_id))) {
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());

            $select = $select
                            ->from('users')
                            ->columns(array('users.*', 'cnt' => new Zend_Db_Expr('COUNT(article_id)')))
                            ->joinLeft('articles', "users.user_id=articles.user_id")
                            ->where('is_locked=0')
                            ->where('cat_id=?', $cat->cat_id)
                            ->group('users.user_id')->order('cnt DESC')->limit(40);

        var_dump($select); die;

            $items = new Zend_Db_Table_Rowset(array('data' => $select->query()->fetchAll()));

            $content = $this->view->partial('widgets/best-authors.phtml', array(
                'items' => $items,
                'title' => '������ ������ �������: ' . $cat->name
                    )
            );
            $cache->save($content, 'widget_bestCatAuthors_'. $cat->cat_id);
//        }
        return $content;
    }

}
