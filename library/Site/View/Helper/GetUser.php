<?php

class Site_View_Helper_GetUser extends Zend_View_Helper_Abstract
{
    public function getUser($user_id)
    {
        $anUsersTable = new UsersTable;
        return $anUsersTable->find($user_id)->current();
    }
}