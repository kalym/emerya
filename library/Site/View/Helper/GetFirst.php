<?php

class Site_View_Helper_GetFirst extends Zend_View_Helper_Abstract
{
    public function getFirst()
    {
        foreach( func_get_args() as $arg ) {
            if ($arg) return $arg;
        }
    }
}