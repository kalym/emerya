<?php

class Site_View_Helper_BestBlogAuthors extends Zend_View_Helper_Abstract
{
   
    public function bestBlogAuthors()
    {
        $anUserTable = new UsersTable;
        
        $items = $anUserTable->fetchAll($anUserTable->select()
                ->where('is_locked=0')
                ->order('posts_count DESC')
                ->limit(7));
        
        return $this->view->partial('widgets/best-authors.phtml',
            array(
                'items'=>$items,
                'title' => '������ ������ ������'
           )
        );

    }

}
