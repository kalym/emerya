<?php
class Site_View_Helper_Teaser extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function teaser()
    {
        return $this->view->partial('widgets/teaser.phtml',
            array()
        );

    }

}