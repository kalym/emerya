<?php

class Site_View_Helper_Cat extends Zend_View_Helper_Abstract
{
    public function cat($active_cat_id = null)
    {
        $anCatsTable = new CatsTable();

        $cats = $anCatsTable->fetchAll(
            $anCatsTable->select()->order('title ASC')
        );

        if (Zend_Controller_Front::getInstance()->getRequest()->getParam('cat')) {
            $active_cat_id = Zend_Controller_Front::getInstance()->getRequest()->getParam('cat')->cat_id;
        }

        return $this->view->partial('widgets/cat.phtml',
            array('cats' => $cats, 'selected' => $active_cat_id)
        );
    }
}