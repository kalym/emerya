<?

Zend_Loader::loadClass('Zend_View_Helper_Abstract');

class Site_View_Helper_Skype extends Zend_View_Helper_Abstract
{
    public function skype($email)
    {
        return $email ? sprintf('<a href="skype:%s">%s</a>', $email, $email) : '&ndash;';
    }

}