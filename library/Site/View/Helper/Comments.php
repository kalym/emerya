<?php

class Site_View_Helper_Comments extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function comments(CommentsProcessor $commentsProcessor)
    {
        $comments = $commentsProcessor->getComments();
        return $this->view->partial('widgets/comments.phtml',
            array(
                'item' => $commentsProcessor->getItem(),
                'images' => $commentsProcessor->getImages(),
                'typology' => $commentsProcessor->typology,
                'typology_id' => $commentsProcessor->typology_id,
                'typologyTypeName' => $commentsProcessor->getCommentTypologyTypeName(),
                'typologyName' => $commentsProcessor->getCommentTypologyName(),
                'comments' => $comments,
                'formSubscribe' => $commentsProcessor->getFormSubscribe(),
                'formComment' => $commentsProcessor->getFormComment(),
                'showSubscribeForm' => $commentsProcessor->showSubscribeForm(),
                'allowSubscribe' => $commentsProcessor->allowSubscribe(),
                'allowRate' => $commentsProcessor->allowRate(),
                'title' => $commentsProcessor->getTitle(),
                'canComment' => $commentsProcessor->canComment(Zend_Auth::getInstance()->getIdentity()),
                'msg' => $commentsProcessor->getMsg(),
                'moderate' => $commentsProcessor->isModerate()
            )
        );
    }

}
