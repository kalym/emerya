<?php

class Site_View_Helper_Premium extends Zend_View_Helper_Abstract
{
    public function premium()
    {
        $anUsersTable = new UsersTable;

//        $items = $anUsersTable->fetchAll(
//                $anUsersTable->select()->where('premium_since IS NOT NULL')
//                ->order('premium_since DESC')->where('is_locked=0')->limit(14));

        //LAST ACTIVE USERS
        $items = $anUsersTable->fetchAll(
            $anUsersTable->select()
                ->where('type=?', User::TYPE_WRITER)
                ->order('last_activity DESC')
                ->limit(Site_Settings::get('sidebar_active_users_count', 14))
        );


        return $this->view->partial('widgets/premium.phtml',
            array('items' => $items)
        );
    }
}
