<?php
class Site_View_Helper_Warning extends Zend_View_Helper_Abstract
{
    public function warning()
    {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            
            $message ='';
            
            do {
                if ($user->isWriter() && !$user->wmr) {
                    $message = '���������� ��������� ��������� ���������, ��� ��� �� �� ������ ���������� ���� ����������';
                    break;
                }
                
                if (!$user->country || !$user->city) {
                    $message = '���������� ������� � ���������� ������� ������ � ����� ������ ����������';
                    break;
                }
                
            } while (false);
            
            if ($message) {
                return $this->view->partial('widgets/warning.phtml',
                    array('message'=>$message)
                );
            }
        }

    }
}
