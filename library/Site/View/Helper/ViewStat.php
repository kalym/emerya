<?php

class Site_View_Helper_ViewStat extends Zend_View_Helper_Abstract
{
    public function viewStat($item)
    {
        
        $anArticleViewTable = new ArticleViewTable;
        $select = $anArticleViewTable->select()->from('article_view', array(
                'dat' => new Zend_Db_Expr('DATE(dattm)'),
                'view'=> new Zend_Db_Expr('COUNT(*)'),
                'unique'=> new Zend_Db_Expr('COUNT(IF(is_unique, view_id, NULL))'),
                'RU'=> new Zend_Db_Expr("COUNT(IF(is_unique AND country='RU', view_id, NULL))"),
                'UA'=> new Zend_Db_Expr("COUNT(IF(is_unique AND country='UA', view_id, NULL))"),
                'BY'=> new Zend_Db_Expr("COUNT(IF(is_unique AND country='BY', view_id, NULL))")
        ))->group(new Zend_Db_Expr('DATE(dattm)'))
            ->where('article_id=?', $item->article_id);
        
        $statRow = $anArticleViewTable->fetchAll($select);
        
        $view = array();
        foreach ($statRow  as $row) {
           $view[$row->dat]['view'] = $row->view; 
           $view[$row->dat]['unique'] = $row->unique;
           $view[$row->dat]['RU'] = $row->RU;
           $view[$row->dat]['UA'] = $row->UA;
           $view[$row->dat]['BY'] = $row->BY;
           
        }
        
        $dat = array();
        $tm = strtotime($item->stat_begin);
        do {
            $d = date('Y-m-d', $tm);
            $dat[] = $d;
            $tm += 3600 * 24;
        } while ($d < date('Y-m-d', strtotime($item->stat_expire)));

        $stat = array();
        foreach ($dat as $d)
           $stat[$d] = array (
               'view' => isset($view[$d]['view']) ? $view[$d]['view'] : 0,
               'unique' => isset($view[$d]['unique']) ? $view[$d]['unique'] : 0,
               'RU' => isset($view[$d]['RU']) ? $view[$d]['RU'] : 0,
               'UA' => isset($view[$d]['UA']) ? $view[$d]['UA'] : 0,
               'BY' => isset($view[$d]['BY']) ? $view[$d]['BY'] : 0,
           );
        

        return $this->view->partial('widgets/viewStat.phtml',
            array('stat'=>$stat, 'item' => $item)
        );

    }

}
