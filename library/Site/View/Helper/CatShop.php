<?php

class Site_View_Helper_CatShop extends Zend_View_Helper_Abstract
{
    public function catShop($active_cat_id = null)
    {

        $anCatsTable = new ShopCatTable();

        $cats = $anCatsTable->fetchAll(
            $anCatsTable->select()->order('name ASC')
        );

        return $this->view->partial('widgets/cat-shop.phtml',
            array('cats' => $cats, 'selected'=>$active_cat_id)
        );
    }

}
