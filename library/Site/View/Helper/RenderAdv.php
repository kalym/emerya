<?php
class Site_View_Helper_RenderAdv extends Zend_View_Helper_Abstract
{
    public function renderAdv()
    {
        $anAdvTable = new AdvTable();
        $adv = $anAdvTable->fetchRow($anAdvTable->select()->where('is_disabled=0')->order('RAND() DESC'));
        if ($adv) {
            $anAdvViewTable = new AdvViewTable();
            $anAdvViewTable->log($adv);
            return $adv->render() . '<div class="dot-line"></div>
<br />';
        }
    }

}