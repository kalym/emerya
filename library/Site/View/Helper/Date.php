<?php

class Site_View_Helper_Date extends Zend_View_Helper_Abstract
{
    public function date($time = null, $format = null)
    {
        $time = is_null($time) ? time() : strtotime($time);
        $format = is_null($format) ? sprintf('%s.%s.%s',
                Zend_Date::DAY,
                Zend_Date::MONTH,
                Zend_Date::YEAR) : $format;

        $date = new Zend_Date($time, Zend_Date::TIMESTAMP); 
        return iconv("UTF-8", "windows-1251", $date->get($format, 'ru_RU'));
    }
}