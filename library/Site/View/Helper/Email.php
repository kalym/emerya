<?

Zend_Loader::loadClass('Zend_View_Helper_Abstract');

class Site_View_Helper_Email extends Zend_View_Helper_Abstract
{
    public function email($email)
    {
        return $email ? sprintf('<a href="mailto:%s">%s</a>', $email, $email) : '&ndash;';
    }

}