<?php
class Site_View_Helper_NotifyBlog extends Zend_View_Helper_Abstract
{
    public function notifyBlog()
    {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            
            $user_id = (int)$user->user_id;
             $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
             $select = $select->from(array('p'=>'posts'), array(
                         'cnt' => new Zend_Db_Expr("SUM(IF(c.user_id=$user_id, 1, 0))"),
                         'p.post_id'
                     ))->joinLeft(array('c'=>'comments'), 
                     "p.post_id=c.typology_id AND c.typology='post'", array())
                             ->where('p.user_id<>?', $user->user_id)
                             ->where('p.is_approved=1')
                             ->group('p.post_id')
                             ->having('cnt=0')
                     ->order('p.date DESC')->limit(1);
             $res = $select->query()->fetchObject();
             if (!$res) return;
             
             $anPostTable = new PostsTable;
             $item = $anPostTable->find($res->post_id)->current();

            return $this->view->partial('widgets/notifyBlog.phtml',
                array('item'=>$item)
            );
        }

    }
}
