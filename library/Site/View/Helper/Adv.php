<?php
class Site_View_Helper_Adv extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function adv()
    {
        return $this->view->partial('widgets/adv.phtml',
            array()
        );

    }

}