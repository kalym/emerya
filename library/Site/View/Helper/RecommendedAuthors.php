<?php

class Site_View_Helper_RecommendedAuthors extends Zend_View_Helper_Abstract
{

    public function recommendedAuthors($userId = null)
    {
        if (!$userId) {
            return '';
        }

        $anRecommend = new RecommendTable();
        $recommend = $anRecommend->fetchAll(
            $anRecommend->select()
                ->where('whom_id=?', $userId)
                ->where('who_id=?', $userId)
        );

        foreach ($recommend as $r) {
            $r->delete();
        }

        $anRecommendWhomView = new RecommendWhoView();
        $items = $anRecommendWhomView->fetchAll(
            $anRecommendWhomView->select()
                ->where('whom_id=?', $userId)
                ->where('name_f IS NOT NULL')
                ->order('recommend_id DESC')
        );

        $content = $this->view->partial('widgets/recommened-authors.phtml', array(
                'items' => $items,
                'title' => ''
            )
        );
        return $content;
    }

}
