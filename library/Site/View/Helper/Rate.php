<?php

class Site_View_Helper_Rate extends Zend_View_Helper_Abstract
{
    public function rate($comment)
    {
        $anRateTable = new RateTable();
        return $this->view->partial('widgets/rate.phtml', array(
                'yes' => $anRateTable->getRateCnt($comment->comment_id, 1),
                'active' => Zend_Auth::getInstance()->hasIdentity() && $anRateTable->getRate($comment->comment_id, Zend_Auth::getInstance()->getIdentity()->user_id),
                'comment_id' => $comment->comment_id,
                'item' => $comment));
    }
}
