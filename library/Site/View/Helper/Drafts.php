<?php

class Site_View_Helper_Drafts extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function drafts()
    {
        $anArticlesView = new ArticlesView();
        $articles = $anArticlesView->fetchAll(
            $anArticlesView->select()
                ->where('is_approved=?', 0)
                ->order(array('date DESC', 'article_id DESC'))
                ->limit(Site_Settings::get('sidebar_drafts_count', 5))
        );

        return $this->view->partial('widgets/drafts.phtml',
            array('articles'=>$articles)
        );

    }

}
