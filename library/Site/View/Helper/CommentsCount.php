<?php
class Site_View_Helper_CommentsCount extends Zend_View_Helper_Abstract
{
    public function commentsCount($typology_id, $typology = CommentsTable::TYPOLOGY_ARTICLE)
    {
       $anCommentsTable = new CommentsTable();
       $comments = $anCommentsTable->fetchAll(
               $anCommentsTable->select()
               ->where('typology_id=?', $typology_id)
               ->where('typology=?', $typology)
               ->where('is_approved=1'))->count();
       return $comments;
    }

}