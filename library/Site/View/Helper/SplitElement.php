<?php

class Site_View_Helper_SplitElement extends Zend_View_Helper_Abstract
{
    public function splitElement($element)
    {
        $result = array();
        $result['label'] = $element->getLabel();
        $result['messages'] = $element->getMessages();
        $result['id'] = $element->getId();
        
        $additionalAttribs = array();
        if (count($result['messages'])) {
            $additionalAttribs['class'] = 'error';
        }
        
        $sep = method_exists($element, 'getSeparator') ? $element->getSeparator() : null;

        $result['xhtml'] = $element->getView()->{$element->helper}(
            $element->getName(),
            $element->getValue(),
            array_diff_key($element->getAttribs(), array('helper'=>1)) +
            $additionalAttribs,
            $element->options,
            $sep 
        );

        

        return $result;

    }
}
