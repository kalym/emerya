<?php

class Site_View_Helper_BestPopularAuthors extends Zend_View_Helper_Abstract
{

    public function bestPopularAuthors($cat = null)
    {
        $frontendOptions = array(
            'lifetime' => 3600 * 24 * 2, // cache lifetime of 2 days
            //'automatic_serialization' => true
        );

        $backendOptions = array(
            'cache_dir' => Zend_Registry::getInstance()->get('root_dir') . '/tmp/' // Directory where to put the cache files
        );

        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
        if (!($content = $cache->load('widget_bestPopularAuthors_' . $cat))) {
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());

            $select = $select
                ->from('users')
                ->columns(array('users.*', 'cnt' => new Zend_Db_Expr('COUNT(article_id)')))
                ->joinLeft('articles_view', "users.user_id=articles_view.user_id")
                ->where('is_locked=0');


            if ($cat) {
                $select = $select->where('cat_id=?', $cat);
            }

            $select = $select->group('users.user_id')->order('cnt DESC')->limit(40);

            $items = new Zend_Db_Table_Rowset(array('data' => $select->query()->fetchAll()));

            $content = $this->view->partial('widgets/best-authors.phtml', array(
                    'items' => $items,
                    'title' => '������ ������ ���������� ������'
                )
            );
            $cache->save($content, 'widget_bestPopularAuthors_' . $cat);
        }
        return $content;
    }

}
