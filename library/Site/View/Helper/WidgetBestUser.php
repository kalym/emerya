<?php

class Site_View_Helper_WidgetBestUser extends Zend_View_Helper_Abstract {

    public function widgetBestUser($user, $widget=false) {
//        $frontendOptions = array(
//            'lifetime' => 3600 * 24 * 1, // cache lifetime of 1 days
//                //'automatic_serialization' => true
//        );
//
//        $backendOptions = array(
//            'cache_dir' => Zend_Registry::getInstance()->get('root_dir') . '/tmp/' // Directory where to put the cache files
//        );
//
//        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
//        if (!($content = $cache->load('widget_widgetBestArticles_' . $cat->cat_id))) {
        $anArticlesView = new ArticlesView;
        $select = $anArticlesView->select()
                ->where('is_approved=1')
                ->where('user_id=?', $user->user_id)
                ->order('clicks DESC')
                ->limit(16);

        $items = array();
        foreach ($anArticlesView->fetchAll($select) as $item) {
            $items[] = array(
                'preview' => $item->widget_img,
                'name' => $item->name,
                'url' => $this->view->url(array($item->alias), 'article', true)
            );
        }

        $content = $this->view->partial('widgets/widget-best.phtml', array(
            'overlay' => '/public/images/pnone.png',
            'widget' => $widget,
            'domain' => 'http://' . Zend_Registry::get('config')->site->domain,
            'params' => http_build_query(array(
                'type' => 'user'
            )),
            'items' => $items,
            'title' => '��������� ������: ' . $user->name_f . ' ' . $user->name_l,
            'name' => 'TopAuthor.ru | ��������� ������ | ' . $user->name_f . ' ' . $user->name_l
                )
        );
//            $cache->save($content, 'widget_widgetBestArticles_'. $cat->cat_id);
//        }
        return $content;
    }

}
