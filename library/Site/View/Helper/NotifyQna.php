<?php
class Site_View_Helper_NotifyQna extends Zend_View_Helper_Abstract
{
    public function notifyQna()
    {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();

             $user_id = (int)$user->user_id;
             $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
             $select = $select->from(array('q'=>'qna'), array(
                 'cnt' => new Zend_Db_Expr("SUM(IF(c.user_id=$user_id, 1, 0))"),
                         'q.qna_id'
                     ))->joinLeft(array('c'=>'comments'), 
                     "q.qna_id=c.typology_id AND c.typology='qna'", array())
                     ->where('q.user_id<>?', $user->user_id)
                     ->where('q.is_approved=1')
                                     ->group('q.qna_id')
                             ->having('cnt=0')
                     ->order('q.datetime DESC')->limit(1);

             $res = $select->query()->fetchObject();
             if (!$res) return;
             
             $anQnaTable = new QnaView;
             $item = $anQnaTable->find($res->qna_id)->current();

            return $this->view->partial('widgets/notifyQna.phtml',
                array('item'=>$item)
            );
        }

    }
}
