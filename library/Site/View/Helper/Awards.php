<?php
class Site_View_Helper_Awards extends Zend_View_Helper_Abstract
{
    public function awards(User $user)
    {
        $items = array();
        
        if ($user->isPremium()) {
            $items[] = 'premium';
        }
        
        $code = $user->getStatus();
        if ($code != 'tra') {
            $items[] = 'mas';
        }

        if ($user->isBestBlog()) {
            $items[] = 'blog';
        }
        if ($user->isBestQna()) {
            $items[] = 'qna';
        }
        if ($user->isBestPopular()) {
            $items[] = 'popular';
        }
        if ($user->isBestCat()) {
            $items[] = 'cat';
        }
        
        $items = array_pad($items, 6, 'none');
        
        return $this->view->partial('widgets/awards.phtml',
            array(
                'items' => $items
            )
        );

    }
}