<?php

class Site_View_Helper_WidgetBestPopular extends Zend_View_Helper_Abstract {

    public function widgetBestPopular($widget=false) {
        $frontendOptions = array(
            'lifetime' => 3600 * 24 * 7, // cache lifetime of 1 week
            'automatic_serialization' => true
        );

        $backendOptions = array(
            'cache_dir' => Zend_Registry::getInstance()->get('root_dir') . '/tmp/' // Directory where to put the cache files
        );

        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
        if (!($popular_article_ids = $cache->load('popular_article_ids'))) {
            $popular_article_ids = array();
            $anPopularView = new PopularView();
            foreach($anPopularView->fetchAll($anPopularView->select()) as $item) {
                $popular_article_ids[] = $item->article_id;
            }
            $cache->save($popular_article_ids, 'popular_article_ids');
        }

        $anArticlesView = new ArticlesView;
        $select = $anArticlesView->select()->where('article_id IN (?)', $popular_article_ids);
        $select = $select->order('next_sunday DESC')->limit(16);

        $items = array();
        foreach ($anArticlesView->fetchAll($select) as $item) {
            $items[] = array(
                'preview' => $item->widget_img,
                'name' => $item->name,
                'url' => $this->view->url(array($item->alias), 'article', true)
            );
        }

        $content = $this->view->partial('widgets/widget-best.phtml', array(
            'overlay' => '/public/images/pnone.png',
            'widget' => $widget,
            'domain' => 'http://' . Zend_Registry::get('config')->site->domain,
            'params' => http_build_query(array(
                'type' => 'popular'
            )),
            'items' => $items,
            'title' => '���������� ������',
            'name' => 'TopAuthor.ru | ���������� ������'
                )
        );
//            $cache->save($content, 'widget_widgetBestArticles_'. $cat->cat_id);
//        }
        return $content;
    }

}
