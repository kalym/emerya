<?php
class Site_View_Helper_CheckSponsor extends Zend_View_Helper_Abstract
{
    public function checkSponsor($sponsor)
    {
        return strpos($sponsor->text, $sponsor->keyword) !== false;
    }

}