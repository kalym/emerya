<?php
class Site_View_Helper_NotepadCount extends Zend_View_Helper_Abstract
{
    public function notepadCount($user_id)
    {
       $anNotepadTable = new NotepadTable();
       $items = $anNotepadTable->fetchAll(
               $anNotepadTable->select()
                    ->where('user_id=?', $user_id));
       return count($items);
    }

}