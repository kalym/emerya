<?php

class Site_View_Helper_Status extends Zend_View_Helper_Abstract
{
    public function status($rate)
    {
        return self::getCode($rate);
    }

    static function getCode($rate)
    {
        $status = new Status($rate);
        return $status->getCode();
    }
}