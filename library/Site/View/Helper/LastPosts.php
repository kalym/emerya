<?php

class Site_View_Helper_LastPosts extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function lastPosts()
    {
        $anPosts = new PostsView;
        $items = $anPosts->fetchAll(
            $anPosts->select()
                ->where('is_approved=1')
                ->order('date DESC')
                ->order('post_id DESC')
                ->limit(Site_Settings::get('sidebar_posts_count', 5))
        );

        return $this->view->partial('widgets/lastPosts.phtml',
            array('items'=>$items)
        );

    }

}
