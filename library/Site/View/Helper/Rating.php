<?php
class Site_View_Helper_Rating extends Zend_View_Helper_Abstract
{
    public function rating($rate)
    {
        if ($rate >= 20) {
            $color = '#fd0505';
        } elseif ($rate>=15) {
            $color = '#d49e1d';
        } elseif ($rate>=10) {
            $color = '#9e23cb';
        } elseif ($rate>=5) {
            $color = '#18719f';
        } elseif ($rate>=3) {
            $color = '#30b770';
        } elseif ($rate>=0) {
            $color = '#60b218';
        } elseif ($rate<=-20) {
            $color = '#830101';
        } elseif ($rate<=-15) {
            $color = '#9c7109';
        } elseif ($rate<=-10) {
            $color = '#6b0d8e';
        } elseif ($rate<=-5) {
            $color = '#094869';
        } elseif ($rate<=-3) {
            $color = '#3b6e0f';
        } elseif ($rate<=0) {
            $color = '#60b218';
        }
 //style="background:%s"
        //$color,
        return sprintf( '<div class="score">%s</div>',
                 $rate);

    }

}