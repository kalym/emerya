<?php

class Site_View_Helper_Today extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function today()
    {
        $anArticlesView = new ArticlesView();
        $articles = $anArticlesView->fetchAll(
            $anArticlesView->select()
                ->where('is_approved=?', 1)
                ->order('date DESC')
                ->limit(6)
        );

        return $this->view->partial('widgets/today.phtml',
            array('items'=>$articles)
        );

    }

}
