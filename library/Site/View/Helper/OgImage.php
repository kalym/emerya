<?php

class Site_View_Helper_OGImage extends Zend_View_Helper_Abstract
{
    public function ogImage($title, $id, $ogImageType = 'article', $image = 'assets/img/stamp-holder.jpg')
    {
        $stamp = imagecreatefrompng('assets/img/stamp.png');

        $imageType = exif_imagetype($image);
        if ($imageType == IMAGETYPE_PNG) {
            $im = imagecreatefrompng($image);
        } else {
            $im = imagecreatefromjpeg($image);
        }

        $marginLeft = 135;
        $marginTop = 180;

        $sting = $title;

        $text = strtoupper($sting);
        $lines = explode('|', wordwrap($text, 25, '|'));
        $y = 330;
        $color = imagecolorallocate($im, 255, 255, 255);
        $font = 'assets/fonts/dinpro/bold/dinpro-bold.ttf';

        foreach ($lines as $key => $line) {
            if ($key >= 3) {
                imagettftext($im, 35, 0, $marginLeft, $y, $color, $font, $this->str_rus($line) . '...');
                break;
            } else {
                imagettftext($im, 35, 0, $marginLeft, $y, $color, $font, $this->str_rus($line));
            }
            $y += 55;
        }

        imagecopy($im, $stamp, $marginLeft, $marginTop, 0, 0, imagesx($stamp), imagesy($stamp));

        $filename = "uploads/og-images/";
        if (!file_exists($filename)) {
            mkdir($filename, 0777);
        }

        $name = $filename . $ogImageType . $id . '.png';
        imagepng($im, $name);
        imagedestroy($im);

        return '/' . $name;
    }

    function str_rus($str)
    {
        $nstr = ""; // итоговая строка
        for ($i = 0; $i < strlen($str); $i++) {
            $symbol = substr($str, $i, 1); // get symbol
            $ascii = ord($symbol); // get ascii code of this symbol
            if ($ascii < 128) {
                $nstr .= $symbol;
            } elseif ($ascii > 191 and $ascii < 256) {
                $nstr .= "&#" . (string)(848 + ord($symbol)) . ';';
            } else {
                $nstr .= $symbol;
            }
        } // end of for( $i ) loop

        return ($nstr);
    }
}