<?php

class Site_View_Helper_Stat extends Zend_View_Helper_Abstract
{

    public function stat()
    {
        $anUsersTable = new UsersTable();
        $userRow = $anUsersTable->fetchRow(
                $anUsersTable->select()
                        ->from('users', array('cnt' => new Zend_Db_Expr('COUNT(user_id)')))
        );

        $anArticlesTable = new ArticlesTable();
        $articleRow = $anArticlesTable->fetchRow(
                $anArticlesTable->select()
                        ->from('articles', array('cnt' => new Zend_Db_Expr('COUNT(article_id)')))
        );

        $anCommentsTable = new CommentsTable();
        $commentsRow = $anCommentsTable->fetchRow(
                $anCommentsTable->select()
                        ->from('comments', array('cnt' => new Zend_Db_Expr('COUNT(comment_id)')))
        );

        return <<<CUT
<ul class="user-statistics-list">
    <li><span>{$userRow->cnt}</span>�������</li>
    <li><span>{$articleRow->cnt}</span>����������</li>
    <li><span>{$commentsRow->cnt}</span>������������</li>
</ul>
CUT;
    }
}