<?php

class Site_View_Helper_CommentsRateCount extends Zend_View_Helper_Abstract
{
    public function commentsRateCount($typology_id, $typology = CommentsTable::TYPOLOGY_QNA)
    {
        $anCommentsTable = new CommentsTable();
        $select = $anCommentsTable->select()
            ->from('comments_view')
            ->setIntegrityCheck(false)
            ->joinInner('rate', 'comments_view.comment_id=rate.comment_id AND rate.rate=1', array(
                'sum' => new Zend_Db_Expr('SUM(rate.rate)'),
            ))
            ->where('typology_id=?', $typology_id)
            ->where('typology=?', $typology)
            ->where('is_approved=1')
            ->group(array('typology'));
        $item = $anCommentsTable->fetchRow($select);
//        var_dump($item);
//        die;
//
//        $comments = $anCommentsTable->fetchRow(
//            $select
//        );

        return ($item) ? $item->sum : 0;
    }

}