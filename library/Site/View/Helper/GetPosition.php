<?php

class Site_View_Helper_GetPosition extends Zend_View_Helper_Abstract
{
    public function getPosition($user)
    {
        $anUsersTable = new UsersTable();
        $userRow = $anUsersTable->fetchRow(
            $anUsersTable->select()
                    ->from('users', array('cnt'=> new Zend_Db_Expr('COUNT(user_id)')))
                    ->where('rating>?', $user->rating)
                    ->where('is_locked=0')
                    ->where('type=?', User::TYPE_WRITER)
          );

      
        return ++$userRow->cnt;

    }

}
