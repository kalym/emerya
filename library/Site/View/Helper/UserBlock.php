<?php
class Site_View_Helper_UserBlock extends Zend_View_Helper_Abstract
{
    public function userBlock()
    {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $anPaymentsTable = new PaymentsTable;
            $amount_internal = $anPaymentsTable->getAmountForType($user->user_id,
                    PaymentsTable::TYPE_INTERNAL);
            $amount_sponsor = $anPaymentsTable->getAmountForType($user->user_id,
                    PaymentsTable::TYPE_INTERNAL, PaymentsTable::TYPOLOGY_SPONSOR, true);
            $amount_shop = $anPaymentsTable->getAmountForType($user->user_id,
                    PaymentsTable::TYPE_INTERNAL, PaymentsTable::TYPOLOGY_SHOP_ARTICLE, true);
            $amount_magazine = $anPaymentsTable->getAmountForType($user->user_id,
                    PaymentsTable::TYPE_INTERNAL, PaymentsTable::TYPOLOGY_ARTICLE, true);
            $amount_view = $anPaymentsTable->getAmountForType($user->user_id,
                    PaymentsTable::TYPE_INTERNAL, PaymentsTable::TYPOLOGY_ARTICLE_VIEW, true);
            $amount_external = $anPaymentsTable->getAmountForType($user->user_id,
                    PaymentsTable::TYPE_EXTERNAL);
            
             $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
             $select = $select->from(array('r'=>'ratings'), array(
                         'sum'=> new Zend_Db_Expr('SUM(r.rate)')
                     ))->joinLeft(array('a'=>'articles'), 
                     "a.article_id=r.article_id", array())->where('a.user_id=?', $user->user_id);

             $rate = $select->query()->fetchColumn();

            $settings = Zend_Registry::get('settings');
            $payout_date = $settings['payout_date'];


            $payout = new Zend_Date($payout_date);
            $today = new Zend_Date();
            if ($today < $payout) {
                $payout->sub($today);
                $days = $payout->get(Zend_Date::DAY_OF_YEAR);
            } else {
                $days = 0;
            }

            $payout_date = sprintf('����� %s %s (%s)',
                    $days,
                    $this->getDayTitle($days),
                    $payout_date);
            
//             $price_ru = Site_Settings::get('ppv_price_RU');
//             $price_ua = Site_Settings::get('ppv_price_UA');
//             $price_by = Site_Settings::get('ppv_price_BY');
//        
//            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
//             $select = $select->from(array('a'=>'articles'), array())
//                     ->joinLeft(array('v'=>'article_view'), 
//                     "a.article_id=v.article_id", array(
//                          'income' => new Zend_Db_Expr("SUM(IF(v.is_unique, CASE
//                        WHEN v.country = 'RU' THEN $price_ru
//                        WHEN v.country = 'UA' THEN $price_ua
//                        WHEN v.country =  'BY' THEN $price_by
//                    END, 0))"
//                     )))
//                     ->where('a.stat_begin IS NOT NULL')
//                     ->where('a.stat_stop = 0')
//                     ->where('a.user_id=?', $user->user_id)
//                     ->group('v.article_id');
//
//             $amount_view = $select->query()->fetchColumn();

            return $this->view->partial('widgets/userBlock.phtml',
                array('user'=>$user, 
                    'amount_shop'=>$amount_shop,
                    'amount_magazine'=>$amount_magazine,
                    'amount_sponsor'=>$amount_sponsor,
                    'amount_internal'=>$amount_internal,
                    'amount_external'=>$amount_external,
                    'amount_view' => $amount_view,
                    'payout_date' => $payout_date,
                    'rate'=>$rate)
            );
        }

    }

    protected function getDayTitle($num)
    {
       return $this->view->plural($num, array('����', '���', '����'));
    }

}
