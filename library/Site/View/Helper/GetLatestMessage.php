<?php

class Site_View_Helper_GetLatestMessage extends Zend_View_Helper_Abstract
{
    public function getLatestMessage($receiver_id)
    {
        $anMessagesView = new MsgTable();
        $user_id = Zend_Auth::getInstance()->getIdentity()->user_id;

        $select = $anMessagesView->select()
            ->where('(sender_id=? AND recipient_id=\'' . $user_id . '\') OR (sender_id=\'' . $user_id . '\' AND recipient_id=?)', $receiver_id)
            ->order('datetime DESC');
        $row = $anMessagesView->fetchRow($select);
        
        if ($row) {
            return $this->view->truncateString($row->content, 40);
        }

        return '';
    }

}