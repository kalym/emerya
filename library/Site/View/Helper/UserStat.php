<?php

class Site_View_Helper_UserStat extends Zend_View_Helper_Abstract {

    public function userStat(User $user) {
        $stats = array();

        $title = ucfirst($user->getTypeTitle());
        
        $anCommentsTable = new CommentsTable;
        $comment = $anCommentsTable->fetchAll(
                        $anCommentsTable->select()->from('comments', array(
                            'cnt' => new Zend_Db_Expr('COUNT(comment_id)')
                        ))->where('user_id=?', $user->user_id)->where('typology<>?', CommentsTable::TYPOLOGY_QNA)
                )->current();

        $stats[] = array(
            'name' => $title . '-�����������',
            'num' => $comment->cnt,
            'weight' => $comment->cnt
        );

        $anPostsTable = new PostsTable;
        $post = $anPostsTable->fetchAll(
                        $anPostsTable->select()->from('posts', array(
                            'cnt' => new Zend_Db_Expr('COUNT(post_id)')
                        ))->where('user_id=?', $user->user_id)
                )->current();

        $stats[] = array(
            'name' => $title . '-������',
            'num' => $post->cnt,
            'weight' => $post->cnt
        );

        $anQnaTable = new QnaTable;
        $qna = $anQnaTable->fetchAll(
                        $anQnaTable->select()->from('qna', array(
                            'cnt' => new Zend_Db_Expr('COUNT(qna_id)')
                        ))->where('user_id=?', $user->user_id)
                )->current();

        $stats[] = array(
            'name' => $title . '-��������������',
            'num' => $qna->cnt,
            'weight' => $qna->cnt
        );

        $anCommentsTable = new CommentsTable;
        $comment = $anCommentsTable->fetchAll(
                        $anCommentsTable->select()->from('comments', array(
                            'cnt' => new Zend_Db_Expr('COUNT(comment_id)')
                        ))->where('user_id=?', $user->user_id)->where('typology=?', CommentsTable::TYPOLOGY_QNA)
                )->current();

        $stats[] = array(
            'name' => $title . '-�������',
            'num' => $comment->cnt,
            'weight' => $comment->cnt
        );

        $max = 1;
        foreach ($stats as $item) {
            if ($item['weight'] > $max)
                $max = $item['weight'];
        }

        return $this->view->partial('widgets/userStat.phtml', array(
                    'stats' => $stats,
                    'max' => $max
        ));
    }

}
