<?php

class Site_View_Helper_GetCssClass extends Zend_View_Helper_Abstract
{
    public function getCssClass($actionName)
    {
        if (Zend_Controller_Front::getInstance()->getRequest()->getActionName() == $actionName
            || Zend_Controller_Front::getInstance()->getRequest()->getControllerName() == $actionName
        ) {
            return 'current-active';
        }
        return '';
    }
}