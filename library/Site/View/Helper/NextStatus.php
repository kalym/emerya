<?php
class Site_View_Helper_NextStatus extends Zend_View_Helper_Abstract
{
    public function nextStatus($rate)
    {
        $status = new Status($rate);
        
        if ($status->getNextLevelCode()) {
            return sprintf('�� ���������� ������ (%s) ��� �������� ������� %d ������',
                    $status->getNextLevelTitle(), $status->getRemainBeforeNextLevel());
        } else {
            return sprintf('�� �������� ������������� ������ (%s)',
                    $status->getTitle());
        }

    }


}