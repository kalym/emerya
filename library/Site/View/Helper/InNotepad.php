<?php
class Site_View_Helper_InNotepad extends Zend_View_Helper_Abstract
{
    public function inNotepad($article_id)
    {
        $user = Zend_Auth::getInstance()->getIdentity();
        $anNotepadTable = new NotepadTable;
        $rows = $anNotepadTable->fetchAll($anNotepadTable->select()
                    ->where('user_id=?', $user->user_id)
                    ->where('article_id=?', $article_id));
        return count($rows) > 0;
    }
}