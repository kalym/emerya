<?php
class Site_View_Helper_Sponsor extends Zend_View_Helper_Abstract
{
    public function sponsor($article_id)
    {
        $anSponsorTable = new SponsorTable;
        $items = $anSponsorTable->fetchAll($anSponsorTable->select()
                ->where('article_id=?', $article_id)
                ->where('is_paid=1')
                );
        
        $links = array();
        foreach ($items as $item) {
            $links[] = str_replace($this->view->escape($item->keyword), 
                    sprintf('<a href="%s" target="_blank">%s</a>', $item->uri, $this->view->escape($item->keyword)), 
                    $this->view->escape($item->text));
        }
        
        $links = implode(', ', $links);
        
        return $this->view->partial('widgets/sponsor.phtml', array(
            'article_id' => $article_id,
            'links' => $links     
        )
        );
    }
}
