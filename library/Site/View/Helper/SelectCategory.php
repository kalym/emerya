<?php

class Site_View_Helper_SelectCategory extends Zend_View_Helper_Abstract
{
    public function selectCategory($active_cat_id = null)
    {
        $anCatsTable = new CatsTable();

        $cats = $anCatsTable->fetchAll(
            $anCatsTable->select()->order('title ASC')
        );

        return $this->view->partial('widgets/catSelect.phtml',
            array('cats' => $cats, 'selected'=>$active_cat_id)
        );
    }
}