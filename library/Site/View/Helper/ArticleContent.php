<?php

class Site_View_Helper_ArticleContent extends Zend_View_Helper_Abstract
{
    public function articleContent($item)
    {
        return $this->view->partial('widgets/articleContent.phtml', array(
            'item' => $item
        ));
    }

}