<?php

class Site_View_Helper_Popular extends Zend_View_Helper_Abstract
{
    public function popular($cat = null)
    {
        $frontendOptions = array(
            'lifetime' => 3600 * 24 * 2, // cache lifetime of 2 days
            //'automatic_serialization' => true
        );

        $backendOptions = array(
            'cache_dir' => Zend_Registry::getInstance()->get('root_dir') . '/tmp/' // Directory where to put the cache files
        );

        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
        if (!($content = $cache->load('widget_Popular_' . $cat))) {
            $anPopularView = new PopularView();

            $select = $anPopularView->select();

            if ($cat) {
                $select = $select->where('cat_id=?', $cat);
            }
            $select = $select->order('date DESC');
            $select = $select->order('next_sunday DESC');
            $select = $select->limit(24);

            $articles = $anPopularView->fetchAll(
                $select
            );

            $content = $this->view->partial('widgets/popular.phtml',
                array('articles' => $articles)
            );

            $cache->save($content, 'widget_Popular_' . $cat);
        }

        return $content;
    }
}