<?php

class Site_View_Helper_FormSelectCustom extends Zend_View_Helper_Abstract
{
    public function formSelectCustom($name, $value = null, $valueData = array(), $placeholder = '�������� ������')
    {
        $xhtml = '<select name="' . $name . '">';
        $xhtml .= '<option>' . $placeholder . '</option>';
        foreach ($valueData as $key => $val) {
            $checked = ($val['value'] == $value) ? 'selected' : '';
            $xhtml .= '<option value="' . $val['value'] . '" ' . $checked . '>' . $val['title'] . '</option>';
        }
        $xhtml .= '</select>';

        return $xhtml;
    }
}