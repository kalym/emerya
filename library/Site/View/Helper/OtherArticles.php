<?php

class Site_View_Helper_OtherArticles extends Zend_View_Helper_Abstract
{
    public function otherArticles($article)
    {
        $anArticlesView = new ArticlesView;
        $anUserTable = new UsersTable;
        $user = $anUserTable->find($article->user_id)->current();

        $items = $anArticlesView->fetchAll(
                $anArticlesView->select()
                ->where('article_id<>?', $article->article_id)
                ->where('user_id=?', $article->user_id)
                //->where('is_approved=1')
                ->limit(3));
        
        if (count($items) == 0) return ;
        
        return $this->view->partial('widgets/otherArticles.phtml',
            array('items'=>$items,
                'user' => $user)
        );

    }

}
