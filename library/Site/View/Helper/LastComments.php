<?php

class Site_View_Helper_LastComments extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function lastComments()
    {
        $anComments = new CommentsView;
        $items = $anComments->fetchAll(
            $anComments->select()
                ->order('datetime DESC')
                ->where('is_approved=1')
                ->limit(Site_Settings::get('sidebar_comments_count', 14))
        );

        return $this->view->partial('widgets/lastComments.phtml',
            array('items'=>$items)
        );

    }

}
