<?php
class Site_View_Helper_Banner extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function banner()
    {
        return $this->view->partial('widgets/banner.phtml',
            array()
        );

    }

}