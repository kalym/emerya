<?php

class Site_View_Helper_CatAt extends Zend_View_Helper_Abstract
{
    public function catAt($active_cat_id = null)
    {

        $anCatsTable = new CatsTable();

        $cats = $anCatsTable->fetchAll(
            $anCatsTable->select()->order('title ASC')
        );

        return $this->view->partial('widgets/cat-at.phtml',
            array('cats' => $cats, 'selected'=>$active_cat_id)
        );
    }

}
