<?

Zend_Loader::loadClass('Zend_View_Helper_Abstract');

class Site_View_Helper_GetSubCat extends Zend_View_Helper_Abstract
{
    public function getSubCat($cat_id)
    {            
        $anCatsTable = new CatsTable();
        
        return $anCatsTable->fetchAll(
                $anCatsTable->select()->where('parent_id=?', $cat_id)
        );
    }
}