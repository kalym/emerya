<?php

class Site_View_Helper_AdvBanner extends Zend_View_Helper_Abstract
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function advBanner($type = 0, $category_id = 0)
    {
        $anAdvTable = new AdvTable();
        $select = $anAdvTable->select()
            ->where('type=?', $type)
            ->where('category_id=?', $category_id)
            ->where('is_disabled=?', 0);
        $row = $anAdvTable->fetchRow(
            $select
        );

        if (!$row) {
            return '<div class="banner"></div>';
        }

        return $this->view->partial('widgets/advBanner.phtml',
            array(
                'item' => $row
            )
        );

    }

}