<?

Zend_Loader::loadClass('Zend_View_Helper_Abstract');

class Site_View_Helper_Url extends Zend_View_Helper_Abstract
{
    
    
    public function url(array $urlOptions = array(), $name = null, $reset = false, $encode = true)
    {   
        foreach ($urlOptions as $k=>$v) {
            $urlOptions[$k] = iconv('windows-1251', 'UTF-8', $v);
        }
        $router = Zend_Controller_Front::getInstance()->getRouter();
        return $router->assemble($urlOptions, $name, $reset, $encode);
    }
}