<?php

class Site_View_Helper_MainMenu extends Zend_View_Helper_Abstract
{
    public function mainMenu()
    {
        $anPagesTable = new PagesTable();
        $out = '';

        $out.= sprintf('<td id="menu-users"><a href="%s">%s</a></td>', $this->view->url(
                        array('controller' => 'users', 'action' => 'index'), 'default', true
                ), '������'
        );

        $out.= sprintf('<td id="menu-qna"><a href="%s">%s</a></td>', $this->view->url(
                        array('controller' => 'question', 'action' => 'index'), 'default', true
                ), '������� � ������'
        );

        foreach ($anPagesTable->fetchAll($anPagesTable->select()->where('onmenu=?', 1)) as $page) {
            $out.= sprintf('<td id="menu-page-%d"><a href="%s">%s</a></td>', $page->page_id, $this->view->url(
                            array($page->alias), 'page', true
                    ), $page->fullname
            );
        }

        $out.= sprintf('<td id="menu-shop"><a href="%s">%s</a></td>', $this->view->url(
                        array('controller' => 'shop', 'action' => 'index'), 'default', true
                ), '������� ������'
        );

//        $out.= sprintf('<td id="menu-job"><a href="%s" class="new">%s</a></td>', $this->view->url(
//                        array('controller' => 'job', 'action' => 'index'), 'default', true
//                ), '����� �������'
//        );

        $out.= sprintf('<td id="menu-popular"><a href="%s">%s</a></td>', $this->view->url(
                        array('controller' => 'popular', 'action' => 'index'), 'default', true
                ), '����������'
        );
//        $out.= sprintf('<td id="menu-news"><a href="%s">%s</a></td>', $this->view->url(
//                        array('controller' => 'news', 'action' => 'index'), 'default', true
//                ), '�������'
//        );
        $out.= sprintf('<td id="menu-blogs"><a href="%s">%s</a></td>', $this->view->url(
                        array('controller' => 'blogs', 'action' => 'index'), 'default', true
                ), '�����'
        );


        $out.= sprintf('<td id="menu-podcasts"><a href="%s">%s</a></td>', $this->view->url(
                        array('controller' => 'podcasts', 'action' => 'index'), 'default', true
                ), '��������'
        );

        return sprintf('<table><tr>%s</tr></table>', $out);
    }

}