<?php

class Site_Settings {

    protected static $instance = null;
    protected $data = array();

    protected function __construct(){}

    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new self;
            $anSettingsTable = new SettingsTable();
            foreach ($anSettingsTable->fetchAll() as $setting) {
                self::$instance->data[$setting->name] = $setting->value;
            }
        }

        return self::$instance;
    }

    public static function get($name, $default = null)
    {
        return (isset(self::getInstance()->data[$name]) && self::getInstance()->data[$name] ) ?
                self::getInstance()->data[$name] :
                $default;
    }
}
