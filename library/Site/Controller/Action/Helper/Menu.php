<?php
class Site_Controller_Action_Helper_Menu extends
    Zend_Controller_Action_Helper_Abstract {

    function select($id) {
        $id = 'menu-' . $id;
        $script = <<<CUT
$(function(){
    $('#{$id}').addClass('active');
})
CUT;
        $this->getActionController()->view->headScript()->appendScript($script);
    }
}