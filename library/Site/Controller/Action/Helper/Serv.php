<?php
class Site_Controller_Action_Helper_Serv extends
    Zend_Controller_Action_Helper_Abstract {

    function requireService($id) {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
            $viewRenderer->setRender('hasNotService', null, true);
            $this->getActionController()->view->message = '���������� �������������, ����� ��������������� ������ �����������������.';
            return true;
        } elseif (!Zend_Auth::getInstance()->getIdentity()->hasService($id)) {
            $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
            $viewRenderer->setRender('hasNotService', null, true);
            $this->getActionController()->view->message = '� ��� ��� ������� � ������� �������';
            return true;
        }
        return false;
    }
}