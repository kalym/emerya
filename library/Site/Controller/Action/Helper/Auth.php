<?php
class Site_Controller_Action_Helper_Auth extends
    Zend_Controller_Action_Helper_Abstract {

    function requireLogin($onlyWriter = false) {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
            $viewRenderer->setRender('notLoggedIn', null, true);
            $this->getActionController()->view->message = '���������� �������������, ����� ��������������� ������ �����������������.';
            return true;
        } elseif (is_bool($onlyWriter) && $onlyWriter && !Zend_Auth::getInstance()->getIdentity()->isWriter()) {
            $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
            $viewRenderer->setRender('notLoggedIn', null, true);
            $this->getActionController()->view->message = '������ ���������������� �������� ������ �������';
            return true;
        } elseif(!is_bool($onlyWriter) && !in_array(Zend_Auth::getInstance()->getIdentity()->type, (array)$onlyWriter)) {
            $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
            $viewRenderer->setRender('notLoggedIn', null, true);
            $this->getActionController()->view->message = '������ ���������������� ��� �� ��������';
            return true;
        }
        return false;
    }
    
    function requireAdminPerm($resourceId) {
        if (!Site_Auth_Admin::getInstance()->hasIdentity()
                || !Site_Auth_Admin::getInstance()->getIdentity()->hasAccess($resourceId))
            die('� ������� ��������!');
    }
}
