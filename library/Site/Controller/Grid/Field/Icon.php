<?php

class Site_Controller_Grid_Field_Icon implements Site_Controller_Grid_Field_Interface { 
    
    protected $icon;
    
    function __construct($icon) {
        $this->icon = $icon;
    }
    
    function render($item)
    {
        return sprintf('<td width="%s"><img src="/public/images/admin/%s.png" /></td>', '1%', $this->icon);
    }
    
    function getTitle()
    {
        return '';
    }
    
    function getId()
    {
        return 'icon-' . $this->icon;
    }
}
