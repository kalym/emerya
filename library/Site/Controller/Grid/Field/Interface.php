<?php

interface Site_Controller_Grid_Field_Interface {    
    function render($item);
    function getTitle();
    function getId();
}
