<?php

class Site_Controller_Grid_Field implements Site_Controller_Grid_Field_Interface {    
    protected $field, $getFunc, $renderFunc;
    function __construct($field, $title = null, $getFunc = null, $renderFunc = null) {
        $this->field = $field;
        $this->title = $title;
        $this->getFunc = $getFunc ? $getFunc : array ($this, '_get');
        $this->renderFunc = $renderFunc ? $renderFunc : array ($this, '_render');
    }
    
    function render($item) {
        return call_user_func($this->renderFunc,
                call_user_func($this->getFunc, $item, $this->field), $item, $this->field);
    }
    
    function getTitle()
    {
        return $this->title;
    }
    
    function getId()
    {
        return $this->field;
    }
    
    function setGetFunc($callback)
    {
        $this->getFunc = $callback;
        return $this;
    }
    
    function setRenderFunc($callback)
    {
        $this->renderFunc = $callback;
        return $this;
    }
    
    protected function _get($item, $field) {
        return $item->{$this->field};
    }
    
    protected function _render($val, $item) {
        return sprintf('<td>%s</td>', htmlspecialchars($val, ENT_COMPAT, 'UTF-8'));
    }
}
