<?php

class Site_Controller_Grid_Filter implements Site_Controller_Grid_Filter_Interface {    
    protected $desc, $fields, $request;
    function __construct($desc, array $fileds) {
        $this->desc = $desc;
        $this->fields = $fileds;
    }
    
    function render(Zend_View_Abstract $view) {
        return $view->partial('grid-filter.phtml', array(
            'desc' => $this->desc
        ));
    }
    
    function applyFilter(Zend_Db_Table_Select $select) {
        if ($q = $this->request->getParam('q')) {
            $cond = array();
            foreach ($this->fields AS $field) {
                $cond[] = $select->getAdapter()->quoteInto("$field LIKE ?", '%' . $q . '%');
            }
            $select->where(implode(' OR ', $cond));
        }
    }
    
    function setRequest(Zend_Controller_Request_Abstract $request)
    {
        $this->request = $request;
    }
}
