<?php

interface Site_Controller_Grid_Filter_Interface { 
    function render(Zend_View_Abstract $view);
    function applyFilter(Zend_Db_Table_Select $select);
    function setRequest(Zend_Controller_Request_Abstract $request);
}
