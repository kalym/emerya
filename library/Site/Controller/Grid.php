<?php

abstract class Site_Controller_Grid extends Zend_Controller_Action {    
    protected $fields;
    protected $actions;
    protected $pk;
    protected $canCreate = true;
    protected $title;
    protected $table = null;
    
    /**
     * @return Zend_Db_Table
     */
    abstract function createTable();
    abstract function initFields();
    
    function getTable()
    {
        is_null($this->table) && ($this->table = $this->createTable());
        return $this->table;
    }


    function getActions($item)
    {
        return array('edit', 'delete');
    }
    
    function getFilter()
    {
        return new Site_Controller_Grid_Filter_Null;
    }
    
    function addField($field, $title=null) {
        if (! $field instanceof Site_Controller_Grid_Field_Interface) {
           $field = new Site_Controller_Grid_Field($field, $title);
        }
        
        $this->fields[$field->getId()] = $field;
        return $field;
    }
    
    function getFields() {
        return $this->fields;
    }
    
    function indexAction()
    {
        
        $this->initFields();
        
        $table = $this->getTable();
        
        $select = $table->select();
        $this->onSelect($select);
        
        $filter = $this->getFilter();
        $filter->setRequest($this->getRequest());
        $filter->applyFilter($select);
        
        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);

        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
        $paginator->setItemCountPerPage(Config::get('backend_perpage', 20));
        $paginator->setPageRange(8);
        
        $this->view->title = $this->title;
        $this->view->paginator = $paginator;      
        $this->view->fields = $this->getFields();
        $this->view->canCreate = $this->canCreate();
        $this->view->pk = $this->pk;
        $this->view->grid = $this;
        $this->view->filter = $filter;
        
        $this->onGridView($this->view); 
        $this->getHelper('viewRenderer')->setNoController(true);
        $this->getHelper('viewRenderer')->setScriptAction('grid');
    }
    
    function newAction()
    {
        if (!$this->canCreate())
            throw new Exception(sprintf('Record creation is 
                forbidden for %s', get_class($this)));
        
        $table = $this->getTable();
        $item = $table->fetchNew();
        $this->doFormAction($item);
    }
    
    function editAction()
    {
        $table = $this->getTable();
        $item = $table->find($this->getRequest()->getParam('id'))->current();
        $this->doFormAction($item);
    }
    
    function doFormAction($item)
    {
        if ($this->getRequest()->isPost()) {
            $oldItem = clone $item;
            $params = $this->getRequest()->getPost();
            $this->onVarsFromForm($params);
            foreach ($params as $k => $v) {
                if ($this->isValidField($k)) {
                    $item->$k = $v;
                }
            }
            $this->onBeforeSave($item, $params, $oldItem);
            $item->save();
            $this->onAfterSave($item, $params, $oldItem);
            $this->_redirect($this->getRequest()->getParam('b', '/admin'));
        } else {
            $this->view->pk = $this->pk;
            $this->view->item = $item;
            $this->onFormItem($item);
            $this->onFormView($this->view, $item);
            $this->getHelper('viewRenderer')->setScriptAction('form');
        }
    }
    
    function deleteAction()
    {
        $params = $this->getRequest()->getParams();
        $this->view->title = 'Удаление';

        $table = $this->getTable();

        if ($this->getRequest()->isPost()) {
            $item =  $table->find($this->getRequest()->getPost('id'))->current();
            $item->delete();
            $this->_redirect($this->getRequest()->getParam('b', '/admin'));
        } else {
            $this->view->pk = $this->pk;
            $this->view->item = $table->find($params['id'])->current();
            $this->getHelper('viewRenderer')->setNoController(true);
            $this->getHelper('viewRenderer')->setScriptAction('grid-del');
        }
    }
    
    function isValidField($field) {
        return $field != 'id' && $field[0] != '_';
    }
    
    function canCreate()
    {
        return $this->canCreate;
    }
    
    function onRowClass($item, & $class) {}
    function onSelect($select) {}
    function onFormItem($item) {}
    function onFormView($view) {}
    function onGridView($view) {}
    function onVarsFromForm($vars) {}
    function onBeforeSave($item, $params, $oldItem) {}
    function onAfterSave($item, $params, $oldItem) {}
    function onBeforeDel($item) {}
    function renderAfterRow($item, $collNum){}
    function renderStatic(){}
}
