<?

class Site_Controller_Plugin_NotFound extends Zend_Controller_Plugin_Abstract 
{
    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        $module = $request->getModuleName();
        $router = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
        if ($module != 'admin' && in_array($router, array(
            'article-index', 'draft-index', 'popular-index',
            'user-index', 'podcast-index', 'blog-index',
            'blog-video', 'blog-photo', 'shop-index',
            'question-index', 'cat-article', 'shop-cat',
            'cat-qna'
        )) && isset($_GET['p'])) {
            $request->setControllerName('error')
                    ->setActionName('not-found');
        }
        return;
    }

}