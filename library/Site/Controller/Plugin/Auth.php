<?

class Site_Controller_Plugin_Auth extends Zend_Controller_Plugin_Abstract 
{
    function preDispatch(Zend_Controller_Request_Abstract $request)
    {   
        $module = $request->getModuleName();
        if ( 'admin' == $module) {
            $auth = Site_Auth_Admin::getInstance();
            
            if (!$auth->hasIdentity()) {
                $request->setControllerName('index');
                $request->setActionName('login');
            } 
        }
        return;
    }


}