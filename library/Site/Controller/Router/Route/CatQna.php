<?php


/** Zend_Controller_Router_Route_Abstract */
require_once 'Zend/Controller/Router/Route/Abstract.php';


class Site_Controller_Router_Route_CatQna extends Zend_Controller_Router_Route_Abstract
{
    /**
     * Instantiates route based on passed Zend_Config structure
     *
     * @param Zend_Config $config Configuration object
     */
    public static function getInstance(Zend_Config $config)
    {
        return new self();
    }

    public function __construct()
    {
    }

    public function getVersion() {
        return 1;
    }

    /**
     * Matches a user submitted path with a previously defined route.
     * Assigns and returns an array of defaults on a successful match.
     *
     * @param  string $path Path used to match against this routing map
     * @return array|false  An array of assigned values or a false on a mismatch
     */
    public function match($path, $partial = false)
    {
        $path = trim(urldecode($path), '/');
        $path = explode('/', $path);

        if (count($path)>3 || count($path)<2)
            return false;
        if ($path[0] != 'question') return false;
        if (isset($path[2]) && !is_numeric($path[2])) return false;

        $anQnaCatTable = new CatQnATable();
        $cat = $anQnaCatTable->fetchRow($anQnaCatTable
                ->select()
                ->where('alias=?', $path[1]));
        
        if (!$cat)
            return false;

        $params = array (
                'controller' => 'question',
                'action'  => 'index',
                'cat' => $cat,
                'from' => 'cat-qna'
            );
        if (isset($path[2]))
            $params['p'] = $path[2];
        return $params;
    }

    /**
     * Assembles a URL path defined by this route
     *
     * @param  array $data An array of name (or index) and value pairs used as parameters
     * @return string Route path with user submitted parameters
     */
    public function assemble($data = array(), $reset = false, $encode = true)
    {
        $prefix = 'question/';
        
        if ($encode) {
            $data = array_map('urlencode', $data);
        }
        
        if (isset($data['alias'])) {
            return $prefix . $data['alias'];
        } else {
            $p = '';
            if (isset($data['p'])) {
                $p = $data['p'] > 1 ? $data['p'] .'/' : '';
                unset($data['p']);
            }
            $query = http_build_query($data);
            $url = ltrim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
            $url = array_filter(explode('/', $url));
            
            if (isset($url[2])) {
                unset($url[2]);
            }
            $url = trim(implode('/', $url) . '/' . $p, '/');
            return $data ? 
                "$url?$query" :
                "$url";
        }       
    }
}
