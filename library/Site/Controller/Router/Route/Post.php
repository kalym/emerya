<?php


/** Zend_Controller_Router_Route_Abstract */
require_once 'Zend/Controller/Router/Route/Abstract.php';


class Site_Controller_Router_Route_Post extends Zend_Controller_Router_Route_Abstract
{
    /**
     * Instantiates route based on passed Zend_Config structure
     *
     * @param Zend_Config $config Configuration object
     */
    public static function getInstance(Zend_Config $config)
    {
        return new self();
    }

    public function __construct()
    {
    }

    public function getVersion() {
        return 1;
    }

    /**
     * Matches a user submitted path with a previously defined route.
     * Assigns and returns an array of defaults on a successful match.
     *
     * @param  string $path Path used to match against this routing map
     * @return array|false  An array of assigned values or a false on a mismatch
     */
    public function match($path, $partial = false)
    {
        $path = trim(urldecode($path), '/');
        $path = explode('/', $path);

        if (count($path)>3 || count($path)<2)
            return false;
        if ($path[0] != 'blogs') return false;

        $is_video = $is_photo = 0;
        $alias = isset($path[2]) ? $path[2] : $path[1];
        if (isset($path[2])) {
            switch ($path[1]) {
                case 'video' :
                   $is_video = 1;
                   break;
               case 'photo' :
                   $is_photo = 1;
                   break;
               default:
                   return false;
            }
        }

        if (!preg_match('/^([-_a-zA-Z0-9\.\,]+?)\.html$/', $alias, $matches))
            return false;

        $anPostsTable = new PostsTable;
        $select = $anPostsTable->select()
                ->where('alias=?', $matches[1])
                ->where('is_video=?', $is_video)
                ->where('is_photo=?', $is_photo);
        
        $post = $anPostsTable->fetchRow($select);

        if (!$post)
            return false;

        return array (
                'controller' => 'blogs',
                'action'  => 'item',
                'id' => $post->post_id,
                'from' => 'post'
            );
    }

    /**
     * Assembles a URL path defined by this route
     *
     * @param  array $data An array of name (or index) and value pairs used as parameters
     * @return string Route path with user submitted parameters
     */
    public function assemble($data = array(), $reset = false, $encode = true)
    {
        if ($encode) {
            $data = array_map('urlencode', $data);
        }
        $prefix = 'blogs/';
        
        if ($data['is_video'])
            $prefix .= 'video/';
        if ($data['is_photo'])
            $prefix .= 'photo/';
        
        return $prefix . $data['alias'] . '.html';
    }
}
