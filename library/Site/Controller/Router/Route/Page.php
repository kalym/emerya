<?php


/** Zend_Controller_Router_Route_Abstract */
require_once 'Zend/Controller/Router/Route/Abstract.php';


class Site_Controller_Router_Route_Page extends Zend_Controller_Router_Route_Abstract
{

    /**
     * Instantiates route based on passed Zend_Config structure
     *
     * @param Zend_Config $config Configuration object
     */
    public static function getInstance(Zend_Config $config)
    {
        return new self();
    }

    public function __construct()
    {
    }
    
    public function getVersion() {
        return 1;
    }

    /**
     * Matches a user submitted path with a previously defined route.
     * Assigns and returns an array of defaults on a successful match.
     *
     * @param  string $path Path used to match against this routing map
     * @return array|false  An array of assigned values or a false on a mismatch
     */
    public function match($path, $partial = false)
    {

        $path = trim(urldecode($path), '/');
        $path = explode('/', $path);

        if (count($path)>1)
            return false;

        $anPagesTable = new PagesTable();
        $page = $anPagesTable->findByAlias($path[0]);

        if (!$page)
            return false;

        return array (
                'controller' => 'page',
                'action'  => 'index',
                'id' => $page->page_id,
                'from' => 'page'
            );
    }

    /**
     * Assembles a URL path defined by this route
     *
     * @param  array $data An array of name (or index) and value pairs used as parameters
     * @return string Route path with user submitted parameters
     */
    public function assemble($data = array(), $reset = false, $encode = true)
    {
        if ($encode) {
            $data = array_map('urlencode', $data);
        }
        return $data[0] . '/';
    }
}
