<?php


class Site_Controller_Router_Route_ShopCat extends Zend_Controller_Router_Route_Abstract
{
    /**
     * Instantiates route based on passed Zend_Config structure
     *
     * @param Zend_Config $config Configuration object
     */
    public static function getInstance(Zend_Config $config)
    {
        return new self();
    }

    public function __construct()
    {
    }

    public function getVersion() {
        return 1;
    }

    /**
     * Matches a user submitted path with a previously defined route.
     * Assigns and returns an array of defaults on a successful match.
     *
     * @param  string $path Path used to match against this routing map
     * @return array|false  An array of assigned values or a false on a mismatch
     */
    public function match($path, $partial = false)
    {

        $path = parse_url($path, PHP_URL_PATH);
        $path = trim(urldecode($path), '/');
        $path = explode('/', $path);

        if (count($path)>3 || count($path)<2)
            return false;
        
        if ($path[0]!='shop')
            return false;


        $anShopCatTable = new ShopCatTable;
        $cat = $anShopCatTable->findByAlias($path[1]);

        if (!$cat)
            return false;

        $params =  array (
                'controller' => 'shop',
                'action'  => 'index',
                'id' => $cat->cat_id,
                'from' => 'shop-cat'
            );
        if (isset($path[2]))
            $params['p'] = $path[2];
        return $params;
    }

    /**
     * Assembles a URL path defined by this route
     *
     * @param  array $data An array of name (or index) and value pairs used as parameters
     * @return string Route path with user submitted parameters
     */
    public function assemble($data = array(), $reset = false, $encode = true)
    {
        if ($encode) {
            $data = array_map('urlencode', $data);
        }

        if (isset($data[0])) {
            $alias = @$data[0];
            unset($data[0]);
        } else {
            $p = '';
            if (isset($data['p'])) {
                $p = $data['p'] > 1 ? $data['p'] .'/' : '';
                unset($data['p']);
            }
            preg_match('#/shop/([-_\.a-zA-Z0-9]*)(?|/|$)#i', $_SERVER['REQUEST_URI'], $matches);
            $alias = @$matches[1];
        }

        $query = array();
        $data = $reset ? $data : array_merge($_GET, $data);
        foreach ($data as $k => $v) {
           $query[] = sprintf("%s=%s", $k, $v);
        }

        $query = implode('&', $query);

        $res = trim('shop/' . $alias . '/' . $p, '/') . ($query ? '?' . $query : '');

        return $res;
    }
}
