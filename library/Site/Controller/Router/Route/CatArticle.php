<?php


/** Zend_Controller_Router_Route_Abstract */
require_once 'Zend/Controller/Router/Route/Abstract.php';


class Site_Controller_Router_Route_CatArticle extends Zend_Controller_Router_Route_Abstract
{
    /**
     * Instantiates route based on passed Zend_Config structure
     *
     * @param Zend_Config $config Configuration object
     */
    public static function getInstance(Zend_Config $config)
    {
        return new self();
    }

    public function __construct()
    {
    }

    public function getVersion() {
        return 1;
    }

    /**
     * Matches a user submitted path with a previously defined route.
     * Assigns and returns an array of defaults on a successful match.
     *
     * @param  string $path Path used to match against this routing map
     * @return array|false  An array of assigned values or a false on a mismatch
     */
    public function match($path, $partial = false)
    {
        $path = trim(urldecode($path), '/');
        $path = explode('/', $path);

        if (count($path)>2)
            return false;

        if (isset($path[1]) && !is_numeric($path[1]))
            return false;
        
        $anCatTable = new CatsTable;
        $cat = $anCatTable->fetchRow($anCatTable
                ->select()
                ->where('alias=?', $path[0]));
        
        if (!$cat)
            return false;

        $params = array (
                'controller' => 'articles',
                'action'  => 'index',
                'cat' => $cat,
                'from' => 'cat-article'
            );
        if (isset($path[1]))
            $params['p'] = $path[1];
        return $params;
    }

    /**
     * Assembles a URL path defined by this route
     *
     * @param  array $data An array of name (or index) and value pairs used as parameters
     * @return string Route path with user submitted parameters
     */
    public function assemble($data = array(), $reset = false, $encode = true)
    {
        $prefix = '';
        
        if ($encode) {
            $data = array_map('urlencode', $data);
        }
        
        if (isset($data['c_alias'])) {
            return $prefix . $data['c_alias'];
        } elseif (isset($data['alias'])) {
            return $prefix . $data['alias'];
        } else {
            $p = '';
            if (isset($data['p'])) {
                $p = $data['p'] > 1 ? $data['p'] .'/' : '';
                unset($data['p']);
            }
            $query = http_build_query($data);
            $url = ltrim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
            $url = explode('/', $url);
            $url = trim($url[0] . '/' . $p, '/');
            
            return $data ? 
                "$url?$query" :
                "$url";
        }       
    }
}
