<?php

class Site_Auth_Admin extends Zend_Auth {
    protected static $_instance = null;
    
    public function getStorage()
    {
        if (null === $this->_storage) {
            $this->setStorage(new Zend_Auth_Storage_Session('Site_Auth_Admin'));
        }

        return $this->_storage;
    }

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

}

