<?php

class Site_Auth_Adapter_Cookie implements Zend_Auth_Adapter_Interface
{
    const SECRET = 'hkjfn3w74mnrd8wodry_dxr9409ctru84JHdsj%%';

    protected $auth;
    protected $_authenticateResultInfo = array();

    static public function getAuthCookie($login) {
        return sprintf('%s-%s', $login, self::getAuthHash($login));
    }
    static protected function getAuthHash($login) {
        return md5($login . md5($login . self::SECRET));
    }

    public function __construct($auth)
    {
        $this->auth = $auth;
    }

    public function authenticate()
    {
        list($login, $hash) = explode('-', $this->auth);
        $usersTable = new UsersTable();
        $user = $usersTable->findByLogin($login);
        if (  is_null($user) || $hash!= $this->getAuthHash($login)) {
            $this->_authenticateResultInfo['code']       = Zend_Auth_Result::FAILURE_UNCATEGORIZED;
            $this->_authenticateResultInfo['identity']   = null;
            $this->_authenticateResultInfo['messages'][] = '';
        } else {
            if ($user->is_locked) {
                $this->_authenticateResultInfo['code']       = Zend_Auth_Result::FAILURE_UNCATEGORIZED;
                $this->_authenticateResultInfo['identity']   = null;
                $this->_authenticateResultInfo['messages'][] = '��� ������� ������������ ���������������';
            } else {
                $this->_authenticateResultInfo['code']       = Zend_Auth_Result::SUCCESS;
                $this->_authenticateResultInfo['identity']   = $user;
                $this->_authenticateResultInfo['messages'][] = '';
            }
        }

        return $this->_authenticateCreateAuthResult();

    }

    protected function _authenticateCreateAuthResult()
    {
        return new Zend_Auth_Result(
            $this->_authenticateResultInfo['code'],
            $this->_authenticateResultInfo['identity'],
            $this->_authenticateResultInfo['messages']
            );

    }

}
