<?

Zend_Loader::loadClass('Zend_Auth_Adapter_Interface');

class Site_Auth_Adapter_Admin implements Zend_Auth_Adapter_Interface
{

    protected $login = null;
    protected $password = null;
    protected $_authenticateResultInfo = array();

    public function __construct($login, $password)
    {
        $this->login    = $login;
        $this->password = $password;
    }
    
    public function authenticate()
    {
        $adminsTable = new AdminsTable();
        $user = $adminsTable->fetchByLogin($this->login);
        if (  is_null($user) ) {
            $this->_authenticateResultInfo['code']       = Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND;
            $this->_authenticateResultInfo['identity']   = null;
            $this->_authenticateResultInfo['messages'][] = '';
        } elseif (md5($this->password) == $user->passhash) {
            $this->_authenticateResultInfo['code']       = Zend_Auth_Result::SUCCESS;
            $this->_authenticateResultInfo['identity']   = $user;
            $this->_authenticateResultInfo['messages'][] = '';
        } else {
            $this->_authenticateResultInfo['code']       = Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID;
            $this->_authenticateResultInfo['identity']   = $user;
            $this->_authenticateResultInfo['messages'][] = '';
        }
        
        return $this->_authenticateCreateAuthResult();
        
    }
    
    protected function _authenticateCreateAuthResult()
    {
        return new Zend_Auth_Result(
            $this->_authenticateResultInfo['code'],
            $this->_authenticateResultInfo['identity'],
            $this->_authenticateResultInfo['messages']
            );
    
    }

}