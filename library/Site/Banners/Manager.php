<?php


class Site_Banners_Manager {
    /*
     * @var Site_Banners_Manager
     */
    protected static $instance = null;

    /*
     * array(
     *    'typology' => array (
     *        'typology_id' => (array)banners
     *    )  
     * )
     * 
     */
    protected $rightBanners = array();
    protected $topBanners = array();

    protected function __construct(){}

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function getTopBanner($typology, $typology_id=0)
    {

        if (!isset($this->topBanners[$typology][$typology_id])) {

            $banners1 = $this->getBanners($typology, $typology_id);
            shuffle($banners1);
            $banners2 = $this->getBanners($typology, 0);
            shuffle($banners2);
            $banners3 = $this->getBanners('default');
            shuffle($banners3);

            $this->topBanners[$typology][$typology_id] =
                $banners1 + $banners1 + $banners3;
        }

        $banner = current($this->topBanners[$typology][$typology_id]);
        if (!next($this->topBanners[$typology][$typology_id])) {
            reset($this->topBanners[$typology][$typology_id]);
        }
        return $banner['banner'];
    }

    public function getRightBanner($typology, $typology_id=0)
    {
        if (!isset($this->rightBanners[$typology][$typology_id])) {

            $banners1 = $this->getBanners($typology, $typology_id);
            shuffle($banners1);
            $banners2 = $this->getBanners($typology, 0);
            shuffle($banners2);

            $this->rightBanners[$typology][$typology_id] =
                $banners1 + $banners2;
        }

        $banner = current($this->rightBanners[$typology][$typology_id]);
        if (!next($this->rightBanners[$typology][$typology_id])) {
            reset($this->rightBanners[$typology][$typology_id]);
        }
        return $banner['banner'];
    }

    protected function getBanners($typology, $typology_id=0)
    {
        $result = array();

        $anBannerLinksTable = new BannerLinksTable();
        $bannerLinks = $anBannerLinksTable->fetchAll(
            $anBannerLinksTable->select()
                ->where('typology=?', $typology)
                ->where('typology_id=?', $typology_id)
        );

        $anBannersTable = new BannersTable();
        foreach ($bannerLinks as $bannerLink) {
            $banner = $anBannersTable->find($bannerLink->banner_id)->current();
            if ($banner) {
                $result[] = $banner;
            }
        }

        return $result;
    }


}