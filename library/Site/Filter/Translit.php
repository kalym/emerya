<?

class Site_Filter_Translit implements Zend_Filter_Interface {

    protected $addTrailingHash;
    
    public function __construct($addTrailingHash = true) {
        $this->addTrailingHash = $addTrailingHash;
    }
    
    public function filter($word) 
    {
        $length = 100;
        
        $uniq = substr( md5(microtime() . $word), 0, 4);
        
        $tbl = array(
		            '�'=>'a', '�'=>'b', '�'=>'v', '�'=>'g', '�'=>'d', '�'=>'e', '�'=>'g', '�'=>'z',
		            '�'=>'i', '�'=>'y', '�'=>'k', '�'=>'l', '�'=>'m', '�'=>'n', '�'=>'o', '�'=>'p',
		            '�'=>'r', '�'=>'s', '�'=>'t', '�'=>'u', '�'=>'f', '�'=>'i', '�'=>'e', '�'=>'A',
		            '�'=>'B', '�'=>'V', '�'=>'G', '�'=>'D', '�'=>'E', '�'=>'G', '�'=>'Z', '�'=>'I',
		            '�'=>'Y', '�'=>'K', '�'=>'L', '�'=>'M', '�'=>'N', '�'=>'O', '�'=>'P', '�'=>'R',
		            '�'=>'S', '�'=>'T', '�'=>'U', '�'=>'F', '�'=>'I', '�'=>'E', '�'=>"yo", '�'=>"h",
		            '�'=>"ts", '�'=>"ch", '�'=>"sh", '�'=>"shch", '�'=>"", '�'=>"", '�'=>"yu", '�'=>"ya",
		            '�'=>"YO", '�'=>"H", '�'=>"TS", '�'=>"CH", '�'=>"SH", '�'=>"SHCH", '�'=>"", '�'=>"",
		            '�'=>"YU", '�'=>"YA", ' '=>"_", '"'=>'', '.'=>'_', ','=>'_'
        );

        //$tbl = array( ' '=>"_", '"'=>'', '.'=>'_', ','=>'_' );
	
        $word = trim($word);
        $word = strip_tags($word);
        $word = preg_replace('/[^-+0-9a-zA-Z�-��-�,\.\s]/', '', $word);
        $word = strtr($word, $tbl);
        $word = substr($word, 0, $length) . ($this->addTrailingHash? ('_' . $uniq) : '');

        return $word;
    }

}