<?

class Site_Filter_PassToHash implements Zend_Filter_Interface {

    protected $salt = null;


    /**
     * @param string $salt
     * @return void
     */                   
    public function __construct($salt) {
        $this->salt = $salt;
    }

    public function filter($pass) {
        $pass = (string)$pass; 
        return md5( $this->salt . md5($pass . $this->salt) );
    }

}