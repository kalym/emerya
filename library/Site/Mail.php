<?php

class Site_Mail {
    protected $tmp = null;
    protected $vars = array();
    protected $email_from, $email_name;


    function __construct($name)
    {
        $anTemplateTable = new TemplateTable();
        
        
        $this->tmp = $anTemplateTable->fetchAll($anTemplateTable->select()->where('name=?', $name))->current();
        
        $settings = Zend_Registry::getInstance()->get('settings');
        $this->email_from = $settings['email_from'];
        $this->email_name = $settings['email_name'];
        $this->setEmailSignature($settings['email_signature']);
        $this->setEmailSignatureHtml($settings['email_signature_html']);
    }
    
    function __call($name, $arguments)
    {
        if (preg_match('/^set([a-zA-Z]*)$/', $name, $matches)) {
            $this->vars[$matches[1]] = array_shift($arguments);
        } else {
            throw new Exception(sprintf('Unknow method call in %s [%s]', __CLASS__, $name));
        }
    }
    
    function send($email)
    {
        return;
        $mail = new Zend_Mail('windows-1251');
        $this->tmp->is_html ?
                $mail->setBodyHtml($this->expandPlaceholders($this->tmp->content)) :
                $mail->setBodyText($this->expandPlaceholders($this->tmp->content));
        
        $mail->setFrom($this->email_from, $this->email_name);
        $mail->setSubject($this->expandPlaceholders($this->tmp->subject));
        $mail->addTo($email, null);
        $mail->send();
    }
    
    function expandPlaceholders($content)
    {
        if (preg_match_all('/%%([a-zA-Z\._]*)?%%/i', $content, $matches)) {
            foreach ($matches[1] as $v) {
               @list($obj, $prop) = explode('.', $v);
               $val = $prop ? $this->vars[$obj]->{$prop} : $this->vars[$obj];
               $content = str_replace('%%' . $v . '%%', $val, $content);
            }
        }
        
        return $content;
    }
}
