<?php

class Site_Validate_NotContainEmailAddress extends Zend_Validate_Abstract {
    const CONTAIN_EMAIL = 'conteinEmail';

    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::CONTAIN_EMAIL => "Message can not contein email address"
    );

    public function isValid($value) {
        $this->_setValue($value);
        if (strpos($value, '@') !== false) {
            $this->_error(self::CONTAIN_EMAIL);
            return false;
        }
        return true;
    }

}
