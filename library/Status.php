<?php

class Status {
    protected $rate;
    const PRO = 'pro';
    
    protected static $levels = array(
        'top' => 5000,
        'mas' => 2000,
        'pro' => 1200,
        'spe' => 700,
        'deb' => 300,
        'tra' => 0  
    );
    protected static $titles = array(
        'top' => 'Top Author',
        'mas' => 'Master',
        'pro' => 'Professional',
        'spe' => 'Specialist',
        'deb' => 'Debutant',
        'tra' => 'Trainee'  
    );
    
    function __construct($rate) {
        $this->rate = $rate;
    }
    
    static function getThreshold($level)
    {
        return self::$levels[$level];
    }
    
    static function getInterval($level)
    {
        $k=null;
        $flag = false;
        foreach (array_reverse(self::$levels) as $c=>$v) {
            if ($flag) {
                $k = $v;
                break;
            }
            if ($c == $level) $flag = true;
        }
        
        return array(self::$levels[$level], is_null($k) ? 1000000000 : $k-1);
    }
    
    static function getLevels()
    {
        return self::$levels;
    }
    
    function getCode() {
        foreach (self::$levels as $code => $level) {
            if ($this->rate >= $level) return $code;
        }
        
        return null;
    }
    
    function getTitle() {
        return self::$titles[$this->getCode()];
    }
    
    function getNextLevelCode() {
        $code = $this->getCode();
        $flag = false;
        foreach (array_reverse(self::$levels, true) as $c => $level) {
            if ($flag) return $c;
            if ($c == $code) $flag = true;
        }
        return null;
    }

    function getNextLevelTitle() {
        $code = $this->getNextLevelCode();
        if (!$code) return null;
        return self::$titles[$code];
    }
    
    function getRemainBeforeNextLevel() {
        $code = $this->getNextLevelCode();
        if (!$code) return 0;
        return self::$levels[$code] - $this->rate;
    }
}
