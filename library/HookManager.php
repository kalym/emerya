<?php

class HookManager {
    const EVENT_NEW_COMMENT = 'newComment'; 
    const EVENT_NEW_POST = 'newPost';
    const EVENT_NEW_QNA = 'newQna';
    const EVENT_NEW_JOB = 'newJob';
    const EVENT_PURCHASE = 'purchase';
    const EVENT_NEW_ARTICLE = 'newArticle';
    const EVENT_NEW_PHOTO = 'newPhoto';
    const EVENT_APPROVE_POST = 'approvePost';
    const EVENT_APPROVE_QNA = 'approveQna';
    const EVENT_APPROVE_COMMENT = 'approveComment';
    const EVENT_LOGIN = 'login';
    
    protected static $_instance = null;
    protected $_registry = array();
    
    protected function __construct() {}
    /**
     *
     * @return HookManager
     */
    static public function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function bind($hookName, $callback) {
        if (!isset($this->_registry[$hookName])) {
            $this->_registry[$hookName] = array();
        }
        array_push($this->_registry[$hookName], $callback);
    }
    
    public function call($hookName, array $params) {
        if (isset($this->_registry[$hookName])) {
            foreach ($this->_registry[$hookName] as $callback) {
                call_user_func_array($callback, $params);
            }
        }
    }
} 