<?php

class StatLoginTable extends Zend_Db_Table {
  protected $_name     = 'stat_login';
  protected $_primary  = 'stat_login_id';

  function log(User $user, $ip, $dattm) {
      $this->insert(array(
          'user_id' => $user->user_id,
          'ip' => $ip,
          'dat' => $dattm,
          'dattm' => $dattm
      ));
  }
}