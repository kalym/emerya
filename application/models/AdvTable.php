<?php

class AdvTable extends Zend_Db_Table
{
    protected $_name = 'adv';
    protected $_primary = 'adv_id';
    protected $_rowClass = 'Adv';

    const ADV_TYPE_MAIN_PAGE = 1;
    const ADV_TYPE_ARTICLE_CATEGORY = 2;

    public static function getAdvTypesArray()
    {
        return array(
            array('title' => '������� ��������', 'value' => self::ADV_TYPE_MAIN_PAGE),
            array('title' => '������ ������', 'value' => self::ADV_TYPE_ARTICLE_CATEGORY),
        );
    }

    public static function getArticlesCategoriesArray()
    {
        $anCatsTable = new CatsTable();
        $responseArray = array();
        $cats = $anCatsTable->fetchAll(
            $anCatsTable->select()->order('title ASC')
        );

        foreach ($cats as $category) {
            $responseArray[] = array(
                'title' => $category->name,
                'value' => $category->cat_id
            );
        }
        
        return $responseArray;
    }
}