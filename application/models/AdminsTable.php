<?php

class AdminsTable extends Zend_Db_Table {
  protected $_name     = 'admins';
  protected $_primary  = 'id';
  protected $_rowClass = 'Admin';
  
  function fetchById($id) {
      return $this->fetchRow(
          $this->select()
              ->where('id = ?', $id)
      );
  }
  
  function fetchByLogin($login) {
      return $this->fetchRow(
          $this->select()
              ->where('login = ?', $login)
      );
  }
}