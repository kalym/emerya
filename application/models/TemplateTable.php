<?php

class TemplateTable extends Zend_Db_Table {
  protected $_name     = 'template';
  protected $_primary  = 'template_id';
}