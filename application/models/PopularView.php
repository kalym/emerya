<?php

class PopularView extends Zend_Db_Table {
  protected $_name     = 'popular_view';
  protected $_primary  = 'article_id';
  protected $_rowClass = 'Article';
}