<?php

class ArticleViewTable extends Zend_Db_Table {

    protected $_name = 'article_view';
    protected $_primary = 'view_id';

    function log($article) {
        $now = date('Y-m-d H:i:s');
        if (is_null($article->stat_begin)) return;

        if ($article->stat_expire < $now && !$article->stat_stop) {
            $this->stop($article);
            return;
        }
        
        if ($article->stat_expire < $now) return;
        
        // now do log
        
        $row = $this->fetchNew();
        
        $anIpCidrTable = new IpCidrTable;
        $code = $anIpCidrTable->getCountry($_SERVER['REMOTE_ADDR']);
        if ($code) {
            $row->country = $code;
        }

        $row->dattm = $now;
        $row->article_id = $article->article_id;
        $row->user_id = Zend_Auth::getInstance()->hasIdentity() ?
                Zend_Auth::getInstance()->getIdentity()->user_id :
                null;
        $row->agent_id = $this->getAgentId();
        $row->ip = $_SERVER['REMOTE_ADDR'];
        $row->is_unique = $this->isUnique($row);
        $row->expire = $article->stat_expire;
        $row->save();
    }
    
    function stop($article, $add_payment = true)
    {
        $article->stat_stop = 1;
        $article->stat_expire = min($article->stat_expire, date('Y-m-d H:i:s'));
        list($article->stat_view, $article->stat_view_unique, $article->stat_income) = $this->getStat($article);
        $article->save();
        
        if ($add_payment && $article->stat_income > 0) {
            $anPaymentsTable = new PaymentsTable;

            $payment = $anPaymentsTable->fetchNew();
            $payment->type = PaymentsTable::TYPE_INTERNAL;
            $payment->amount = $article->stat_income;
            $payment->datetime = date('Y-m-d H:i:s');
            $payment->user_id = $article->user_id;
            $payment->article_id = $article->article_id;
            $payment->typology_id = $article->article_id;
            $payment->typology = PaymentsTable::TYPOLOGY_ARTICLE_VIEW;
            $payment->save();
        } 
    }

    function isUnique($row) {
        return $this->getDefaultAdapter()->fetchOne("SELECT COUNT(*)
          FROM article_view
          WHERE article_id = :article_id AND
          ((ip = :ip AND dattm > :dat) 
            OR agent_id = :agent_id 
            OR (user_id IS NOT NULL AND user_id = :user_id))", array(
                    ':article_id' => $row->article_id,
                    ':ip' => $row->ip,
                    ':dat' => date('Y-m-d H:i:s', strtotime($row->dattm) - 3600 * 5),
                    ':agent_id' => $row->agent_id,
                    ':user_id' => $row->user_id
                )) ? 0 : 1;
    }

    function getAgentId() {
        if (!(isset($_COOKIE['agent']) && $_COOKIE['agent'])) {
            $id = substr(md5(microtime() . rand(0, 9999)), 0, 255);
            $_COOKIE['agent'] = $id;
            setcookie('agent', $id, time() + 3600 * 24 * 366 * 20);
        }

        return $_COOKIE['agent'];
    }
    
    function getStat($article)
    {
        $price_ru = Site_Settings::get('ppv_price_RU');
        $price_ua = Site_Settings::get('ppv_price_UA');
        $price_by = Site_Settings::get('ppv_price_BY');
        
        $select = $this->select()
                ->setIntegrityCheck(false)
                ->from('article_view', array(
                    'view_unique' => new Zend_Db_Expr('COUNT(IF(is_unique, view_id, NULL))'),
                    'view' => new Zend_Db_Expr('COUNT(view_id)'),
                    'income' => new Zend_Db_Expr("SUM(IF(is_unique, CASE
                        WHEN country = 'RU' THEN $price_ru
                        WHEN country = 'UA' THEN $price_ua
                        WHEN country =  'BY' THEN $price_by
                    END, 0))")
                ))
                ->where('article_id=?',$article->article_id);
        
        $row = $this->fetchRow($select);
        return array(
            $row->view,
            $row->view_unique,
            ceil($row->income)
        );
    }
}