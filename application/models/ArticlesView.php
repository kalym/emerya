<?php

class ArticlesView extends Zend_Db_Table {
  protected $_name     = 'articles_view';
  protected $_primary  = 'article_id';
  protected $_rowClass = 'Article';
}