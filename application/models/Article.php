<?

class Article extends Zend_Db_Table_Row {

    /**
     *
     * @return User
     *
     */
    function getUser() {
        $anUsersTable = new UsersTable();

        return $anUsersTable->find($this->user_id)->current();
    }
    
    function approve()
    {
        $this->is_approved = 1;
        if (!$this->at_id) {
            $user = $this->getUser();
            $status = $user->getStatus();
            $this->stat_begin = date('Y-m-d H:i:s');
            $this->stat_expire = date('Y-m-d H:i:s', strtotime(sprintf('+%d days', 
                    Site_Settings::get('ppv_' . $status, 0))));
        }
    }

    function getPayments() {
        $anPaymentsTable = new PaymentsTable;
        return $anPaymentsTable->fetchAll(
                        $anPaymentsTable->select()->where('article_id=?', $this->article_id)
        );
    }

    function getRates() {
        $anRatingsView = new RatingsView;
        return $anRatingsView->fetchAll(
                        $anRatingsView->select()->where('article_id=?', $this->article_id)
        );
    }

    function getPayAmount() {
        $res = 0;
        foreach ($this->getPayments() as $payment) {
            $res += $payment->amount;
        }

        return $res;
    }

    function setLinks($links) {
        $anArticleLink = new ArticleLinkTable();
        $anArticleLink->delete('article_id = ' . $this->article_id);

        foreach ($links as $word => $article_link_id) {
            $anArticleLink->insert(array(
                'article_id' => $this->article_id,
                'word' => $word,
                'article_link_id' => $article_link_id
            ));
        }
    }
    
    function getLinks() {
        $anArticleLink = new ArticleLinkTable();
        $res = array();
        foreach( $anArticleLink->fetchAll($anArticleLink->select()
                ->where('article_id=?', $this->article_id))
                as $link) {
            
            $res[$link->word] = $link->article_link_id;        
        }
        return $res;
    }
    
    function getImgGallery()
    {
        return $this->img_gallery ? unserialize($this->img_gallery) : array();
    }
    function setImgGallery($gallery)
    {
        $this->img_gallery = serialize($gallery);
    }
    
    function getVideoGallery()
    {
        return $this->video_gallery ? unserialize($this->video_gallery) : array();
    }
    function setVideoGallery($gallery)
    {
        $this->video_gallery = serialize($gallery);
    }



    function delete() {
        $anNotepadTable = new NotepadTable;
        $anNotepadTable->delete(sprintf('article_id=%d', $this->article_id));
        
        if ($this->at_id) {
            $anATTable = new ATTable();
            $at = $anATTable->find($this->at_id)->current();
            $at->is_active = 1;
            $at->save();
        }

        $anUsersTable = new UsersTable();
        $user = $anUsersTable->find(
                        $this->user_id
                )->current();
        if ($user) {
            $user->pub_count--;
            $user->save();
        }
        $anCommentsTable = new CommentsTable();
        $comments = $anCommentsTable->fetchAll(
                $anCommentsTable->select()
                        ->where('typology=?', CommentsTable::TYPOLOGY_ARTICLE)
                        ->where('typology_id=?', $this->article_id)
        );
        foreach ($comments as $comment) {
            $comment->delete();
        }

        $anCSTable = new CSTable();

        $csc = $anCSTable->fetchAll(
                $anCSTable->select()
                        ->where('typology=?', CommentsTable::TYPOLOGY_ARTICLE)
                        ->where('typology_id=?', $this->article_id)
        );

        foreach ($csc as $cs) {
            $cs->delete();
        }
        
        $anArticleLink = new ArticleLinkTable;
        $anArticleLink->delete(sprintf('article_id = %1$d OR article_link_id = %1$d',
                $this->article_id));
        
        $anFeedTable = new FeedTable;
        $anFeedTable->delete(sprintf("type='%s' AND content_id=%d", FeedTable::TYPE_ARTICLE, $this->article_id));

        return parent::delete();
    }

    static function getNextSunday($time) {
        $time = strtotime($time);
        $days = (7 - date('w', $time));
        return (string) date('Y-m-d', strtotime("+$days day", $time));
    }

}