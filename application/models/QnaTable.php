<?php

class QnaTable extends Zend_Db_Table {
  protected $_name     = 'qna';
  protected $_primary  = 'qna_id';
  protected $_rowClass = 'Qna';
}