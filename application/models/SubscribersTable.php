<?php

class SubscribersTable extends Zend_Db_Table {
  protected $_name     = 'subscribers';
  protected $_primary  = 'subscriber_id';
  protected $_rowClass = 'Subscriber';
}