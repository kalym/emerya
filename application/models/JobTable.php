<?php

class JobTable extends Zend_Db_Table {
  protected $_name     = 'job';
  protected $_primary  = 'job_id';
  protected $_rowClass = 'Job';
  
  public function findByAlias($alias)
  {
        return $this->fetchAll(
            $this->select()
                ->where('alias=?', $alias)
        )->current();
  }
}