<?php

class JobView extends Zend_Db_Table {
  protected $_name     = 'job_view';
  protected $_primary  = 'job_id';
  protected $_rowClass = 'Job';
}