<?php

class Subscriber extends Zend_Db_Table_Row {
    public function getSC() {
        return base64_encode(
                        sprintf('%s-%s',
                                $this->subscriber_id,
                                $this->security_code
                ));
    }
}