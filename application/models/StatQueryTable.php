<?php

class StatQueryTable extends Zend_Db_Table {
  protected $_name     = 'stat_query';
  protected $_primary  = 'stat_query_id';
 
  const TYPE_USER = 'user';
  const TYPE_ARTICLE = 'article';
  
  function log($query, $type, $dattm) {
      $this->getAdapter()->query('INSERT INTO stat_query (`query`, `type`, `dattm`) 
          VALUES (:query, :type, :dattm)', 
          array(
              ':query' => $query,
              ':type' => $type,
              ':dattm' => $dattm
          ));
  }
}