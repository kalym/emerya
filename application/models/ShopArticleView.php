<?php

class ShopArticleView extends Zend_Db_Table {
  protected $_name     = 'shop_article_view';
  protected $_primary  = 'article_id';
  protected $_rowClass = 'ShopArticle';
}