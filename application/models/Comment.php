<?

class Comment extends Zend_Db_Table_Row {

  function getUrlParams()
  {
      switch($this->typology) {
            case CommentsTable::TYPOLOGY_ARTICLE :
                $table = new ArticlesTable();
                $article = $table->find($this->typology_id)->current();
                return array(
                    array($article->alias),
                    'article',
                    true
                );
                break;
            case CommentsTable::TYPOLOGY_POST :
                $table = new PostsTable();
                $post = $table->find($this->typology_id)->current();
                return array(
                    $post->toArray(),
                    'post',
                    true
                );
                break;
            case CommentsTable::TYPOLOGY_QNA :
                $table = new QnaTable();
                $qna = $table->find($this->typology_id)->current();
                $catTable = new CatQnATable;
                $cat = $catTable->find($qna->cat_id)->current();
                return array(
                    array(
                        'alias' => $qna->alias,
                        'c_alias' => $cat->alias
                    ),
                    'qna',
                    true
                );
                break;
            case CommentsTable::TYPOLOGY_NEWS :
                $table = new NewsTable();
                $news = $table->find($this->typology_id)->current();
                return array(
                    array(
                        'controller' => 'news',
                        'action' => 'item',
                        'id' => $news->news_id
                    ),
                    'default',
                    true
                );
                break;
            case CommentsTable::TYPOLOGY_JOB :
                $table = new JobTable();
                $job = $table->find($this->typology_id)->current();
                return array(
                    array(
                        $job->alias
                    ),
                    'job',
                    true
                );
                break;
	    case CommentsTable::TYPOLOGY_USER :
                $table = new UsersTable();
                $user = $table->find($this->typology_id)->current();
                return array(
                    array(
                         $user->login
                    ),
                    'user',
                    true
                );
                break;
      }
  }
  
  /**
     *
     * @return User
     *
     */
    function getUser() {
        $anUsersTable = new UsersTable();

        return $anUsersTable->find($this->user_id)->current();
    }
  
  function delete()
  {
        $anFeedTable = new FeedTable;
        $anFeedTable->delete(sprintf("type IN (%s) AND content_id=%d", implode(',', array_map(function($el){return sprintf("'%s'", $el);}, array(FeedTable::TYPE_COMMENT_ARTICLE,
        FeedTable::TYPE_COMMENT_POST, FeedTable::TYPE_COMMENT_QNA))), $this->comment_id));
        parent::delete();
  }
}