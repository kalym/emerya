<?

class CatQna extends Zend_Db_Table_Row {

    function delete() {
        $anQnaTable = new QnaTable;
        $qna = $anQnaTable->fetchAll(
                $anQnaTable->select()
                        ->where('cat_id=?', $this->cat_id)
        );
        foreach ($qna as $q) {
            $q->delete();
        }

        return parent::delete();
    }

}