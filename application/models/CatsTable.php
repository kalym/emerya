<?php

class CatsTable extends Zend_Db_Table {
  protected $_name     = 'categories';
  protected $_primary  = 'cat_id';

    function getOptions($addSelectOffer=false)
    {
        $cats = $this->fetchAll(
            $this->select()->order('priority')
        );
        $options = array();
        if ($addSelectOffer) {
            $options[''] = '-- �������� ������� --';
        }
        foreach ($cats as $cat) {
            $options[$cat->cat_id] = $cat->name;
        }
        return $options;
    }
}