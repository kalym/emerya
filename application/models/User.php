<?php

class User extends Zend_Db_Table_Row {

    const BOOK_VIEW_LIMIT = 10;

    const TYPE_WRITER = 'writer';
    const TYPE_READER = 'reader';
    const TYPE_EMPLOYER = 'employer';

    protected $status = null;
    protected $msgError = null;

    function hasAddress()
    {
        return $this->country || $this->city;
    }
    
    function getAddress()
    {
        return implode(', ', array_filter($_ = array($this->country, $this->city)));
    }
    
    function getName()
    {
        return trim("{$this->name_f} {$this->name_l}");
    }
    
    /**
     * Return generated pass
     *
     * @param integer $length
     * @return string
     */
    static function generatePass($length = null) {
        $pass_symbols = 'abcdefghijklmnopqrstuvwxyz';
        $pass_symbols .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pass_symbols .= '1234567890';
        $pass_symbols .= '#$@!_';
        $count_simbols = strlen($pass_symbols);
        $count = ($length) ? $length : rand(6, 10);
        $pass = '';
        for ($i = 0; $i < $count; $i++) {
            $n = rand(0, ($count_simbols - 1));
            $pass.=$pass_symbols[$n];
        }
        return $pass;
    }

    function setPassword($pass) {
        $this->salt = self::generateSalt();
        $this->passhash = Zend_Filter::filterStatic(
                        $pass, 'PassToHash', array($this->salt));
    }

    /**
     * Return generate salt
     *
     * @return string
     */
    static function generateSalt() {
        return self::generatePass(4);
    }

    function getKarma() {

        if ($this->karma_last_update < date('Y-m-d H:i:s', time() - 3600 * 24)) {
            $karma = 0;

            $anCommentsTable = new CommentsTable;
            $comment = $anCommentsTable->fetchAll(
                            $anCommentsTable->select()->from('comments', array(
                                'cnt' => new Zend_Db_Expr('COUNT(comment_id)')
                            ))->where('user_id=?', $this->user_id)->where('typology<>?', CommentsTable::TYPOLOGY_QNA)
                    )->current();

            $karma+=floor($comment->cnt / 2);

            $anPostsTable = new PostsTable;
            $post = $anPostsTable->fetchAll(
                            $anPostsTable->select()->from('posts', array(
                                'cnt' => new Zend_Db_Expr('COUNT(post_id)')
                            ))->where('user_id=?', $this->user_id)
                    )->current();

            $karma+=$post->cnt;

            $anQnaTable = new QnaTable;
            $qna = $anQnaTable->fetchAll(
                            $anQnaTable->select()->from('qna', array(
                                'cnt' => new Zend_Db_Expr('COUNT(qna_id)')
                            ))->where('user_id=?', $this->user_id)
                    )->current();

            $karma+=$qna->cnt;

            $anCommentsTable = new CommentsTable;
            $comment = $anCommentsTable->fetchAll(
                            $anCommentsTable->select()->from('comments', array(
                                'cnt' => new Zend_Db_Expr('COUNT(comment_id)')
                            ))->where('user_id=?', $this->user_id)->where('typology=?', CommentsTable::TYPOLOGY_QNA)
                    )->current();

            $karma+=$comment->cnt;

            $karma/=5;
            $this->karma = floor($karma);
            $this->karma_last_update = date('Y-m-d H:i:s');
            $this->save();
        }

        return $this->karma - $this->karma_correction;
    }

    public function isOnline() {
        $delay = Zend_Registry::getInstance()->get('config')->site->online_delay;
        return (bool) ((time() - strtotime($this->last_activity)) < $delay);
    }

    public function isWriter() {
        return $this->type == self::TYPE_WRITER;
    }

    public function isReader() {
        return $this->type == self::TYPE_READER;
    }

    public function isEmployer() {
        return $this->type == self::TYPE_EMPLOYER;
    }

    public function getTypeTitle() {
        switch ($this->type) {
            case self::TYPE_WRITER :
                return 'автор';
            case self::TYPE_READER :
                return 'читатель';
            case self::TYPE_EMPLOYER :
                return 'работодатель';
        }
    }

    public function getTypeTitle2() {
        switch ($this->type) {
            case self::TYPE_WRITER :
                return 'автора';
            case self::TYPE_READER :
                return 'читателя';
            case self::TYPE_EMPLOYER :
                return 'работодателя';
        }
    }

    public function haveAprovedArticles() {
        return (bool) $this->pub_approved_count;
    }

    public function countApprovedArticles() {
        $anArticlesTable = new ArticlesTable();
        $articles = $anArticlesTable->fetchAll(
                $anArticlesTable->select()
                        ->where('user_id=?', $this->user_id)
                        ->where('is_approved>0')
        );

        return $articles->count();
    }

    public function countRecommend() {
        $anRecommendTable = new RecommendWhoView;
        $recommend = $anRecommendTable->fetchAll(
                $anRecommendTable->select()
                        ->where('whom_id=?', $this->user_id)
                        ->where('type=?', self::TYPE_WRITER)
        );

        return $recommend->count();
    }

    public function countReaders() {
        $anRecommendTable = new RecommendWhoView;
        $recommend = $anRecommendTable->fetchAll(
                $anRecommendTable->select()
                        ->where('whom_id=?', $this->user_id)
                        ->where('type=?', self::TYPE_READER)
        );

        return $recommend->count();
    }

    public function countPhoto() {
        $anPhotoTable = new PhotoTable();
        $photo = $anPhotoTable->fetchAll(
                $anPhotoTable->select()
                        ->where('user_id=?', $this->user_id)
        );

        return $photo->count();
    }

    public function countQna() {
        $anQnaTable = new QnaTable();
        $qna = $anQnaTable->fetchAll(
                $anQnaTable->select()
                        ->where('user_id=?', $this->user_id)
        );

        return $qna->count();
    }

    public function countJob() {
        $anJobTable = new JobTable;

        return $anJobTable->fetchAll(
                        $anJobTable->select()
                                ->from('job')
                                ->columns(array('cnt' => new Zend_Db_Expr('COUNT(job_id)')))
                                ->where('user_id=?', $this->user_id)
                )->current()->cnt;
    }

    public function countPurchase() {
        $anShopArticleTable = new ShopArticleTable;

        return $anShopArticleTable->fetchAll(
                        $anShopArticleTable->select()
                                ->from('shop_article')
                                ->columns(array('cnt' => new Zend_Db_Expr('COUNT(article_id)')))
                                ->where('owner_id=?', $this->user_id)
                )->current()->cnt;
    }

    public function countMsg($is_new = false) {
        $anMsgTable = new MsgTable();

        $select = $anMsgTable->select()
                ->where('recipient_id=?', $this->user_id);

        if ($is_new) {
            $select = $select->where('is_new=1');
        }

        $msg = $anMsgTable->fetchAll(
                $select
        );

        return $msg->count();
    }

    public function countFeed($is_new = false) {
        $anFeedTable = new FeedTable();

        $select = $anFeedTable->select()
                ->where('user_id=?', $this->user_id);

        if ($is_new) {
            $select = $select->where('is_new=1');
        }

        $feed = $anFeedTable->fetchAll(
                $select
        );

        return $feed->count();
    }

    public function countWhomRecommend() {
        $anRecommendTable = new RecommendTable();
        $recommend = $anRecommendTable->fetchAll(
                $anRecommendTable->select()
                        ->where('who_id=?', $this->user_id)
        );

        return $recommend->count();
    }

    public function countWhoRecommend() {
        $anRecommendTable = new RecommendTable();
        $recommend = $anRecommendTable->fetchAll(
                $anRecommendTable->select()
                        ->where('whom_id=?', $this->user_id)
        );

        return $recommend->count();
    }

    public function isAlreadyRecommend($user_id) {
        $anRecommendTable = new RecommendTable();
        $recommend = $anRecommendTable->fetchAll(
                $anRecommendTable->select()
                        ->where('who_id=?', $this->user_id)
                        ->where('whom_id=?', $user_id)
        );

        return (bool) $recommend->count();
    }

    function isBestBlog() {
        $anUserTable = new UsersTable;

        $res = $anUserTable->fetchAll($anUserTable->select()
                        ->where('is_locked=0')
                        ->where('posts_count>?', $this->posts_count)
                        ->limit(10));

        return $res->count() < 7;
    }

    function isBestQna() {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());

        $select = $select
                        ->from('users')
                        ->columns(array('users.*', 'cnt' => new Zend_Db_Expr('COUNT(comment_id)')))
                        ->joinLeft('comments', "users.user_id=comments.user_id AND comments.typology = 'qna'")
                        ->where('is_locked=0')
                        ->group('users.user_id')->order('cnt DESC')->limit(7);
        $items = new Zend_Db_Table_Rowset(array('data' => $select->query()->fetchAll()));
        foreach ($items as $item)
            if ($item->user_id == $this->user_id)
                return true;

        return false;
    }

    function isBestPopular() {
        return in_array($this->user_id, $this->getBestPopularIds());
    }

    function isBestCat() {
        return in_array($this->user_id, $this->getBestCatIds());
    }
    
    protected function getBestPopularIds() {
        $frontendOptions = array(
            'lifetime' => 3600 * 24 * 7, // cache lifetime of 2 days
            'automatic_serialization' => true
        );

        $backendOptions = array(
            'cache_dir' => Zend_Registry::getInstance()->get('root_dir') . '/tmp/' // Directory where to put the cache files
        );
        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
        if (!($stat_best_popular_ids = $cache->load('stat_best_popular_ids'))) {
            $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
            $select = $select
                            ->from('users')
                            ->columns(array('users.*', 'cnt' => new Zend_Db_Expr('COUNT(article_id)')))
                            ->joinLeft('popular_view', "users.user_id=popular_view.user_id")
                            ->where('is_locked=0')
                            ->group('users.user_id')->order('cnt DESC')->limit(7);

            $items = new Zend_Db_Table_Rowset(array('data' => $select->query()->fetchAll()));
            foreach ($items as $item) {
                $stat_best_popular_ids[] = $item->user_id;
            }
            $cache->save($stat_best_popular_ids, 'stat_best_popular_ids');
        }
        return $stat_best_popular_ids;
    }

    protected function getBestCatIds() {
        $frontendOptions = array(
            'lifetime' => 3600 * 24 * 7, // cache lifetime of 2 days
            'automatic_serialization' => true
        );

        $backendOptions = array(
            'cache_dir' => Zend_Registry::getInstance()->get('root_dir') . '/tmp/' // Directory where to put the cache files
        );
        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
        if (!($stat_best_cat_ids = $cache->load('stat_best_cat_ids'))) {
            $anCatTable = new CatsTable;
            foreach ($anCatTable->fetchAll() as $cat) {
                $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
                $select = $select
                                ->from('users')
                                ->columns(array('users.*', 'cnt' => new Zend_Db_Expr('COUNT(article_id)')))
                                ->joinLeft('articles', "users.user_id=articles.user_id")
                                ->where('is_locked=0')
                                ->where('cat_id=?', $cat->cat_id)
                                ->group('users.user_id')->order('cnt DESC')->limit(7);

                $items = new Zend_Db_Table_Rowset(array('data' => $select->query()->fetchAll()));

                $items = new Zend_Db_Table_Rowset(array('data' => $select->query()->fetchAll()));
                foreach ($items as $item) {
                    $stat_best_cat_ids[] = $item->user_id;
                }
            }
            $cache->save($stat_best_cat_ids, 'stat_best_cat_ids');
        }
        return $stat_best_cat_ids;
    }

    function isPremium() {
        if (!$this->premium_since)
            return false;
        $anUsersTable = new UsersTable();
        $premium = $anUsersTable->fetchAll(
                $anUsersTable->select()
                        ->where('is_locked=?', 0)
                        ->where('premium_since>?', $this->premium_since)
        );

        return $premium->count() < 14;
    }

    function isSmile() {
        return $this->smile_until > date('Y-m-d h:i:s');
    }

    function isAnonimus() {
        return $this->anonimus_until > date('Y-m-d h:i:s');
    }

    function isContact() {
        return $this->contact_until > date('Y-m-d h:i:s');
    }

    function isSecurity() {
        return $this->security_until > date('Y-m-d h:i:s');
    }

    function isStat() {
        return $this->stat_until > date('Y-m-d h:i:s');
    }

    function isBall() {
        return $this->ball_until > date('Y-m-d h:i:s');
    }

    function isSnigenie() {
        return $this->snigenie_until > date('Y-m-d h:i:s');
    }

    function hasService($id) {
        $method = 'is' . ucfirst($id);
        if (method_exists($this, $method)) {
            return call_user_func(array($this, $method));
        } else {
            return false;
        }
    }

    public function activateService($id, $date, $additional = array()) {
        $method = 'activateService' . ucfirst($id);
        if (method_exists($this, $method)) {
            return call_user_func(array($this, $method), $date, $additional);
        }
    }

    public function activateServiceArticle($date, $additional) {
        $anShopArticle = new ShopArticleTable();
        $item = $anShopArticle->find($additional['id'])->current();
        if ($item->owner_id) {
            return $this->activateServicePayin($date, $additional);
        }
        $item->owner_id = $this->user_id;
        $item->date_paid = date('Y-m-d H:i:s');
        $item->save();

        $anPaymentsTable = new PaymentsTable;
        $payment = $anPaymentsTable->fetchNew();
        $payment->type = PaymentsTable::TYPE_INTERNAL;
        $payment->amount = $item->price;
        $payment->datetime = date('Y-m-d H:i:s');
        $payment->user_id = $item->user_id;
        $payment->typology = PaymentsTable::TYPOLOGY_SHOP_ARTICLE;
        $payment->typology_id = $item->article_id;
        $payment->save();

        HookManager::getInstance()->call(HookManager::EVENT_PURCHASE, array(
            'article' => $item,
            'payment' => $payment
        ));

        $mail = new Site_Mail('purchase');
        $mail->setName($item->name);
        $mail->setContent($item->content);
        $mail->send($this->email);
    }

    public function activateServicePayin($date, $additional) {

        $this->account += $additional['amount'];
        $this->save();
    }

    public function activateServicePremium($date) {
        $this->premium_since = $date;
        $this->save();
    }

    public function activateServiceSmile($date) {
        $this->activateServiceUntil('smile', $date);
    }

    public function activateServiceAnonimus($date) {
        $this->activateServiceUntil('anonimus', $date);
    }

    public function activateServiceContact($date) {
        $this->activateServiceUntil('contact', $date);
    }

    public function activateServiceSecurity($date) {
        $this->activateServiceUntil('security', $date);
    }

    public function activateServiceStat($date) {
        $this->activateServiceUntil('stat', $date);
    }

    public function activateServiceBall($date) {
        $this->activateServiceUntil('ball', $date);
    }

    public function activateServiceIgnor($date, $add) {
        $anUsersTable = new UsersTable();
        $user = $anUsersTable->findByLogin($add['login']);

        $anIgnorTable = new IgnorTable();
        $date = max($date, $anIgnorTable->getUntil($this->user_id, $user->user_id));

        $anIgnorTable->replace(array(
            'who' => $this->user_id,
            'whom' => $user->user_id,
            'until' => date('Y-m-d H:i:s', strtotime($date) + 3600 * 24 * Site_Settings::get('ignor_period'))
        ));
    }

    public function activateServiceDarball($date, $add) {
        $anUsersTable = new UsersTable();
        $user = $anUsersTable->findByLogin($add['login']);

        $user->rating += $add['cnt'];
        $this->rating -= $add['cnt'];

        $user->save();
        $this->save();
    }

    public function activateServiceSalebal($date, $add) {
        $this->rating += $add['cnt'];
        $this->save();
    }

    public function activateServiceSnigenie($date, $add) {
        $date = max($date, $this->snigenie_until);
        $this->snigenie_until = date('Y-m-d H:i:s', strtotime($date) + 3600 * 24 * 1);
        $this->save();

        $anUsersTable = new UsersTable();
        $user = $anUsersTable->findByLogin($add['login']);
        if ($user->isSecurity())
            return;

        $user->rating -= $add['cnt'];
        $user->save();
    }

    protected function activateServiceUntil($id, $date) {
        $prop = $id . '_until';
        $date = max($date, $this->$prop);
        $this->$prop = date('Y-m-d H:i:s', strtotime($date) + 3600 * 24 * Site_Settings::get($id . '_period'));
        $this->save();
    }

    public function countBook() {
        static $count = null;
        if (is_null($count)) {
            $anATTable = new ATTable;
            $books = $anATTable->fetchAll(
                    $anATTable->select()
                            ->where('is_active=1')
                            ->where('book_user_id=?', $this->user_id)
                            ->where('book_due>?', date('Y-m-d H:i:s'))
            );
            $count = $books->count();
        }
        return $count;
    }

    public function canBook() {
        return $this->countBook() < (2 + floor($this->getKarma() / Site_Settings::get('book_limit_increase')));
    }

    public function getStatus() {
        if (is_null($this->status)) {
            $status = new Status($this->rating);
            $this->status = $status->getCode();
        }

        return $this->status;
    }

    public function canSendMsg($id) {
        if (!$id instanceof User) {
            /* @var $user User */
            $user = $this->getTable()->find($id)->current();
        } else {
            $user = $id;
        }

        if (!$user)
            return false;

        $anIgnorTable = new IgnorTable();
        if ($anIgnorTable->isIgnored($user->user_id, $this->user_id)) {
            return false;
        }

        if ($user->isEmployer() && $this->isEmployer()) {
            return false;
        }

        if ($user->isEmployer() && $this->isWriter()) {
            if (!$this->hasActiveComments($user->user_id)) {
                return false;
            }
        }

        if ($user->isWriter() && $this->isEmployer()) {
            if (!$user->hasActiveComments($this->user_id)) {
                return false;
            }
        }
        
        if ($user->isReader() || $this->isReader()) {
            return false;
        }

        return true;
    }

    function hasActiveComments($id) {
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select = $select->from(array('c' => 'comments'), array('cnt' => new Zend_Db_Expr('COUNT(comment_id)')))
                ->joinLeft(array('j' => 'job'), 'c.typology_id=j.job_id')
                ->where('j.is_closed<>1')
                ->where('j.user_id=?', $id)
                ->where('c.user_id=?', $this->user_id)
                ->where('c.typology=?', CommentsTable::TYPOLOGY_JOB);
        $res = $select->query()->fetchObject();
        return $res->cnt;
    }

}