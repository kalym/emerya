<?php

class PagesTable extends Zend_Db_Table {
  protected $_name     = 'pages';
  protected $_primary  = 'page_id';
  
  public function findByAlias($alias)
  {
        return $this->fetchAll(
            $this->select()
                ->where('alias=?', $alias)
        )->current();
  }
}