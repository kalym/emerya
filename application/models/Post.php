<?

class Post extends Zend_Db_Table_Row {

    /**
     *
     * @return User
     *
     */
    function getUser() {
        $anUsersTable = new UsersTable();

        return $anUsersTable->find($this->user_id)->current();
    }
    
    function delete() {
        $anFeedTable = new FeedTable;
        $anFeedTable->delete(sprintf("type='%s' AND content_id=%d", FeedTable::TYPE_POST, $this->post_id));
        parent::delete();
    }
}