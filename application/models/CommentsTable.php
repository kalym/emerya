<?php

class CommentsTable extends Zend_Db_Table {
  protected $_name     = 'comments';
  protected $_primary  = 'comment_id';
  protected $_rowClass = 'Comment';
  const TYPOLOGY_ARTICLE = 'article';
  const TYPOLOGY_NEWS = 'news';
  const TYPOLOGY_POST = 'post';
  const TYPOLOGY_USER = 'user';
  const TYPOLOGY_QNA = 'qna';
  const TYPOLOGY_JOB = 'job';

}