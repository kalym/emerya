<?

class ShopArticle extends Zend_Db_Table_Row {

    const TYPE_COPYWRITING = 1;
    const TYPE_REWRITING = 2;
    const TYPE_TRANSLATION = 3;
    
    /**
     *
     * @return User
     *
     */
    function getUser() {
        $anUsersTable = new UsersTable();

        return $anUsersTable->find($this->user_id)->current();
    }
    
    function getPrice() {
        $commission = Site_Settings::get('article_commission');
        return $this->price + ceil(($this->price * $commission)/100);
    }
    
    function isCopywriting()
    {
        return $this->type == self::TYPE_COPYWRITING;
    }
    
    function isRewriting()
    {
        return $this->type == self::TYPE_REWRITING;
    }
    
    function isTranslation()
    {
        return $this->type == self::TYPE_TRANSLATION;
    }
    
    function calculateFrequency()
    {
        $fr = array();
        $res = array();
        preg_match_all('/[-a-zA-Z�-��-�]+/i', $this->content, $matches);
        foreach($matches[0] as $word) {
           if (strlen($word)>3) @$fr[strtolower($word)]++; 
        }
        arsort($fr);
        $i = 0;
        foreach ($fr as $word => $count) { 
            $res[$word] = $count;
            if (++$i >= 5) break; 
        }
        
        $this->frequency = serialize($res);
        
    }
    
    function getFrequency() {
        return @unserialize($this->frequency);
    }
    
    function save() {
        if (isset($this->_cleanData['is_approved']) &&
                $this->_cleanData['is_approved'] == 0 &&
                $this->is_approved == 1) {
            $this->date_approved = date('Y-m-d H:i:s');
        }
        $this->calculateFrequency();
        parent::save();
    }
}