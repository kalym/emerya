<?
class Admin extends Zend_Db_Table_Row {    
    const PERM_NO_ACL = 'no-acl';
    const PERM_ROOT = 'root';
    
    function isRoot() {
        return (bool)$this->is_root;
    }
    
    function hasAccess($resourceId)
    {
        return $this->isRoot() || array_intersect((array)$resourceId, $this->prepareAcl($this->getAcl()));
    }
    
    protected function prepareAcl($acl) 
    {
        $add = array('no-acl'); //recource do not require anything 
        if ($this->isRoot()) {
            $add[] = 'root';
        } 
        
        return array_merge(array_keys(array_filter($acl)), $add);
    }
    
    function getAcl() {
        return unserialize($this->acl);
    }
    
    function setAcl($acl) {
        $this->acl = serialize($acl);
    }
}