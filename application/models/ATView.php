<?php

class ATView extends Zend_Db_Table {
  protected $_name     = 'article_templates_view';
  protected $_primary  = 'at_id';
  protected $_rowClass = 'AT';

  function getOptions($addSelectOffer=false, $user_id=null, $group = false)
  {
        $is_order_set = false;
        $select = $this->select()->where('is_active=1');
        if (!is_null($user_id)) {
           $select =  $select->where('book_user_id=? OR book_due<NOW() OR book_due IS NULL',
                   $user_id
           );
        }
        if ($user_id) {
            $anUserTable = new UsersTable;
            $user = $anUserTable->find($user_id)->current();
            if (!$user->haveAprovedArticles()) {
               $is_order_set = true;
               $select->order(new Zend_Db_Expr("book_user_id = $user_id DESC"));
               $select->limit(User::BOOK_VIEW_LIMIT); 
            }
        }
        if (!$is_order_set) {
            $select->order('c_name')->order('name');
        }
        
        $ats = $this->fetchAll($select);
        $options = array();
        if ($addSelectOffer) {
            $options[''] = '-- �������� ���� ������ --';
        }
        foreach ($ats as $at) {
            if ($group) {
                $dist = & $options[$at->c_name][$at->at_id];
            } else {
                $dist = & $options[$at->at_id];
            }
            $dist = sprintf('%s (%s)', $at->name, $at->c_name);
        }

        return $options;
  }
}