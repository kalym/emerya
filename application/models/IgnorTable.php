<?php

class IgnorTable extends Zend_Db_Table {
      protected $_name = 'ignor';
      protected $_primary = 'ignor_id';

      public function replace($data)
      {
          $keys = array();
          $values = array();
          foreach ($data as $key => $value) {
              $keys[] = $this->getAdapter()->quoteIdentifier($key);
              $values[] = $this->getAdapter()->quote($value);
          }
          $sql = "REPLACE ignor
              (" . implode(',', $keys) . ")
              VALUE (" . implode(',', $values) . ")";

          $this->getAdapter()->query($sql);
      }

      public function getUntil($who, $whom)
      {
          $row = $this->fetchRow(
              $this->select()->where('who=?', $who)
                  ->where('whom=?', $whom)
          );

          return $row ? $row->until : null;

      }
      
      public function isIgnored($who, $whom) 
      {
          return date('Y-m-d H:i:s') < $this->getUntil($who, $whom);
      }

}
