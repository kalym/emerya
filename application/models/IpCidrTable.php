<?php

class IpCidrTable extends Zend_Db_Table {
  protected $_name     = 'ip_cidr';
  protected $_primary  = 'cidr_id';
  
  function getAddr($ip)
  {
      $record = $this->fetchAll($this->select()
              ->where("begin<=INET_ATON(?)", $ip)
              ->where("end>=INET_ATON(?)", $ip))->current();
      if (!$record) return;
      
      $anCountryTable = new CountryTable();
      $country = $anCountryTable->find($record->country)->current();
      $country = $country->name_ru ? $country->name_ru : $country->name;
      
      if ($record->city_id) {
          $anIpCityTable = new IpCityTable;
          $city = $anIpCityTable->find($record->city_id)->current();
          $city = $city->name;
      } else {
          $city = '';
      }
      
      return array($country, $city);
  }
  
  function getCountry($ip)
  {
      $record = $this->fetchAll($this->select()
              ->where("begin<=INET_ATON(?)", $ip)
              ->where("end>=INET_ATON(?)", $ip))->current();
      if (!$record) return null;
      return $record->country;
  }
}