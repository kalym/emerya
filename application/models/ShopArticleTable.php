<?php

class ShopArticleTable extends Zend_Db_Table {
  protected $_name     = 'shop_article';
  protected $_primary  = 'article_id';
  protected $_rowClass = 'ShopArticle';
}