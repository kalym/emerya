<?php

class DebtsView extends Zend_Db_Table {
  protected $_name     = 'debts';
  protected $_primary  = 'user_id';

  function getTotal() {
      $row = $this->fetchRow(
              $this->select()
                  ->from($this->_name, array('amount'=> new Zend_Db_Expr('SUM(amount)')))
          );

          return $row->amount;
  }
}
