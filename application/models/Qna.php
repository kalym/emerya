<?

class Qna extends Zend_Db_Table_Row {

    /**
     *
     * @return User
     *
     */
    function getUser() {
        $anUsersTable = new UsersTable();

        return $anUsersTable->find($this->user_id)->current();
    }
    
    function delete() {

        $anCommentsTable = new CommentsTable();
        $comments = $anCommentsTable->fetchAll(
                $anCommentsTable->select()
                    ->where('typology=?', CommentsTable::TYPOLOGY_QNA)
                    ->where('typology_id=?', $this->qna_id)
        );
        foreach ($comments as $comment) {
            $comment->delete();
        }
        
        $anFeedTable = new FeedTable;
        $anFeedTable->delete(sprintf("type='%s' AND content_id=%d", FeedTable::TYPE_QNA, $this->qna_id));
        
        return parent::delete();
    }

}