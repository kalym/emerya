<?

class Job extends Zend_Db_Table_Row {
    /**
     *
     * @return User
     *
     */
    function getUser() {
        $anUsersTable = new UsersTable();

        return $anUsersTable->find($this->user_id)->current();
    }
    
    function setAllowed($allowed)
    {
        $this->allowed = implode(';', $allowed);
    }
    
    function getAllowed()
    {
        return explode(';', $this->allowed);
    }
    
    function delete() {
        
        $anCommentsTable = new CommentsTable();
        $comments = $anCommentsTable->fetchAll(
                $anCommentsTable->select()
                        ->where('typology=?', CommentsTable::TYPOLOGY_JOB)
                        ->where('typology_id=?', $this->job_id)
        );
        foreach ($comments as $comment) {
            $comment->delete();
        }
        parent::delete();
    }
}