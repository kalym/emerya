<?php

class StatsTable extends Zend_Db_Table {
  protected $_name     = 'stats';
  protected $_primary  = 'id';
  
  public function replace($data)
      {
          $keys = array();
          $values = array();
          foreach ($data as $key => $value) {
              $keys[] = $this->getAdapter()->quoteIdentifier($key);
              $values[] = $this->getAdapter()->quote($value);
          }
          $sql = "REPLACE stats
              (" . implode(',', $keys) . ")
              VALUE (" . implode(',', $values) . ")";

          $this->getAdapter()->query($sql);
      }
}