<?php

class ShopCatTable extends Zend_Db_Table {

    protected $_name = 'shop_cat';
    protected $_primary = 'cat_id';

    function getOptions($addSelectOffer=false) {
        $cats = $this->fetchAll(
                $this->select()->order('name ASC')
        );
        $options = array();
        if ($addSelectOffer) {
            $options[''] = '-- �������� ������� --';
        }
        foreach ($cats as $cat) {
            $options[$cat->cat_id] = $cat->name;
        }
        return $options;
    }

    public function findByAlias($alias) {
        return $this->fetchAll(
                        $this->select()
                                ->where('alias=?', $alias)
                )->current();
    }

}