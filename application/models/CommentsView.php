<?php

class CommentsView extends Zend_Db_Table {
  protected $_name     = 'comments_view';
  protected $_primary  = 'comment_id';
  protected $_rowClass = 'Comment';
}
