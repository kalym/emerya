<?php

class BanTable extends Zend_Db_Table {
  protected $_name     = 'ban';
  protected $_primary  = 'ban_id';
  
  function getTypeOptions()
  {
      return array(
          'ip' => 'IP',
          'referrer' => 'Referrer'
      );
  }
}