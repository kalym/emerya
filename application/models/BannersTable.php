<?php
class BannersTable extends Zend_Db_Table {
  protected $_name     = 'banners';
  protected $_primary  = 'banner_id';
  const TYPY_BIG = 'big';
  const TYPE_SMALL = 'small';
  const DEVICE_IPHONE = 'iphone';
  const DEVICE_IPAD = 'ipad';

  function getTypeOptions() {
      return array (
          self::TYPY_BIG => '�������',
          self::TYPE_SMALL => '���������'
      );
  }
  
  function getDeviceOptions() {
      return array (
          self::DEVICE_IPAD => 'IPad',
          self::DEVICE_IPHONE => 'IPhone'
      );
  }
}