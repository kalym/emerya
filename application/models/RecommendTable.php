<?php

class RecommendTable extends Zend_Db_Table
{
    protected $_name = 'recommend';
    protected $_primary = 'recommend_id';

    public function replace($data)
    {
        $keys = array();
        $values = array();
        foreach ($data as $key => $value) {
            $keys[] = $this->getAdapter()->quoteIdentifier($key);
            $values[] = $this->getAdapter()->quote($value);
        }
        $sql = "REPLACE recommend
              (" . implode(',', $keys) . ")
              VALUE (" . implode(',', $values) . ")";

        $this->getAdapter()->query($sql);
    }

    public function getCountWhom($user_id)
    {
        $row = $this->fetchRow(
            $this->select()
                ->from('recommend', array('count' => new Zend_Db_Expr('COUNT(recommend_id)')))
                ->where('who_id=?', $user_id)
        );

        return $row->count;
    }

    public function getCountWho($user_id)
    {
        $row = $this->fetchRow(
            $this->select()
                ->from('recommend', array('count' => new Zend_Db_Expr('COUNT(recommend_id)')))
                ->where('whom_id=?', $user_id)
        );

        return $row->count;
    }

    public function getIsRecommend($who_id, $whom_id)
    {
        $row = $this->fetchRow(
            $this->select()
                ->from('recommend', array('count' => new Zend_Db_Expr('COUNT(recommend_id)')))
                ->where('whom_id=?', $whom_id)
                ->where('who_id=?', $who_id)
        );

        return $row->count;
    }
}