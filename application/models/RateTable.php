<?php

class RateTable extends Zend_Db_Table {

    protected $_name = 'rate';
    protected $_primary = 'rate_id';

    public function replace($data) {
        $keys = array();
        $values = array();
        foreach ($data as $key => $value) {
            $keys[] = $this->getAdapter()->quoteIdentifier($key);
            $values[] = $this->getAdapter()->quote($value);
        }
        $sql = "REPLACE rate
              (" . implode(',', $keys) . ")
              VALUE (" . implode(',', $values) . ")";

        $this->getAdapter()->query($sql);
    }

    public function getRateCnt($comment_id, $type = 1) {
        $row = $this->fetchRow(
                $this->select()
                        ->from('rate', array('cnt' => new Zend_Db_Expr('COUNT(rate_id)')))
                        ->where('comment_id=?', $comment_id)
                        ->where('rate=?', $type)
        );
        return $row->cnt;
    }

    public function getRate($comment_id, $user_id) {
        $row = $this->fetchRow(
                $this->select()
                        ->from('rate')
                        ->where('comment_id=?', $comment_id)
                        ->where('user_id=?', $user_id)
        );
        return $row ? $row->rate : 0;
    }

}