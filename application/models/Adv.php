<?php
class Adv extends Zend_Db_Table_Row {    
   function render()
   {
       return sprintf('<noindex>%s</noindex>', $this->prepareLinks($this->html));
   }
   
   function prepareLinks($html)
   {
       return str_replace('%%url%%', '/adv/go/id/' . $this->adv_id, $html);
   }
}