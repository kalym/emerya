<?php

class ArticlesTable extends Zend_Db_Table {
  protected $_name     = 'articles';
  protected $_primary  = 'article_id';
  protected $_rowClass = 'Article';

  public function findByAlias($alias)
  {
        return $this->fetchAll(
            $this->select()
                ->where('alias=?', $alias)
        )->current();
  }
}