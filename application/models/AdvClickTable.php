<?php

class AdvClickTable extends Zend_Db_Table {
  protected $_name     = 'adv_click';
  protected $_primary  = 'adv_click_id';

  function log(Adv $adv)
  {
      $this->insert(array(
          'adv_id' => $adv->adv_id,
          'dattm' => date('Y-m-d H:i:s'),
          'ip' => $_SERVER['REMOTE_ADDR']
      ));
  }
  
}