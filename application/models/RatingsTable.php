<?php

class RatingsTable extends Zend_Db_Table {
      protected $_name = 'ratings';
      protected $_primary = 'rating_id';

      public function replace($data)
      {
          $keys = array();
          $values = array();
          foreach ($data as $key => $value) {
              $keys[] = $this->getAdapter()->quoteIdentifier($key);
              $values[] = $this->getAdapter()->quote($value);
          }
          $sql = "REPLACE ratings
              (" . implode(',', $keys) . ")
              VALUE (" . implode(',', $values) . ")";

          $this->getAdapter()->query($sql);
      }

      public function getRating($article_id)
      {
          $row = $this->fetchRow(
              $this->select()
                  ->from('ratings', array('rate'=> new Zend_Db_Expr('SUM(rate)')))
                  ->where('article_id=?', $article_id)
          );

          return $row->rate;

      }

}