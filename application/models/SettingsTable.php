<?php

class SettingsTable extends Zend_Db_Table {
  protected $_name     = 'settings';
  protected $_primary  = 'setting_id';


  public function replace($data)
  {
      $keys = array();
      $values = array();
      foreach ($data as $key => $value) {
          $keys[] = $this->getAdapter()->quoteIdentifier($key);
          $values[] = $this->getAdapter()->quote($value);
      }
      $sql = "REPLACE " . $this->_name . "
          (" . implode(',', $keys) . ")
          VALUE (" . implode(',', $values) . ")";

      $this->getAdapter()->query($sql);
  }

}