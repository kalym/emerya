<?php

class AT extends Zend_Db_Table_Row {

   public function isBooked($user_id = null) {
       $userCheck = $user_id ? $this->book_user_id == $user_id : true;  
       return ($userCheck && $this->book_due>date('Y-m-d H:i:s'));
   }
}