<?php

class UsersTable extends Zend_Db_Table {
    protected $_name     = 'users';
    protected $_primary  = 'user_id';
    protected $_rowClass = 'User';

    public function replace($data)
    {
      $keys = array();
      $values = array();
      foreach ($data as $key => $value) {
          $keys[] = $this->getAdapter()->quoteIdentifier($key);
          $values[] = $this->getAdapter()->quote($value);
      }
      $sql = "REPLACE " . $this->_name . "
          (" . implode(',', $keys) . ")
          VALUE (" . implode(',', $values) . ")";

      $this->getAdapter()->query($sql);
    }

    public function findByLogin($login) {
        return $this->fetchRow(
              $this->select()
                  ->where('login = ?', $login)
        );
    }

    public function findByEmail($email) {
        return $this->fetchRow(
              $this->select()
                  ->where('email = ?', $email)
        );
    }

}