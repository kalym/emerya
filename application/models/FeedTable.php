<?php

class FeedTable extends Zend_Db_Table {
  protected $_name     = 'feed';
  protected $_primary  = 'feed_id';
  const TYPE_COMMENT_ARTICLE = 10;
  const TYPE_COMMENT_POST = 11;
  const TYPE_COMMENT_QNA = 12;
  const TYPE_POST = 20;
  const TYPE_PURCHASE = 30;
  const TYPE_QNA = 40;
  const TYPE_JOB = 50;
  const TYPE_ADMIN = 60;
  const TYPE_ARTICLE = 70;
  const TYPE_PAYOUT = 80;
  const TYPE_PHOTO = 90;
}