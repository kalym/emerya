<?php

class PaymentsTable extends Zend_Db_Table {
  protected $_name     = 'payments';
  protected $_primary  = 'payment_id';

  const TYPE_INTERNAL = 0;
  const TYPE_EXTERNAL = 1;
  
  const TYPOLOGY_SPONSOR = 'sponsor';
  const TYPOLOGY_ARTICLE = 'article';
  const TYPOLOGY_ARTICLE_VIEW = 'article_view';
  const TYPOLOGY_SHOP_ARTICLE = 'shop_article';

  public function getAmountForType($user_id, $type, $typology=null, $not_paid = false) {

      $select = $this->select()
              ->from($this->_name, array('amount'=>'SUM(amount)'))
              ->where('user_id=?', $user_id)
              ->where('`type`=?', $type);
      if (!is_null($typology)) {
          $select = $select->where('typology=?', $typology);
      }
      
      if ($not_paid) {
          $select = $select->where('is_paid=0');
      }
      
      $row = $this->fetchRow($select);

      return (float)$row->amount;
  }

}
