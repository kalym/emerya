<?php

class PostsTable extends Zend_Db_Table {
  protected $_name     = 'posts';
  protected $_primary  = 'post_id';
  protected $_rowClass = 'Post';
}