<?php

class CatQnATable extends Zend_Db_Table {
  protected $_name     = 'cat_qna';
  protected $_primary  = 'cat_id';
  protected $_rowClass = 'CatQna';

    function getOptions($addSelectOffer=false)
    {
        $cats = $this->fetchAll(
            $this->select()->order('name ASC')
        );
        $options = array();
        if ($addSelectOffer) {
            $options[''] = '-- �������� ������� --';
        }
        foreach ($cats as $cat) {
            $options[$cat->cat_id] = $cat->name;
        }
        return $options;
    }
}