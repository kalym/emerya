<?php

class ArticleLinkTable extends Zend_Db_Table {
  protected $_name     = 'article_link';
  protected $_primary  = 'link_id';
}