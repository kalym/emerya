<?php

class IpCityTable extends Zend_Db_Table {
  protected $_name     = 'ip_city';
  protected $_primary  = 'city_id';
}