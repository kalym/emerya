<?php

class UploadController extends Zend_Controller_Action
{
    function init()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
	        $params = $this->getRequest()->getParams();
        	$params = $this->decode("UTF-8", "windows-1251", $params);
	        $this->getRequest()->setParams($params);
	}
    }
    
    function decode($from, $to, $value) {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->decode($from, $to, $v);
            } 
        } else {
            $value = iconv($from, $to, $value);
        }
        return $value;
    }
    
    function indexAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $params  = $this->getRequest()->getParams();
        $registry = Zend_Registry::getInstance();

        $iu = new ImageUploader($registry->get('root_dir'));

        try {
            $urls = $iu->upload($sizes = array(array(80,80), array(198,170), array(266, 229), array(110, 110)), $original = true);
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return;
        }
        $resp = array();
        $resp['errorCode'] = 0;
        $resp['preview'] = $urls[0];
        $resp['preview2'] = $urls[1];
        $resp['image'] = $urls[2];
        $resp['widget_img'] = $urls[3];
        $resp['original'] = $urls[4];

        $resp = Zend_Json::encode( $resp );

        $this->getResponse()
          ->setHeader('Content-Type', 'application/json; charset=UTF-8')
          ->appendBody($resp);

        return;
    }
    
     function advAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $params  = $this->getRequest()->getParams();
        $registry = Zend_Registry::getInstance();

        $iu = new ImageUploader($registry->get('root_dir'));

        try {
            $urls = $iu->upload($sizes = array(), $original = true);
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return;
        }
        $resp = array();
        $resp['errorCode'] = 0;
        $resp['img'] = $urls[0];

        $resp = Zend_Json::encode( $resp );

        $this->getResponse()
          ->setHeader('Content-Type', 'application/json; charset=UTF-8')
          ->appendBody($resp);

        return;
    }
    
    function uploadAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $params  = $this->getRequest()->getParams();
        $registry = Zend_Registry::getInstance();

        $uploader = new Uploader($registry->get('root_dir'));

        try {
            $url = $uploader->upload();
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return;
        }
        $resp = array();
        $resp['errorCode'] = 0;
        $resp['url'] = $url;

        $resp = Zend_Json::encode( $resp );

        $this->getResponse()
          ->setHeader('Content-Type', 'application/json; charset=UTF-8')
          ->appendBody($resp);
    }
    
    function galleryAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $params  = $this->getRequest()->getParams();
        $registry = Zend_Registry::getInstance();

        $iu = new ImageUploader($registry->get('root_dir'));

        try {
            $urls = $iu->upload($sizes = array(array(130, 99), array(150, 100)), $original = true);
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return;
        }
        
        $resp = array();
        $resp['errorCode'] = 0;
        $resp['preview'] = $urls[0];
        $resp['small'] = $urls[1];
        $resp['big'] = $urls[2];

        $resp = Zend_Json::encode( $resp );

        $this->getResponse()
          //->setHeader('Content-Type', 'application/json; charset=UTF-8')
          ->appendBody($resp);

        return;
    }
    
    function multiGalleryAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $params  = $this->getRequest()->getParams();
        $registry = Zend_Registry::getInstance();

        $iu = new ImageUploader($registry->get('root_dir'));

        try {
            $urls = $iu->upload($sizes = array(array(130, 99), array(150, 100)), $original = true);
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return;
        }
        
        $resp = array();
        $resp['errorCode'] = 0;
        foreach ($urls as $url) {
            $resp['files'][] = array(
                'preview' => $url[0],
                'small' => $url[1],
                'big' => $url[2]
            );
        }

        $resp = Zend_Json::encode( $resp );

        $this->getResponse()
          //->setHeader('Content-Type', 'application/json; charset=UTF-8')
          ->appendBody($resp);

        return;
    }
    
    function postPreviewAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $params  = $this->getRequest()->getParams();
        $registry = Zend_Registry::getInstance();

        $iu = new ImageUploader($registry->get('root_dir'));

        try {
            $urls = $iu->upload($sizes = array(array(180, 116)), $original = false);
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return;
        }
        
        $resp = array();
        $resp['errorCode'] = 0;
        $resp['preview'] = $urls[0];

        $resp = Zend_Json::encode( $resp );

        $this->getResponse()
          //->setHeader('Content-Type', 'application/json; charset=UTF-8')
          ->appendBody($resp);

        return;
    }
    
     function imgGalleryAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $params  = $this->getRequest()->getParams();
        $registry = Zend_Registry::getInstance();

        $iu = new ImageUploader($registry->get('root_dir'));

        try {
            $urls = $iu->upload($sizes = array(array(96, 69)), $original = true);
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return;
        }
        
        $resp = array();
        $resp['errorCode'] = 0;
        $resp['preview'] = $urls[0];
        $resp['orig'] = $urls[1];

        $resp = Zend_Json::encode( $resp );

        $this->getResponse()
          //->setHeader('Content-Type', 'application/json; charset=UTF-8')
          ->appendBody($resp);

        return;
    }
    
     function videoGalleryAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $params  = $this->getRequest()->getParams();
        $registry = Zend_Registry::getInstance();

        $iu = new ImageUploader($registry->get('root_dir'));

        try {
            $urls = $iu->upload($sizes = array(array(80, 62), array(96, 69)), $original = false);
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return;
        }
        
        $resp = array();
        $resp['errorCode'] = 0;
        $resp['thumbnail'] = $urls[0];
        $resp['preview'] = $urls[1];

        $resp = Zend_Json::encode( $resp );

        $this->getResponse()
          //->setHeader('Content-Type', 'application/json; charset=UTF-8')
          ->appendBody($resp);

        return;
    }
    
    function photoAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $params  = $this->getRequest()->getParams();
        $registry = Zend_Registry::getInstance();

        $iu = new ImageUploader($registry->get('root_dir'));

        try {
            $urls = $iu->upload($sizes = array(array(130, 99), array(150, 100), array(
                'type' => ImageUploader::RESIZE_TYPE_MATCH_HEIGHT,
                'width' => 800, 
                'height' => 800)), $original = true);
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return;
        }
        
        $resp = array();
        $resp['errorCode'] = 0;
        $resp['preview'] = $urls[0];
        $resp['small'] = $urls[1];
        $resp['big'] = $urls[2];
        $resp['original'] = $urls[3];

        $resp = Zend_Json::encode( $resp );

        $this->getResponse()
          //->setHeader('Content-Type', 'application/json; charset=UTF-8')
          ->appendBody($resp);

        return;
    }
    
    function avatarAction()
    {
        
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        /* @var $user User */
        if ($user = Zend_Auth::getInstance()->getIdentity()) {
            $params  = $this->getRequest()->getParams();
            $registry = Zend_Registry::getInstance();

            $iu = new ImageUploader($registry->get('root_dir'));

            try {
                $urls = $iu->upload($sizes = array(array(93, 93), array(111, 142)), $original = true);
            } catch (Exception $e) {
                $this->error($e->getMessage());
                return;
            }

            $resp = array();
            $resp['errorCode'] = 0;
            $resp['avatar'] = $urls[0];
            $resp['avatar_big'] = $urls[1];
            $resp['avatar_orig'] = $urls[2];
            $resp['msg'] = iconv("windows-1251", "UTF-8", '��� ������ ������� �������!');

            $user->setTable(new UsersTable);
            
            $user->avatar = $urls[0];
            $user->avatar_big = $urls[1];
            $user->avatar_orig = $urls[2];
            $user->save();
            
                     
            $resp = Zend_Json::encode( $resp );

            $this->getResponse()
              ->setHeader('Content-Type', 'application/json; charset=UTF-8')
              ->appendBody($resp);
        }
        return;
    }
    
    protected function error($msg) {
        $resp['errorCode'] = 1;
        $resp['msg'] = iconv("windows-1251", "UTF-8", $msg);
        $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

        $this->_helper->viewRenderer->setNoRender();
        $resp = Zend_Json::encode($resp);

        $this->getResponse()
                ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                ->appendBody($resp);
    }
}