<?php

class GoldshopController extends Zend_Controller_Action
{
    function indexAction()
    {
        Zend_Layout::getMvcInstance()->setLayout('layout-wide');
        
        $anShopArticleView = new ShopArticleView;     
        $this->view->items = $anShopArticleView->fetchAll(
                $anShopArticleView->select()
                    ->where('u_rating>=?', Status::getThreshold(Status::PRO))
                    ->where('type=?', ShopArticle::TYPE_COPYWRITING)
                    ->where('is_approved=1')
                    ->where('owner_id=0')
                    ->order('date DESC')
                );
        
    }

}