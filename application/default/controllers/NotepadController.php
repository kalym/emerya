<?php

class NotepadController extends Zend_Controller_Action
{

    function preDispatch()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            Zend_Layout::getMvcInstance()->disableLayout();
            $params = $this->getRequest()->getParams();
            $params = $this->decode("UTF-8", "windows-1251", $params);
            $this->getRequest()->setParams($params);
            $_POST = $this->decode("UTF-8", "windows-1251", $_POST);
            $_GET = $this->decode("UTF-8", "windows-1251", $_GET);
        }
    }

    function decode($from, $to, $value)
    {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->decode($from, $to, $v);
            }
        } else {
            $value = iconv($from, $to, $value);
        }
        return $value;
    }

    function indexAction()
    {
        if ($this->_helper->auth->requireLogin()) {
            return;
        }

        $anNotepadTable = new NotepadTable;
        $this->view->user = $user = Zend_Auth::getInstance()->getIdentity();

        $select = $anNotepadTable->select()
            ->from('notepad')
            ->joinLeft('articles_view', 'articles_view.article_id=notepad.article_id')
            ->where('notepad.user_id=?', $user->user_id)
            ->setIntegrityCheck(false)
            ->order('date DESC');

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_articles_perpage', 20));
        $paginator->setPageRange(10);

        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->view->paginator = $paginator;
        } else {
            $this->_helper->viewRenderer->setNoRender(true);
            $this->_helper->layout()->disableLayout();
            $paginator->setItemCountPerPage(Site_Settings::get('front_articles_perpage', 20));
            $this->view->paginator = $paginator;

            $paginator->setItemCountPerPage($this->getRequest()->getParam('p', 1) * Site_Settings::get('front_articles_perpage', 20));
            $this->view->paginatorNew = $paginator;
            $this->view->currentPage = $this->getRequest()->getParam('p', 1);

            echo $this->view->render('notepad/articles.phtml');
        }
    }

    function addAction()
    {
        $this->redirectIfNotAjax();
        if ($this->_helper->auth->requireLogin()) {
            return;
        }
        $user = Zend_Auth::getInstance()->getIdentity();
        $anNotepadTable = new NotepadTable;
        $anNotepadTable->replace(array(
            'user_id' => $user->user_id,
            'article_id' => $this->getRequest()->getParam('id')
        ));
        $resp = array();
        $resp['errorCode'] = 0;
        $resp['msg'] = iconv("windows-1251", "UTF-8", '������ ��������� � ��� �������');
        $resp = Zend_Json::encode($resp);
        $this->_helper->viewRenderer->setNoRender();
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json; charset=UTF-8')
            ->appendBody($resp);

    }

    function removeAction()
    {
        $this->redirectIfNotAjax();
        if ($this->_helper->auth->requireLogin()) {
            return;
        }
        $user = Zend_Auth::getInstance()->getIdentity();
        $anNotepadTable = new NotepadTable;
        $anNotepadTable->delete(sprintf('user_id=%d AND article_id=%d',
            $user->user_id,
            $this->getRequest()->getParam('id')));
        $resp = array();
        $resp['errorCode'] = 0;
        $resp['msg'] = iconv("windows-1251", "UTF-8", '������ ������� �� ������ ��������');
        $resp = Zend_Json::encode($resp);
        $this->_helper->viewRenderer->setNoRender();
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json; charset=UTF-8')
            ->appendBody($resp);

    }

    protected function error($msg)
    {
        $resp['errorCode'] = 1;
        $resp['msg'] = iconv("windows-1251", "UTF-8", $msg);
        $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

        $this->_helper->viewRenderer->setNoRender();
        $resp = Zend_Json::encode($resp);

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json; charset=UTF-8')
            ->appendBody($resp);
    }

    protected function redirectIfNotAjax()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->_redirect('/');
        }
    }

}