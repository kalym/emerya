<?php

class AdvController extends Zend_Controller_Action {
    function goAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $anAdvTable = new AdvTable();
        $adv = $anAdvTable->find($this->getRequest()->getParam('id'))->current();
        
        if (!$adv) {
            Zend_Layout::getMvcInstance()->enableLayout();
            throw new Zend_Controller_Dispatcher_Exception('');
        }
        
        $anAdvClickTable = new AdvClickTable();
        $anAdvClickTable->log($adv);
        
        $this->_redirect($adv->url);
    }
}
