<?php

class Form_Shop extends Zend_Form {

    function init() {
        $this->setAction('/shop/add')
                ->setMethod(Zend_Form::METHOD_POST)
                ->setAttrib('encode', Zend_Form::ENCTYPE_MULTIPART)
                ->setAttrib('id', 'articles-add');

        $this->addElement('text', 'name', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '���������'
        ));

        $name = $this->getElement('name');

        $name->setRequired(true);
        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('��������� ������ ����������� ��� ����������');
        $name->addValidator($validator);


        $this->addElement('textarea', 'desc', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '������� �������� ������'
        ));

        $desc = $this->getElement('desc');

        $desc->setRequired(true);
        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('��������� ���� ������� �������� ������');
        $desc->addValidator($validator);

        $this->addElement('radio', 'type', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '��� ����������',
            'escape' => false
        ));

        $type = $this->getElement('type');

        $type->setSeparator(' ')->setMultiOptions(array(
            ShopArticle::TYPE_COPYWRITING => '<img src="/public/images/article-copywriting.png" />',
            ShopArticle::TYPE_REWRITING => '<img src="/public/images/article-rewriting.png" />',
            ShopArticle::TYPE_TRANSLATION => '<img src="/public/images/article-translation.png" />'
        ));

        $type->setRequired(true);
        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('�������� ��� ����������');
        $type->addValidator($validator);

        $this->addElement('textarea', 'content', array(
            'required' => true,
            'label' => '����� ����������',
            'class' => 'big-textarea'
        ));

        $content = $this->getElement('content');


        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('����������, ������� ����� ����������');
        $content->addValidator($validator);

        $this->addElement('select', 'cat_id', array(
            'label' => '�������'
        ));

        $cat_id = $this->getElement('cat_id');

        $anShopCatTable = new ShopCatTable();

        $cat_id->setMultiOptions($anShopCatTable->getOptions(true));

        $cat_id->setRequired(true);
        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('����������, �������� �������');
        $cat_id->addValidator($validator);

        $this->addElement('text', 'price', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '���������'
        ));

        $price = $this->getElement('price');

        $price->setRequired(true);
        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� ������ ������������ ��� ����������');
        $price->addValidator($validator);

        $validator = new Zend_Validate_Int();
        $validator->setMessage('������� ���������� �������� ����, ��� ������');
        $price->addValidator($validator);

        $validator = new Zend_Validate_GreaterThan(0);
        $validator->setMessage('������� ������������� ����');
        $price->addValidator($validator);
    }

}

class ShopController extends Zend_Controller_Action {

    protected $images = array();

    function preDispatch() {
        if ($this->getRequest()->isXmlHttpRequest()) {
            Zend_Layout::getMvcInstance()->disableLayout();
            $params = $this->getRequest()->getParams();
            $params = $this->decode("UTF-8", "windows-1251", $params);
            $this->getRequest()->setParams($params);
            $_POST = $this->decode("UTF-8", "windows-1251", $_POST);
            $_GET = $this->decode("UTF-8", "windows-1251", $_GET);
        } else {
            $this->_helper->menu->select('shop');
        }
    }

    function decode($from, $to, $value) {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->decode($from, $to, $v);
            }
        } else {
            $value = iconv($from, $to, $value);
        }
        return $value;
    }

    protected function redirectIfNotAjax() {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->_redirect('/');
        }
    }

    protected function error($msg) {
        $resp['errorCode'] = 1;
        $resp['msg'] = iconv("windows-1251", "UTF-8", $msg);
        $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

        $this->_helper->viewRenderer->setNoRender();
        $resp = Zend_Json::encode($resp);

        $this->getResponse()
                ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                ->appendBody($resp);
    }

    protected function getErrors($messages) {
        $result = array();
        foreach ($messages as $msg) {
            $result = array_merge($result, array_values($msg));
        }

        return $result;
    }

    function indexAction() {
        if ($this->getRequest()->isPost()) {
            $this->_redirect(
                    Zend_Controller_Front::getInstance()->getRouter()->assemble(array(
                        'q' => $this->getRequest()->getPost('q'),
                        'p_from' => $this->getRequest()->getPost('p_from'),
                        'p_to' => $this->getRequest()->getPost('p_to')
                    ))
            );
        }

        $anShopArticleView = new ShopArticleView;
        $anCatsTable = new ShopCatTable;

        $cat = $anCatsTable->find(
                        $this->getRequest()->getParam('id')
                )->current();

        $select = $anShopArticleView->select()
                ->where('is_approved=?', 1)
                ->where('owner_id=0')
                ->order('date DESC');

        if ($cat && !$this->getRequest()->getParam('from')) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        if ($cat) {
            $this->view->cat = $cat;
            $select = $select->where('cat_id=?', $cat->cat_id);
        }

        if ($type = $this->getRequest()->getParam('type')) {
            $select = $select->where('type=?', $type);
        }

        if ($a = $this->getRequest()->getParam('a')) {
            list($start, $stop) = Status::getInterval($a);
            $select = $select->where(sprintf('u_rating BETWEEN %d AND %d', $start, $stop));
        }

        if ($p_from = $this->getRequest()->getParam('p_from')) {
            $select = $select->where('price>?', floor(($p_from * 100) / (100 + Site_Settings::get('article_commission'))));
        }

        if ($p_to = $this->getRequest()->getParam('p_to')) {
            $select = $select->where('price<?', ceil(($p_to * 100) / (100 + Site_Settings::get('article_commission'))));
        }

        if ($q = $this->getRequest()->getParam('q')) {
            $q = $anShopArticleView->getAdapter()->quote('%' . $q . '%');
            $select = $select->where(sprintf('content LIKE %1$s OR name LIKE %1$s', $q));
        }


        $paginator = new Zend_Paginator(
                        new Zend_Paginator_Adapter_DbTableSelect(
                                $select
                        )
        );
        $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_articles_perpage', 36));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }

    function userAction() {
        $anShopArticleView = new ShopArticleView;
        $id = $this->getRequest()->getParam('id');

        $anUserTable = new UsersTable;

        $user = $anUserTable->find($id)->current();

        if (!$user) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $this->view->user = $user;

        $select = $anShopArticleView->select()
                ->where('owner_id=0')
                ->where('user_id=?', $id)
                ->order('date DESC');

        if (Zend_Auth::getInstance()->hasIdentity()
                && Zend_Auth::getInstance()->getIdentity()->user_id == $this->getRequest()->getParam('id')) {
            $select = $select->where('is_approved=1 OR is_rework=1');
        } else {
            $select = $select->where('is_approved=1');
        }

        $paginator = new Zend_Paginator(
                        new Zend_Paginator_Adapter_DbTableSelect(
                                $select
                        )
        );
        $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_articles_perpage', 36));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }

    function addAction() {
        if ($this->_helper->auth->requireLogin(true)) {
            return;
        }


        $form = new Form_Shop;

        $anShopArticleTable = new ShopArticleTable();
        $article = $anShopArticleTable->fetchNew();
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $prev_article = $anShopArticleTable->find($id)->current();
            if ($prev_article &&
                    !$prev_article->is_approved &&
                    $prev_article->user_id == Zend_Auth::getInstance()->getIdentity()->user_id) {

                if ($this->getRequest()->isGet()) {
                    $form->populate($prev_article->toArray());
                    $this->images = array(
                        0 => $prev_article->preview,
                        1 => $prev_article->image,
                        2 => $prev_article->original
                    );
                }

                $article = $prev_article;
            }
        }

        $this->view->article = $article;

        $uploadError = $this->uploadImage();

        if (!$uploadError && $this->getRequest()->isPost() && !$this->getImage()) {
            $uploadError = '�� �� ��������� ��������';
        }

        if ($this->getRequest()->isPost() && $form->isValid(array_merge($form->getValues(true), $_POST)) && !$uploadError) {

            $article->name = $form->getValue('name');
            $article->content = $form->getValue('content');
            $article->cat_id = $form->getValue('cat_id');
            $article->desc = $form->getValue('desc');
            $article->type = $form->getValue('type');
            $article->price = $form->getValue('price');
            $article->image = $this->getImage();
            $article->preview = $this->getImagePreview();
            $article->original = $this->getImageOriginal();
            $article->date = $article->date ? $article->date : date('Y-m-d H:i:s');
            $article->user_id = Zend_Auth::getInstance()->getIdentity()->user_id;
            $article->is_approved = 0;
            $article->is_rework = 0;
            $article->save();
        } else {
            $this->view->image = $this->getImage();
            $this->view->imagePreview = $this->getImagePreview();
            $this->view->imageOriginal = $this->getImageOriginal();
            $this->view->uploadError = $uploadError;
            $this->view->form = $form;
        }
    }

    public function orderAction() {
        $this->redirectIfNotAjax();

        //check if user is logged in in template @see shop/order.phtml

        $anShopArticleView = new ShopArticleView;
        $item = $anShopArticleView->find($this->getRequest()->getParam('id'))->current();
        if (!$item) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $anUserTable = new UsersTable();
        $this->view->user = $anUserTable->find($item->user_id)->current();
        $this->view->item = $item;
    }

    function doOrderAction() {
        $this->redirectIfNotAjax();

        if ($this->_helper->auth->requireLogin(User::TYPE_EMPLOYER)) {
            return;
        }

        $anShopArticleTable = new ShopArticleTable;
        $item = $anShopArticleTable->find($this->getRequest()->getParam('id'))->current();

        if ($item && !$item->owner_id) {
            if ($this->getRequest()->isPost()) {
                switch ($this->getRequest()->getParam('type')) {
                    case 'pay' :
                        $resp = $this->orderPay($item);
                        break;
                    case 'account':
                        $resp = $this->orderAccount($item);
                        break;
                    default :
                        die();
                }
            } else {
                die();
            }
        } else {
            $resp = array(
                'errorCode' => 1,
                'msg' => ($item && $item->owner_id) ? '��� ������ ��� ��������, ��������, ����������, ������� ������ ��� �������' : '�� ������ ������'
            );
        }

        $resp = Zend_Json::encode($this->decode('windows-1251', 'UTF-8', $resp));
        $this->_helper->viewRenderer->setNoRender();
        $this->getResponse()
                ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                ->appendBody($resp);
    }

    function orderPay($item) {
        $anPaymentTable = new PaymentTable;
        $payment = $anPaymentTable->fetchNew();

        $payment->user_id = Zend_Auth::getInstance()->getIdentity()->user_id;
        $payment->amount = $item->getPrice();
        $payment->tm_added = date('Y-m-d h:i:s');
        $payment->completed = 0;
        $payment->ip = $_SERVER['REMOTE_ADDR'];
        $payment->service = 'article';
        $payment->id = $item->article_id;

        $payment->save();

        $resp = array();
        $resp['errorCode'] = 0;
        $resp['payment_id'] = $payment->payment_id;
        $resp['amount'] = $payment->amount;

        return $resp;
    }

    function orderAccount($item) {
            $user = Zend_Auth::getInstance()->getIdentity();
            if ($user->account > $item->getPrice()) {
                $user->account -= $item->getPrice();
                $user->activateServiceArticle(date('Y-m-d H:i:s'), array(
                    'id' => $item->article_id
                ));
                $user->save();
                $resp['errorCode'] = 0;
            } else {
                $resp['errorCode'] = 1;
                $resp['msg'] = '� ��� �� ���������� ������� �� �����';
            }
            
            return $resp;
    }

    function itemAction() {
        if ($this->_helper->auth->requireLogin(array(User::TYPE_EMPLOYER))) {
            return;
        }

        $anShopArticleView = new ShopArticleView;
        $id = $this->getRequest()->getParam('id');

        if (!$id) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $item = $anShopArticleView->find($id)->current();

        if (!$item || $item->owner_id != Zend_Auth::getInstance()->getIdentity()->user_id) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $this->view->item = $item;
    }

    protected function _getImage($num) {
        if (isset($this->images[$num]) && $this->images[$num]) {
            return $this->images[$num];
        } elseif (isset($_POST['images'][$num]) && $_POST['images'][$num]) {
            return $_POST['images'][$num];
        } else {
            return '';
        }
    }

    protected function getImage() {
        return $this->_getImage(1);
    }

    protected function getImagePreview() {
        return $this->_getImage(0);
    }

    protected function getImageOriginal() {
        return $this->_getImage(2);
    }

    protected function uploadImage() {
        if ($this->getRequest()->isPost() &&
                isset($_FILES['image']['name']) &&
                $_FILES['image']['name']) {

            $imUploader = new ImageUploader(Zend_Registry::getInstance()->get('root_dir'), 'image');

            try {
                $this->images = $imUploader->upload(array(array(80, 80), array(266, 229)), true);
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }

        return '';
    }

}