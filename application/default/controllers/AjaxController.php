<?php

class AjaxController extends Zend_Controller_Action
{
    public function preDispatch()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            throw new Zend_Controller_Dispatcher_Exception('');
        } else {
            $this->getResponse()->setHeader('Content-type', 'text/plain')
                ->setHeader('Cache-Control', 'no-cache');
            $this->_helper->viewRenderer->setNoRender(true);
            $this->_helper->layout()->disableLayout();
        }
    }

    public function articlesAction()
    {
        $anArticlesView = new ArticlesView();
        $select = $anArticlesView->select();
        $select = $select->where('is_approved=?', 1);
        $this->view->header = 'Публикации';
        $this->view->route = 'article-index';
        $this->view->displayPopular = true;
        $perpage = Config::get('index_article_perpage', 24);
        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select->order('date DESC')
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage($perpage);
        $this->view->paginator = $paginator;

        echo $this->view->render('ajax/article.phtml');
    }

    public function draftAction()
    {
        $anArticlesView = new ArticlesView();
        $select = $anArticlesView->select();
        $select = $select->where('is_approved=?', 0);
        $this->view->header = 'Публикации';
        $this->view->route = 'article-index';
        $this->view->displayPopular = true;
        $perpage = Config::get('index_article_perpage', 24);
        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select->order('date DESC')
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage($perpage);
        $this->view->paginator = $paginator;

        echo $this->view->render('ajax/article2.phtml');
    }

    public function articlecategoryAction()
    {
        $anArticlesView = new ArticlesView();
        $category_id = $this->getRequest()->getParam('c', 1);
        $select = $anArticlesView->select()
            ->where('is_approved=?', 1)
            ->where('cat_id=?', $category_id)
            ->order('date DESC');
        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('cat_article_perpage', 36));
        $this->view->paginator = $paginator;

        echo $this->view->render('ajax/article.phtml');
    }

    public function usersAction()
    {
        $anUsersTable = new UsersTable();

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $anUsersTable->select()
                    ->where('is_locked=0')
                    ->where('type=?', User::TYPE_WRITER)
                    ->order('rating DESC')
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('users_item_perpage', 50));
        $this->view->paginator = $paginator;

        echo $this->view->render('ajax/users.phtml');
    }

    public function questionsAction()
    {
        $anQnaTable = new QnaView;
        $select = $anQnaTable->select()
            ->where('is_approved=1')
            ->order('datetime DESC');
        $category_id = $this->getRequest()->getParam('c');
        if ($category_id > 0) {
            $select = $select->where('cat_id=?', $category_id);
        }
        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(5);
        $this->view->paginator = $paginator;

        echo $this->view->render('ajax/questions.phtml');
    }

    public function popularAction()
    {
        $frontendOptions = array(
            'lifetime' => 3600 * 24 * 7, // cache lifetime of 1 week
            'automatic_serialization' => true
        );

        $backendOptions = array(
            'cache_dir' => Zend_Registry::getInstance()->get('root_dir') . '/tmp/' // Directory where to put the cache files
        );

        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
        if (!($popular_article_ids = $cache->load('popular_article_ids'))) {
            $popular_article_ids = array();
            $anPopularView = new PopularView();
            foreach ($anPopularView->fetchAll($anPopularView->select()) as $item) {
                $popular_article_ids[] = $item->article_id;
            }
            $cache->save($popular_article_ids, 'popular_article_ids');
        }
        $anArtticleView = new ArticlesView;
        $select = $anArtticleView->select()->where('article_id IN (?)', $popular_article_ids);
        $select = $select->order('next_sunday DESC');

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select->order('date DESC')
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('popular_item_perpage', 8));
        $this->view->paginator = $paginator;

        echo $this->view->render('ajax/popular.phtml');
    }

    public function podcastsAction()
    {
        $anArticlesView = new ArticlesView();
        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $anArticlesView->select()
                    ->where('is_approved=?', 1)
                    ->where('podcast<>?', '')
                    ->order('date DESC')
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_podcasts_perpage', 12));
        $this->view->paginator = $paginator;

        echo $this->view->render('ajax/podcasts.phtml');
    }

    public function searchAction()
    {
        $q = $this->getRequest()->getParam('q');
        $anArticlesView = new ArticlesView();

        $select = $anArticlesView->select()
//            ->where('content LIKE ?', '%' . $q . '%')
            ->where('name LIKE ?', '%' . $q . '%')
            ->order('date DESC');
//            ->order(new Zend_Db_Expr(sprintf('name LIKE %s DESC',
//                $anArticlesView->getAdapter()->quote('%' . $q . '%')
//            )));

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_articles_perpage', 12));
        $p = $this->getRequest()->getParam('p', 1);
        $this->view->paginator = $paginator;
        $this->view->isFirst = ($p === 1);
        $this->view->q = $q;

        echo $this->view->render('ajax/search.phtml');
    }

    public function userarticlesAction()
    {
        $user_id = $this->getRequest()->getParam('u_id');
        $anArticlesView = new ArticlesView();
        $select = $anArticlesView->select()
            ->where('user_id=?', $user_id)
            ->order('date DESC');
        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(10);
        $this->view->paginator = $paginator;

        echo $this->view->render('ajax/article.phtml');
    }

    public function usernotepadAction()
    {
        $user_id = $this->getRequest()->getParam('u_id');
        $anNotepadTable = new NotepadTable;
        $select = $anNotepadTable->select()
            ->from('notepad')
            ->joinLeft('articles_view', 'articles_view.article_id=notepad.article_id')
            ->where('notepad.user_id=?', $user_id)
            ->setIntegrityCheck(false)
            ->order('date DESC');
        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(10);
        $this->view->paginator = $paginator;

        echo $this->view->render('ajax/article.phtml');
    }

    public function currentuserquestionsAction()
    {
        $user = Zend_Auth::getInstance()->getIdentity();

        $anQnaTable = new QnaView;
        $select = $anQnaTable->select()
            ->where('is_approved=1')
            ->order('datetime DESC')
            ->where('user_id=?', $user->user_id);

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('qna_item_perpage', 5));
        $this->view->paginator = $paginator;

        echo $this->view->render('ajax/questions.phtml');
    }

    public function anotherarticlesAction()
    {
        $category_id = $this->getRequest()->getParam('c', 1);
        $article_id = $this->getRequest()->getParam('a', 1);
        $p = $this->getRequest()->getParam('p', 1);

        $anArticlesView = new ArticlesView();
        $select = $anArticlesView->select()
            ->where('cat_id=?', $category_id)
            ->where('article_id<>?', $article_id)
            ->where('is_approved=?', 1)
            ->order('date DESC');
        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $p
        );
        $paginator->setItemCountPerPage(10);
        $this->view->paginator = $paginator;

        echo $this->view->render('ajax/article.phtml');
    }

    public function commentsAction()
    {
        $commentsProcessor = new CommentsProcessor(
            $this->getRequest()->getParam('id'),
            $this->getRequest()->getParam('t')
        );
        $commentsProcessor->page = $this->getRequest()->getParam('p', 1);
        $commentsProcessor->run($this->getRequest());
        $this->view->paginator = $commentsProcessor->getComments();
        $this->view->allowRate = $commentsProcessor->allowRate();
        $this->view->item = $commentsProcessor->getItem();

        echo $this->view->render('ajax/comments.phtml');
    }

    public function listAction()
    {
        $anArticlesView = new ArticlesView();
        $select = $anArticlesView->select()
            ->where('is_approved=?', 1)
            ->order('date DESC');
        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbTableSelect($select));
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('cat_article_perpage', 36));
        $this->view->paginator = $paginator;

        echo $this->view->render('ajax/popularList.phtml');
    }

    public function addAction()
    {
        $anQnaTable = new QnaView();
        $select = $anQnaTable->select()
            ->where('is_approved=1')
            ->where('cat_id=?', $this->getRequest()->getParam('c', 1))
            ->where('qna_id<?', $this->getRequest()->getParam('q', 1))
            ->order('datetime DESC');
        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbTableSelect($select));
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(5);
        $this->view->add = $paginator;

        echo $this->view->render('ajax/add.phtml');
    }

    public function popularlistAction()
    {
        $frontendOptions = array(
            'lifetime' => 3600 * 24 * 7, // cache lifetime of 1 week
            'automatic_serialization' => true
        );

        $backendOptions = array(
            'cache_dir' => Zend_Registry::getInstance()->get('root_dir') . '/tmp/' // Directory where to put the cache files
        );

        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
        if (!($popular_article_ids = $cache->load('popular_article_ids'))) {
            $popular_article_ids = array();
            $anPopularView = new PopularView();
            foreach ($anPopularView->fetchAll($anPopularView->select()) as $item) {
                $popular_article_ids[] = $item->article_id;
            }
            $cache->save($popular_article_ids, 'popular_article_ids');
        }
        $anArtticleView = new ArticlesView;
        $select = $anArtticleView->select()->where('article_id IN (?)', $popular_article_ids);
        $select = $select->order('next_sunday DESC');

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select->order('date DESC')
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('cat_article_perpage', 36));
        $this->view->paginator = $paginator;

        echo $this->view->render('ajax/popularList.phtml');
    }

    public function userviewstatAction()
    {
        if ($this->_helper->auth->requireLogin(array(User::TYPE_WRITER))) {
            echo '';
            return;
        }

        $user = Zend_Auth::getInstance()->getIdentity();
        $this->view->user = $user;

        $user->setTable(new UsersTable());

        if (!$user->stat_i_agree && !$this->getRequest()->isPost()) {
            $anPagesTable = new PagesTable;
            $this->view->page = $anPagesTable->find(13)->current();
            return;
        } elseif ($this->getRequest()->isPost()) {
            $user->stat_i_agree = 1;
            $user->save();
        }

        $anArticlesTable = new ArticlesView();

        $price_ru = Site_Settings::get('ppv_price_RU');
        $price_ua = Site_Settings::get('ppv_price_UA');
        $price_by = Site_Settings::get('ppv_price_BY');

        $select = $anArticlesTable->select()
            ->from('articles_view')
            ->setIntegrityCheck(false)
            ->joinLeft('article_view', 'articles_view.article_id=article_view.article_id', array(
                'view_cnt_unique' => new Zend_Db_Expr('COUNT(IF(article_view.is_unique, view_id, NULL))'),
                'view_cnt' => new Zend_Db_Expr('COUNT(view_id)'),
                'income' => new Zend_Db_Expr("SUM(IF(article_view.is_unique, CASE
                        WHEN article_view.country = 'RU' THEN $price_ru
                        WHEN article_view.country = 'UA' THEN $price_ua
                        WHEN article_view.country =  'BY' THEN $price_by
                    END, 0))")
            ))
            ->where('articles_view.user_id=?', $user->user_id)
            ->where('stat_begin IS NOT NULL')
            ->group('articles_view.article_id')
            ->order('date DESC');

        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);

        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('posts_item_perpage', 5));
        $this->view->paginator = $paginator;

        echo $this->view->render('ajax/view-stat.phtml');
    }

    public function bookingAction()
    {
        if ($this->_helper->auth->requireLogin(true)) {
            return;
        }
        $anATView = new ATView();
        $anCatsTable = new CatsTable();

        $cat = $anCatsTable->find(
            $this->getRequest()->getParam('id')
        )->current();

        $user_id = Zend_Auth::getInstance()->getIdentity()->user_id;
        $user = Zend_Auth::getInstance()->getIdentity();

        $ids = array();
        if (!$user->haveAprovedArticles()) {
            $records = $anATView->fetchAll($anATView->select()
                ->where('is_active=?', 1)
                ->order(new Zend_Db_Expr("book_user_id = $user_id DESC"))
//                ->limit(User::BOOK_VIEW_LIMIT)
            );
            foreach ($records as $rec) {
                $ids[] = $rec->at_id;
            }
        }

        $select = $anATView->select()
            ->where('is_active=?', 1)
            ->order(new Zend_Db_Expr("book_user_id = $user_id DESC, name ASC"));

        $this->view->active_cat_id = null;

        if ($cat) {
            $select = $select->where('cat_id=?', $cat->cat_id);
            $this->view->active_cat_id = $cat->cat_id;
        }

        if ($ids) {
            $select->where('at_id IN (?)', $ids);
        }

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );

        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
        $paginator->setItemCountPerPage(Site_Settings::get('front_at_perpage', 10));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
        $this->view->user = Zend_Auth::getInstance()->getIdentity();

        echo $this->view->render('ajax/booking.phtml');
    }

    public function currentuserarticlesAction()
    {
        $user = Zend_Auth::getInstance()->getIdentity();

        if (!$user) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $anArticlesView = new ArticlesView();
        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $anArticlesView->select()
                    ->where('user_id=?', $user->user_id)
                    ->order('is_rework DESC')
                    ->order('is_approved ASC')
                    ->order('date DESC')
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_user_articles_perpage', 20));
        $this->view->paginator = $paginator;

        echo $this->view->render('ajax/userarticles.phtml');
    }

    public function currentusernotepadAction()
    {
        $user = Zend_Auth::getInstance()->getIdentity();

        if (!$user) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $anNotepadTable = new NotepadTable;
        $select = $anNotepadTable->select()
            ->from('notepad')
            ->joinLeft('articles_view', 'articles_view.article_id=notepad.article_id')
            ->where('notepad.user_id=?', $user->user_id)
            ->setIntegrityCheck(false)
            ->order('date DESC');

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_articles_perpage', 20));
        $this->view->paginator = $paginator;

        echo $this->view->render('ajax/usernotepad.phtml');
    }

    public function userfeedAction()
    {
        $user = Zend_Auth::getInstance()->getIdentity();

        if (!$user) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }
        $anFeedTable = new FeedView();

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $anFeedTable->select()
                    ->where('user_id=?', Zend_Auth::getInstance()->getIdentity()->user_id)
                    ->order('datetime DESC')
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('feed_item_perpage', 100));
        $this->view->paginator = $paginator;
        $anFeedTable->update(array(
            'is_new' => 0,
        ), array(
            'user_id=' . Zend_Auth::getInstance()->getIdentity()->user_id,
            'is_new=1'));

        echo $this->view->render('ajax/userfeed.phtml');
    }

    public function usermessagesAction()
    {
        $anMessagesView = new MsgTable();
        $selectLatestConvertation = $anMessagesView->select()
            ->from('message')
            ->setIntegrityCheck(false)
            ->joinLeft('users', 'message.sender_id=users.user_id', array(
                'user' => '*'
            ))
            ->where('(sender_id=? AND recipient_id=\'' . $this->getRequest()->getParam('uid', 1) . '\') 
            OR (sender_id=\'' . $this->getRequest()->getParam('uid', 1) . '\' AND recipient_id=?)', $this->getRequest()->getParam('rid', 1))
            ->group(array('message.message_id'))
            ->order('datetime ASC');
        $this->view->latestMessages = $anMessagesView->fetchAll($selectLatestConvertation);
        $this->view->receiverId = $this->getRequest()->getParam('rid', 1);

        echo $this->view->render('ajax/usermessages.phtml');
    }
}


