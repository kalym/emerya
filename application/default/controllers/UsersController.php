<?php

class Form_Share extends Zend_Form
{

    function init()
    {
        $this->setAction('/users/share')
            ->setMethod(Zend_Form::METHOD_POST)
            ->setAttrib('class', 'ajax')
            ->setAttrib('id', 'share');

        $this->addElement('hidden', 'user_id');
        $user_id = $this->getElement('user_id');
        $user_id->setDecorators(array('ViewHelper'));

        $this->addElement('text', 'email', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => 'E-mail �����'
        ));

        $email = $this->getElement('email');

        $email->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        ))));

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� E-mail ����������� ��� ����������');
        $email->addValidator($validator);

        $validator = new Zend_Validate_EmailAddress();
        $validator->setMessage('�� ������ ������ E-mail');
        $email->addValidator($validator);

        $this->setDecorators(array('FormElements', array('Callback', array(
            'callback' => array($this, 'appendSubmit')
        )), 'Form', array('HtmlTag', array(
            'tag' => 'ul',
            'class' => 'form'
        ))));
    }

    function setUserId($id)
    {
        $this->getElement('user_id')->setValue($id);
    }

    function appendSubmit($content, $element, $options)
    {
        return '<li><a class="btn-red" href="#"><span>��������� �����</span></a></li>';
    }

}

class Form_Signup extends Zend_Form
{

    function init()
    {
        $this->setAction('/users/signup')
            ->setMethod(Zend_Form::METHOD_POST)
            ->setAttrib('class', 'ajax-form')
            ->setAttrib('id', 'signup');

        $this->addElement('text', 'login', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '�����'
        ));

        $login = $this->getElement('login');

        $login->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        ))));

        $login->clearValidators();

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� ����� ����������� ��� ����������');
        $login->addValidator($validator);

        $validator = new Zend_Validate_Regex('/^[a-zA-Z0-9]*$/');
        $validator->setMessage('�� ������ ������ ������');
        $login->addValidator($validator);

        $validator = new Zend_Validate_Db_NoRecordExists('users', 'login');
        $validator->setMessage('����� %value% ��� �����');
        $login->addValidator($validator);

        $this->addElement('text', 'email', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => 'E-mail'
        ));

        $email = $this->getElement('email');
        $email->clearValidators();

        $email->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        ))));

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� E-mail ����������� ��� ����������');
        $email->addValidator($validator);

        $validator = new Zend_Validate_EmailAddress();
        $validator->setMessage('�� ������ ������ E-mail');
        $email->addValidator($validator);

        $validator = new Zend_Validate_Db_NoRecordExists('users', 'email');
        $validator->setMessage('E-mail %value% ��� �����');
        $email->addValidator($validator);

        $this->addElement('text', 'name_f', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '���'
        ));

        $name_f = $this->getElement('name_f');

        $name_f->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        ))));

        $name_f->clearValidators();

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� ��� ����������� ��� ����������');


        $name_f->addValidator($validator);

        $this->addElement('text', 'name_l', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '�������'
        ));

        $name_l = $this->getElement('name_l');

        $name_l->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        )), array('Callback', array(
            'callback' => array($this, 'appendNote')
        ))));

        $name_l->clearValidators();

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� ������� ����������� ��� ����������');
        $name_l->addValidator($validator);

//        $this->addElement('text', 'wmr', array(
//            'required' => true,
//            'filters' => array('StripNewlines', 'StripTags'),
//            'label' => '����� WMR �������� (R123456789)'
//        ));
//
//        $wmr = $this->getElement('wmr');
//
//        $wmr->setDecorators(array(array('ViewScript', array(
//                    'viewScript' => 'form.phtml',
//            )), array('Callback', array(
//                    'callback' => array($this, 'appendNote')
//            ))));
//
//        $wmr->clearValidators();
//
//        $validator = new Zend_Validate_NotEmpty();
//        $validator->setMessage('���� WMR ����������� ��� ����������');
//        $wmr->addValidator($validator);

        $this->addElement('checkbox', 'i_agree', array(
            'required' => true,
            'disabled' => 'disabled',
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '� ����������� � �������� <a href="/terms/">������� �����</a>'
        ));

        $i_agree = $this->getElement('i_agree');
        $i_agree->setValue(1);

        $i_agree->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        ))));

        $i_agree->clearValidators();

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('�� ������ ������� ���������������� ����������');
        $i_agree->addValidator($validator);

        $this->setDecorators(array('FormElements', array('Callback', array(
            'callback' => array($this, 'appendSubmit')
        )), 'Form', array('HtmlTag', array(
            'tag' => 'ul',
            'class' => 'form'
        ))));
    }

    function appendNote($content, $element, $options)
    {
        return '<li>
            <div class="blue-block2">������������ ������ ����� ��������� ��� �� ����������� �����
                    <div class="corner tl"></div>
                    <div class="corner tr"></div>
                    <div class="corner bl"></div>
                    <div class="corner br"></div>
            </div>
    </li>';
    }

    function appendSubmit($content, $element, $options)
    {
        $out = <<<CUT
<li><a class="btn-signup-employer" href="javascript:;" onclick="$(this).closest('form').find('input[name=type]').val('employer'); return ajaxSubmit($(this).closest('form').get(0));"><span>������������</span></a></li>   
<li><a class="btn-signup-writer" href="javascript:;" onclick="$(this).closest('form').find('input[name=type]').val('writer'); return ajaxSubmit($(this).closest('form').get(0));"><span>����������� ������</span></a></li>
<li><a class="btn-signup-reader" href="javascript:;" onclick="$(this).closest('form').find('input[name=type]').val('reader'); return ajaxSubmit($(this).closest('form').get(0));"><span>����������� ��������</span></a></li>
CUT;
        return $out;
    }

}

class Form_Sendpass extends Zend_Form
{

    function init()
    {
        $this->setAction('/users/sendpass')
            ->setMethod(Zend_Form::METHOD_POST)
            ->setAttrib('class', 'ajax')
            ->setAttrib('id', 'sendpass');

        $this->addElement('text', 'email', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '������� ��� Email',
            'description' => '����� ������ ����� ��������� ��� �� ����������� �����'
        ));

        $email = $this->getElement('email');
        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� E-mail ����������� ��� ����������');
        $email->addValidator($validator);

        $validator = new Zend_Validate_EmailAddress();
        $validator->setMessage('�� ������ ������ E-mail');
        $email->addValidator($validator);

        $this->setDecorators(array('FormElements', array('Callback', array(
            'callback' => array($this, 'appendSubmit')
        )), 'Form', array('HtmlTag', array(
            'tag' => 'ul',
            'class' => 'form'
        ))));
    }

    function appendNote($content, $element, $options)
    {
        return '<li>
            <div class="blue-block2">������������ ������ ����� ��������� ��� �� ����������� �����
                    <div class="corner tl"></div>
                    <div class="corner tr"></div>
                    <div class="corner bl"></div>
                    <div class="corner br"></div>
            </div>
    </li>';
    }

    function appendSubmit($content, $element, $options)
    {
        return '<li><a class="btn-red" href="#"><span>������ ������</span></a></li>';
    }

}

class Form_Login extends Zend_Form
{

    function init()
    {
        $this->setAction('/users/login')
            ->setMethod(Zend_Form::METHOD_POST)
            ->setAttrib('class', 'ajax')
            ->setAttrib('id', 'login');


        $this->addElement('text', 'login', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '�����'
        ));

        $login = $this->getElement('login');
        $login->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        ))));

        $this->addElement('password', 'pass', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '������'
        ));


        $pass = $this->getElement('pass');
        $pass->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        ))));

        $this->setDecorators(array('FormElements', array('Callback', array(
            'callback' => array($this, 'appendSubmit')
        )), 'Form', array('HtmlTag', array(
            'tag' => 'ul',
            'class' => 'form'
        ))));
    }

    function appendSubmit($content, $element, $options)
    {
        return '<li><a class="btn-red" href="#"><span>�����</span></a></li>';
    }

}

class Form_Profile extends Zend_Form
{

    function init()
    {
        $this->setAction('/users/profile')
            ->setMethod(Zend_Form::METHOD_POST)
            ->setAttrib('id', 'profile');

        $this->addElement('text', 'login', array(
            'label' => '�����',
            'disabled' => true
        ));

        $this->addElement('text', 'email', array(
            //'required'=>true,
            //'filters'=>array('StripNewlines', 'StripTags'),
            'label' => 'E-mail',
            'disabled' => true
        ));

//        $email = $this->getElement('email');
//
//        $validator = new Zend_Validate_NotEmpty();
//        $validator->setMessage('���� E-mail ����������� ��� ����������');
//        $email->addValidator($validator);
//
//        $validator = new Zend_Validate_EmailAddress();
//        $validator->setMessage('�� ������ ������ E-mail');
//        $email->addValidator($validator);

        $this->addElement('text', 'web', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '����'
        ));

        $this->addElement('text', 'icq', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => 'ICQ'
        ));

        $this->addElement('text', 'skype', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => 'Skype'
        ));

        $this->addElement('text', 'country', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '������'
        ));

        $this->addElement('text', 'city', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '�����'
        ));

        $this->addElement('text', 'name_f', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '���'
        ));

        $name_f = $this->getElement('name_f');

        $name_f->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        ))));

        $name_f->clearValidators();

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� ��� ����������� ��� ����������');


        $name_f->addValidator($validator);

        $this->addElement('text', 'name_l', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '�������'
        ));

        $name_l = $this->getElement('name_l');

        $name_l->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        ))));

        $name_l->clearValidators();

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� ������� ����������� ��� ����������');
        $name_l->addValidator($validator);

        $this->addElement('textarea', 'about', array(
            'label' => '� ����',
            'rows' => 3
        ));

        $about = $this->getElement('about');


        $validator = new Zend_Validate_StringLength(0, 255);
        $validator->setMessage('������������ ����� ���� � ���� - 255 ��������');
        $about->addValidator($validator);

        $validator = new Site_Validate_NotContainEmailAddress();
        $validator->setMessage('������ ��������� ������ ����������� �����');
        $about->addValidator($validator);

        $this->addElement('text', 'wmr', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => 'WMR'
        ));

        $wmr = $this->getElement('wmr');

        $wmr->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        ))));

        $wmr->clearValidators();

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� wmr ����������� ��� ����������');
        $wmr->addValidator($validator);

        $this->addElement('password', 'pass', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '������'
        ));

        $pass = $this->getElement('pass');

        $pass = $this->getElement('pass');

        $pass->clearValidators();

        $validator = new Zend_Validate_Identical(@$_POST['pass1']);
        $validator->setMessage('������ ������ ���������');
        $pass->addValidator($validator);

        $this->addElement('password', 'pass1', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '������������� ������'
        ));

        $pass1 = $this->getElement('pass1');

        $pass1->clearValidators();

        $validator = new Zend_Validate_Identical(@$_POST['pass']);
        $validator->setMessage('������ ������ ���������');
        $pass1->addValidator($validator);
    }

}

class UsersController extends Zend_Controller_Action
{
    function preDispatch()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            Zend_Layout::getMvcInstance()->disableLayout();
            $params = $this->getRequest()->getParams();
            $params = $this->decode("UTF-8", "windows-1251", $params);
            $this->getRequest()->setParams($params);
            $_POST = $this->decode("UTF-8", "windows-1251", $_POST);
            $_GET = $this->decode("UTF-8", "windows-1251", $_GET);
        } else {
            $this->_helper->menu->select('users');
        }
    }

    function decode($from, $to, $value)
    {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->decode($from, $to, $v);
            }
        } else {
            $value = iconv($from, $to, $value);
        }
        return $value;
    }

    function payinAction()
    {
        $this->redirectIfNotAjax();

        if ($this->_helper->auth->requireLogin(User::TYPE_EMPLOYER)) {
            return;
        }
    }

    function doPayinAction()
    {
        $this->redirectIfNotAjax();

        if ($this->_helper->auth->requireLogin(User::TYPE_EMPLOYER)) {
            return;
        }


        if ($this->getRequest()->isPost()) {

            $anPaymentTable = new PaymentTable;
            $payment = $anPaymentTable->fetchNew();

            $payment->user_id = Zend_Auth::getInstance()->getIdentity()->user_id;
            $payment->amount = $this->getRequest()->getParam('amount');
            $payment->tm_added = date('Y-m-d h:i:s');
            $payment->completed = 0;
            $payment->ip = $_SERVER['REMOTE_ADDR'];
            $payment->service = 'payin';

            $payment->save();

            $resp = array();
            $resp['errorCode'] = 0;
            $resp['payment_id'] = $payment->payment_id;
            $resp['amount'] = $payment->amount;
        } else {
            die();
        }


        $resp = Zend_Json::encode($this->decode('windows-1251', 'UTF-8', $resp));
        $this->_helper->viewRenderer->setNoRender();
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json; charset=UTF-8')
            ->appendBody($resp);
    }

    function mapAction()
    {
//        Zend_Layout::getMvcInstance()->setLayout('layout');
        $anUserTable = new UsersTable;
        $this->view->items = $anUserTable->fetchAll(
            $anUserTable->select()
                ->where('x<>0')
                ->where('y<>0')
                ->where("avatar<>''")
                ->where('type=?', User::TYPE_WRITER)
                ->where('is_locked=0'));
    }

    function indexAction()
    {
        $anUsersTable = new UsersTable();

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $anUsersTable->select()
                    ->where('is_locked=0')
                    ->where('type=?', User::TYPE_WRITER)
                    ->order('rating DESC')
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('users_item_perpage', 50));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }

    function faceAction()
    {
        $anUsersTable = new UsersTable();

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $anUsersTable->select()
                    ->where("avatar<>''")
                    ->where('is_locked=0')
                    ->where('type=?', User::TYPE_WRITER)
                    ->order('rating DESC')
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('face_item_perpage', 140));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }

    function searchAction()
    {
        if ($this->getRequest()->isPost()) {
            $anStatQuery = new StatQueryTable;
            $anStatQuery->log($this->getRequest()->getParam('q'), StatQueryTable::TYPE_USER, date('Y-m-d H:i:s'));
            $this->_redirect(
                Zend_Controller_Front::getInstance()->getRouter()->assemble(array(
                    'controller' => 'users',
                    'action' => 'search',
                    'q' => $this->getRequest()->getParam('q')
                ))
            );
        }

        $q = $this->getRequest()->getParam('q');
        $anUsersTable = new UsersTable;
        $select = $anUsersTable->select()
            ->where('name_f LIKE ?', '%' . $q . '%')
            ->orWhere('name_l LIKE ?', '%' . $q . '%')
            ->orWhere('login LIKE ?', '%' . $q . '%')
            ->orWhere('about LIKE ?', '%' . $q . '%')
            ->orWhere('country LIKE ?', '%' . $q . '%')
            ->orWhere('city LIKE ?', '%' . $q . '%')
            ->order('rating DESC');

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('users_item_perpage', 50));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;

    }

    function ratesAction()
    {
        if ($this->_helper->auth->requireLogin(true)) {
            return;
        }

        if ($this->_helper->serv->requireService('ball')) {
            return;
        }

        $anUsersTable = new UsersTable();

        $user = $anUsersTable
            ->find(Zend_Auth::getInstance()->getIdentity()->user_id)
            ->current();

        if (!$user) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $this->view->user = $user;
        $anArticlesView = new ArticlesView();

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $anArticlesView->select()
                    ->where('user_id=?', $user->user_id)
                    ->order('date DESC')->limit(10)
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_user_articles_perpage', 100));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }

    function photoAction()
    {
        $anUsersTable = new UsersTable();

        $user = $anUsersTable
            ->find($this->getRequest()->getParam('id'))
            ->current();

        if (!$user) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $this->view->user = $user;
        $anPhotoTable = new PhotoTable;

        $this->view->photo = $anPhotoTable->fetchAll(
            $anPhotoTable->select()
                ->where('user_id=?', $this->getRequest()->getParam('id'))
                ->order('datetime DESC')
        );
    }

    function articlesAction()
    {
        $anUsersTable = new UsersTable();

        $user = $anUsersTable
            ->find($this->getRequest()->getParam('id'))
            ->current();

        if (!$user) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        if (!Zend_Auth::getInstance()->getIdentity() || $user->user_id !== Zend_Auth::getInstance()->getIdentity()->user_id) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        if (!$user->isWriter()) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $this->view->user = $user;
        $anArticlesView = new ArticlesView();

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $anArticlesView->select()
                    ->where('user_id=?', $this->getRequest()->getParam('id'))
                    ->order('is_rework DESC')
                    ->order('is_approved ASC')
                    ->order('date DESC')
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_user_articles_perpage', 20));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }

    function notepadAction()
    {
        $anUsersTable = new UsersTable();

        $user = $anUsersTable
            ->find($this->getRequest()->getParam('id'))
            ->current();

        if (!$user) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $this->view->user = $user;

        $anNotepadTable = new NotepadTable;
        $select = $anNotepadTable->select()
            ->from('notepad')
            ->joinLeft('articles_view', 'articles_view.article_id=notepad.article_id')
            ->where('notepad.user_id=?', $user->user_id)
            ->setIntegrityCheck(false)
            ->order('date DESC');

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_articles_perpage', 36));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }

//    function jobsAction() {
//        $anUsersTable = new UsersTable();
//
//        $user = $anUsersTable
//                ->find($this->getRequest()->getParam('id'))
//                ->current();
//
//        if (!$user) {
//            throw new Zend_Controller_Dispatcher_Exception('');
//        }
//        
//        if (!$user->isEmployer()) {
//            throw new Zend_Controller_Dispatcher_Exception('');
//        }
//
//        $this->view->user = $user;
//        $anJobTable = new JobTable();
//
//        $paginator = new Zend_Paginator(
//                        new Zend_Paginator_Adapter_DbTableSelect(
//                                        $anJobTable->select()
//                                        ->where('user_id=?', $this->getRequest()->getParam('id'))
//                                        ->order('datetime DESC')
//                        )
//        );
//        $paginator->setCurrentPageNumber(
//                $this->getRequest()->getParam('p', 1)
//        );
//        $paginator->setItemCountPerPage(Site_Settings::get('front_user_articles_perpage', 100));
//        $paginator->setPageRange(10);
//
//        $this->view->paginator = $paginator;
//    }

    function feedAction()
    {
        if ($this->_helper->auth->requireLogin(array(User::TYPE_WRITER))) {
            return;
        }

        $this->view->user = Zend_Auth::getInstance()->getIdentity();
        $anFeedTable = new FeedView();

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $anFeedTable->select()
                    ->where('user_id=?', Zend_Auth::getInstance()->getIdentity()->user_id)
                    ->where('type!=?', FeedTable::TYPE_PHOTO)
                    ->where('type!=?', FeedTable::TYPE_POST)
                    ->order('datetime DESC')
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 0)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('feed_item_perpage', 100));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;

        $anFeedTable->update(array(
            'is_new' => 0,
        ), array(
            'user_id=' . Zend_Auth::getInstance()->getIdentity()->user_id,
            'is_new=1'));

    }

    function viewStatAction()
    {
        if ($this->_helper->auth->requireLogin(array(User::TYPE_WRITER))) {
            return;
        }

        $user = Zend_Auth::getInstance()->getIdentity();
        $this->view->user = $user;

        $user->setTable(new UsersTable());

        if (!$user->stat_i_agree && !$this->getRequest()->isPost()) {
            $anPagesTable = new PagesTable;
            $this->view->page = $anPagesTable->find(13)->current();
            return;
        } elseif ($this->getRequest()->isPost()) {
            $user->stat_i_agree = 1;
            $user->save();
        }

        $anArticlesTable = new ArticlesView();

        $price_ru = Site_Settings::get('ppv_price_RU');
        $price_ua = Site_Settings::get('ppv_price_UA');
        $price_by = Site_Settings::get('ppv_price_BY');

        $select = $anArticlesTable->select()
            ->from('articles_view')
            ->setIntegrityCheck(false)
            ->joinLeft('article_view', 'articles_view.article_id=article_view.article_id', array(
                'view_cnt_unique' => new Zend_Db_Expr('COUNT(IF(article_view.is_unique, view_id, NULL))'),
                'view_cnt' => new Zend_Db_Expr('COUNT(view_id)'),
                'income' => new Zend_Db_Expr("SUM(IF(article_view.is_unique, CASE
                        WHEN article_view.country = 'RU' THEN $price_ru
                        WHEN article_view.country = 'UA' THEN $price_ua
                        WHEN article_view.country =  'BY' THEN $price_by
                    END, 0))")
            ))
            ->where('articles_view.user_id=?', $user->user_id)
            ->where('stat_begin IS NOT NULL')
            ->group('articles_view.article_id')
            ->order('date DESC');


        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);

        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('posts_item_perpage', 5));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }

    function purchaseAction()
    {
        if ($this->_helper->auth->requireLogin(array(User::TYPE_EMPLOYER))) {
            return;
        }

        $anUsersTable = new UsersTable();

        $user = $anUsersTable
            ->find(Zend_Auth::getInstance()->getIdentity()->user_id)
            ->current();


        if (!$user) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        if (!$user->isEmployer()) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $this->view->user = $user;
        $anShopArticleTable = new ShopArticleView;

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $anShopArticleTable->select()
                    ->where('owner_id=?', $user->user_id)
                    ->order('date DESC')
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_user_articles_perpage', 100));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }

    function wallAction()
    {
        $anUsersTable = new UsersTable();

        $user = $anUsersTable
            ->find($this->getRequest()->getParam('id'))
            ->current();

        if (!$user) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $this->view->user = $user;
        $anCommentsView = new CommentsView();

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $anCommentsView->select()
                    ->where('typology_id=?', $this->getRequest()->getParam('id'))
                    ->where('typology=?', CommentsTable::TYPOLOGY_USER)
                    ->order('datetime DESC')
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_user_comments_perpage', 100));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }

    function lastCommentAction()
    {
        $anUsersTable = new UsersTable();

        $user = $anUsersTable
            ->find($this->getRequest()->getParam('id'))
            ->current();

        if (!$user) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select = $select->from(array('c' => 'comments_view'))->joinLeft(array('a' => 'articles'), "a.article_id=c.typology_id AND c.typology = 'article'", array(
            'a_name' => 'a.name'
        ))->where('c.is_approved=1')->where('a.user_id=?', $user->user_id)->order('c.datetime DESC');

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbSelect($select));

        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_user_comments_perpage', 100));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;

        $this->view->items = new Zend_Db_Table_Rowset(array(
            'rowClass' => 'Comment',
            'data' => $paginator->getCurrentItems()->getArrayCopy()));

        $this->view->user = $user;
    }

    function recommendWhoAction()
    {
        $anUsersTable = new UsersTable();

        $user = $anUsersTable
            ->find($this->getRequest()->getParam('id'))
            ->current();
        if (!$user->isWriter()) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }
        $anRecommendWhoView = new RecommendWhoView();
        $select = $anRecommendWhoView->select()
            ->where('whom_id=?', $this->getRequest()->getParam('id'))
            ->where('type=?', User::TYPE_WRITER)
            ->order('rating DESC');
        return $this->showRecommend($select);
    }

    function recommendWhomAction()
    {
        $anRecommendWhomView = new RecommendWhomView();
        $select = $anRecommendWhomView->select()
            ->where('who_id=?', $this->getRequest()->getParam('id'))
            ->order('rating DESC');
        return $this->showRecommend($select);
    }

    function readersAction()
    {
        $anUsersTable = new UsersTable();

        $user = $anUsersTable
            ->find($this->getRequest()->getParam('id'))
            ->current();
        if (!$user->isWriter()) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }
        $anRecommendWhoView = new RecommendWhoView();
        $select = $anRecommendWhoView->select()
            ->where('whom_id=?', $this->getRequest()->getParam('id'))
            ->where('type=?', User::TYPE_READER)
            ->order('rating DESC');
        return $this->showRecommend($select);
    }

    protected function showRecommend(Zend_Db_Table_Select $select)
    {
        $anUsersTable = new UsersTable();

        $user = $anUsersTable
            ->find($this->getRequest()->getParam('id'))
            ->current();

        if (!$user) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $this->view->user = $user;

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_user_articles_perpage', 100));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }

    function itemAction()
    {
        $anUsersTable = new UsersTable();

        $user = $anUsersTable
            ->find($this->getRequest()->getParam('id'))
            ->current();

        $this->view->user = $user;

        $commentsProcessor = new CommentsProcessor($this->getRequest()->getParam('id'), CommentsTable::TYPOLOGY_USER);
        $commentsProcessor->run($this->getRequest());
        $this->view->commentsProcessor = $commentsProcessor;

        //small protection against cheating
        if (!isset($_SESSION['view_user'])) {
            $_SESSION['view_user'] = array();
        }

        if (!in_array($user->user_id, $_SESSION['view_user'])) {
            $user->clicks++;
            $user->save();
            $_SESSION['view_user'][] = $user->user_id;
        }

        $anPhotoTable = new PhotoTable;
        $this->view->photo = $anPhotoTable->fetchAll(
            $anPhotoTable->select()->where('user_id=?', $this->getRequest()->getParam('id'))
                ->order('datetime DESC')
                ->limit(4));

        $anRecommendWhoView = new RecommendWhoView();
        $this->view->recommend_who = $anRecommendWhoView->fetchAll(
            $anRecommendWhoView->select()
                ->where('whom_id=?', $this->getRequest()->getParam('id'))
                ->where('type=?', User::TYPE_WRITER)
                ->order('rating DESC')
                ->limit(7)
        );

        $this->view->readers = $anRecommendWhoView->fetchAll(
            $anRecommendWhoView->select()
                ->where('whom_id=?', $this->getRequest()->getParam('id'))
                ->where('type=?', User::TYPE_READER)
                ->order('rating DESC')
                ->limit(7)
        );

        $anRecommendWhomView = new RecommendWhomView();
        $this->view->recommend_whom = $anRecommendWhomView->fetchAll(
            $anRecommendWhomView->select()
                ->where('who_id=?', $this->getRequest()->getParam('id'))
                ->order('rating DESC')
                ->limit(30)
        );

        $anRecommendTable = new RecommendTable();

        if (Zend_Auth::getInstance()->getIdentity()) {
            $this->view->isRecommend = $anRecommendTable->getIsRecommend(Zend_Auth::getInstance()->getIdentity()->user_id, $user->user_id);
        } else {
            $this->view->isRecommend = false;
        }

        $anArticlesView = new ArticlesView();
        $select = $anArticlesView->select()
            ->where('user_id=?', $this->getRequest()->getParam('id'))
            ->order('date DESC');
        $articles = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $articles->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $articles->setItemCountPerPage(10);


        $this->view->articles = $articles;

        $anShopArticleTable = new ShopArticleView;


//        $shopSelect = $anShopArticleTable->select()
//            ->where('user_id=?', $this->getRequest()->getParam('id'))
//            ->where('owner_id=0')
//            ->order('date DESC')
//            ->limit(3);

//        $shopCntSelect = $anShopArticleTable
//            ->select()
//            ->from('shop_article_view')
//            ->columns(array('cnt' => new Zend_Db_Expr('COUNT(qna_id)')))
//            ->where('user_id=?', $this->getRequest()->getParam('id'))
//            ->where('owner_id=0');
//
//        if (Zend_Auth::getInstance()->hasIdentity()
//            && Zend_Auth::getInstance()->getIdentity()->user_id == $this->getRequest()->getParam('id')
//        ) {
//            $shopSelect = $shopSelect->where('is_approved=1 OR is_rework=1');
//            $shopCntSelect = $shopCntSelect->where('is_approved=1 OR is_rework=1');
//        } else {
//            $shopSelect = $shopSelect->where('is_approved=1');
//            $shopCntSelect = $shopCntSelect->where('is_approved=1');
//        }
//
//
//        $this->view->shop_article = $anShopArticleTable->fetchAll(
//            $shopSelect
//        );
//
//        $this->view->shop_article_cnt = $anShopArticleTable->fetchAll($shopCntSelect)->current()->cnt;

//        $anJobTable = new JobTable;
//        
//        $this->view->jobs = $anJobTable->fetchAll(
//                        $anJobTable->select()
//                                ->where('user_id=?', $this->getRequest()->getParam('id'))
//                                ->order('datetime DESC')
//                                ->limit(10)
//        );


        $anNotepadTable = new NotepadTable;

        $select = $anNotepadTable->select()
            ->from('notepad')
            ->joinLeft('articles_view', 'articles_view.article_id=notepad.article_id')
            ->where('notepad.user_id=?', $this->getRequest()->getParam('id'))
            ->setIntegrityCheck(false)
            ->order('date DESC');
        $notepad = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $notepad->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $notepad->setItemCountPerPage(10);
        $this->view->notepad = $notepad;
    }

    function recommendAction()
    {
        $this->redirectIfNotAjax();
        $this->_helper->viewRenderer->setNoRender();
        Zend_Layout::getMvcInstance()->disableLayout();

        $params = $this->getRequest()->getParams();
        $anRecommendTable = new RecommendTable();

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();

            $data = array(
                'who_id' => Zend_Auth::getInstance()->getIdentity()->user_id,
                'whom_id' => $params['id']
            );

            $recommend = $anRecommendTable->fetchRow(
                $anRecommendTable->select()
                    ->where('who_id=?', $user->user_id)
                    ->where('whom_id=?', $params['id'])
            );

            if ($recommend) {
                $recommend->delete();
            } else {
                $anRecommendTable->replace($data);
            }
            $resp['isRecommend'] = $anRecommendTable->getIsRecommend($user->user_id, $params['id']);
            $resp['count'] = (int)$anRecommendTable->getCountWho($params['id']);
        } else {
            $resp['isRecommend'] = $anRecommendTable->getIsRecommend(Zend_Auth::getInstance()->getIdentity()->user_id, $params['id']);
            $resp['count'] = (int)$anRecommendTable->getCountWho($params['id']);
            $resp['error'] = iconv('windows-1251', 'UTF-8', '���������� ����������������� ��� ������� �� ���� ��� ���� ����� ��������������� ������ ������������');
        }

        $resp = Zend_Json::encode($resp);
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json; charset=UTF-8')
            ->appendBody($resp);

        return;
    }

    function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        setcookie('auth', '', time() - 100, '/');
        $this->_redirect('/');
    }

    function loginAction()
    {
        $this->redirectIfNotAjax();
        $loginForm = new Form_Login();

        if ($this->getRequest()->isPost()) {

            $auth = Zend_Auth::getInstance();
            $authAdapter = new Site_Auth_Adapter(
                $this->getRequest()->getParam('login'),
                $this->getRequest()->getParam('pass'));

            $result = $auth->authenticate($authAdapter);

            if (!$result->isValid()) {
                $messages = $result->getMessages();
                return $this->error($messages[0]);
            } else {
                if ($this->getRequest()->getParam('remember')) {
                    $user = Zend_Auth::getInstance()->getIdentity();
                    setcookie('auth', Site_Auth_Adapter_Cookie::getAuthCookie($user->login), time() + 3600 * 24 * 12, '/');
                }
                HookManager::getInstance()->call(HookManager::EVENT_LOGIN,
                    array('user' => Zend_Auth::getInstance()->getIdentity()));
                $resp = array();
                $resp['errorCode'] = 0;
                $resp['msg'] = iconv("windows-1251", "UTF-8", '�� ������ ������������');
                $resp['action'] = 'refresh';
                $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');
            }

            $resp = Zend_Json::encode($resp);
            $this->_helper->viewRenderer->setNoRender();
            $this->getResponse()
                ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                ->appendBody($resp);
        } else {
            $this->view->form = $loginForm;
        }
    }

    function sendpassAction()
    {
        $this->redirectIfNotAjax();

        $sendpassForm = new Form_Sendpass();

        if ($this->getRequest()->isPost()) {
            if ($sendpassForm->isValid($_POST)) {
                $anUsersTable = new UsersTable();

                $user = $anUsersTable->findByEmail($sendpassForm->getValue('email'));

                if (!$user) {
                    return $this->error('�������� ����� ����������� �����');
                } else {
                    $newpass = User::generatePass();
                    $user->passhash = Zend_Filter::filterStatic($newpass, 'PassToHash', array($user->salt));
                    $user->save();


                    $mail = new Site_Mail('sendpass');
                    $mail->setUser($user);
                    $mail->setPassword($newpass);
                    $mail->send($user->email);

                    $resp = array();
                    $resp['errorCode'] = 0;
                    $resp['msg'] = iconv("windows-1251", "UTF-8", '������ ��������� �� ��� ����� ����������� �����');
                    $resp['action'] = 'refresh';
                    $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');
                }

                $resp = Zend_Json::encode($resp);
                $this->_helper->viewRenderer->setNoRender();
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                    ->appendBody($resp);
            } else {
                $errors = $this->getErrors($sendpassForm->getMessages());
                $this->error(array_shift($errors));
            }
        } else {
            $this->view->form = $sendpassForm;
        }
    }

    function profileAction()
    {
        if ($this->_helper->auth->requireLogin()) {
            return;
        }

        if ($this->getRequest()->isXmlHttpRequest()) {
            $form = new Form_Profile();
            if ($this->getRequest()->isPost() && $form->isValid($_POST)) {
                $anUserTable = new UsersTable();
                $user = $anUserTable->find(Zend_Auth::getInstance()->getIdentity()->user_id)->current();

                $user->name_f = $form->getValue('name_f');
                $user->name_l = $form->getValue('name_l');
//            $user->email = $form->getValue('email');
                $user->web = $form->getValue('web');
                $user->icq = $form->getValue('icq');
                $user->skype = $form->getValue('skype');
                $user->wmr = (string)$form->getValue('wmr');
                if ($user->wmr && $user->type != User::TYPE_WRITER) {
                    $user->type = User::TYPE_WRITER;
                }
                $user->about = $form->getValue('about');
                $country = $form->getValue('country');
                $city = $form->getValue('city');
                if ($country && $city && ($user->country != $country || $user->city != $city)) {
                    $user->country = $form->getValue('country');
                    $user->city = $form->getValue('city');
                    list($x, $y) = YMap::getInstance()->getCoordinate(YMap::getInstance()->getAddrLine($user));
                    $user->x = $x;
                    $user->y = $y;
                }
                $user->country = $form->getValue('country');
                $user->city = $form->getValue('city');
                if ($pass = $form->getValue('pass')) {
                    $user->setPassword($pass);
                }
                $user->save();

                $adapter = new Site_Auth_Adapter_Null($user->login, '');
                Zend_Auth::getInstance()->authenticate($adapter);

                $resp = array();
                $resp['errorCode'] = 0;
                $resp['msg'] = iconv("windows-1251", "UTF-8", '��� ������� ��������');
                $resp['action'] = 'redirect';
                $resp['url'] = '/users/profile';
                $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

                $resp = Zend_Json::encode($resp);
                $this->_helper->viewRenderer->setNoRender();
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                    ->appendBody($resp);
            } else {
                $errors = $this->getErrors($form->getMessages());
                $this->error(array_shift($errors));
            }
        } else {
            $this->view->user = Zend_Auth::getInstance()->getIdentity();

            $anArticlesView = new ArticlesView();
            $select = $anArticlesView->select()
                ->where('user_id=?', $this->view->user->user_id)
                ->order('date DESC');
            $paginator = new Zend_Paginator(
                new Zend_Paginator_Adapter_DbTableSelect(
                    $select
                )
            );
            $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
            );
            $paginator->setItemCountPerPage(10);
            $this->view->articles = $paginator;

            $anNotepadTable = new NotepadTable;
            $select = $anNotepadTable->select()
                ->from('notepad')
                ->joinLeft('articles_view', 'articles_view.article_id=notepad.article_id')
                ->where('notepad.user_id=?', $this->view->user->user_id)
                ->setIntegrityCheck(false)
                ->order('date DESC');
            $paginator = new Zend_Paginator(
                new Zend_Paginator_Adapter_DbTableSelect(
                    $select
                )
            );
            $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
            );
            $paginator->setItemCountPerPage(10);
            $this->view->notepad = $paginator;
        }
    }

    function signupAction()
    {
        $this->redirectIfNotAjax();

        $signupForm = new Form_Signup();

        if ($this->getRequest()->isPost()) {
            if ($signupForm->isValid($_POST)) {
                $anUsersTable = new UsersTable();

                $user = $anUsersTable->fetchNew();
                $salt = User::generateSalt();
                $pass = User::generatePass();

                $user->login = $signupForm->getValue('login');
                $user->email = $signupForm->getValue('email');
                $user->type = User::TYPE_WRITER;
//                $user->wmr = $signupForm->getValue('wmr');
                $user->name_f = $signupForm->getValue('name_f');
                $user->name_l = $signupForm->getValue('name_l');
                $user->salt = $salt;
                $user->passhash = Zend_Filter::filterStatic($pass, 'PassToHash', array($salt));
                $user->last_activity = date('Y-m-d H:i:s');
                $user->date = date('Y-m-d H:i:s');
                $user->ip = $_SERVER['REMOTE_ADDR'];

                $anIpCidrTable = new IpCidrTable;
                $addr = $anIpCidrTable->getAddr($user->ip);
                if ($addr) {
                    list($user->country, $user->city) = $addr;
                    list($user->ip_country, $user->ip_city) = $addr;
                    list($x, $y) = YMap::getInstance()->getCoordinate(YMap::getInstance()->getAddrLine($user));
                    $user->x = $x;
                    $user->y = $y;
                }
                $user->save();

                $mail = new Site_Mail('signup-' . $user->type);
                $mail->setUser($user);
                $mail->setPassword($pass);
                $mail->send($user->email);

                $auth = Zend_Auth::getInstance();

                $authAdapter = new Site_Auth_Adapter(
                    $user->login,
                    $pass
                );

                $result = $auth->authenticate($authAdapter);

                HookManager::getInstance()->call(HookManager::EVENT_LOGIN,
                    array('user' => Zend_Auth::getInstance()->getIdentity()));

                $resp = array();
                $resp['errorCode'] = 0;
                $resp['msg'] = iconv("windows-1251", "UTF-8", '������� �� �����������, ������ ������ �� ��� ������ ����������� �����');
                $resp['action'] = 'redirect';
                $resp['url'] = '/users/profile';
                $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

                $resp = Zend_Json::encode($resp);
                $this->_helper->viewRenderer->setNoRender();
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                    ->appendBody($resp);
            } else {
                $errors = $this->getErrors($signupForm->getMessages());
                $this->error(array_shift($errors));
            }
        } else {
            $this->view->form = $signupForm;
        }
    }

    function shareAction()
    {
        $this->redirectIfNotAjax();
        $form = new Form_Share;
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {

                $anUsersTable = new UsersTable;
                $user = $anUsersTable->find($form->getValue('user_id'))->current();

                $mail = new Site_Mail('share-user');
                $mail->setUser($user);
                $mail->setUrl('http://www.topauthor.ru' . $this->view->url(array($user->login), 'user', true));
                $mail->send($form->getValue('email'));

                $resp = array();
                $resp['errorCode'] = 0;
                $resp['msg'] = iconv("windows-1251", "UTF-8", '������ �� ������� ���������� �� ��������� ���� E-mail');
                $resp['action'] = 'hide-form';
                $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

                $anShareLogTable = new ShareLogTable;
                $anShareLogTable->insert(array(
                    'dattm' => date('Y-m-d H:i:s'),
                    'user_id' => Zend_Auth::getInstance()->hasIdentity() ?
                        Zend_Auth::getInstance()->getIdentity()->user_id :
                        null,
                    'typology_id' => $user->user_id,
                    'typology' => 'user',
                    'email' => $form->getValue('email')
                ));

                $resp = Zend_Json::encode($resp);
                $this->_helper->viewRenderer->setNoRender();
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                    ->appendBody($resp);
            } else {
                $errors = $this->getErrors($form->getMessages());
                $this->error(array_shift($errors));
            }
        } else {
            $form->setUserId($this->getRequest()->getParam('id'));
            $this->view->form = $form;
        }
    }

    protected function redirectIfNotAjax()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->_redirect('/');
        }
    }

    protected function error($msg)
    {
        $resp['errorCode'] = 1;
        $resp['msg'] = iconv("windows-1251", "UTF-8", $msg);
        $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

        $this->_helper->viewRenderer->setNoRender();
        $resp = Zend_Json::encode($resp);

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json; charset=UTF-8')
            ->appendBody($resp);
    }

    protected function getErrors($messages)
    {
        $result = array();
        foreach ($messages as $msg) {
            $result = array_merge($result, array_values($msg));
        }

        return $result;
    }
}