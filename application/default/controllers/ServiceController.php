<?php

class ServiceController extends Zend_Controller_Action
{
    function preDispatch() {
        parent::preDispatch();
        $this->_helper->menu->select('market');
        if ($this->getRequest()->isXmlHttpRequest()) {
            Zend_Layout::getMvcInstance()->disableLayout();
            $params = $this->getRequest()->getParams();
            $params = $this->decode("UTF-8", "windows-1251", $params);
            $this->getRequest()->setParams($params);
            $_POST = $this->decode("UTF-8", "windows-1251", $_POST);
            $_GET = $this->decode("UTF-8", "windows-1251", $_GET);
        }
    }
    
    function decode($from, $to, $value) {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->decode($from, $to, $v);
            }
        } else {
            $value = iconv($from, $to, $value);
        }
        return $value;
    }
    
    function __call($methodName, $args) {
        $action = $this->getRequest()->getActionName();
        if (array_key_exists($action, $this->getServices())) {
            $this->getRequest()->setActionName('form');
            $this->getRequest()->setParam('id', $action);
            return $this->formAction();
        }
        
        parent::__call($methodName, $args);
    }
    
    function indexAction()
    {
        if ($this->_helper->auth->requireLogin(true)) {
            return;
        }
        
        $this->view->services = $this->getServices();
    }
    
    function formAction() {
        $id = $this->getRequest()->getParam('id');
        
        $this->view->price_rating = Site_Settings::get($id . '_price_rating', 0);
        $this->view->price_pay = Site_Settings::get($id . '_price_pay', 0); 
        $services = $this->getServices();
        
        $this->view->def = $services[$id];
        $this->view->service = $id;
    }
    
    function activateAction()
    {
        
        $id = $this->getRequest()->getParam('service');
        if (!($err = $this->validate($id))) {
            switch($this->getRequest()->getParam('type')) {
                case 'pay' :
                    $resp = $this->activatePay($id);
                    break;
                case 'rating':
                    $resp = $this->activateRating($id);
                    break;
                default :
                    die();
            }
        } else {
           $resp = array(
               'errorCode' => 1,
               'msg' => $err
           );
        }
        
        $resp = Zend_Json::encode($this->decode('windows-1251', 'UTF-8', $resp));
        $this->_helper->viewRenderer->setNoRender();
        $this->getResponse()
                ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                ->appendBody($resp);
    }
    
    function calculate($id, $perUnit) {
        $method = 'calculate' . ucfirst($id);
        if (method_exists($this, $method)) {
            return $this->$method($perUnit);
        }
        return $perUnit;
    }
    
    function calculateDarball($perUnit)
    {
        return $this->getRequest()->getParam('cnt') * $perUnit;
    }
    
    function calculateSalebal($perUnit)
    {
        return $this->getRequest()->getParam('cnt') * $perUnit;
    }
    
    function calculateSnigenie($perUnit)
    {
        return $this->getRequest()->getParam('cnt') * $perUnit;
    }
    
    function validate($id) {
        $method = 'validate' . ucfirst($id);
        if (method_exists($this, $method)) {
            return $this->$method();
        }
        return null;
    }
    
    function validateIgnor() 
    {
        $login = $this->getRequest()->getParam('login');
        $anUsersTable = new UsersTable;
        
        $user = $anUsersTable->findByLogin($login);
        
        if (!$user) return '������� ����� ����� ������������';
        if ($user->user_id == Zend_Auth::getInstance()->getIdentity()->user_id) return '�� �������� ������������ ����';
    }
    
    function validateDarball() 
    {
        $login = $this->getRequest()->getParam('login');
        $anUsersTable = new UsersTable;
        
        $user = $anUsersTable->findByLogin($login);
        
        if (!$user) return '������� ����� ����� ������������';
        if ($user->user_id == Zend_Auth::getInstance()->getIdentity()->user_id) return '�� �������� �������� ���� ����';
        
        $cnt = $this->getRequest()->getParam('cnt');
        if ($cnt <= 0) return '������� ������������� ���������� ������';
    }
    
    function validateSalebal() 
    {
        $cnt = $this->getRequest()->getParam('cnt');
        if ($cnt <= 0) return '������� ������������� ���������� ������';
    }
    
    function validateSnigenie() 
    {
        $login = $this->getRequest()->getParam('login');
        $anUsersTable = new UsersTable;
        
        $user = $anUsersTable->findByLogin($login);
        
        if (!$user) return '������� ����� ����� ������������';
        if ($user->user_id == Zend_Auth::getInstance()->getIdentity()->user_id) return '�� �������� �������� ���� ����';
        
        $cnt = $this->getRequest()->getParam('cnt');
        if ($cnt <= 0) return '������� ������������� ���������� ������';
    }
    
    function activatePay($id) {
        if ($this->getRequest()->isPost()) {
            
            $anPaymentTable = new PaymentTable;
            $payment = $anPaymentTable->fetchNew();
            
            $payment->user_id = Zend_Auth::getInstance()->getIdentity()->user_id;
            $payment->amount = $this->calculate($id, Site_Settings::get($id . '_price_pay'));
            $payment->tm_added = date('Y-m-d h:i:s');
            $payment->completed = 0;
            $payment->ip = $_SERVER['REMOTE_ADDR'];
            $payment->service = $id;
            $payment->login = $this->getRequest()->getParam('login', '');
            $payment->cnt = $this->getRequest()->getParam('cnt', 0);
            
            $payment->save();
                
            $resp = array();
            $resp['errorCode'] = 0;
            $resp['payment_id'] = $payment->payment_id;
            $resp['amount'] = $payment->amount;
        } else {
            die();
        }
        
        return $resp;
    }
    
    function activateRating($id) {   
        if ($this->getRequest()->isPost()) {
            $cost = $this->calculate($id, Site_Settings::get($id . '_price_rating'));
            
            $user = Zend_Auth::getInstance()->getIdentity();
            if ($user->rating > $cost) {
                $user->rating -= $cost;
                $user->activateService($id, date('Y-m-d H:i:s'), array(
                    'login' => $this->getRequest()->getParam('login'),
                    'cnt' => $this->getRequest()->getParam('cnt')
                ));
                $user->save();
                $resp['errorCode'] = 0;
            } else {
                $resp['errorCode'] = 1;
                $resp['msg'] = '� ��� �� ���������� �������� ��� ��������� ������';
            }
        } else {
            die();
        }
        
        return $resp;
    }
    
    function getServices() {
        return array(
            'premium' => array(
                'name' => '������� ���������� �� ������� ��������',
                'desc' => '��� ������� ��������� ������ ������ ����� ���������� ���� ������� �� 
            ������� �������� �����. ��� ���������� �������� ������ ��������� ��������� 
            ������ �������� ����� � 20 ���. �����, ������� ��������� ���� ������� 
            �� ������� ��������, ������ ����� �������� ��� ��� ���������, 
            ��� � ��� ������������� ����������.',
                'instant' => 1,
                'need_login' => 0,
                'need_cnt' => 0,
                'only_pay' => 0,
                'only_rating' => 0
            ),
            'smile' => array(
                'name' => '�������� � ����������',
                'desc' => '�������� � ��� ������ ��������. ������������� ��������� � ������ 
            ���������� ��������� ������� ����� �������� ���� ������. ��� ������� 
            �������� ������������ �� ����� �� �����, ��� ���� ����������� ��������� 
            ����������� ��� ���������.',
                'instant' => 0,
                'need_login' => 0,
                'need_cnt' => 0,
                'only_pay' => 0,
                'only_rating' => 0
                
            ),
            'ball' => array(
                'name' => '�������� ������ �� ������',
                'desc' => '� ������� ������ ������ ����������, ��� ����� ������ �������� 
            �� ����� �� ����� ������. ��������� ���� ������� �������� ������ 
            ��� ������, ������� ���� ���������� �� ����� ������, � ����� 
            �������, ��� ������ �� ��������.',
                'instant' => 0,
                'need_login' => 0,
                'need_cnt' => 0,
                'only_pay' => 0,
                'only_rating' => 0
            ),
            'stat' => array(
                'name' => '�������� ���������� �����',
                'desc' => '��� ������� ��������� �������, ������ ����� ��������� �������� ���������, 
            ������ ����� ������� ������ ������ �����, ������� ����� ���������� � 
            ����������� ������, � ����� � ����� ������ �������� ������� �� �����.',
                'instant' => 0,
                'need_login' => 0,
                'need_cnt' => 0,
                'only_pay' => 0,
                'only_rating' => 0
            ),
            'ignor' => array(
                'name' => '������������� ������������',
                'desc' => '��� ������� ��������� ������������ ��������� ������ ������ ������ ����� 
            ����������� � �������, ����������� �� ����� ������ � ���������.',
                'instant' => 0,
                'need_login' => 1,
                'need_cnt' => 0,
                'only_pay' => 0,
                'only_rating' => 0
            ),
            'darball' => array(
                'name' => '������ ����� ����� ������',
                'desc' => '��� ������� ��������� ������ ������ ����� �������� ����� ����� 
            ������ ������ ������� ������.',
                'instant' => 1,
                'need_login' => 1,
                'need_cnt' => 1,
                'only_pay' => 1,
                'only_rating' => 0
            ),
            'salebal' => array(
                'name' => '������ �������',
                'desc' => '��� ������� ��������� ������ ������ ����� ������ ����������� 
            ���������� ������.',
                'instant' => 1,
                'need_login' => 0,
                'need_cnt' => 1,
                'only_pay' => 1,
                'only_rating' => 0
            ),
            'security' => array(
                'name' => '������ ��������',
                'desc' => '��� ������� ��������� ������ �������� ���� ������� � ������� �� ������ �� ��������.',
                'instant' => 0,
                'need_login' => 0,
                'need_cnt' => 0,
                'only_pay' => 1,
                'only_rating' => 0
            ),
            'contact' => array(
                'name' => '��������� � �������� ��� ���������',
                'desc' => '����� ����� �������� � ��������������������� ��������������, � ����� 
            ���������� ���� ������ ������ ��� ������������� ����������.',
                'instant' => 0,
                'need_login' => 0,
                'need_cnt' => 0,
                'only_pay' => 0,
                'only_rating' => 0
            ),
            'snigenie' => array(
                'name' => '�������� ������ ������',
                'desc' => '��� ������� ��������� ������� ������� ������ ������ � ����� �� ��� �������. 
                    �� ������ ��������������� ������ ������� 1 ��� � �����. �������� ���������� �� ���� 
                    ���������� ����� ������, ����������� ����� ������ ���������� �� ��������.',
                'instant' => 1,
                'need_login' => 1,
                'need_cnt' => 1,
                'only_pay' => 0,
                'only_rating' => 1
            ),
            'anonimus' => array(
                'name' => '������',
                'desc' => '��� ������� ��������� ������ ������ ������ ����������� � ������� �� 
            ��� ����� ������, � ��� ������ ���������. ����������, ��� ����� 
            ����� ����������� ����������.',
                'instant' => 0,
                'need_login' => 0,
                'need_cnt' => 0,
                'only_pay' => 0,
                'only_rating' => 0
            )
        );
    }
}