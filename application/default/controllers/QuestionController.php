<?php

class Form_Qna extends Zend_Form
{

    function init()
    {
        $this->setAction('/question/add')
            ->setMethod(Zend_Form::METHOD_POST)
            ->setAttrib('id', 'qna-add');

        $this->addElement('text', 'name', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '������'
        ));

        $name = $this->getElement('name');

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('����������, ������� ������');
        $name->addValidator($validator);

        $this->addElement('select', 'cat_id', array(
            'label' => '�������'
        ));

        $cat_id = $this->getElement('cat_id');

        $anCatQnATable = new CatQnATable;

        $cat_id->setMultiOptions($anCatQnATable->getOptions(false));

        $this->addElement('text', 'video', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '����� (Youtube)'
        ));
    }

}

class QuestionController extends Zend_Controller_Action
{

    function preDispatch()
    {
        parent::preDispatch();
        $this->_helper->menu->select('qna');
    }

    function indexAction()
    {
        if ($this->getRequest()->getParam('id')) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        if ($this->getRequest()->isPost()) {
            $this->redirectIfNotAjax();
            if (Zend_Auth::getInstance()->hasIdentity()) {
                $this->addQna();
            }
        } else {
            $this->view->canAdd = !$this->checkLimits();

            $anQnaTable = new QnaView;

            $select = $anQnaTable->select()
                ->where('is_approved=1')
                ->order('datetime DESC');

            $selectPopular = $anQnaTable->select()
                ->where('is_approved=1')
                ->order('datetime DESC')
                ->limit(16);

            if ($cat = $this->getRequest()->getParam('cat')) {
                $this->view->cat = $cat;
                $select = $select->where('cat_id=?', $cat->cat_id);

                $qnaTable = new CatQnATable();
                $this->view->currentCat = $qnaTable->fetchRow(
                    $qnaTable->select()
                        ->where('cat_id=?', $cat->cat_id)
                );

                $selectPopular->where('cat_id=?', $cat->cat_id);
            }

            $paginator = new Zend_Paginator(
                new Zend_Paginator_Adapter_DbTableSelect(
                    $select
                )
            );
            $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
            );
            $paginator->setItemCountPerPage(5);
            $paginator->setPageRange(10);

            $this->view->paginator = $paginator;
        }
    }

    function itemAction()
    {
        if (!$this->getRequest()->getParam('from')) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $anQnaTable = new QnaView();
        $item = $this->getRequest()->getParam('qna');

        if (!$item) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        //small protection against cheating
        if (!isset($_SESSION['read_qna'])) {
            $_SESSION['read_qna'] = array();
        }

        if (!in_array($item->qna_id, $_SESSION['read_qna'])) {
            $item->clicks++;
            $item->save();
            $_SESSION['read_qna'][] = $item->qna_id;
        }

        $anUserTable = new UsersTable();
        $user = $anUserTable->fetchRow(
            $anUserTable->select()
                ->where('user_id=?', $item->user_id)
        );

        if (!$user) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $this->view->user = $user;

        $commentsProcessor = new CommentsProcessor($item->qna_id, CommentsTable::TYPOLOGY_QNA);
        $commentsProcessor->run($this->getRequest());
        $this->view->commentsProcessor = $commentsProcessor;

        $this->view->item = $item;

        $select = $anQnaTable->select()
            ->where('is_approved=1')
            ->where('cat_id=?', $item->cat_id)
            ->where('qna_id<?', $item->qna_id)
            ->order('datetime DESC');
        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbTableSelect($select));
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(5);
        $this->view->add = $paginator;
    }

    function userAction()
    {
        $anQnaTable = new QnaView;

        $select = $anQnaTable->select()
            ->where('is_approved=1')
            ->order('datetime DESC')
            ->where('user_id=?', $this->getRequest()->getParam('id'));

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('qna_item_perpage', 5));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
        $this->view->user = Zend_Auth::getInstance()->getIdentity();
    }

    function addQna()
    {
        $form = new Form_Qna;

        $anQnaTable = new QnaTable();
        $qna = $anQnaTable->fetchNew();

        if ($form->isValid($_POST)) {
            if (!$this->checkLimits()) {
                $qna->name = iconv('UTF-8', 'windows-1251', $form->getValue('name'));
//                var_dump($qna->name); die;
                $qna->video = $form->getValue('video');
                $qna->cat_id = $form->getValue('cat_id');
                $qna->datetime = date('Y-m-d H:i:s');
                $qna->user_id = Zend_Auth::getInstance()->getIdentity()->user_id;
//            $qna->gallery = serialize($images);
                $qna->is_approved = (int)$this->isApproved(Zend_Auth::getInstance()->getIdentity());
                $qna->alias = Zend_Filter::filterStatic($qna->name, 'Translit');
                $qna->save();

                HookManager::getInstance()->call(HookManager::EVENT_NEW_QNA, array(
                    'qna' => $qna
                ));

                $anQnaTable = new QnaView;
                $select = $anQnaTable->select()
                    ->where('is_approved=1')
                    ->where('cat_id=?', $qna->cat_id)
                    ->order('datetime DESC');
                $paginator = new Zend_Paginator(
                    new Zend_Paginator_Adapter_DbTableSelect(
                        $select
                    )
                );
                $paginator->setCurrentPageNumber(
                    $this->getRequest()->getParam('p', 1)
                );
                $paginator->setItemCountPerPage(5);
                $paginator->setPageRange(10);

                $this->view->paginator = $paginator;

                $resp = array();
                $resp['errorCode'] = 0;
                $resp['msg'] = iconv("windows-1251", "UTF-8", $qna->is_approved ? '������ ��������.' : '������ �������� � ����� �������� �� ����� ����� �������� �����������.');
                $resp['replace_content'] = iconv("windows-1251", "UTF-8", $this->view->render('question/_question_list.phtml'));
                $resp['replace_id'] = 'question-list';
                $resp['action'] = 'comment-form';
                $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

                $resp = Zend_Json::encode($resp);
                $this->_helper->viewRenderer->setNoRender();
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                    ->appendBody($resp);

                echo $resp;
                exit;
            } else {
                $this->error(sprintf('�� �� ������ �������� ������, ��� ��� ��������� ������������ ���������� �������� �� ����.'));
            }
        } else {
            $errors = $this->getErrors($form->getMessages());
            $this->error(array_shift($errors));
        }
    }

    protected function isApproved(User $user)
    {
        return $user->isWriter() && $user->haveAprovedArticles();
    }

    protected function checkLimits()
    {
        $limit = Site_Settings::get('qna_limit', 0);
        if (!$limit) return false;

        $user = Zend_Auth::getInstance()->getIdentity();
        if (!$user) return true;

        $anQnaTable = new QnaTable;
        $items = $anQnaTable->fetchAll($anQnaTable
            ->select()
            ->where('user_id=?', $user->user_id)
            ->where('DATE(datetime)=?', date('Y-m-d')));

        if ($items->count() >= $limit) {
            return true;
        }

        return false;
    }


    protected function redirectIfNotAjax()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->_redirect('/');
        }
    }

    protected function error($msg)
    {
        $resp['errorCode'] = 1;
        $resp['msg'] = iconv("windows-1251", "UTF-8", $msg);
        $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

        $this->_helper->viewRenderer->setNoRender();
        $resp = Zend_Json::encode($resp);

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json; charset=UTF-8')
            ->appendBody($resp);

        echo $resp;
        exit;
    }

    protected function getErrors($messages)
    {
        $result = array();
        foreach ($messages as $msg) {
            $result = array_merge($result, array_values($msg));
        }

        return $result;
    }
}