<?php

class IndexController extends Zend_Controller_Action
{
    const TYPE_ALL = 'all';
    const TYPE_DRAFT = 'draft';

    function indexAction()
    {
        $anArticlesView = new ArticlesView();
        $select = $anArticlesView->select();

        switch ($this->getType()) {
            case self::TYPE_DRAFT :
                $select = $select->where('is_approved=?', 0);
                $this->view->header = '���������';
                $this->view->route = 'draft-index';
                $this->view->displayPopular = false;
                $perpage = Config::get('draft_article_perpage', 24);
                break;
            default :
                $select = $select->where('is_approved=?', 1);
                $this->view->header = '����������';
                $this->view->route = 'article-index';
                $this->view->displayPopular = true;
                $perpage = Config::get('index_article_perpage', 24);
                break;

        }

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select->order('date DESC')
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage($perpage);
        $paginator->setPageRange(10);

        $this->_helper->viewRenderer->setNoRender();
        $this->view->paginator = $paginator;

        switch ($this->getType()) {

            case self::TYPE_DRAFT :
                echo $this->view->render('index/draft.phtml');
                break;
            default :
                echo $this->view->render('index/index.phtml');
                break;
        }
    }

    protected function getType() {
        return self::TYPE_ALL;
    }

}