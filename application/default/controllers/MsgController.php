<?php

class MsgController extends Zend_Controller_Action
{

    function preDispatch()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            Zend_Layout::getMvcInstance()->disableLayout();
            $params = $this->getRequest()->getParams();
            $params = $this->decode("UTF-8", "windows-1251", $params);
            $this->getRequest()->setParams($params);
            $_POST = $this->decode("UTF-8", "windows-1251", $_POST);
            $_GET = $this->decode("UTF-8", "windows-1251", $_GET);
        }
    }

    function decode($from, $to, $value)
    {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->decode($from, $to, $v);
            }
        } else {
            $value = iconv($from, $to, $value);
        }
        return $value;
    }

    function getThreadAction()
    {
        if ($this->_helper->auth->requireLogin()) {
            return;
        }

        $this->view->user = Zend_Auth::getInstance()->getIdentity();

        $expr = new Zend_Db_Expr(sprintf('IF(sender_id=%1$d, CONCAT(sender_id, \'-\', recipient_id), CONCAT(recipient_id, \'-\', sender_id)) = \'%1$d-%2$d\'',
            Zend_Auth::getInstance()->getIdentity()->user_id,
            $this->getRequest()->getParam('id')
        ));

        $select = $this->getSelect()->where($expr);

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );

        $paginator->setCurrentPageNumber(1);
        $this->view->paginator = $paginator;

    }

    /**
     * @return Zend_Db_Table_Select
     */
    function getSelect()
    {
        $anMsgView = new MsgView;
        $user_id = (int)Zend_Auth::getInstance()->getIdentity()->user_id;

        $select = $anMsgView->select()
            ->from('msg_view', array('*',
                'cnt' => new Zend_Db_Expr('COUNT(message_id)'),
                'datecontent' => new Zend_Db_Expr('MAX(CONCAT(datetime, \'|\', content))'),
                'datesender_id' => new Zend_Db_Expr('MAX(CONCAT(datetime, \'|\', sender_id))'),
                'dateis_new' => new Zend_Db_Expr('MAX(CONCAT(datetime, \'|\', is_new))'),
                'dateread_date' => new Zend_Db_Expr('MAX(CONCAT(datetime, \'|\', read_date))'),
                'datelastmsg' => new Zend_Db_Expr("MAX(CONCAT(IF(recipient_id = $user_id, 1, 0), '|', datetime, '|', content))"),
                'maxdate' => new Zend_Db_Expr('MAX(datetime)')
            ))
            ->where(sprintf('recipient_id = %1$d OR sender_id = %1$d', Zend_Auth::getInstance()->getIdentity()->user_id))
            ->order('maxdate DESC')
            ->group(new Zend_Db_Expr(sprintf('IF(sender_id=%1$d, CONCAT(sender_id, \'-\', recipient_id), CONCAT(recipient_id, \'-\', sender_id))',
                Zend_Auth::getInstance()->getIdentity()->user_id
            )));
        return $select;
    }

    function indexAction()
    {
        if ($this->_helper->auth->requireLogin()) {
            return;
        }

        $this->view->user = Zend_Auth::getInstance()->getIdentity();

//        $select = new Zend_Db_Select(Zend_Db_Table_Abstract::getDefaultAdapter());
//
//        $select = $select->from('message', array('cnt' => new Zend_Db_Expr(sprintf(
//            "DISTINCT CONCAT(IF(sender_id=%1\$d,sender_id,recipient_id) ,'-', IF(sender_id=%1\$d,recipient_id,sender_id))",
//            Zend_Auth::getInstance()->getIdentity()->user_id
//        ))))->where(sprintf('sender_id = %1$d OR recipient_id = %1$d',
//            Zend_Auth::getInstance()->getIdentity()->user_id
//        ));
//
//        $res = $select->query(PDO::FETCH_OBJ)->fetch();
//
//        var_dump($select->__toString()); die;
//        $cnt = $res->cnt;
//
//        $adapter = new Zend_Paginator_Adapter_DbTableSelect(
//            $this->getSelect()
//        );
//        $adapter->setRowCount((int)$cnt);
//
//        $paginator = new Zend_Paginator($adapter);
//        $paginator->setCurrentPageNumber(
//            $this->getRequest()->getParam('p', 1)
//        );
//        $paginator->setItemCountPerPage(Site_Settings::get('front_dialog_perpage', 24));
//        $paginator->setPageRange(10);
//        $paginator->getPages();
//
//        $this->view->paginator = $paginator;
//        $anMsgTable = new MsgTable();
//        $anMsgTable->update(array(
//            'is_new' => 0,
//            'read_date' => date('Y-m-d H:i:s')
//        ), array(
//            'recipient_id=' . Zend_Auth::getInstance()->getIdentity()->user_id,
//            'is_new=1'));

        //contact list
        $anMessagesView = new MsgTable();
        $select = $anMessagesView->select()
            ->from('message as m')
            ->setIntegrityCheck(false)
            ->joinLeft('users as u', 'm.recipient_id=u.user_id', array(
                'u.*',
                'last_date' => new Zend_Db_Expr('MAX(m.datetime)')
            ))
            ->where('m.sender_id=? OR m.recipient_id=? AND u.user_id!=\'' . Zend_Auth::getInstance()->getIdentity()->user_id . '\'', Zend_Auth::getInstance()->getIdentity()->user_id)
            ->group(array('u.user_id'))
            ->order('last_date DESC');
        $this->view->users = $anMessagesView->fetchAll($select);
    }

    function updateAction()
    {
        if ($this->_helper->auth->requireLogin()) {
            return;
        }
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout()->disableLayout();
        set_time_limit(0);
        Zend_Session::writeClose();
        $uid = Zend_Auth::getInstance()->getIdentity()->user_id;
        $anMessagesView = new MsgTable();
        $selectLatestConvertation = $anMessagesView->select()
            ->from('message')
            ->setIntegrityCheck(false)
            ->joinLeft('users', 'message.sender_id=users.user_id', array(
                'user' => '*'
            ))
            ->where('(sender_id=? AND recipient_id=\'' . $uid . '\') 
            OR (sender_id=\'' . $uid . '\' AND recipient_id=?)', $this->getRequest()->getParam('rid', 1))
            ->where('message_id>?', $this->getRequest()->getParam('mid', 0))
            ->group(array('message.message_id'))
            ->order('datetime ASC');
        while (true) {
            sleep(1);
            $latestMessages = $anMessagesView->fetchAll($selectLatestConvertation);
            if (count($latestMessages)) {
                break;
            }
        }
 
        $this->view->latestMessages = $latestMessages;
        $this->view->receiverId = $this->getRequest()->getParam('rid', 1);
        $this->view->noForm = true;
        $out = json_encode(iconv("windows-1251", "UTF-8", $this->view->render('ajax/usermessages.phtml')));
        
        echo <<<CUT
$('.add-message').before($out);
CUT;
    }
    
    function dialogAction()
    {
        if ($this->_helper->auth->requireLogin()) {
            return;
        }

        $this->view->user = Zend_Auth::getInstance()->getIdentity();

        $anMsgView = new MsgView;

        $select = $anMsgView->select()
            ->where(sprintf('recipient_id = %1$d AND sender_id = %2$d',
                Zend_Auth::getInstance()->getIdentity()->user_id, $this->getRequest()->getParam('id')))
            ->orWhere(sprintf('recipient_id = %2$d AND sender_id = %1$d',
                Zend_Auth::getInstance()->getIdentity()->user_id, $this->getRequest()->getParam('id')))
            ->order('datetime DESC');

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_messages_perpage', 24));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }

    function sendAction()
    {
        $this->redirectIfNotAjax();

        if ($cb = $this->getRequest()->getParam('cb')) {
            $this->view->cb = $cb;
        }

        $sendForm = new Zend_Form();

        $sendForm->setAction('/msg/send')
            ->setMethod(Zend_Form::METHOD_POST)
            ->setAttrib('class', 'ajax')
            ->setAttrib('id', 'sendmsg');

        $sendForm->addElement('textarea', 'content', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '����� ���������'
        ));

        $content = $sendForm->getElement('content');

        $content->clearValidators();

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('��������, ����������, ���������');
        $content->addValidator($validator);

        $sendForm->addElement('hidden', 'id', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '����������'
        ));
        $sendForm->getElement('id')->setValue($this->getRequest()->getParam('id'));

        $anUsersTable = new UsersTable();
        $this->view->user = $anUsersTable->find($this->getRequest()->getParam('id'))->current();

        if ($this->getRequest()->isPost()) {
            if ($sendForm->isValid($_POST)) {

                if (!Zend_Auth::getInstance()->getIdentity()->canSendMsg($sendForm->getValue('id'))) {
                    return $this->error('�� �� ������ �������� ������ ��������� ����� ������������');
                }

                $anMsgTable = new MsgTable();

                $msg = $anMsgTable->fetchNew();

                $msg->sender_id = Zend_Auth::getInstance()->getIdentity()->user_id;
                $msg->recipient_id = $sendForm->getValue('id');
                $msg->datetime = date('Y-m-d H:i:s');
                $msg->content = $sendForm->getValue('content');
                $msg->is_new = 1;

                $msg->save();

                $this->notifyRecipient($msg);

                $resp = array();
                $resp['errorCode'] = 0;
                $resp['msg'] = iconv("windows-1251", "UTF-8", '��������� ����������');
                $resp['action'] = $this->view->cb ? 'cb' : 'close';
                $resp['recipient_id'] = $msg->recipient_id;
                $resp['cb'] = $this->view->cb;
                $resp['action'] = 'reset';
                $resp['url'] = $this->view->url(array('controller' => 'msg', 'action' => 'index'), 'default', true);
                $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');
                $resp = Zend_Json::encode($resp);
                $this->_helper->viewRenderer->setNoRender();
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                    ->appendBody($resp);
                return;
            } else {
                $messages = $sendForm->getMessages();
                return $this->error(current(current($messages)));
            }
        } else {
            $this->view->form = $sendForm;
        }
    }

    protected function notifyRecipient($msg)
    {
        $anUserTable = new UsersTable;

        $sender = $anUserTable->find($msg->sender_id)->current();
        $recipient = $anUserTable->find($msg->recipient_id)->current();

        $mail = new Site_Mail('msg');
        $mail->setRecipient($recipient);
        $mail->setSender($sender);
        $mail->send($recipient->email);
    }

    protected function error($msg)
    {
        $resp['errorCode'] = 1;
        $resp['msg'] = iconv("windows-1251", "UTF-8", $msg);
        $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

        $this->_helper->viewRenderer->setNoRender();
        $resp = Zend_Json::encode($resp);

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json; charset=UTF-8')
            ->appendBody($resp);
    }

    protected function redirectIfNotAjax()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->_redirect('/');
        }
    }

}