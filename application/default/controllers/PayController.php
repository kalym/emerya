<?php
//http://money.yandex.ru/doc.xml?id=459801
class PayController extends Zend_Controller_Action {
    function preDispatch() {
        if ($this->getRequest()->isXmlHttpRequest()) {
            Zend_Layout::getMvcInstance()->disableLayout();
            $params = $this->getRequest()->getParams();
            $params = $this->decode("UTF-8", "windows-1251", $params);
            $this->getRequest()->setParams($params);
            $_POST = $this->decode("UTF-8", "windows-1251", $_POST);
            $_GET = $this->decode("UTF-8", "windows-1251", $_GET);
        }
    }

    function decode($from, $to, $value) {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->decode($from, $to, $v);
            }
        } else {
            $value = iconv($from, $to, $value);
        }
        return $value;
    }
    
    //http://wiki.webmoney.ru/wiki/show/Web+Merchant+Interface
    //Result url topproduct.ru/pay/wm-ipn
    //Success url topproduct.ru/pay/wm-success
    //Fail url topproduct.ru/pay/wm-fail
    
    function wmIpnAction() {
        //file_put_contents(dirname(__FILE__) . '/../../../uploads/log.txt', serialize($this->getRequest()->getParams()));
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $anPaymentTable = new PaymentTable;
        
        $payment = $anPaymentTable->find($this->getRequest()->getParam('LMI_PAYMENT_NO'))
                ->current();
        
        if (!$payment || $payment->completed) {
            return;
        }
                    
        $hash = $this->getRequest()->getParam('LMI_HASH');
        
        $params = $this->getRequest()->getParams();
        $sign = strtoupper(md5($params['LMI_PAYEE_PURSE'] . 
                $params['LMI_PAYMENT_AMOUNT'] . 
                $params['LMI_PAYMENT_NO'] . 
                $params['LMI_MODE'] .
                $params['LMI_SYS_INVS_NO'] .
                $params['LMI_SYS_TRANS_NO'] .
                $params['LMI_SYS_TRANS_DATE'] .
                Site_Settings::get('wm-secret-key') .
                $params['LMI_PAYER_PURSE'] .
                $params['LMI_PAYER_WM']));
        
        if ($sign!=$hash) {
            return;
        }
                      
        $payment->completed = 1;
        $payment->tm_completed = date('Y-m-d H:i:s');
        $payment->receipt = sprintf('%s/%s',
                $this->getRequest()->getParam('LMI_SYS_INVS_NO'),
                $this->getRequest()->getParam('LMI_SYS_TRANS_NO'));
        $payment->amount = $this->getRequest()->getParam('LMI_PAYMENT_AMOUNT', 0);
        $payment->save();
        
        $anUserTable = new UsersTable;
	$user = $anUserTable->find($payment->user_id)->current();
        
        $service = $payment->service ? $payment->service : 'premium';
        $user->activateService($service, $payment->tm_completed, array(
            'amount' => $payment->amount,
            'login' => $payment->login,
            'cnt' => $payment->cnt,
            'id' => $payment->id
        ));
    }
    
    function wmSuccessAction() {
        header("Location: /");
        exit;
        $this->view->message = "��� ������ ������!";
        $this->_helper->viewRenderer->setScriptAction('message');
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();
            $adapter = new Site_Auth_Adapter_Null($user->login, '');
	    Zend_Auth::getInstance()->authenticate($adapter);
        }
    }
    
    function wmFailAction() {
        $this->view->message = "�������� ������. ���������� ��������� �������.";
        $this->_helper->viewRenderer->setScriptAction('message');
    }

    protected function redirectIfNotAjax() {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->_redirect('/');
        }
    }

    protected function error($msg) {
        $resp['errorCode'] = 1;
        $resp['msg'] = iconv("windows-1251", "UTF-8", $msg);
        $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

        $this->_helper->viewRenderer->setNoRender();
        $resp = Zend_Json::encode($resp);

        $this->getResponse()
                ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                ->appendBody($resp);
    }
}
