<?php

class StatsController extends Zend_Controller_Action {

    function preDispatch() {
        parent::preDispatch();
        $this->_helper->menu->select('stats');
    }

    function getStats() {
        $stats = array();

        $anCatsView = new CatsView();
        $stats['cats'] = $anCatsView->fetchAll(
                $anCatsView->select()->order('pub_clicks DESC')->limit(10)
        );

        $catsRow = $anCatsView->fetchRow(
                $anCatsView->select()
                        ->from('cats_view', array('max' => new Zend_Db_Expr('MAX(pub_clicks)')))
        );

        $stats['catsMax'] = $catsRow->max;

        $anUsersTable = new UsersTable();
        $stats['users'] = $anUsersTable->fetchAll(
                $anUsersTable->select()->order('clicks DESC')->limit(10)
        );

        $usersRow = $anUsersTable->fetchRow(
                $anUsersTable->select()
                        ->from('users', array('max' => new Zend_Db_Expr('MAX(clicks)')))
        );

        $stats['usersMax'] = $usersRow->max;

        $anArticlesTable = new ArticlesTable();
        $stats['articles'] = $anArticlesTable->fetchAll(
                $anArticlesTable->select()->order('clicks DESC')->limit(10)
        );

        $articlesRow = $anArticlesTable->fetchRow(
                $anArticlesTable->select()
                        ->from('articles', array('max' => new Zend_Db_Expr('MAX(clicks)')))
        );

        $stats['articlesMax'] = $articlesRow->max;

        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select = $select->from(array('a' => 'articles'))->joinLeft(array('c' => 'comments'), "a.article_id=c.typology_id AND c.typology = 'article'", array(
                    'c_cnt' => new Zend_Db_Expr('COUNT(c.comment_id)')
                ))->group('a.article_id')->order('c_cnt DESC')->limit(10);
        $stats['discussion'] = new Zend_Db_Table_Rowset(array('data' => $select->query()->fetchAll()));
        $stats['discussionMax'] = $stats['discussion']->current()->c_cnt;

        //active users       
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select = $select->from(array('a' => 'users'))->joinLeft(array('c' => 'comments'), "a.user_id=c.user_id", array(
                    'c_cnt' => new Zend_Db_Expr('COUNT(c.comment_id)')
                ))->group('a.user_id')->order('c_cnt DESC')->limit(10);
        $stats['activeUsers'] = new Zend_Db_Table_Rowset(array('data' => $select->query()->fetchAll()));
        $stats['activeUsersMax'] = $stats['activeUsers']->current()->c_cnt;
        return $stats;
    }

    function indexAction() {
        
        if ($this->_helper->serv->requireService('stat')) {
            return;
        }
        
        $frontendOptions = array(
            'lifetime' => 3600 * 24 * 7, // cache lifetime of 1 week
            'automatic_serialization' => true
        );

        $backendOptions = array(
            'cache_dir' => Zend_Registry::getInstance()->get('root_dir') . '/tmp/' // Directory where to put the cache files
        );

        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
        if (!($stats = $cache->load('stats'))) {
            ignore_user_abort(1);
            $stats = $this->getStats();
            $cache->save($stats, 'stats');
        }

        foreach ($stats as $key => $value) {
            $this->view->{$key} = $value;
        }
    }

}