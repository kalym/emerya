<?php

class Form_Articles extends Zend_Form
{

    function init()
    {
        $this->setAction('/articles/add')
            ->setMethod(Zend_Form::METHOD_POST)
            ->setAttrib('encode', Zend_Form::ENCTYPE_MULTIPART)
            ->setAttrib('id', 'articles-add');

        $this->addElement('text', 'name', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '���������'
        ));

        $this->addElement('textarea', 'content', array(
            'required' => true,
            'label' => '����� ����������',
            'class' => 'big-textarea'
        ));

        $this->addElement('checkbox', 'isPremium', array(
            'label' => '������ ��� ��������',
            'class' => 'big-textarea',
            'id' => 'isPremium'
        ));

        $content = $this->getElement('content');


        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('����������, ������� ����� ����������');
        $content->addValidator($validator);

        $this->addElement('select', 'cat_id', array(
            'label' => '�������'
        ));

        $cat_id = $this->getElement('cat_id');

        $anCatsTable = new CatsTable;

        $cat_id->setMultiOptions($anCatsTable->getOptions(true, Zend_Auth::getInstance()->getIdentity()->user_id));

        $anATView = new ATView;
        $atOptions = $anATView->getOptions(!ArticlesController::isAtOnly(), Zend_Auth::getInstance()->getIdentity()->user_id, true);
        if (count($atOptions) > 1 || ArticlesController::isAtOnly()) {
            $this->addElement('select', 'at_id', array(
                'label' => '������������ ������'
            ));

            $at_id = $this->getElement('at_id');
            $at_id->setMultiOptions($atOptions);
        }
    }

    public function isValid($data)
    {
        if (!@$data['at_id']) {
            $cat_id = $this->getElement('cat_id');

            $cat_id->setRequired(true);
            $validator = new Zend_Validate_NotEmpty();
            $validator->setMessage('����������, �������� �������');
            $cat_id->addValidator($validator);

            $name = $this->getElement('name');

            $name->setRequired(true);
            $validator = new Zend_Validate_NotEmpty();
            $validator->setMessage('��������� ������ ����������� ��� ����������');
            $name->addValidator($validator);

        }
        return parent::isValid($data);
    }
}


class Form_Sponsor extends Zend_Form
{

    function init()
    {
        $this->setAction('/articles/sponsor')
            ->setMethod(Zend_Form::METHOD_POST)
            ->setAttrib('class', 'ajax')
            ->setAttrib('id', 'sponsor');

        $this->addElement('hidden', 'article_id');
        $article_id = $this->getElement('article_id');
        $article_id->setDecorators(array('ViewHelper'));

        $this->addElement('text', 'name', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '���� ���'
        ));

        $name = $this->getElement('name');

        $name->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        ))));

        $name->clearValidators();

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� ��� ����������� ��� ����������');
        $name->addValidator($validator);

        $this->addElement('text', 'email', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '������� ���� e-mail'
        ));

        $email = $this->getElement('email');

        $email->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        ))));

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� E-mail ����������� ��� ����������');
        $email->addValidator($validator);

        $validator = new Zend_Validate_EmailAddress();
        $validator->setMessage('�� ������ ������ E-mail');
        $email->addValidator($validator);


        $this->addElement('text', 'text', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '��� �����'
        ));

        $text = $this->getElement('text');

        $text->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        ))));

        $text->clearValidators();

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� ����� ����������� ��� ����������');


        $text->addValidator($validator);


        $this->addElement('text', 'keyword', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '�������� ����� ��� ����� � ������'
        ));

        $text = $this->getElement('keyword');

        $text->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        ))));

        $text->clearValidators();

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� �������� ����� ����������� ��� ����������');


        $text->addValidator($validator);

        $this->addElement('text', 'uri', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '������'
        ));

        $text = $this->getElement('uri');

        $text->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        ))));

        $text->clearValidators();

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� ������ ����������� ��� ����������');


        $text->addValidator($validator);


        $this->addElement('text', 'aff', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '��� �������� (����� ������), �������� ���� ������, ���� ������������:'
        ));

        $aff = $this->getElement('aff');

        $validator = new Zend_Validate_Db_RecordExists('users', 'login');
        $validator->setMessage('����� � ������� %value% �� ����� �� ���������������');
        $aff->addValidator($validator);

        $aff->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form-aff.phtml',
        ))));


        $this->setDecorators(array('FormElements', array('Callback', array(
            'callback' => array($this, 'appendSubmit')
        )), 'Form', array('HtmlTag', array(
            'tag' => 'ul',
            'class' => 'form'
        ))));
    }

    function setArticleId($id)
    {
        $this->getElement('article_id')->setValue($id);
    }

    function appendSubmit($content, $element, $options)
    {
        return '<li><a class="btn-red" href="#"><span>��������� ������</span></a></li>';
    }

}

class Form_Share extends Zend_Form
{

    function init()
    {
        $this->setAction('/articles/share')
            ->setMethod(Zend_Form::METHOD_POST)
            ->setAttrib('class', 'ajax')
            ->setAttrib('id', 'share');

        $this->addElement('hidden', 'article_id');
        $article_id = $this->getElement('article_id');
        $article_id->setDecorators(array('ViewHelper'));

        $this->addElement('text', 'email', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => 'E-mail �����'
        ));

        $email = $this->getElement('email');

        $email->setDecorators(array(array('ViewScript', array(
            'viewScript' => 'form.phtml',
        ))));

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� E-mail ����������� ��� ����������');
        $email->addValidator($validator);

        $validator = new Zend_Validate_EmailAddress();
        $validator->setMessage('�� ������ ������ E-mail');
        $email->addValidator($validator);

        $this->setDecorators(array('FormElements', array('Callback', array(
            'callback' => array($this, 'appendSubmit')
        )), 'Form', array('HtmlTag', array(
            'tag' => 'ul',
            'class' => 'form'
        ))));
    }

    function setArticleId($id)
    {
        $this->getElement('article_id')->setValue($id);
    }

    function appendSubmit($content, $element, $options)
    {
        return '<li><a class="btn-red" href="#"><span>��������� �����</span></a></li>';
    }

}

class ArticlesController extends Zend_Controller_Action
{
    protected $images = array();

    function preDispatch()
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            Zend_Layout::getMvcInstance()->disableLayout();
            $params = $this->getRequest()->getParams();
            $params = $this->decode("UTF-8", "windows-1251", $params);
            $this->getRequest()->setParams($params);
            $_POST = $this->decode("UTF-8", "windows-1251", $_POST);
            $_GET = $this->decode("UTF-8", "windows-1251", $_GET);
        }
    }

    function decode($from, $to, $value)
    {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->decode($from, $to, $v);
            }
        } else {
            $value = iconv($from, $to, $value);
        }
        return $value;
    }

    protected function redirectIfNotAjax()
    {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->_redirect('/');
        }
    }

    protected function error($msg)
    {
        $resp['errorCode'] = 1;
        $resp['msg'] = iconv("windows-1251", "UTF-8", $msg);
        $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

        $this->_helper->viewRenderer->setNoRender();
        $resp = Zend_Json::encode($resp);

        $this->getResponse()
            ->setHeader('Content-Type', 'application/json; charset=UTF-8')
            ->appendBody($resp);
    }

    protected function getErrors($messages)
    {
        $result = array();
        foreach ($messages as $msg) {
            $result = array_merge($result, array_values($msg));
        }

        return $result;
    }

    static function isAtOnly()
    {
        $anATView = new ATView;
        $atOptions = $anATView->getOptions(true, Zend_Auth::getInstance()->getIdentity()->user_id);
        return Site_Settings::get('at_only', 0) && count($atOptions) > 1;
    }

    function shareAction()
    {
        $this->redirectIfNotAjax();
        $form = new Form_Share;
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {

                $anArticlesTable = new ArticlesTable;
                $article = $anArticlesTable->find($form->getValue('article_id'))->current();

                $mail = new Site_Mail('share');
                $mail->setArticle($article);
                $mail->setAbstract($this->view->strip(strip_tags($article->content)));
                $mail->setUrl('http://www.topauthor.ru' . $this->view->url(array($article->alias), 'article', true));
                $mail->send($form->getValue('email'));

                $resp = array();
                $resp['errorCode'] = 0;
                $resp['msg'] = iconv("windows-1251", "UTF-8", '������ � ��������� ������ ���������� �� ��������� ���� E-mail');
                $resp['action'] = 'hide-form';
                $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

                $anShareLogTable = new ShareLogTable;
                $anShareLogTable->insert(array(
                    'dattm' => date('Y-m-d H:i:s'),
                    'user_id' => Zend_Auth::getInstance()->hasIdentity() ?
                        Zend_Auth::getInstance()->getIdentity()->user_id :
                        null,
                    'typology_id' => $article->article_id,
                    'typology' => 'article',
                    'email' => $form->getValue('email')
                ));

                $resp = Zend_Json::encode($resp);
                $this->_helper->viewRenderer->setNoRender();
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                    ->appendBody($resp);
            } else {
                $errors = $this->getErrors($form->getMessages());
                $this->error(array_shift($errors));
            }
        } else {
            $form->setArticleId($this->getRequest()->getParam('id'));
            $this->view->form = $form;
        }
    }

    function videoAction()
    {
        $this->redirectIfNotAjax();
        Zend_Layout::getMvcInstance()->disableLayout();

        $id = $this->getRequest()->getParam('id');
        $video = $this->getRequest()->getParam('video');

        $anArticlesTable = new ArticlesTable;
        $article = $anArticlesTable->find($id)->current();

        if (!$article)
            throw new Zend_Controller_Dispatcher_Exception('');

        $video_gallery = $article->getVideoGallery();
        if (!isset($video_gallery[$video]))
            throw new Zend_Controller_Dispatcher_Exception('');

        $this->view->video = $video_gallery[$video]['video'];
    }

    function searchAction()
    {
        if ($this->getRequest()->isPost()) {
            $anStatQuery = new StatQueryTable;
            $anStatQuery->log($this->getRequest()->getParam('q'), StatQueryTable::TYPE_ARTICLE, date('Y-m-d H:i:s'));

            $this->_redirect(
                Zend_Controller_Front::getInstance()->getRouter()->assemble(array(
                    'controller' => 'articles',
                    'action' => 'search',
                    'q' => $this->getRequest()->getParam('q')
                ))
            );
        }

        $q = $this->getRequest()->getParam('q');
        $anArticlesView = new ArticlesView();

        $select = $anArticlesView->select()
            ->where('content LIKE ?', '%' . $q . '%')
            ->orWhere('name LIKE ?', '%' . $q . '%')
            ->order(new Zend_Db_Expr(sprintf('name LIKE %s DESC',
                $anArticlesView->getAdapter()->quote('%' . $q . '%')
            )));

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_articles_perpage', 12));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;

    }

    function sponsorAction()
    {
        $this->redirectIfNotAjax();
        $form = new Form_Sponsor;
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {
                $anSponsorTable = new SponsorTable();
                $sponsor = $anSponsorTable->fetchNew();

                $sponsor->name = $form->getValue('name');
                $sponsor->email = $form->getValue('email');
                $sponsor->text = $form->getValue('text');
                $sponsor->keyword = $form->getValue('keyword');
                $sponsor->uri = $form->getValue('uri');
                $sponsor->article_id = $form->getValue('article_id');
                $sponsor->datetime = date('Y-m-d H:i:s');
                $sponsor->aff = $form->getValue('aff');

                $sponsor->save();

                $resp = array();
                $resp['errorCode'] = 0;
                $resp['msg'] = iconv("windows-1251", "UTF-8", '���� ������ ������� � ����� ���������� � ��������� �����. ����� ��������� ��� ����� ������� ������ � ������������.');
                $resp['action'] = 'hide-form';
                $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

                $resp = Zend_Json::encode($resp);
                $this->_helper->viewRenderer->setNoRender();
                $this->getResponse()
                    ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                    ->appendBody($resp);
            } else {
                $errors = $this->getErrors($form->getMessages());
                $this->error(array_shift($errors));
            }
        } else {
            $form->setArticleId($this->getRequest()->getParam('id'));
            $this->view->form = $form;
        }
    }

    function indexAction()
    {
        $anArticlesView = new ArticlesView();
        $anCatsTable = new CatsTable();

        $cat = $this->getRequest()->getParam('cat');

        if (!$cat) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }
        $this->view->cat = $cat;

        $select = $anArticlesView->select()
            ->where('is_approved=?', 1)
            ->where('cat_id=?', $cat->cat_id)
            ->order('date DESC');

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('cat_article_perpage', 36));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }

    function itemAction()
    {
        if (!$this->getRequest()->getParam('from')) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $commentsProcessor = new CommentsProcessor($this->getRequest()->getParam('id'), CommentsTable::TYPOLOGY_ARTICLE);
        $commentsProcessor->run($this->getRequest());
        $this->view->commentsProcessor = $commentsProcessor;

        $anArticlesView = new ArticlesView();
        $anCatsTable = new CatsTable();

        $item = $anArticlesView->find(
            $this->getRequest()->getParam('id')
        )->current();

        if (!$item) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        if (!$item->ogimage) {
            $item->ogimage = $this->view->ogImage($item->name, $item->article_id);
            $item->save();
        }

        //small protection against cheating
        if (!isset($_SESSION['read_article'])) {
            $_SESSION['read_article'] = array();
        }

        if (!in_array($item->article_id, $_SESSION['read_article'])) {
            $item->clicks++;
            $item->save();
            $_SESSION['read_article'][] = $item->article_id;
        }

        $anUsersTable = new UsersTable();
        $user = $anUsersTable->find($item->user_id)->current();
        $user->rating += Config::get('rating_click_article', 0);
        $user->save();

        $this->view->cat = $anCatsTable->find($item->cat_id)->current();

        $select = $anArticlesView->select()
            ->where('cat_id=?', $item->cat_id)
            ->where('article_id<>?', $item->article_id)
            ->where('is_approved=?', 1)
            ->order('date DESC');
        $other = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $other->setCurrentPageNumber(
            1
        );
        $other->setItemCountPerPage(10);
        $this->view->other = $other;

        $anArticleViewTable = new ArticleViewTable;
        $anArticleViewTable->log($item);

        $this->view->item = $item;

        if (Zend_Auth::getInstance()->getIdentity()) {
            $this->view->user = Zend_Auth::getInstance()->getIdentity();

            $anRatingsTable = new RatingsTable();
            $prevRate = $anRatingsTable->fetchAll(
                $anRatingsTable->select()
                    ->where('article_id=?', $this->getRequest()->getParam('id'))
                    ->where('user_id=?', $this->view->user->user_id)
            )->current();
            $this->view->isRated = $prevRate;

            $anNotepadTable = new NotepadTable();
            $prevNotepad = $anNotepadTable->fetchAll(
                $anNotepadTable->select()
                    ->where('article_id=?', $this->getRequest()->getParam('id'))
                    ->where('user_id=?', $this->view->user->user_id)
            )->current();
            $this->view->isNotepad = $prevNotepad;
        }
    }

    protected function checkLimits()
    {
        $limit = Site_Settings::get('article_limit', 0);
        if (!$limit) return false;

        $limit += floor(Zend_Auth::getInstance()->getIdentity()->getKarma() / Site_Settings::get('article_limit_increase')); //increase limit for karma

        $user = Zend_Auth::getInstance()->getIdentity();
        $anArticleTable = new ArticlesTable;
        $items = $anArticleTable->fetchAll($anArticleTable
            ->select()
            ->where('user_id=?', $user->user_id)
            ->where('DATE(date)=?', date('Y-m-d')));

        if ($items->count() >= $limit) {
            $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
            $viewRenderer->setRender('limit');
            return true;
        }

        return false;
    }

    function addAction()
    {
        if ($this->_helper->auth->requireLogin(true)) {
            return;
        }
        $this->view->user = Zend_Auth::getInstance()->getIdentity();

        //do not allow write alresdy booked articles
        if ($at_id = $this->getRequest()->getParam('at_id')) {
            $atView = new ATView();
            $at = $atView->find($at_id)->current();
            if (!$at ||
                ($at->isBooked() && !$at->isBooked(Zend_Auth::getInstance()->getIdentity()->user_id))
            ) {
                throw new Zend_Controller_Dispatcher_Exception('');
            } else {
                $this->view->at = $at;
            }
        }

        $form = new Form_Articles;

        $anArticlesTable = new ArticlesTable();
        $article = $anArticlesTable->fetchNew();
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $prev_article = $anArticlesTable->find($id)->current();
            if ($prev_article &&
                !$prev_article->is_approved &&
                $prev_article->user_id == Zend_Auth::getInstance()->getIdentity()->user_id
            ) {

                if ($this->getRequest()->isGet()) {
                    $form->populate($prev_article->toArray());
                    $this->images = array(
                        0 => $prev_article->preview,
                        1 => $prev_article->preview2,
                        2 => $prev_article->image,
                        3 => $prev_article->widget_img,
                        4 => $prev_article->original
                    );
                }

                if ($prev_article->at_id || self::isAtOnly()) {
                    $name = $form->getElement('name');
                    $name->setAttrib('disabled', 'disabled');
                    $name->setValue($prev_article->name);
                    $cat_id = $form->getElement('cat_id');
                    $cat_id->setAttrib('disabled', 'disabled');
                    $cat_id->setValue($prev_article->cat_id);
                    $form->removeElement('at_id');
                }

                $article = $prev_article;
            }
        }
        //it is new article and ...
        if (!$article->article_id && $this->checkLimits()) {
            return;
        }

        $this->view->article = $article;

        $uploadError = $this->uploadImage();

        if (!$uploadError && $this->getRequest()->isPost() && !$this->getImage()) {
            $uploadError = '�� �� ��������� ��������';
        }

        if ($this->getRequest()->isPost() && $form->isValid(array_merge($form->getValues(true), $_POST)) && !$uploadError) {
            if ($at_id = $form->getValue('at_id')) {
                $anATTable = new ATTable();
                $at = $anATTable->find($at_id)->current();
                $article->at_id = $at_id;
                $name = $at->name;
                $cat_id = $at->cat_id;
                $at->is_active = 0;
                $at->save();
            } else {
                $name = $form->getValue('name') ? $form->getValue('name') : $article->name;
                $cat_id = $form->getValue('cat_id') ? $form->getValue('cat_id') : $article->cat_id;
            }

            $article->name = $name;
            $article->content = $form->getValue('content');
            $article->isPremium = $form->getValue('isPremium');
            $article->cat_id = $cat_id;
            $article->image = $this->getImage();
            $article->preview = $this->getImagePreview();
            $article->preview2 = $this->getImagePreview2();
            $article->original = $this->getImageOriginal();
            $article->widget_img = $this->getImageWidget();
            $article->date = $article->date ? $article->date : date('Y-m-d H:i:s');
            $article->user_id = Zend_Auth::getInstance()->getIdentity()->user_id;
            $article->is_approved = 0;
            $article->is_rework = 0;
            $article->alias = $article->alias ? $article->alias : strtolower(Zend_Filter::filterStatic($article->name, 'Translit'));
            $article->next_sunday = Article::getNextSunday($article->date);
            $article->save();

            $article->ogimage = $this->view->ogImage($article->name, $article->article_id);
            $article->save();

            if (!$id) {
                $anUsersTable = new UsersTable();
                $user = $anUsersTable->find(
                    Zend_Auth::getInstance()->getIdentity()->user_id
                )->current();
                $user->pub_count++;
                $user->save();

                //subscribe user for comments
                $data['email'] = $user->email;
                $data['user_id'] = $user->user_id;
                $data['typology_id'] = $article->article_id;
                $data['typology'] = CommentsTable::TYPOLOGY_ARTICLE;
                $anCSTable = new CSTable;
                $anCSTable->replace($data);

                HookManager::getInstance()->call(HookManager::EVENT_NEW_ARTICLE, array(
                    'article' => $article
                ));
            }

            $this->_redirect($this->view->url(array('controller' => 'users', 'action' => 'articles', 'id' => Zend_Auth::getInstance()->getIdentity()->user_id), 'default', true));
        } else {
            $this->view->image = $this->getImage();
            $this->view->imagePreview = $this->getImagePreview();
            $this->view->imagePreview2 = $this->getImagePreview2();
            $this->view->imageOriginal = $this->getImageOriginal();
            $this->view->imageWidget = $this->getImageWidget();
            $this->view->uploadError = $uploadError;
            $this->view->form = $form;
        }
    }

    protected function _getImage($num)
    {
        if (isset($this->images[$num]) && $this->images[$num]) {
            return $this->images[$num];
        } elseif (isset($_POST['images'][$num]) && $_POST['images'][$num]) {
            return $_POST['images'][$num];
        } else {
            return '';
        }
    }

    protected function getImage()
    {
        return $this->_getImage(2);
    }

    protected function getImagePreview()
    {
        return $this->_getImage(0);
    }

    protected function getImagePreview2()
    {
        return $this->_getImage(1);
    }

    protected function getImageOriginal()
    {
        return $this->_getImage(4);
    }

    protected function getImageWidget()
    {
        return $this->_getImage(3);
    }

    protected function uploadImage()
    {
        if ($this->getRequest()->isPost() &&
            isset($_FILES['image']['name']) &&
            $_FILES['image']['name']
        ) {
            $imUploader = new ImageUploader(Zend_Registry::getInstance()->get('root_dir'), 'image');

            try {
                $this->images = $imUploader->upload(array(
                    array(80, 80),
                    array(198, 170),
                    array(266, 229), array(110, 110)), false);
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }

        return '';
    }

    function bookStatusAction()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) exit;

        Zend_Layout::getMvcInstance()->disableLayout();
        $anATTable = new ATTable();
        $at = $anATTable->find($this->getRequest()->getParam('id'))->current();
        $this->view->at = $at;
    }

    function bookAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        Zend_Layout::getMvcInstance()->disableLayout();

        $resp = array();
        $anATTable = new ATTable();
        $at = $anATTable->find($this->getRequest()->getParam('id'))->current();

        if (!$at) {
            $resp['error'] = iconv('windows-1251', 'UTF-8', '�� ������ ������');
        } elseif (!Zend_Auth::getInstance()->hasIdentity()) {
            $resp['error'] = iconv('windows-1251', 'UTF-8', '���������� ����������������� ��� ������� �� ����');
        } elseif (!Zend_Auth::getInstance()->getIdentity()->canBook()) {
            $resp['error'] = iconv('windows-1251', 'UTF-8', '��� �� �������� ������� ������������');
        } elseif ($at->book_user_id != Zend_Auth::getInstance()->getIdentity()->user_id &&
            $at->book_due > date('Y-m-d H:i:s')
        ) {

            $resp['error'] = iconv('windows-1251', 'UTF-8', '��� ���� ��� ������������� ������ ������������');
        } elseif ($at->book_user_id == Zend_Auth::getInstance()->getIdentity()->user_id) {
            $resp['error'] = iconv('windows-1251', 'UTF-8', '�� �� ������ ����������� ���� � �� �� ���� ��� ���� ������');
        } else {
            $at->book_user_id = Zend_Auth::getInstance()->getIdentity()->user_id;
            $at->book_due = date('Y-m-d H:i:s', time() + 3600 * Site_Settings::get('book_delay', 48));
            $at->save();

            $anATView = new ATView();
            $currentBooking = $anATView->fetchRow(
                $anATView->select()->where('at_id=?', $at->at_id)
            );
            $this->view->user = Zend_Auth::getInstance()->getIdentity();
            $this->view->item = $currentBooking;
            $resp['html'] = iconv('windows-1251', 'UTF-8',
                $this->view->render('booking.inc.phtml'));
        }


        $resp = Zend_Json::encode($resp);

        $this->getResponse()
            ->setHeader('Content-Type', 'text/plain; charset=UTF-8')
            ->appendBody($resp);

        return;
    }

    function hasComments($user_id, $article_id, $min_length = 0)
    {
        $anCommentsTable = new CommentsTable();

        $select = $anCommentsTable->select()
            ->where('typology_id=?', $article_id)
            ->where('typology=?', CommentsTable::TYPOLOGY_ARTICLE)
            ->where('user_id=?', $user_id);

        if ($min_length) {
            $select = $select->where('CHAR_LENGTH(content) >= ?', $min_length);
        }

        $comments = $anCommentsTable->fetchAll($select);

        return count($comments);
    }

    function rateAction()
    {
        $this->redirectIfNotAjax();
        $this->_helper->viewRenderer->setNoRender();
        Zend_Layout::getMvcInstance()->disableLayout();

        $params = $this->getRequest()->getParams();
        $anRatingsTable = new RatingsTable();

        $anArticleTable = new ArticlesTable();
        $article = $anArticleTable->find($params['id'])->current();

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $user = Zend_Auth::getInstance()->getIdentity();

            if (!$user->isWriter()) {
                $resp['errorCode'] = 1;
                $resp['rate'] = (int)$anRatingsTable->getRating($params['id']);
                $resp['id'] = $params['id'];
                $resp['msg'] = iconv('windows-1251', 'UTF-8', '������ ������ ����� ��������� ������' . ($user->isReader() ? ', �� ������ ��������� ���� ������� � ������ ������ � ����� �������' : ''));
            } elseif (!$user->haveAprovedArticles()) {
                $resp['errorCode'] = 1;
                $resp['rate'] = (int)$anRatingsTable->getRating($params['id']);
                $resp['id'] = $params['id'];
                $resp['msg'] = iconv('windows-1251', 'UTF-8',
                    '��� ���������� �������� ������ � ���������� ������� ��� ������� � ��������');
            } elseif ($user->user_id == $article->user_id) {
                $resp['errorCode'] = 1;
                $resp['rate'] = (int)$anRatingsTable->getRating($params['id']);
                $resp['id'] = $params['id'];
                $resp['msg'] = iconv('windows-1251', 'UTF-8', '�� �� ������ ��������� ���� ������');
            } elseif (!$this->hasComments($user->user_id, $article->article_id, Site_Settings::get('comment_min_length_to_rate', 0))) {
                $resp['errorCode'] = 1;
                $limit = Site_Settings::get('comment_min_length_to_rate', 0);
                $msg = $limit ? sprintf('��� ������ ������ ��������� ����������� ����� %d ��������', $limit) : '� ���� ������, ��� ������ �����������, � ����� � ����, �� �� ������ ��������� ������';
                $resp['rate'] = (int)$anRatingsTable->getRating($params['id']);
                $resp['id'] = $params['id'];
                $resp['msg'] = iconv('windows-1251', 'UTF-8', $msg);
            } else {
                $resp['errorCode'] = 0;
                $resp['msg'] = iconv('windows-1251', 'UTF-8', '���� ������ ���������');
                $rate = $params['rate'] == 1 ? 1 : -1;
                $data = array(
                    'article_id' => $params['id'],
                    'user_id' => Zend_Auth::getInstance()->getIdentity()->user_id,
                    'rater_id' => null,
                    'is_anonimus' => Zend_Auth::getInstance()->getIdentity()->isAnonimus() ? 1 : 0,
                    'rate' => $rate,
                    'date' => date('Y-m-d H:i:s')
                );

                $prevRate = $anRatingsTable->fetchAll(
                    $anRatingsTable->select()
                        ->where('article_id=?', $data['article_id'])
                        ->where('user_id=?', $data['user_id'])
                )->current();

                $anRatingsTable->replace($data);

                $resp['rate'] = (int)$anRatingsTable->getRating($params['id']);
                $resp['id'] = $params['id'];

                $anArticlesTable = new ArticlesTable();
                $article = $anArticlesTable->find($params['id'])->current();
                $article->rating = $resp['rate'];
                $article->save();

                $anUsersTable = new UsersTable();
                $user = $anUsersTable->find($article->user_id)->current();

                $user->rating = $prevRate ?
                    $user->rating - Config::get('rating_rate_article', 1) * ($prevRate->rate - $data['rate']) :
                    $user->rating + Config::get('rating_rate_article', 1) * $data['rate'];

                $user->save();
            }
        } else {
            $resp['errorCode'] = 1;
            $resp['rate'] = (int)$anRatingsTable->getRating($params['id']);
            $resp['id'] = $params['id'];
            $resp['msg'] = iconv('windows-1251', 'UTF-8', '���������� ����������������� ��� ������� �� ���� ��� ������� � ��������');
        }

        $resp = Zend_Json::encode($resp);
        $this->getResponse()
            ->setHeader('Content-Type', 'application/json; charset=UTF-8')
            ->appendBody($resp);

        return;
    }

    private function getRaterId()
    {

        if (!(isset($_COOKIE['id']) && $_COOKIE['id'])) {
            $id = substr(md5(microtime() . rand(0, 9999)), 0, 255);
            $_COOKIE['id'] = $id;
            setcookie('id', $id, time() + 3600 * 24 * 366 * 20);
        }

        return $_COOKIE['id'];
    }

    public function rssAction()
    {

        $articlesTable = new ArticlesTable;
        $articles = $articlesTable->fetchAll($articlesTable->select()
            ->where('is_approved=?', 1)
            ->order('date DESC')
            ->limit(50));

        $feedArray = array(
            'title' => 'www.topauthor.ru',
            'link' => 'http://www.topauthor.ru',
            'charset' => 'UTF-8',
            'entries' => array()
        );
        foreach ($articles as $item) {
            $date = new Zend_Date($item->date, 'YYYY-MM-dd');
            $itemTimestamp = $date->getTimestamp();

            $feedArray['entries'][] = array(
                'title' => iconv("windows-1251", "UTF-8", $item->name),
                'link' => 'http://www.topauthor.ru' . '/' . $item->alias . '.html',
                'description' => iconv("windows-1251", "UTF-8", substr($item->content, 0, 252) . '...'),
                'lastUpdate' => $itemTimestamp
            );

        }

        $this->_helper->viewRenderer->setNoRender(true);
        Zend_Layout::getMvcInstance()->disableLayout();
        $feed = Zend_Feed::importArray($feedArray, 'rss');

        $feed->send();

    }

}