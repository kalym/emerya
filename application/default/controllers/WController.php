<?php

class WController extends Zend_Controller_Action
{
    function getCodeAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $type = $this->getRequest()->getParam('type', 'articles');
        $cat_id = $this->getRequest()->getParam('cat_id', null);
        $user_id = Zend_Auth::getInstance()->hasIdentity() ?
                Zend_Auth::getInstance()->getIdentity()->user_id :
                0;
        
        switch ($type) {
            case 'articles' :
                $name = '���������� ������';
                break;
            case 'photo' :
                $name = '���������� ����������';
                break;
            case 'articles' :
                $name = '���������� �����';
                break;
            case 'user' :
                $name = '��������� ������';
                break;
            case 'popular' :
                $name = '���������� ������';
                break;
        }
        
        if ($type == 'articles') {
            $anCatTable = new CatsTable;
            $options = $anCatTable->getOptions();
            $s = $this->view->formSelect('cat_id', $cat_id, array('id'=>'widget-cat_id'), $options);
            $select = <<<CUT
<p>���������: $s</p>
CUT;
        } else {
            $select = '';
        }
        
        echo '
<h2>��� ��� ������� �� ����, � ���� ��� �����</h2>
<p>������: <strong style="color:#00518e">' . $name . '</strong></p>' .
$select .
'<p>�� ������ ����������� ��� ������� � �������� �� ���� ����, � ���� ��� �����</p>            
<textarea rows="6" style="width:100%; color: #9c9d9d; border: 1px #818181 solid">' . $this->view->escape($this->getCode($type, $user_id, $cat_id)) . '</textarea>
<script type="text/javascript">
$("#widget-cat_id").change(function(){
    $(".modal-content").load("/w/get-code", {
        type : "articles",
        cat_id : $(this).val()
    })
})
</script>
';
    }

    function getCodeArticleAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $anArticlesView = new ArticlesView;
        $item = $anArticlesView->find($this->getRequest()->getParam('id'))->current();
        if (!$item)
            throw new Zend_Controller_Dispatcher_Exception('');
        
        $this->view->item = $item;
    }
    
    protected function getCode($type, $user_id, $cat_id = null) {
       $code = <<<CUT
<!-- Topauthor.ru -->
<script type="text/javascript" src="http://www.topauthor.ru/w/get/{$type}/{$user_id}/{$cat_id}"></script>
CUT;
        return $code;
    }

    protected function getWidget($type, $user_id, $cat_id = null) {

        if ($cat_id) {
            $anCatTable = new CatsTable;
            $cat = $anCatTable->find($cat_id)->current();
            $widget = $this->view->{'widgetBest' . ucfirst($type)}($cat, true);
        } elseif ($type == 'user') {
            $anUserTable = new UsersTable;
            $user = $anUserTable->find($user_id)->current();
            $widget = $this->view->{'widgetBest' . ucfirst($type)}($user, true);
        } else {
            $widget = $this->view->{'widgetBest' . ucfirst($type)}(true);
        }
        $widget = @iconv("windows-1251", "UTF-8", $widget);
        $this->getResponse()
          ->setHeader('Content-Type', 'text/html; charset=UTF-8');
        return $widget;
    }

    function getAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
//        $this->_helper->viewRenderer->setNoRender();

        $user_id = $this->getRequest()->getParam('user_id', 0);
        $type = $this->getRequest()->getParam('type', 'articles');
        $cat_id = $this->getRequest()->getParam('cat_id', null);

        $data = array(
            'widget' => $this->getWidget($type, $user_id, $cat_id)
        );
        $this->view->data = Zend_Json::encode($data);

    }
}