<?php

class RateController extends Zend_Controller_Action {

    function preDispatch() {
        if ($this->getRequest()->isXmlHttpRequest()) {
            Zend_Layout::getMvcInstance()->disableLayout();
            $params = $this->getRequest()->getParams();
            $params = $this->decode("UTF-8", "windows-1251", $params);
            $this->getRequest()->setParams($params);
            $_POST = $this->decode("UTF-8", "windows-1251", $_POST);
            $_GET = $this->decode("UTF-8", "windows-1251", $_GET);
        }
    }

    function decode($from, $to, $value) {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->decode($from, $to, $v);
            }
        } else {
            $value = iconv($from, $to, $value);
        }
        return $value;
    }

    function rateAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $anRateTable = new RateTable();
        $comment_id = $this->getRequest()->getParam('id');
        $user_id = Zend_Auth::getInstance()->getIdentity()->user_id;
        $rate = $anRateTable->getRate($comment_id, $user_id);

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $data = array(
                'rate' => $rate == 1 ? 0 : 1,
                'user_id' => $user_id,
                'comment_id' => $comment_id
            );
            $anRateTable->replace($data);
        }


        $resp = array(
            'yes' => $anRateTable->getRateCnt($this->getRequest()->getParam('id'), 1)
        );
        $resp = $this->decode('windows-1251', 'UTF-8', $resp);
        $resp = Zend_Json::encode($resp);

        $this->getResponse()
                ->setHeader('Content-Type', 'text/plain; charset=UTF-8')
                ->appendBody($resp);

        return;
    }

}