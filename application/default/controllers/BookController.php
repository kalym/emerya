<?php

class BookController extends Zend_Controller_Action
{

    function indexAction()
    {
        if ($this->_helper->auth->requireLogin(true)) {
            return;
        }
        $anATView = new ATView();
        $anCatsTable = new CatsTable();

        $cat = $anCatsTable->find(
            $this->getRequest()->getParam('id')
        )->current();

        $user_id = Zend_Auth::getInstance()->getIdentity()->user_id;
        $user = Zend_Auth::getInstance()->getIdentity();

        $ids = array();
        if (!$user->haveAprovedArticles()) {
            $records = $anATView->fetchAll($anATView->select()
                ->where('is_active=?', 1)
                ->order(new Zend_Db_Expr("book_user_id = $user_id DESC"))
//                ->limit(User::BOOK_VIEW_LIMIT)
            );
            foreach ($records as $rec) {
                $ids[] = $rec->at_id;
            }
        }

        $select = $anATView->select()
            ->where('is_active=?', 1)
            ->order(new Zend_Db_Expr("book_user_id = $user_id DESC, name ASC"));

        $this->view->active_cat_id = null;

        if ($cat) {
            $select = $select->where('cat_id=?', $cat->cat_id);
            $this->view->active_cat_id = $cat->cat_id;
        }

        if ($ids) {
            $select->where('at_id IN (?)', $ids);
        }

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );

        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
        $paginator->setItemCountPerPage(Site_Settings::get('front_at_perpage', 10));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
        $this->view->user = Zend_Auth::getInstance()->getIdentity();
    }

    function searchAction()
    {
        if ($this->_helper->auth->requireLogin(true)) {
            return;
        }

        if ($this->getRequest()->isPost()) {
            $this->_redirect(
                Zend_Controller_Front::getInstance()->getRouter()->assemble(array(
                    'controller' => 'book',
                    'action' => 'search',
                    'q' => $this->getRequest()->getParam('q')
                ))
            );
        }

        $q = $this->getRequest()->getParam('q');
        $anATView = new ATView();

        $select = $anATView->select()
            ->where('name LIKE ?', '%' . $q . '%')
            ->where('is_active=?', 1);

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_at_perpage', 80));
        $paginator->setPageRange(10);

        $this->getHelper('viewRenderer')->setScriptAction('index');

        $this->view->paginator = $paginator;

    }
}