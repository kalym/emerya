<?php

class PhotoController extends Zend_Controller_Action {
    
    function indexAction() {
        
        if ($this->_helper->auth->requireLogin()) {
            return;
        }
        
        
        $anPhotoTable = new PhotoTable;
        $this->view->photos = $anPhotoTable->fetchAll($anPhotoTable->select()
                                        ->where('user_id=?', Zend_Auth::getInstance()->getIdentity()->user_id)
                                        ->order('datetime DESC'));
    }
    
    function addAction() {
        if ($this->_helper->auth->requireLogin()) {
            return;
        }
        
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        if ($this->getRequest()->isPost()) {
            $anPhotoTable = new PhotoTable;
            $photo = $anPhotoTable->fetchNew();
            
            $photo->user_id = Zend_Auth::getInstance()->getIdentity()->user_id;
            $photo->small = $this->getRequest()->getParam('small');
            $photo->big = $this->getRequest()->getParam('big');
            $photo->preview = $this->getRequest()->getParam('preview');
            $photo->original = $this->getRequest()->getParam('original');
            $photo->datetime = date('Y-m-d H:i:s');
            $photo->save();
            
            HookManager::getInstance()->call(HookManager::EVENT_NEW_PHOTO, array(
                'photo' => $photo
            ));
            
            $resp = $this->getRequest()->getPost();
            $resp['photo_id'] = $photo->photo_id;
            $resp = Zend_Json::encode( $resp );

            $this->getResponse()
              ->setHeader('Content-Type', 'application/json; charset=UTF-8')
              ->appendBody($resp);
        }
    }
    
    function delAction() {
        if ($this->_helper->auth->requireLogin()) {
            return;
        }
        
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        if ($this->getRequest()->isPost()) {
            $anPhotoTable = new PhotoTable;
            $photo = $anPhotoTable->find($this->getRequest()->getParam('id'))->current();
            
            if ($photo->user_id == Zend_Auth::getInstance()->getIdentity()->user_id) {
                $photo->delete();
            }
        }
    }
}