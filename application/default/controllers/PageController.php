<?php

class PageController extends Zend_Controller_Action
{
    
    function indexAction()
    {
        $anPagesTable = new PagesTable();
        $this->view->page = $anPagesTable->find(
                $this->getRequest()->getParam('id', 1)
        )->current();

        if (!$this->view->page) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }
        
        if (!$this->getRequest()->getParam('from')) {
            $this->_redirect('/' . $this->view->page->alias . '/', array(
                'exit' => true,
                'code' => 301
            ));
        }
        
        $this->_helper->menu->select('page-' . $this->view->page->page_id);
    }

}