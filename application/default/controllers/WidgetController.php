<?php

class WidgetController extends Zend_Controller_Action
{
    function preDispatch() {
        parent::preDispatch();
        $this->_helper->menu->select('widget');
    }
    
    function indexAction()
    {
        if ($this->_helper->auth->requireLogin(true)) {
            return;
        }

        $this->view->user = Zend_Auth::getInstance()->getIdentity();
        $anCatTable = new CatsTable;
        $this->view->cats = $anCatTable->fetchAll();
        
        //$this->view->code = $this->getCode(300,2,Zend_Auth::getInstance()->getIdentity()->user_id);
    }

    function getCodeAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $width = $this->getRequest()->getParam('width', 300);
        $count = $this->getRequest()->getParam('count', 2);
        $user_id = Zend_Auth::getInstance()->getIdentity()->user_id;
        
        echo $this->getCode($width, $count, $user_id);
    }

    function getWidgetAction() {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $width = $this->getRequest()->getParam('width', 300);
        $count = $this->getRequest()->getParam('count', 2);
        $user_id = Zend_Auth::getInstance()->getIdentity()->user_id;

        echo $this->getWidget($width, $count, $user_id);
    }


    protected function getCode($width, $count, $user_id) {
       $code = <<<CUT
<script type="text/javascript" src="http://www.topauthor.ru/widget/get/id/{$user_id}/count/{$count}/width/{$width}"></script>
CUT;
        return $code;
    }

    protected function getWidget($width, $count, $user_id) {

        $width = $width;

        $anUsersTable = new UsersTable();
        $user = $anUsersTable->find($user_id)->current();

        $anArticlesView = new ArticlesView();

        $articles = $anArticlesView->fetchAll(
             $anArticlesView->select()
                ->where('is_approved>0')
                ->where('user_id=?', $user_id)
                ->order('date DESC')
                ->limit($count)
        );

        $items = $articles;

        $widget = $this->view->partial('widget/widget.phtml', array(
            'user_id' => $user_id,
            'user' => $user,
            'items' => $articles,
            'width' => $width
        ));
        $widget = @iconv("windows-1251", "UTF-8", $widget);
        $this->getResponse()
          ->setHeader('Content-Type', 'text/html; charset=UTF-8');
        return $widget;
    }

    function getAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
//        $this->_helper->viewRenderer->setNoRender();

        $user_id = (int)$this->getRequest()->getParam('id');
        $count = (int)$this->getRequest()->getParam('count', 2);
        $width = (int)$this->getRequest()->getParam('width', 300);

        $data = array(
            'widget' => $this->getWidget($width, $count, $user_id)
        );
        $this->view->data = Zend_Json::encode($data);

    }
}