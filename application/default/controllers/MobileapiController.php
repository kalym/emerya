<?php

//���������� ������
//http://www.topauthor.ru/mobileapi/article/?article=1
//���������� ������:
//http://www.topauthor.ru/mobileapi/top-articles?page=1&perpage=20
//C�����:
//http://www.topauthor.ru/mobileapi/articles?page=1&perpage=20
//������:
//http://www.topauthor.ru/mobileapi/authors?page=1&perpage=20
//����� ������:
//http://www.topauthor.ru/mobileapi/author-articles?author=1&page=1&perpage=20
//�������:
//http://www.topauthor.ru/mobileapi/categories
//������ � �������:
//http://www.topauthor.ru/mobileapi/category-articles?category=1&page=1&perpage=20
//�������
//http://www.topauthor.ru/mobileapi/banners-small?device=iphone
//http://www.topauthor.ru/mobileapi/banners-big?device=iphone

class MobileapiController extends Zend_Controller_Action {

    function preDispatch() {
        parent::preDispatch();
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
    }

    function decode($str) {
        return @iconv("windows-1251", "UTF-8", $str);
    }

    protected function addChild($xml, $name, $value = null) {
        return $xml->addChild($name, is_null($value) ? $value : $this->decode($value));
    }

    protected function buildArticlesList($paginator) {
        $xml = $this->createXMLElement('<articles></articles>');
        $this->addChild($xml, 'total-articles', $paginator->getTotalItemCount());
        foreach ($paginator as $article) {
            $art = $this->addChild($xml, 'article');
            $this->addChild($art, 'id', $article->article_id);
            $this->addChild($art, 'author-id', $article->user_id);
            $this->addChild($art, 'title', $article->name);
            $this->addChild($art, 'big-picture-url', 'http://www.topauthor.ru' . $article->image);
            $this->addChild($art, 'small-picture-url', 'http://www.topauthor.ru' . $article->preview);
            $this->addChild($art, 'category-id', $article->cat_id);
            $this->addChild($art, 'category-name', $article->c_name);
            $this->addChild($art, 'rating', $article->rating);
            $this->addChild($art, 'read', $article->clicks);
            $this->addChild($art, 'comments', $this->view->commentsCount($article->article_id));
            $this->addChild($art, 'article-url', 'http://topauthor.ru' .
                    $this->view->url(array($article->alias), 'article', true));
            $this->addChild($art, 'add-date', $article->date);
            $this->addChild($art, 'author-fullname', $article->u_name_f . ' ' . $article->u_name_l);
        }

        return $xml;
    }

    function bannersSmallAction() {
        $anBannersTable = new BannersTable();
        $banners = $anBannersTable->fetchAll(
                $anBannersTable->select()->where('type=?', BannersTable::TYPE_SMALL)
                        ->where('device=?', $this->getRequest()->getParam('device', BannersTable::DEVICE_IPAD))
        );
    	$this->send($this->getBannersXML($banners));
    }

    function bannersBigAction() {
        $anBannersTable = new BannersTable();
        $banners = $anBannersTable->fetchAll(
                $anBannersTable->select()->where('type=?', BannersTable::TYPY_BIG)
                        ->where('device=?', $this->getRequest()->getParam('device', BannersTable::DEVICE_IPAD))
        );
    	$this->send($this->getBannersXML($banners));
    }
    
    function getBannersXML($banners) {
        $xml = $this->createXMLElement('<banners></banners>');
        foreach ($banners as $banner) {
            $b = $this->addChild($xml, 'banner');
            $this->addChild($b, 'url', $banner->url);
            $this->addChild($b, 'picture-url', $banner->picture_url);
        }
        
        return $xml;
    }

    function topArticlesAction() {
        $page = $this->getRequest()->getParam('page', 1);
        $perpage = $this->getRequest()->getParam('perpage', 20);
        $adddate = $this->getRequest()->getParam('adddate', '1970-01-01');

        $anPopularView = new PopularView();

        $paginator = new Zend_Paginator(
                        new Zend_Paginator_Adapter_DbTableSelect(
                                        $anPopularView->select()
                                        ->where('date>=?', $adddate)
                                        ->order('date DESC')
                        )
        );

        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($perpage);

        $xml = $this->buildArticlesList($paginator);

        $this->send($xml);
    }

    function articleAction() {
        $article_id = $this->getRequest()->getParam('article');
        $anArticlesTable = new ArticlesView();
        $article = $anArticlesTable->find($article_id)->current();
        if (!$article) {
            $this->error('Cannot retrieve article for id = ' . $article_id);
        }
        $xml = $this->createXMLElement('<articlecontent></articlecontent>');
        $this->addChild($xml, 'author-fullname', $article->u_name_f . ' ' . $article->u_name_l);
        $this->addChild($xml, 'content', $article->content);

        $this->send($xml);
    }

    function articlesAction() {
        $page = $this->getRequest()->getParam('page', 1);
        $perpage = $this->getRequest()->getParam('perpage', 20);
        $adddate = $this->getRequest()->getParam('adddate', '1970-01-01');

        $anArticlesView = new ArticlesView();

        $paginator = new Zend_Paginator(
                        new Zend_Paginator_Adapter_DbTableSelect(
                                        $anArticlesView->select()
                                        ->where('is_approved>0')
                                        ->where('date>=?', $adddate)
                                        ->order('date DESC')
                        )
        );

        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($perpage);

        $xml = $this->buildArticlesList($paginator);

        $this->send($xml);
    }

    function authorsAction() {
        $page = $this->getRequest()->getParam('page', 1);
        $perpage = $this->getRequest()->getParam('perpage', 20);
        $adddate = $this->getRequest()->getParam('adddate', '1970-01-01');

        $anUsersTable = new UsersTable();

        $paginator = new Zend_Paginator(
                        new Zend_Paginator_Adapter_DbTableSelect(
                                        $anUsersTable->select()
                                        ->where('date>=?', $adddate)
                                        ->where('pub_approved_count>0')
                                        ->where('is_locked=0')
                                        ->order('rating DESC')
                        )
        );

        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($perpage);

        $xml = $this->createXMLElement('<authors></authors>');
        $this->addChild($xml, 'total-authors', $paginator->getTotalItemCount());
        foreach ($paginator as $user) {
            $author = $this->addChild($xml, 'author');
            $this->addChild($author, 'id', $user->user_id);
            $this->addChild($author, 'name', $user->name_f);
            $this->addChild($author, 'surname', $user->name_l);
            $this->addChild($author, 'articles-number', $user->pub_count);
            $this->addChild($author, 'rating', $user->rating);
            $this->addChild($author, 'status-image-url',
                    'http://www.topauthor.ru/public/images/' .
                    Site_View_Helper_Status::getCode($user->rating) .
                    '.png');
            $this->addChild($author, 'add-date', $user->date);
            $this->addChild($author, 'avatar-url', 'http://www.topauthor.ru' . ($user->avatar ? $user->avatar : '/public/images/avatar.png'));
        }

        $this->send($xml);
    }

    function authorArticlesAction() {
        $page = $this->getRequest()->getParam('page', 1);
        $perpage = $this->getRequest()->getParam('perpage', 20);
        $adddate = $this->getRequest()->getParam('adddate', '1970-01-01');

        $anArticlesView = new ArticlesView();
        $user_id = $this->getRequest()->getParam('author', 0);
        $paginator = new Zend_Paginator(
                        new Zend_Paginator_Adapter_DbTableSelect(
                                        $anArticlesView->select()
                                        ->where('is_approved>0')
                                        ->where('date>=?', $adddate)
                                        ->where('user_id=?', $user_id)
                                        ->order('date DESC')
                        )
        );

        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($perpage);

        $xml = $this->buildArticlesList($paginator);

        $this->send($xml);
    }

    function searchArticlesAction() {
        ini_set('display_errors', 1);
        $page = $this->getRequest()->getParam('page', 1);
        $perpage = $this->getRequest()->getParam('perpage', 20);
        $adddate = $this->getRequest()->getParam('adddate', '1970-01-01');
        $q = $this->getRequest()->getParam('q');

        $anArticlesView = new ArticlesView();
        $select = $anArticlesView->select()
                        ->where('content LIKE ? OR name LIKE ?', '%' . $q . '%', '%' . $q . '%')
                        ->order(new Zend_Db_Expr(sprintf('name LIKE %s DESC',
                                                $anArticlesView->getAdapter()->quote('%' . $q . '%')
                                )))
                        ->where('is_approved>0')
                        ->where('date>=?', $adddate);

        $paginator = new Zend_Paginator(
                        new Zend_Paginator_Adapter_DbTableSelect(
                                $select
                        )
        );

        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($perpage);

        $xml = $this->buildArticlesList($paginator);

        $this->send($xml);
    }

    function categoriesAction() {
        $anCatsTable = new CatsTable();

        $cats = $anCatsTable->fetchAll(
                        $anCatsTable->select()->order('title DESC')
        );

        $xml = $this->createXMLElement('<categories></categories>');

        foreach ($cats as $cat) {
            $category = $this->addChild($xml, 'category');
            $this->addChild($category, 'id', $cat->cat_id);
            $this->addChild($category, 'title', $cat->name);
        }

        $this->send($xml);
    }

    function categoryArticlesAction() {
        $page = $this->getRequest()->getParam('page', 1);
        $perpage = $this->getRequest()->getParam('perpage', 20);
        $adddate = $this->getRequest()->getParam('adddate', '1970-01-01');

        $anArticlesView = new ArticlesView();
        $cat_id = $this->getRequest()->getParam('category', 0);
        $paginator = new Zend_Paginator(
                        new Zend_Paginator_Adapter_DbTableSelect(
                                        $anArticlesView->select()
                                        ->where('is_approved>0')
                                        ->where('date>=?', $adddate)
                                        ->where('cat_id=?', $cat_id)
                                        ->order('date DESC')
                        )
        );

        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($perpage);

        $xml = $this->buildArticlesList($paginator);

        $this->send($xml);
    }

    protected function error($msg) {
        $xml = $this->createXMLElement('<error></error>');
        $this->addChild($xml, 'message', $msg);
        $this->send($xml);
        exit;
    }

    protected function createXMLElement($root) {
        return new SimpleXMLElement($this->getXMLHeader() . $root);
    }

    protected function send($xml) {
        $this->sendHeader();
        echo $xml->asXML();
    }

    protected function sendHeader() {
        header('Content-type: text/xml; UTF-8');
    }

    protected function getXMLHeader() {
        return '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>';
    }

}