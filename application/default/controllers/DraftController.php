<?php

include_once('application/default/controllers/IndexController.php');

class DraftController extends IndexController
{
    function getType()
    {
        return self::TYPE_DRAFT;
    }
}