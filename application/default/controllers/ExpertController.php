<?php

class ExpertController extends Zend_Controller_Action
{
    function preDispatch()
    {
        $this->_helper->menu->select('expert');
    }

    public function indexAction()
    {
        $anQnaTable = new QnaView;
        $this->view->popular = $anQnaTable->fetchAll(
            $anQnaTable->select()
                ->where('is_approved=1')
                ->order('datetime DESC')
                ->limit(16)
        );

        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());

        $select = $select
            ->from('users')
            ->columns(array('users.*', 'cnt' => new Zend_Db_Expr('COUNT(comment_id)')))
            ->joinLeft('comments', "users.user_id=comments.user_id AND comments.typology = 'qna'")
            ->where('is_locked=0')
            ->group('users.user_id')->order('cnt DESC')->limit(30);

        $this->view->experts = new Zend_Db_Table_Rowset(array('data' => $select->query()->fetchAll()));
    }
}