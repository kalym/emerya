<?php

class NewsController extends Zend_Controller_Action
{
    function preDispatch() {
        parent::preDispatch();
        $this->_helper->menu->select('news');
    }
    
    function indexAction()
    {
        $params  = $this->getRequest()->getParams();

        $anNewsTable = new NewsTable();
        $select = $anNewsTable->select();

        $adapter = new Zend_Paginator_Adapter_DbTableSelect( $select->order('date DESC') );

        $page = (isset($params['p'])) ? $params['p'] : 1;
        $paginator = new Zend_Paginator( $adapter );
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(Site_Settings::get('news_item_perpage', 10));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }

    function itemAction()
    {
        $params  = $this->getRequest()->getParams();
        $anNewsTable = new NewsTable();
        $news = $anNewsTable->find($params['id'])->current();

        if (!$news) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $commentsProcessor = new CommentsProcessor($this->getRequest()->getParam('id'), CommentsTable::TYPOLOGY_NEWS);
        $commentsProcessor->run($this->getRequest());
        $this->view->commentsProcessor = $commentsProcessor;

        $this->view->item = $news;
    }

}