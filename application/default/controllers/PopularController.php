<?php

class PopularController extends Zend_Controller_Action
{
    function preDispatch() {
        parent::preDispatch();
        $this->_helper->menu->select('popular');
    }
    
    function indexAction()
    {
        $frontendOptions = array(
            'lifetime' => 3600 * 24 * 7, // cache lifetime of 1 week
            'automatic_serialization' => true
        );

        $backendOptions = array(
            'cache_dir' => Zend_Registry::getInstance()->get('root_dir') . '/tmp/' // Directory where to put the cache files
        );

        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
        if (!($popular_article_ids = $cache->load('popular_article_ids'))) {
            $popular_article_ids = array();
            $anPopularView = new PopularView();
            foreach($anPopularView->fetchAll($anPopularView->select()) as $item) {
                $popular_article_ids[] = $item->article_id;
            }
            $cache->save($popular_article_ids, 'popular_article_ids');
        }
        
        $anArtticleView = new ArticlesView;
        $select = $anArtticleView->select()->where('article_id IN (?)', $popular_article_ids);
        $select = $select->order('next_sunday DESC');

        $paginator = new Zend_Paginator(
           new Zend_Paginator_Adapter_DbTableSelect(
               $select->order('date DESC')
           )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('popular_item_perpage', 8));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }

}