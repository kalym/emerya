<?php

class Form_QA extends Zend_Form {

    function init()
    {
        $this->setMethod(Zend_Form::METHOD_POST);

        $this->addElement('textarea', 'content', array(
            'required'=>true,
            'filters'=>array('StripTags'),
            'label' => '����� �������',
            'rows' => 8
        ));

        $content = $this->getElement('content');


        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('����������, ������� ����� �������');
        $content->addValidator($validator);
    }
}

class QaController extends Zend_Controller_Action
{
    
    function preDispatch() {
        parent::preDispatch();
        $this->_helper->menu->select('qa');
    }
    
    function indexAction()
    {
	$anQATable = new QATable();

        $form = new Form_QA;

        $this->view->form = $form;

        if ($this->getRequest()->isPost()
                && Zend_Auth::getInstance()->hasIdentity()
                && $form->isValid($_POST) ) {

                $qa = $anQATable->fetchNew();
                $qa->question = $form->getValue('content');
                $qa->datetime = date("Y-m-d H:i:s");
                $qa->user_id = Zend_Auth::getInstance()->getIdentity()->user_id;
                $qa->save();

                $form->reset();
        }



        $select = $anQATable->select()
                    ->where('answer<>""')
                    ->order('datetime DESC');

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
               $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_qa_perpage', 24));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }
}