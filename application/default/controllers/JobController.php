<?php

class Form_Job extends Zend_Form {

    function init() {
        $this->setAction('/job/add')
                ->setMethod(Zend_Form::METHOD_POST)
                ->setAttrib('encode', Zend_Form::ENCTYPE_MULTIPART)
                ->setAttrib('id', 'job-add');

        $this->addElement('text', 'name', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '���������'
        ));

        $name = $this->getElement('name');

        $name->setRequired(true);
        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('��������� ����������� ��� ����������');
        $name->addValidator($validator);
        
        $this->addElement('multiCheckbox', 'allowed', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '�������� �� ������ ����� ������ ������������ �� ��������',
            'escape' => false
        ));

        $allowed = $this->getElement('allowed');

        $options = array();
        foreach (Status::getLevels() as $code => $v) {
            $options[$code] = '<img src="/public/images/' . $code . '.png" />';
        }
        
        $allowed->setSeparator(' ')->setMultiOptions($options);

        $allowed->setRequired(true);
        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('�������� ������� ������� ��������� � �������');
        $allowed->addValidator($validator);
        
        
        $this->addElement('textarea', 'content', array(
            'required' => true,
            'label' => '�����',
            'class' => 'big-textarea'
        ));

        $content = $this->getElement('content');


        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('����������, ������� �����');
        $content->addValidator($validator);

        $this->addElement('text', 'price', array(
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => '������'
        ));

        $price = $this->getElement('price');

        $price->setRequired(true);
        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('��������� ���� ������ �������');
        $price->addValidator($validator);

        $validator = new Zend_Validate_Int();
        $validator->setMessage('������� ���������� �������� �������, ��� ������');
        $price->addValidator($validator);

        $validator = new Zend_Validate_GreaterThan(0);
        $validator->setMessage('������� ������������� ������');
        $price->addValidator($validator);
    }

}

//class JobController extends Zend_Controller_Action {
//    function preDispatch() {
//        if ($this->getRequest()->isXmlHttpRequest()) {
//            Zend_Layout::getMvcInstance()->disableLayout();
//            $params = $this->getRequest()->getParams();
//            $params = $this->decode("UTF-8", "windows-1251", $params);
//            $this->getRequest()->setParams($params);
//            $_POST = $this->decode("UTF-8", "windows-1251", $_POST);
//            $_GET = $this->decode("UTF-8", "windows-1251", $_GET);
//        } else {
//            $this->_helper->menu->select('job');
//        }
//    }
//
//    function decode($from, $to, $value) {
//        if (is_array($value)) {
//            foreach ($value as $k => $v) {
//                $value[$k] = $this->decode($from, $to, $v);
//            }
//        } else {
//            $value = iconv($from, $to, $value);
//        }
//        return $value;
//    }
//
//    protected function redirectIfNotAjax() {
//        if (!$this->getRequest()->isXmlHttpRequest()) {
//            $this->_redirect('/');
//        }
//    }
//
//    protected function error($msg) {
//        $resp['errorCode'] = 1;
//        $resp['msg'] = iconv("windows-1251", "UTF-8", $msg);
//        $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');
//
//        $this->_helper->viewRenderer->setNoRender();
//        $resp = Zend_Json::encode($resp);
//
//        $this->getResponse()
//                ->setHeader('Content-Type', 'application/json; charset=UTF-8')
//                ->appendBody($resp);
//    }
//
//    protected function getErrors($messages) {
//        $result = array();
//        foreach ($messages as $msg) {
//            $result = array_merge($result, array_values($msg));
//        }
//
//        return $result;
//    }
//
//    function indexAction() {
//        $params  = $this->getRequest()->getParams();
//
//        $anJobView = new JobView();
//        $select = $anJobView->select();
//
//        $adapter = new Zend_Paginator_Adapter_DbTableSelect( $select->order('datetime DESC') );
//
//        $page = (isset($params['p'])) ? $params['p'] : 1;
//        $paginator = new Zend_Paginator( $adapter );
//        $paginator->setCurrentPageNumber($page);
//        $paginator->setItemCountPerPage(Site_Settings::get('news_item_perpage', 10));
//        $paginator->setPageRange(10);
//        $this->view->paginator = $paginator;
//    }
//    
//    function itemAction()
//    {
//        
//        if (!$this->getRequest()->getParam('from')) {
//            throw new Zend_Controller_Dispatcher_Exception('');
//        }
//        
//        $params  = $this->getRequest()->getParams();
//        $anJobView = new JobView();
//        $job = $anJobView->find($params['id'])->current();
//
//        if (!$job) {
//            throw new Zend_Controller_Dispatcher_Exception('');
//        }
//
//        $commentsProcessor = new CommentsProcessor($this->getRequest()->getParam('id'), CommentsTable::TYPOLOGY_JOB);
//        $commentsProcessor->run($this->getRequest());
//        $this->view->commentsProcessor = $commentsProcessor;
//
//        $this->view->item = $job;
//    }
//    
//   
//    function addAction() {
//        if ($this->_helper->auth->requireLogin(User::TYPE_EMPLOYER)) {
//            return;
//        }
//
//        $form = new Form_Job;
//        
//        $anJobTable = new JobTable();
//        $job = $anJobTable->fetchNew();
//        $id = $this->getRequest()->getParam('id');
//        if ($id) {
//            $prev_job = $anJobTable->find($id)->current();
//            if ($prev_job &&
//                    !$prev_job->is_closed &&
//                    $prev_job->user_id == Zend_Auth::getInstance()->getIdentity()->user_id) {
//
//                if ($this->getRequest()->isGet()) {
//                    $form->populate(array_merge($prev_job->toArray(), array('allowed'=>$prev_job->getAllowed())));
//                }
//
//                $job = $prev_job;
//            }
//        }
//        
//        if ($this->getRequest()->isPost() && $form->isValid(array_merge($form->getValues(true), $_POST))) {
//
//            $job->name = $form->getValue('name');
//            $job->content = $form->getValue('content');
//            $job->price = $form->getValue('price');
//            $job->datetime = date('Y-m-d H:i:s');
//            $job->user_id = Zend_Auth::getInstance()->getIdentity()->user_id;
//            $job->alias = $job->alias ? $job->alias : Zend_Filter::filterStatic($job->name, 'Translit');
//            $job->setAllowed($form->getValue('allowed'));
//            $job->save();
//            HookManager::getInstance()->call(HookManager::EVENT_NEW_JOB, array(
//                'job' => $job
//            ));
//        } else {
//            $this->view->form = $form;
//        }
//    }
//    
//    function toggleCloseAction()
//    {
//        if ($this->_helper->auth->requireLogin(User::TYPE_EMPLOYER)) {
//            return;
//        }
//        $id = $this->getRequest()->getParam('id');
//        if (!$id) {
//            throw new Zend_Controller_Dispatcher_Exception('');
//        }
//        
//        
//        $anJobTable = new JobTable();
//        $job = $anJobTable->find($id)->current();
//        
//        if (!$job || Zend_Auth::getInstance()->getIdentity()->user_id != $job->user_id) {
//            throw new Zend_Controller_Dispatcher_Exception('');
//        }
//        
//        $job->is_closed = !$job->is_closed;
//        $job->save();
//        $this->_redirect('/job/' . $job->alias . '.html');
//    }
//}