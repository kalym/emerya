<?php

class Form_NewsSubscribe extends Zend_Form {

    function init() {
        $this->addElement('text', 'email', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => 'E-mail'
        ));

        $email = $this->getElement('email');
        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� E-mail ����������� ��� ����������');
        $email->addValidator($validator);

        $validator = new Zend_Validate_EmailAddress();
        $validator->setMessage('�� ������ ������ E-mail');
        $email->addValidator($validator);

        $validator = new Zend_Validate_Db_NoRecordExists('subscribers', 'email');
        $validator->setMessage('E-mail %value% ��� ��������');
        $email->addValidator($validator);
    }
}



class SubscribeController extends Zend_Controller_Action {
    function preDispatch() {
        if ($this->getRequest()->isXmlHttpRequest()) {
            Zend_Layout::getMvcInstance()->disableLayout();
            $params = $this->getRequest()->getParams();
            $params = $this->decode("UTF-8", "windows-1251", $params);
            $this->getRequest()->setParams($params);
            $_POST = $this->decode("UTF-8", "windows-1251", $_POST);
            $_GET = $this->decode("UTF-8", "windows-1251", $_GET);
        }
    }

    function decode($from, $to, $value) {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->decode($from, $to, $v);
            }
        } else {
            $value = iconv($from, $to, $value);
        }
        return $value;
    }

    function unsubscribeAction() {
        $sc = $this->getRequest()->getParam('sc');
        list($subscriber_id, $security_code) = explode('-', base64_decode($sc));

        $anSubscribersTable = new SubscribersTable;
        $sub = $anSubscribersTable->find($subscriber_id)->current();
        if ($sub
                && $sub->security_code == $security_code
                && $sub->security_code) {

            $sub->delete();
            $this->view->msg = '���� ����� ������ �� ����� ��������.';
        } else {
            $this->view->msg = '�� ������ ��� ���������.';
        }
    }

    function activateAction() {
        $sc = $this->getRequest()->getParam('sc');
        list($subscriber_id, $security_code) = explode('-', base64_decode($sc));

        $anSubscribersTable = new SubscribersTable;
        $sub = $anSubscribersTable->find($subscriber_id)->current();
        if ($sub
                && $sub->security_code == $security_code
                && !$sub->is_verified) {

            $sub->is_verified = 1;
            $sub->save();
            $this->view->msg = '���� �������� ������������.';
        } else {
            $this->view->msg = '�� ������ ��� ���������.';
        }
    }

    function indexAction() {
        $this->redirectIfNotAjax();

        $form = new Form_NewsSubscribe();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {
                $anSubscribersTable = new SubscribersTable;

                $sub = $anSubscribersTable->fetchNew();

                $sub->datetime = date('Y-m-d H:i:s');
                $sub->email = $form->getValue('email');
                $sub->is_verified = 0;
                $sub->security_code = User::generatePass(30);
                $sub->save();

                $this->sendVerifyEmail($sub);

                $resp = array();
                $resp['errorCode'] = 0;
                $resp['msg'] = iconv("windows-1251", "UTF-8", '��� ������� ������ � ������� ������������� ��������');
                $resp['action'] = 'reset';
                $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

                $resp = Zend_Json::encode($resp);
                $this->_helper->viewRenderer->setNoRender();
                $this->getResponse()
                        ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                        ->appendBody($resp);
            } else {
                $errors = $this->getErrors($form->getMessages());
                $this->error(array_shift($errors));
            }
        } else {
            $this->view->form = $form;
        }
    }

    protected function sendVerifyEmail(Subscriber $sub) {
        $settings = Zend_Registry::getInstance()->get('settings');
        $code = $sub->getSC();

        $uri = 'http://topauthor.ru/subscribe/activate?sc=' . $code;

        $mail = new Site_Mail('verify');
        $mail->setUri($uri);
        $mail->send($sub->email);
    }

    protected function error($msg) {
        $resp['errorCode'] = 1;
        $resp['msg'] = iconv("windows-1251", "UTF-8", $msg);
        $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

        $this->_helper->viewRenderer->setNoRender();
        $resp = Zend_Json::encode($resp);

        $this->getResponse()
                ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                ->appendBody($resp);
    }

    protected function getErrors($messages) {
        $result = array();
        foreach ($messages as $msg) {
            $result = array_merge($result, array_values($msg));
        }

        return $result;
    }

    protected function redirectIfNotAjax() {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->_redirect('/');
        }
    }
}