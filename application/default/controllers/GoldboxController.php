<?php

class GoldboxController extends Zend_Controller_Action
{
    function indexAction()
    {
        Zend_Layout::getMvcInstance()->setLayout('layout-wide');
        
        $anArticleView = new ArticlesView;     
        $this->view->items = $anArticleView->fetchAll(
                $anArticleView->select()
                    ->where('rating>=?', 17)
                    ->where('is_approved=1')
                    ->order('rating DESC')
                );
        
        
    }
}