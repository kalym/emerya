<?php

class ListController extends Zend_Controller_Action {

    function indexAction() {
        $anArticlesView = new ArticlesView();
        $anCatsTable = new CatsTable();

        $cat = $this->getRequest()->getParam('cat');

//        if (!$cat) {
//            throw new Zend_Controller_Dispatcher_Exception('');
//        }

        $this->view->cat = $cat;

        $select = $anArticlesView->select()
                ->where('is_approved=?', 1)
                ->order('date DESC');

//        if ($cat) {
//            $select->where('cat_id=?', $cat->cat_id);
//        }

        $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_DbTableSelect($select));
        $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('cat_article_perpage', 36));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }

}