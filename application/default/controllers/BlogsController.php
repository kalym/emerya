<?php

class Form_Post extends Zend_Form {

    function init()
    {
        $this->setAction('/blogs/add')
            ->setMethod(Zend_Form::METHOD_POST)
            ->setAttrib('id', 'blogs-add');

        $this->addElement('text', 'name', array(
            'required'=>true,
            'filters'=>array('StripNewlines', 'StripTags'),
            'label' => '���������'
        ));

        $name = $this->getElement('name');

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('����������, ������� �������� �������');
        $name->addValidator($validator);
        
        $this->addElement('text', 'video', array(
            'filters'=>array('StripNewlines', 'StripTags'),
            'label' => '����� (Youtube)'
        ));

        $this->addElement('textarea', 'content', array(
            'required'=>true,
            'label' => '�����',
            'filters'=>array('StripTags')
        ));

        $content = $this->getElement('content');
        $content->setAttrib('rows', 3);

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('����������, ������� ����� ����������');
        $content->addValidator($validator);
    }
}

class Form_Share extends Zend_Form {

    function init() {
        $this->setAction('/blogs/share')
                ->setMethod(Zend_Form::METHOD_POST)
                ->setAttrib('class', 'ajax')
                ->setAttrib('id', 'share');

        $this->addElement('hidden', 'post_id');
        $post_id = $this->getElement('post_id');
        $post_id->setDecorators(array('ViewHelper'));
        
        $this->addElement('text', 'email', array(
            'required' => true,
            'filters' => array('StripNewlines', 'StripTags'),
            'label' => 'E-mail �����'
        ));

        $email = $this->getElement('email');

        $email->setDecorators(array(array('ViewScript', array(
                    'viewScript' => 'form.phtml',
            ))));

        $validator = new Zend_Validate_NotEmpty();
        $validator->setMessage('���� E-mail ����������� ��� ����������');
        $email->addValidator($validator);

        $validator = new Zend_Validate_EmailAddress();
        $validator->setMessage('�� ������ ������ E-mail');
        $email->addValidator($validator);

        $this->setDecorators(array('FormElements', array('Callback', array(
                    'callback' => array($this, 'appendSubmit')
            )), 'Form', array('HtmlTag', array(
                    'tag' => 'ul',
                    'class' => 'form'
            ))));
    }

    function setPostId($id) {
        $this->getElement('post_id')->setValue($id);
    }
    
    function appendSubmit($content, $element, $options) {
        return '<li><a class="btn-red" href="#"><span>��������� �����</span></a></li>';
    }

}

class BlogsController extends Zend_Controller_Action
{
    function preDispatch() {
        if ($this->getRequest()->isXmlHttpRequest()) {
            Zend_Layout::getMvcInstance()->disableLayout();
            $params = $this->getRequest()->getParams();
            $params = $this->decode("UTF-8", "windows-1251", $params);
            $this->getRequest()->setParams($params);
            $_POST = $this->decode("UTF-8", "windows-1251", $_POST);
            $_GET = $this->decode("UTF-8", "windows-1251", $_GET);
        } else {
            $this->_helper->menu->select('blogs');
        }
    }
    function decode($from, $to, $value) {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->decode($from, $to, $v);
            }
        } else {
            $value = iconv($from, $to, $value);
        }
        return $value;
    }
    
     protected function error($msg) {
        $resp['errorCode'] = 1;
        $resp['msg'] = iconv("windows-1251", "UTF-8", $msg);
        $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');

        $this->_helper->viewRenderer->setNoRender();
        $resp = Zend_Json::encode($resp);

        $this->getResponse()
                ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                ->appendBody($resp);
    }
    
    protected function getErrors($messages) {
        $result = array();
        foreach ($messages as $msg) {
            $result = array_merge($result, array_values($msg));
        }

        return $result;
    }
    
    protected function redirectIfNotAjax() {
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->_redirect('/');
        }
    }
    
    function indexAction()
    {

        if (Zend_Auth::getInstance()->hasIdentity()) {
            $this->addPost();
        }

	$anPostsView = new PostsView();

        $select = $anPostsView->select()
                   ->where('is_approved=1')
                    ->order('date DESC')
                    ->order('post_id DESC');

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
               $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('posts_item_perpage', 10));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }
    
    function photoAction()
    {
	$anPostsView = new PostsView();

        $select = $anPostsView->select()
                   ->where('is_approved=1')
                   ->where('is_photo=1')
                    ->order('date DESC')
                    ->order('post_id DESC');

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
               $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('posts_item_perpage', 10));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }
    
    function videoAction()
    {

	$anPostsView = new PostsView();

        $select = $anPostsView->select()
                   ->where('is_approved=1')
                ->where('is_video=1')
                    ->order('date DESC')
                    ->order('post_id DESC');

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
               $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('posts_item_perpage', 10));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }
    
    function shareAction()
    {
        $this->redirectIfNotAjax();
        $form = new Form_Share;       
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($_POST)) {
              
                $anPostTable = new PostsTable;
                $post = $anPostTable->find($form->getValue('post_id'))->current();
                
                $mail = new Site_Mail('share-post');
                $mail->setPost($post);
                $mail->setUrl('http://www.topauthor.ru' . $this->view->url($post->toArray(), 'post', true));
                $mail->send($form->getValue('email'));
                
                $resp = array();
                $resp['errorCode'] = 0;
                $resp['msg'] = iconv("windows-1251", "UTF-8", '������ � ��������� ����� ���������� �� ��������� ���� E-mail');
                $resp['action'] = 'hide-form';
                $resp['_form_name_'] = $this->getRequest()->getParam('_form_name_');
                
                $anShareLogTable = new ShareLogTable;
                $anShareLogTable->insert(array(
                    'dattm' => date('Y-m-d H:i:s'),
                    'user_id' => Zend_Auth::getInstance()->hasIdentity() ?
                        Zend_Auth::getInstance()->getIdentity()->user_id :
                        null,
                    'typology_id' => $post->post_id,
                    'typology' => 'post',
                    'email' => $form->getValue('email')
                ));

                $resp = Zend_Json::encode($resp);
                $this->_helper->viewRenderer->setNoRender();
                $this->getResponse()
                        ->setHeader('Content-Type', 'application/json; charset=UTF-8')
                        ->appendBody($resp);
            } else {
                $errors = $this->getErrors($form->getMessages());
                $this->error(array_shift($errors));
            }
        } else {
            $form->setPostId($this->getRequest()->getParam('id'));
            $this->view->form = $form;
        }
    }

    function itemAction()
    {
        if (!$this->getRequest()->getParam('from')) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }
        
        $anPostsView = new PostsView();

        $item = $anPostsView->find(
            $this->getRequest()->getParam('id')
        )->current();

        if (!$item) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }
        
        //small protection against cheating
        if (!isset($_SESSION['read_blogs'])) {
            $_SESSION['read_blogs'] = array();
        } 

        if (!in_array($item->post_id, $_SESSION['read_blogs'])) {
            $item->clicks++;
            $item->save();
            $_SESSION['read_blogs'][] = $item->post_id;
        }

        $commentsProcessor = new CommentsProcessor($this->getRequest()->getParam('id'), CommentsTable::TYPOLOGY_POST);
        $commentsProcessor->run($this->getRequest());
        $this->view->commentsProcessor = $commentsProcessor;

        $this->view->item = $item;
        
        $this->view->add = $anPostsView->fetchAll(
                $anPostsView->select()
                ->where('is_photo=?', $item->is_photo)
                ->where('is_video=?', $item->is_video)
                ->where('is_approved=1')
                ->where('post_id<?', $item->post_id)
                ->order('date DESC')
                ->limit(($item->is_video || $item->is_photo) ? 3 : 5)
                );
    }

    function userAction() {
	$anPostsView = new PostsView();
        $anUserTable = new UsersTable;
        
        $this->view->user = $anUserTable->find($this->getRequest()->getParam('id'))->current();
        if (!$this->view->user) {
            throw new Zend_Controller_Dispatcher_Exception('');
        }

        $select = $anPostsView->select()
                ->where('is_approved=1')
                    ->order('date DESC')
                    ->where('user_id=?', $this->getRequest()->getParam('id'));

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
               $select
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_user_posts_perpage', 13));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }

    function addPost()
    {
        $form = new Form_Post;

        $anPostsTable = new PostsTable();
        $post = $anPostsTable->fetchNew();
        
        $images = array();
        if ($this->getRequest()->isPost()) {
            $p = $this->getRequest()->getPost();
            foreach ($p['img_small'] as $k=>$v) {
                if ($v) {
                    $images_item = array (
                        'small' => $v,
                        'big' => $p['img_big'][$k],
                        'preview' => $p['img_preview'][$k]
                    );
                    $images[] = $images_item;
                }
            }
        }
      

        if ($this->getRequest()->isPost() && $form->isValid($_POST)) {
            $post->name = $form->getValue('name');
            $post->content = $form->getValue('content');
            $post->video = $form->getValue('video');
            $post->date = date('Y-m-d H:i:s');
            $post->user_id = Zend_Auth::getInstance()->getIdentity()->user_id;
            $post->gallery = serialize($images);
            $post->is_video = $post->video ? 1 : 0;
            $post->is_photo = (!$post->video && count($images)) ? 1 : 0;
            $post->alias = Zend_Filter::filterStatic($post->name, 'Translit');
            
            if ($post->is_photo) {
                try {
                    $iu = new ImageUploader(Zend_Registry::getInstance()->get('root_dir'));
                    $urls = $iu->resize(Zend_Registry::getInstance()->get('root_dir') . $images[0]['big'], 
                            array(array(180, 116)));
                    $post->preview = $urls[0];     
                } catch (Exception $e) {}
            }
            
            if ($post->is_video && strpos($post->video, 'youtube.com') !== false) {
                $url = parse_url($post->video);
                $a = explode("/", trim($url['path'], '/'));
                $v = array_pop($a);
                try {
                    $uri = sprintf('http://img.youtube.com/vi/%s/0.jpg', $v);
                    $iu = new ImageUploader(Zend_Registry::getInstance()->get('root_dir'));
                    $urls = $iu->resize($uri, array(array(
                        'width' => 180,
                        'height' => 116,
                        'type' => ImageUploader::RESIZE_CENTER)));
                    $post->preview = $urls[0];
                } catch (Exception $e) {}
            }

            $post->is_approved = (int)$this->isApproved(Zend_Auth::getInstance()->getIdentity());
            $post->save();
            
            $this->view->addMsg = $post->is_approved ? 
                    '������ ���������.' :
                    '������ ��������� � ����� ������� �� ����� ����� �������� �����������.';
            
            HookManager::getInstance()->call(HookManager::EVENT_NEW_POST, array(
                'post' => $post
            ));
            Zend_Auth::getInstance()->getIdentity()->posts_count++;
            Zend_Auth::getInstance()->getIdentity()->save();
        } else {
            $this->view->form = $form;
            $this->view->images = array_reverse($images);
        }
    }
    
    protected function isApproved(User $user)
    {
        return $user->isWriter() && $user->haveAprovedArticles();
    }
}