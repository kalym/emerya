<?php

class PodcastsController extends Zend_Controller_Action
{

    function preDispatch()
    {
        $this->_helper->menu->select('podcasts');
    }

    function indexAction()
    {
        $anArticlesView = new ArticlesView();

        $this->view->popular = $anArticlesView->fetchAll(
            $anArticlesView->select()
                ->where('is_approved=?', 1)
                ->where('podcast<>?', '')
                ->order('rating DESC')
                ->limit(16)
        );

        $paginator = new Zend_Paginator(
            new Zend_Paginator_Adapter_DbTableSelect(
                $anArticlesView->select()
                    ->where('is_approved=?', 1)
                    ->where('podcast<>?', '')
                    //->where('rating>8')
                    ->order('date DESC')
            )
        );
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('front_podcasts_perpage', 12));
        $paginator->setPageRange(10);

        $this->view->paginator = $paginator;
    }

    function rssAction()
    {
        $anArticlesView = new ArticlesView();

        $items = $anArticlesView->fetchAll(
            $anArticlesView->select()
                ->where('is_approved=?', 1)
                ->where("podcast<>''")
                ->order('date DESC')
                ->where("date < '2011-09-02 00:00:00' OR rating>14")
        );


        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $feed = new Zend_Feed_Writer_Feed;
        $feed->setEncoding('UTF-8');
        $feed->setType('RSS');
        $feed->setTitle($this->encode('��� �����'));
        $feed->setDescription($this->encode('�������� ������ �� ���������� ������������ ��� �����'));
        $feed->setLink('http://topauthor.ru');
        $feed->setFeedLink('http://topauthor.ru' . $this->getFrontController()->getRouter()->assemble(
                array(
                    'controller' => 'podcasts',
                    'action' => 'rss'), 'default', true
            ), 'RSS');
        $feed->setCopyright($this->encode('��������� ���������� � ��� ����� � ������ ������� ������������'));
        $feed->setImage(array(
            'uri' => 'http://topauthor.ru/public/images/top-logo.jpg',
            'title' => $this->encode('��� �����'),
            'link' => 'http://topauthor.ru'
        ));

        foreach ($items as $item) {
            $entry = $feed->createEntry();
            $entry->setDateModified(strtotime($item->date));
            $entry->setDateCreated(strtotime($item->date));
            $entry->setTitle($this->encode($item->name));
            $entry->setLink('http://topauthor.ru' . $this->getFrontController()->getRouter()->assemble(
                    array($item->alias), 'article', true
                ));
            $entry->setContent($this->encode(substr($item->content, 0, 255) . '...'));
            $entry->setEnclosure(array(
                'uri' => 'http://topauthor.ru' . $item->podcast,
                'type' => 'audio/mpeg',
                'length' => filesize(Zend_Registry::get('root_dir') . $item->podcast)
            ));
            $entry->addAuthor($this->encode($item->u_name));
//            $entry->setImage($item->preview);
            $feed->addEntry($entry);
        }

        $this->getResponse()->setBody(
            $feed->export('RSS')
        );
    }

    protected function encode($text)
    {
        //use this becouse of Zend_Feed_Writer_Feed
        //require input in UTF-8 becouse of DOMDocument
        //require it
        return iconv('windows-1251', 'UTF-8', $text);
    }

}