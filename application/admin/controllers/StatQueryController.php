<?php

class Admin_StatQueryController extends Zend_Controller_Action {

    function indexAction() {

        $this->_helper->auth->requireAdminPerm('stat-query');

        $this->view->title = '��������� �������';

        $anStatQueryTable = new StatQueryTable;
       
        $select = $anStatQueryTable->select()->from('stat_query', array(
            'count' => new Zend_Db_Expr('COUNT(*)'),
            'query' => 'query',
            'type' => 'type',
            'dattm_last' => new Zend_Db_Expr('MAX(dattm)')
        ))->order('count DESC')->group(array('query', 'type'));
        
        $filter_type = $this->getRequest()->getParam('filter_type', 'all');
        switch ($filter_type) {
            case StatQueryTable::TYPE_USER :
                $select = $select->where('type=?', StatQueryTable::TYPE_USER);
                break;
            case StatQueryTable::TYPE_ARTICLE :
                $select = $select->where('type=?', StatQueryTable::TYPE_ARTICLE);
                break;
            case 'all' :
            default :
                $select = $select;
        }

        $filter_q = $this->getRequest()->getParam('filter_q', '');
        if ($filter_q) {
            $filter_q = $select->getAdapter()->quote('%' . $filter_q . '%');
            $select = $select->where("query LIKE {$filter_q}");
        }
        
        $filter_from = $this->getRequest()->getParam('filter_from', '');
        if ($filter_from) {
           $select = $select->where('dattm>?', $filter_from . ' 00:00:00');
        }
        
        $filter_to = $this->getRequest()->getParam('filter_to', '');
        if ($filter_to) {
           $select = $select->where('dattm<?', $filter_to . ' 23:59:59');
        }


        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);

        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }
}