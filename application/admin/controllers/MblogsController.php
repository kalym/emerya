<?php

class Admin_MblogsController extends Zend_Controller_Action
{
    function indexAction()
    {
        
      $this->_helper->auth->requireAdminPerm('mblogs');
        
      $params  = $this->getRequest()->getParams();
      $this->view->title = '�����';

      $anPostsTable = new PostsTable;
      
      if ( $this->getRequest()->isPost() ) {
          $post = $this->getRequest()->getPost();
          foreach($post['post_id'] as $post_id) {
              $post = $anPostsTable->find($post_id)->current();
              $post->is_approved = 1;
              $post->save();

              HookManager::getInstance()->call(HookManager::EVENT_APPROVE_POST, array(
                'post' => $post
              ));
          }
      }  
        
      $adapter = new Zend_Paginator_Adapter_DbTableSelect( $anPostsTable->select()
                                                                        ->where('is_approved=?', '0')
                                                                        ->order('date DESC') );
        
      $page = (isset($params['p'])) ? $params['p'] : 1;
      $paginator = new Zend_Paginator( $adapter );
      $paginator->setCurrentPageNumber($page);
      $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
      $paginator->setPageRange(10);
      $this->view->paginator = $paginator;    
    }

}