<?php

class Admin_BlogsController extends Zend_Controller_Action {

    function indexAction() {
        $this->_helper->auth->requireAdminPerm('blogs');

        $this->view->title = '����� �� ������';

        $anPostsTable = new PostsTable();
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            foreach ($post['item_id'] as $item_id) {
                $item = $anPostsTable->find($item_id)->current();
                $item->delete();
            }
        }

        $select = $anPostsTable->select()->order('date DESC')->order('post_id DESC');
        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);

        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }

    function editAction() {

        $this->_helper->auth->requireAdminPerm('blogs-edit');
        $params = $this->getRequest()->getParams();
        $this->view->title = '�������������� �����';

        $anPostsTable = new PostsTable();
        $post = $anPostsTable->find($params['id'])->current();

        if ($this->getRequest()->isPost()) {
            $__post = $this->getRequest()->getPost();

            $post->name = $__post['name'];
            $post->content = $__post['content'];
            $post->preview = $__post['preview'];
            $post->video = $__post['video'];
            
            $gallery = array();
            foreach ($params['_gallery_preview'] as $k => $v) {
                if (!$v) continue;
                $gallery[] = array(
                    'preview' => $v,
                    'small' => $params['_gallery_small'][$k],
                    'big' => $params['_gallery_big'][$k],
                );
            }
            
            $post->gallery = serialize($gallery);
            
            $post->is_video = $post->video ? 1 : 0;
            $post->is_photo = (!$post->video && count($gallery)) ? 1 : 0;
            
            $post->save();

            $msg['content'] = '���������� ���������';
            $msg['code'] = 0;
            $this->view->messages = array($msg);
        }
        $this->view->post = $post;
    }

    function deleteAction() {

        $this->_helper->auth->requireAdminPerm('blogs-del');
        $params = $this->getRequest()->getParams();
        $this->view->title = '�������� ������ � �����';

        $anPostsTable = new PostsTable();

        if ($this->getRequest()->isPost()) {
            $post = $anPostsTable->find($this->getRequest()->getPost('post_id'))->current();

            $anUsersTable = new UsersTable();
            $user = $anUsersTable->find($post->user_id)->current();
            $user->posts_count--;
            $user->save();

            $anCommentsTable = new CommentsTable();
            $comments = $anCommentsTable->fetchAll(
                    $anCommentsTable->select()
                            ->where('typology=?', CommentsTable::TYPOLOGY_POST)
                            ->where('typology_id=?', $post->post_id)
            );
            foreach ($comments as $comment) {
                $comment->delete();
            }

            $post->delete();
            header('Location: /admin/blogs/');
            exit;
        }

        $post = $anPostsTable->find($params['id'])->current();
        $this->view->post = $post;
    }

}