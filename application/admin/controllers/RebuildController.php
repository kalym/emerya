<?php

class Admin_RebuildController extends Zend_Controller_Action
{
    function indexAction()
    {
       die(); 
        
      $params  = $this->getRequest()->getParams();
      $this->view->title = '�������� ������'; 
      
      
      if ($this->getRequest()->isPost()) {
          $post = $this->getRequest()->getPost();
          
          $anUsersTable = new UsersTable();
          $anArticlesTable = new ArticlesTable();
          
          $users = $anUsersTable->fetchAll();
          
          foreach ($users as $user) {
          
                 $select = $anArticlesTable->select()
                              ->from('articles', array('count'=>'COUNT(article_id)'))
                              ->where('user_id=?', $user->user_id);
                              
               
          
                 $row = $anArticlesTable->fetchAll($select);
                 
                 $user->pub_count = $row->current()->count;
                 $user->save();
          }

          $articles = $anArticlesTable->fetchAll();

          foreach ($articles as $article) {
              if (!$article->alias) {
                  $article->alias = Zend_Filter::filterStatic($article->name, 'Translit');
                  $article->save();
              }
              $article->next_sunday = Article::getNextSunday($article->date);
              $article->save();

          }
          
          $msg['content'] = '�������� ���������';
          $msg['code']    = 0;
          $this->view->messages = array($msg);
      }
                
    }
}