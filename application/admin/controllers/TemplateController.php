<?php

class Admin_TemplateController extends Zend_Controller_Action
{
    
    function init()
    {
        $this->view->addHelperPath('Site/View/Helper', 'Site_View_Helper');

    }
    
    function indexAction()
    {
      $params  = $this->getRequest()->getParams();
      $this->view->title = '������� �����';
      $templateTable = new TemplateTable();
        
      $adapter = new Zend_Paginator_Adapter_DbTableSelect( $templateTable->select()->order('name DESC') );
        
      $page = (isset($params['p'])) ? $params['p'] : 1;
      $paginator = new Zend_Paginator( $adapter );
      $paginator->setCurrentPageNumber($page);
      $paginator->setItemCountPerPage(100);
      $paginator->setPageRange(10);
      $this->view->paginator = $paginator;
        
    }
    
    function editAction()
    {
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������������� �������';
        
        $templateTable = new TemplateTable();
        $template = $templateTable->find($params['id'])->current();
        if ( $this->getRequest()->isPost() ) {
            $post = $this->getRequest()->getPost();
            
            $template->subject = $post['subject'];
            $template->content = $post['content'];
            $template->is_html = $post['is_html'];
            
            $template->save();
            
            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);        
        }
        
        $this->view->template = $template;
    }
}