<?php

class Admin_IndexController extends Zend_Controller_Action {

    function indexAction() {
        //$this->view->title = 'Система управления контентом';
        
        $anQaTable = new QATable();
        $qa = $anQaTable->fetchAll($anQaTable->select()->where('answer=?', ''));

        $this->view->qaNum = $qa->count();

        $anSponsorTable = new SponsorTable();
        $sponsor = $anSponsorTable->fetchAll($anSponsorTable->select()->where('is_approved=0'));

        $this->view->sponsorNum = $sponsor->count();

        $anQnaTable = new QnaTable();
        $qna = $anQnaTable->fetchAll($anQnaTable->select()->where('is_approved=0'));

        $this->view->mqnaNum = $qna->count();

        $anPostsTable = new PostsTable();
        $post = $anPostsTable->fetchAll($anPostsTable->select()->where('is_approved=0'));

        $this->view->mpostNum = $post->count();

        $anUsersTable = new UsersTable;

        if (Site_Auth_Admin::getInstance()->getIdentity()->hasAccess('last-user')) {
            $this->view->users = $anUsersTable->fetchAll($anUsersTable->select()->order('date DESC')->limit(10));
        }

        if (Site_Auth_Admin::getInstance()->getIdentity()->hasAccess('quick-stat')) {

            $usersTable = new UsersTable;

            foreach (array(
                         User::TYPE_WRITER, User::TYPE_READER, User::TYPE_EMPLOYER
                     ) as $type) {
                $row = $usersTable->fetchRow(
                    $usersTable->select()
                        ->from('users', array(
                            'all' => new Zend_Db_Expr('COUNT(user_id)'),
                            'lock' => new Zend_Db_Expr('COUNT(IF(is_locked, user_id, NULL))')
                        ))
                        ->where('type=?', $type)
                );
                @$total_all += $row->all;
                @$total_lock += $row->lock;
                $user_stat[$type] = array(
                    'all' => $row->all,
                    'lock' => $row->lock
                );
            }
            $user_stat['all'] = array(
                'all' => $total_all,
                'lock' => $total_lock
            );
            $this->view->user_stat = $user_stat;

            $subscribersTable = new SubscribersTable;

            $row = $subscribersTable->fetchRow(
                $subscribersTable->select()
                    ->from('subscribers', array(
                        'all' => new Zend_Db_Expr('COUNT(subscriber_id)'),
                        'verified' => new Zend_Db_Expr('COUNT(IF(is_verified, subscriber_id, NULL))')
                    ))
            );

            $this->view->sub_all = $row->all;
            $this->view->sub_verified = $row->verified;

            $datetime = date('Y-m-d H:i:s', strtotime('-1 day'));
            $date = date('Y-m-d', strtotime('-1 day'));
            $user = $anUsersTable->fetchAll($anUsersTable->select()->where('last_activity>=?', $datetime));
            $this->view->activeUserNum = $user->count();

            $anCommentsTable = new CommentsTable;
            $comments = $anCommentsTable->fetchAll($anCommentsTable->select()->where('datetime>=?', $datetime));
            $this->view->commentsNum = $comments->count();

            $anPostsTable = new PostsTable;
            $posts = $anPostsTable->fetchAll($anPostsTable->select()->where('date>=?', $datetime));
            $this->view->postsNum = $posts->count();

            $anArticleTable = new ArticlesTable;
            $articles = $anArticleTable->fetchAll($anArticleTable->select()->where('date>=?', $datetime));
            $this->view->articlesNum = $articles->count();

            $anShopArticleTable = new ShopArticleTable;
            $articles = $anShopArticleTable->fetchAll($anShopArticleTable->select()->where('date>=?', $datetime));
            $this->view->shopArticlesNum = $articles->count();

            $paid = $anShopArticleTable->fetchAll($anShopArticleTable->select()->where('date_paid>=?', $datetime));
            $this->view->shopPaidNum = $paid->count();

            $users = $anUsersTable->fetchAll($anUsersTable->select()->where('date>=?', $datetime));
            $this->view->newUsersNum = $users->count();

            $anQnaTable = new QnaTable;
            $qna = $anQnaTable->fetchAll($anQnaTable->select()->where('datetime>=?', $datetime));
            $this->view->qnaNum = $qna->count();
            $this->view->datetime = $datetime;

            $select_approved = $anShopArticleTable->select()->from('shop_article', array(
                'dat' => new Zend_Db_Expr('DATE(date_approved)'),
                'approved' => new Zend_Db_Expr('SUM(IF(date_approved IS NULL, 0, 1))')))
                ->group(new Zend_Db_Expr('DATE(date_approved)'))->where('date_approved>?', date('Y-m-d', time() - 3600 * 24 * 14));
            $select_paid = $anShopArticleTable->select()->from('shop_article', array(
                'dat' => new Zend_Db_Expr('DATE(date_paid)'),
                'paid' => new Zend_Db_Expr('SUM(IF(date_paid IS NULL, 0, 1))')))
                ->group(new Zend_Db_Expr('DATE(date_paid)'))->where('date_paid>?', date('Y-m-d', time() - 3600 * 24 * 14));

            $approvedCnt = $anShopArticleTable->fetchAll($select_approved);
            $paidCnt = $anShopArticleTable->fetchAll($select_paid);

            $approved = array();
            foreach ($approvedCnt as $ac) {
                $approved[$ac->dat] = $ac->approved;
            }

            $paid = array();
            foreach ($paidCnt as $pc) {
                $paid[$pc->dat] = $pc->paid;
            }

            $dat = array();
            for ($i = 14; $i >= 0; $i--)
                $dat[] = date('Y-m-d', strtotime("- $i day"));

            $stat = array();
            foreach ($dat as $d)
                $stat[$d] = array(
                    'approved' => isset($approved[$d]) ? $approved[$d] : 0,
                    'paid' => isset($paid[$d]) ? $paid[$d] : 0
                );

            $this->view->statApprovedPaid = $stat;


            $anPaymentsTable = new PaymentsTable;
            $select = $anPaymentsTable->select()->from('payments', array(
                'article' => new Zend_Db_Expr("SUM(IF(typology='article', amount, 0))"),
                'shop_article' => new Zend_Db_Expr("SUM(IF(typology='shop_article', amount, 0))"),
                'article_view' => new Zend_Db_Expr("SUM(IF(typology='article_view', amount, 0))"),
                'dat' => "CONCAT(YEAR(datetime), '-', MONTH(datetime))"))->group('dat')
                ->where('datetime>?', date('Y-m-01 00:00:00', strtotime('- 12 month')));
            $this->view->statUserIncome = $anPaymentsTable->fetchAll($select);

            $select = $anPaymentsTable->select()->from('payments', array(
                'total' => new Zend_Db_Expr("SUM(IF(type=0, amount, 0))"),
                'cnt' => new Zend_Db_Expr("COUNT(IF(typology='shop_article', payment_id, NULL))"),
                'article' => new Zend_Db_Expr("SUM(IF(typology='article', amount, 0))"),
                'article_view' => new Zend_Db_Expr("SUM(IF(typology='article_view', amount, 0))"),
                'shop_article' => new Zend_Db_Expr("SUM(IF(typology='shop_article', amount, 0))")));
            $this->view->statTotalUserIncome = $anPaymentsTable->fetchRow($select)->toArray();

            $commission = Site_Settings::get('article_commission');
            $price = $this->view->statTotalUserIncome['shop_article'];
            $this->view->statTotalUserIncome['shop_article_paid'] = $price + ceil(($price * $commission) / 100);
        }

        if (Site_Auth_Admin::getInstance()->getIdentity()->hasAccess('stat-login')) {
            $anStatLoginView = new StatLoginView;
            $this->view->statLogin = $anStatLoginView->fetchAll($anStatLoginView->select()->from('stat_login_view', array('dat' => 'dat', 'cnt' => new Zend_Db_Expr('COUNT(*)'), 'cnt_unique' => new Zend_Db_Expr('COUNT(DISTINCT user_id)')))->group('dat')->where('dat>?', date('Y-m-d', time() - 3600 * 24 * 14)));
        }

        if (Site_Auth_Admin::getInstance()->getIdentity()->hasAccess('stat-share')) {
            $dat = array();
            for ($i = 14; $i >= 0; $i--)
                $dat[] = date('Y-m-d', strtotime("- $i day"));

            $anShareLogTable = new ShareLogTable;
            $rows = $anShareLogTable->fetchAll($anShareLogTable->select()->from('share_log', array('dat' => new Zend_Db_Expr('DATE(dattm)'), 'cnt' => new Zend_Db_Expr('COUNT(share_log_id)'), 'cnt_user' => new Zend_Db_Expr('COUNT(user_id)')))->group('dat')->where('dattm>?', date('Y-m-d', time() - 3600 * 24 * 14)));
            $share_cnt = array();
            $share_cnt_user = array();
            foreach ($rows as $row) {
                $share_cnt[$row->dat] = $row->cnt;
                $share_cnt_user[$row->dat] = $row->cnt_user;
            }


            $stat = array();
            foreach ($dat as $d)
                $stat[$d] = array(
                    'cnt' => isset($share_cnt[$d]) ? $share_cnt[$d] : 0,
                    'cnt_user' => isset($share_cnt_user[$d]) ? $share_cnt_user[$d] : 0
                );
            $this->view->statShare = $stat;
        }
    }

    function loginAction() {      
        //TODO store datatime and IP to admin's record
        Zend_Layout::getMvcInstance()->disableLayout();
        if ($this->getRequest()->isPost()) {
            //TODO check if user already logged in
            $post = $this->getRequest()->getPost();
            $auth = Site_Auth_Admin::getInstance();

            $authAdapter = new Site_Auth_Adapter_Admin($post['login'], $post['pass']);
            $result = $auth->authenticate($authAdapter);

            if ($result->isValid()) {
                $admin = $result->getIdentity();
                $admin->last_ip = $admin->ip;
                $admin->ip = $_SERVER['REMOTE_ADDR'];
                $admin->last_login_time = $admin->login_time;
                $admin->login_time = date('Y-m-d H:i:s');
                $admin->save();
                Zend_Layout::getMvcInstance()->enableLayout();
                $this->getRequest()->setControllerName('index');
                $this->getRequest()->setActionName('index');
                $this->getRequest()->setDispatched(false);
            }
        }
    }

    function logoutAction() {
        $auth = Site_Auth_Admin::getInstance();
        $auth->clearIdentity();
        header("Location: /");
        exit;
    }

}