<?php

class Admin_NewsController extends Zend_Controller_Action
{
    function indexAction()
    {
        
        $this->_helper->auth->requireAdminPerm('news');
        
      $params  = $this->getRequest()->getParams();
      $this->view->title = '������� �����';

      $anNewsTable = new NewsTable();

      $adapter = new Zend_Paginator_Adapter_DbTableSelect( $anNewsTable->select() );

      $page = (isset($params['p'])) ? $params['p'] : 1;
      $paginator = new Zend_Paginator( $adapter );
      $paginator->setCurrentPageNumber($page);
      $paginator->setItemCountPerPage(30);
      $paginator->setPageRange(8);
      $this->view->paginator = $paginator;

    }

    function editAction()
    {
        $this->_helper->auth->requireAdminPerm('news-edit');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������������� �������';

        $anNewsTable = new NewsTable();
        $news = $anNewsTable->find($params['id'])->current();

        if ( $this->getRequest()->isPost() ) {
            $post = $this->getRequest()->getPost();

            $news->name = $post['name'];
            $news->fullname = $post['fullname'];
            $news->keywords = $post['keywords'];
            $news->description = $post['description'];
            $news->title = $post['title'];
            $news->content = $post['content'];
            $news->date = $post['date'];


            $news->save();
            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);

        }

        $this->view->news = $news;

    }

    function newAction()
    {
        $this->_helper->auth->requireAdminPerm('news-new');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������� �������';


        if ( $this->getRequest()->isPost() ) {
            $post = $this->getRequest()->getPost();
            $anNewsTable = new NewsTable();


            $news['name'] = $post['name'];
            $news['fullname'] = $post['fullname'];
            $news['date'] = $post['date'];
            $news['keywords'] = $post['keywords'];
            $news['description'] = $post['description'];
            $news['title'] = $post['title'];
            $news['content'] = $post['content'];

            $anNewsTable->insert($news);
            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);

        }

    }

    function deleteAction()
    {
        $this->_helper->auth->requireAdminPerm('news-del');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������� �������';

        $anNewsTable = new NewsTable();

        if ( $this->getRequest()->isPost() ) {
            $news = $anNewsTable->find($this->getRequest()->getPost('news_id'))->current();
            $news->delete();
            header('Location: /admin/news/');
            exit;

        }

        $news = $anNewsTable->find($params['id'])->current();
        $this->view->news = $news;

    }

}