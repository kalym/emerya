<?php

class Admin_CmsController extends Zend_Controller_Action
{
    function indexAction()
    {
        $this->_helper->auth->requireAdminPerm('cms');
      $this->view->title = '�������� �����';

      $anPagesTable = new PagesTable();
        
      $adapter = new Zend_Paginator_Adapter_DbTableSelect( $anPagesTable->select()->order('priority DESC') );
        
      $paginator = new Zend_Paginator( $adapter );
      $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
      );
      $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
      $paginator->setPageRange(10);
      $this->view->paginator = $paginator;
        
    }
    
    function editAction()
    {
        $this->_helper->auth->requireAdminPerm('cms-edit');
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������������� ��������';
        
        $anPagesTable = new PagesTable();
        $page = $anPagesTable->find($params['id'])->current();
        
        if ( $this->getRequest()->isPost() ) {
            $post = $this->getRequest()->getPost();
            
            $page->name        = $post['name'];
            $page->fullname    = $post['fullname'];
            $page->priority    = $post['priority'];
            $page->keywords    = $post['keywords'];
            $page->description = $post['description'];
            $page->title       = $post['title'];
            $page->content     = $post['content'];
            $page->onmenu     = $post['onmenu'];
            $page->alias     = $post['alias'];

            $page->save();
            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);
        
        }
        $this->view->page = $page;
    
    }
    
    function newAction()
    {
        $this->_helper->auth->requireAdminPerm('cms-edit');
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������� ��������';
        
        
        if ( $this->getRequest()->isPost() ) {
            $post = $this->getRequest()->getPost();
            $anPagesTable = new PagesTable();
            
            
            $page['name']        = $post['name'];
            $page['fullname']    = $post['fullname'];
            $page['priority']    = $post['priority'];
            $page['keywords']    = $post['keywords'];
            $page['description'] = $post['description'];
            $page['title']       = $post['title'];
            $page['content']     = $post['content'];
            $page['onmenu']     = $post['onmenu'];
            $page['alias']     = $post['alias'];
            
            $anPagesTable->insert($page);
            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);
        
        }
    
    }
    
    function deleteAction()
    {
        $this->_helper->auth->requireAdminPerm('cms-del');
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������� ��������';
        
        $anPagesTable = new PagesTable();
        
        if ( $this->getRequest()->isPost() ) {
            $page = $anPagesTable->find($this->getRequest()->getPost('page_id'))->current();
            $page->delete();
            header('Location: /admin/cms/');
            exit;

        }
        
        $page = $anPagesTable->find($params['id'])->current();
        $this->view->page = $page;
    
    }

}