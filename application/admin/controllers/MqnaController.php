<?php

class Admin_MqnaController extends Zend_Controller_Action
{
    function indexAction()
    {
        
      $this->_helper->auth->requireAdminPerm('mqna');
        
      $params  = $this->getRequest()->getParams();
      $this->view->title = '�������';

      $anQnaTable = new QnaTable;
      
      if ( $this->getRequest()->isPost() ) {
          $qna = $this->getRequest()->getPost();
          foreach($qna['qna_id'] as $qna_id) {
              $qna = $anQnaTable->find($qna_id)->current();
              $qna->is_approved = 1;
              $qna->save();

              HookManager::getInstance()->call(HookManager::EVENT_APPROVE_QNA, array(
                'qna' => $qna
              ));
          }
      }  
        
      $adapter = new Zend_Paginator_Adapter_DbTableSelect( $anQnaTable->select()
                                                                        ->where('is_approved=?', '0')
                                                                        ->order('datetime DESC') );
        
      $page = (isset($params['p'])) ? $params['p'] : 1;
      $paginator = new Zend_Paginator( $adapter );
      $paginator->setCurrentPageNumber($page);
      $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
      $paginator->setPageRange(10);
      $this->view->paginator = $paginator;    
    }

}