<?php

class Admin_CommentsController extends Zend_Controller_Action
{
    function indexAction()
    {
        $this->_helper->auth->requireAdminPerm('comments');
      $this->view->title = '�����������';

      $anCommentsTable = new CommentsView();
      if ( $this->getRequest()->isPost() ) {
          $post = $this->getRequest()->getPost();
          foreach($post['item_id'] as $item_id) {
              $item = $anCommentsTable->find($item_id)->current();
              $item->delete();
          }
      }

      $select = $anCommentsTable->select()->order('datetime DESC');
      $filter_type = $this->getRequest()->getParam('filter_type', 'all');
      switch ($filter_type) {
          case 'article' :
                $select = $select->where('typology=?', 'article');
                break;
          case 'news' :
                $select = $select->where('typology=?', 'news');
                break;
          case 'post' :
                $select = $select->where('typology=?', 'post');
                break;
          case 'qna' :
                $select = $select->where('typology=?', 'qna');
                break;
          case 'all' :
          default :
              $select = $select;
      }
      
      $filter_q = $this->getRequest()->getParam('filter_q', '');
        if ($filter_q) {
            $filter_q = $select->getAdapter()->quote('%' . $filter_q . '%');
            $select = $select->where("content LIKE {$filter_q}");
        }


      $adapter = new Zend_Paginator_Adapter_DbTableSelect( $select );

      $paginator = new Zend_Paginator( $adapter );
      $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
      );
      $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
      $paginator->setPageRange(10);
      $this->view->paginator = $paginator;

    }

    function editAction()
    {
        $this->_helper->auth->requireAdminPerm('comments-edit');
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������������� �����������';

        $anCommentsTable = new CommentsTable();
        $comment = $anCommentsTable->find($params['id'])->current();

        if ( $this->getRequest()->isPost() ) {
            $post = $this->getRequest()->getPost();

            $comment->content = $post['content'];
            $comment->video = $post['video'];
            
            $gallery = array();
            foreach ($params['_gallery_preview'] as $k => $v) {
                if (!$v) continue;
                $gallery[] = array(
                    'preview' => $v,
                    'small' => $params['_gallery_small'][$k],
                    'big' => $params['_gallery_big'][$k],
                );
            }
            
            $comment->gallery = serialize($gallery);
            
            $comment->save();

            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);

        }

        $this->view->comment = $comment;

    }

    function deleteAction()
    {
        $this->_helper->auth->requireAdminPerm('comments-del');
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������� �����������';

        $anCommentsTable = new CommentsTable();

        if ( $this->getRequest()->isPost() ) {
            $comment = $anCommentsTable->find($this->getRequest()->getPost('comment_id'))->current();
            $comment->delete();
            header('Location: /admin/comments/');
            exit;

        }

        $comment = $anCommentsTable->find($params['id'])->current();
        $this->view->comment = $comment;

    }

}
