<?php

class Admin_QaController extends Zend_Controller_Action
{
    function indexAction()
    {
        $this->_helper->auth->requireAdminPerm('qa');
        
      $this->view->title = '������� �������������';

      $anQATable = new QATable();
      if ( $this->getRequest()->isPost() ) {
          $post = $this->getRequest()->getPost();
          foreach($post['item_id'] as $item_id) {
              $item = $anQATable->find($item_id)->current();
              $item->delete();
          }
      }

      $select = $anQATable->select()->order('datetime DESC');
      $filter = $this->getRequest()->getParam('filter', 'all');
      switch ($filter) {
          case 'answered' :
                $select = $select->where('answer<>""');
                break;
          case 'unanswered' :
                $select = $select->where('answer=""');
                break;
          case 'all' :
          default :
              $select = $select;
      }

      $adapter = new Zend_Paginator_Adapter_DbTableSelect( $select );

      $paginator = new Zend_Paginator( $adapter );
      $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
      );
      $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
      $paginator->setPageRange(10);
      $this->view->paginator = $paginator;

    }

    function editAction()
    {
        $this->_helper->auth->requireAdminPerm('qa-edit');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������������� �������';

        $anQATable = new QATable();
        $qa = $anQATable->find($params['id'])->current();

        if ( $this->getRequest()->isPost() ) {
            $post = $this->getRequest()->getPost();

            $qa->question = $post['question'];
            $qa->answer = $post['answer'];

            $qa->save();

            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);

        }

        $this->view->qa = $qa;

    }

    function newAction()
    {
        $this->_helper->auth->requireAdminPerm('qa-new');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������� �������';


        if ( $this->getRequest()->isPost() ) {
            $post = $this->getRequest()->getPost();
            $anQATable = new QATable();


            $page['question'] = $post['question'];
            $page['answer'] = $post['answer'];
            $page['datetime'] = date("Y-m-d H:i:s");


            $anQATable->insert($page);
            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);

        }

    }

    function deleteAction()
    {
        $this->_helper->auth->requireAdminPerm('qa-del');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������� �������';

        $anQATable = new QATable();

        if ( $this->getRequest()->isPost() ) {
            $qa = $anQATable->find($this->getRequest()->getPost('qa_id'))->current();
            $qa->delete();
            header('Location: /admin/qa/');
            exit;

        }

        $qa = $anQATable->find($params['id'])->current();
        $this->view->qa = $qa;

    }

}