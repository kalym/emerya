<?php

class Admin_StatLoginController extends Zend_Controller_Action {

    function indexAction() {

        $this->_helper->auth->requireAdminPerm('stat-login');

        $this->view->title = '����� �� ����';

        $anStatLoginTable = new StatLoginView;
        $select = $anStatLoginTable->select()->from('stat_login_view', array(
            'cnt' => 'COUNT(*)',
            '*'
        ))->order('cnt DESC')->group(array('user_id'));
        
        $filter_q = $this->getRequest()->getParam('filter_q', '');
        if ($filter_q) {
            $filter_q = $select->getAdapter()->quote('%' . $filter_q . '%');
            $select = $select->where("u_name_f LIKE {$filter_q} 
            OR u_name_l LIKE {$filter_q}
            OR u_email LIKE {$filter_q}
            OR u_login LIKE {$filter_q}");
        }
        
        $filter_from = $this->getRequest()->getParam('filter_from', '');
        if ($filter_from) {
           $select = $select->where('dattm>?', $filter_from . ' 00:00:00');
        }
        
        $filter_to = $this->getRequest()->getParam('filter_to', '');
        if ($filter_to) {
           $select = $select->where('dattm<?', $filter_to . ' 23:59:59');
        }
        
        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);

        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }
    
    function detailsShortAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $user_id = $this->getRequest()->getParam('user_id');
        $anStatLoginTable = new StatLoginTable;
        $select = $anStatLoginTable->select()->order('dattm DESC')
                ->where('user_id=?', $user_id)
                ->limit(10);
        
        $filter_from = $this->getRequest()->getParam('from', '');
        if ($filter_from) {
           $select = $select->where('dattm>?', $filter_from . ' 00:00:00');
        }
        
        $filter_to = $this->getRequest()->getParam('to', '');
        if ($filter_to) {
           $select = $select->where('dattm<?', $filter_to . ' 23:59:59');
        }
        
        $this->view->query = http_build_query(array(
            'user_id' => $user_id,
            'from' => $filter_from,
            'to' => $filter_to
        ));
        
        $this->view->items = $anStatLoginTable->fetchAll($select);   
    }
    
    function detailsAction()
    {
        $this->view->title = '����� �� ���� (��������� ����������)';
        
        $user_id = $this->getRequest()->getParam('user_id');
        $anStatLoginTable = new StatLoginView;
        $select = $anStatLoginTable->select()->order('dattm DESC')
                ->where('user_id=?', $user_id);
        
        $filter_from = $this->getRequest()->getParam('from', '');
        if ($filter_from) {
           $select = $select->where('dattm>?', $filter_from . ' 00:00:00');
        }
        
        $filter_to = $this->getRequest()->getParam('to', '');
        if ($filter_to) {
           $select = $select->where('dattm<?', $filter_to . ' 23:59:59');
        }
        
        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);

        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }
    
    
}