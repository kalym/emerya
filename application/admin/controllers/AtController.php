<?php

class Admin_AtController extends Zend_Controller_Action {

    function indexAction() {

        $this->_helper->auth->requireAdminPerm('at');

        $this->view->title = '��������� ��� ������';

        $anATView = new ATView();

        $adapter = new Zend_Paginator_Adapter_DbTableSelect($anATView->select()->where('is_active=1')->order('name DESC'));

        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }

    function editAction() {

        $this->_helper->auth->requireAdminPerm('at-edit');

        $params = $this->getRequest()->getParams();
        $this->view->title = '�������������� ���������';

        $anATView = new ATView();
        $at = $anATView->find($params['id'])->current();

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();

            $at->name = $post['name'];
            $at->cat_id = $post['cat_id'];

            $at->save();
            $msg['content'] = '���������� ���������';
            $msg['code'] = 0;
            $this->view->messages = array($msg);
        }

        $anCatsTable = new CatsTable();
        $this->view->catOptions = $anCatsTable->getOptions();
        $this->view->at = $at;
    }

    function newAction() {

        $this->_helper->auth->requireAdminPerm('at-edit');

        $params = $this->getRequest()->getParams();
        $this->view->title = '�������� ���������';


        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            $anATTable = new ATTable();


            $at['name'] = $post['name'];
            $at['cat_id'] = $post['cat_id'];

            $anATTable->insert($at);
            $msg['content'] = '���������� ���������';
            $msg['code'] = 0;
            $this->view->messages = array($msg);
        }

        $anCatsTable = new CatsTable();
        $this->view->catOptions = $anCatsTable->getOptions();
    }
    
    function suggestAction() 
    {
        
        $this->_helper->auth->requireAdminPerm('at-edit');
        Zend_Layout::getMvcInstance()->disableLayout();
        $name = $this->getRequest()->getParam('name');
        $name = iconv('UTF-8', 'windows-1251', $name);
        if (!$name) return;
        $anATTable = new ATTable;
        $this->view->items = $anATTable->fetchAll($anATTable->select()
                    ->where('name LIKE ?', '%' . $name .'%')->limit(40));
    }

    function deleteAction() {

        $this->_helper->auth->requireAdminPerm('at-del');

        $params = $this->getRequest()->getParams();
        $this->view->title = '�������� ���������';

        $anATView = new ATView();
        $anATTable = new ATTable();

        if ($this->getRequest()->isPost()) {
            $at = $anATTable->find($this->getRequest()->getPost('at_id'))->current();
            $at->delete();
            header('Location: /admin/at/');
            exit;
        }

        $at = $anATView->find($params['id'])->current();
        $this->view->at = $at;
    }

}