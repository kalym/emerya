<?php

class Admin_PhotoController extends Zend_Controller_Action {

    function indexAction() {
        
        $this->_helper->auth->requireAdminPerm('photo');
        
        $params = $this->getRequest()->getParams();
        $this->view->title = '����������';

        $anPhotoTable = new PhotoTable;
        $select = $anPhotoTable->select()->order('datetime DESC');

        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);


        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }

    function deleteAction() {
        $this->_helper->auth->requireAdminPerm('photo-del');
        $anPhotoTable = new PhotoTable();

        $photo = $anPhotoTable->find($this->getRequest()->getParam('id'))->current();

        $photo->delete();
        $back_url = $this->getRequest()->getParam('bu');
        header('Location: ' . ($back_url ? $back_url : '/admin/photo/'));
        exit;
    }

}