<?php

class Admin_CatsController extends Zend_Controller_Action
{
    function indexAction()
    {
        $this->_helper->auth->requireAdminPerm('cats');
      $params  = $this->getRequest()->getParams();
      $this->view->title = '������� ��������';
      $anCatsTable = new CatsTable();
      $this->view->cats = $anCatsTable->fetchAll();

    }

    function editAction()
    {
        $this->_helper->auth->requireAdminPerm('cats-edit');
        $this->view->title = '�������������� ������� ��������';
        $params  = $this->getRequest()->getParams();

        $anCatsTable = new CatsTable();
        $cat = $anCatsTable->find($params['id'])->current();
        if ( $this->getRequest()->isPost() ) {
            $cat->priority    = $this->getRequest()->getPost('priority');
            $cat->name        = $this->getRequest()->getPost('name');
            $cat->fullname    = $this->getRequest()->getPost('fullname');
            $cat->alias    = $this->getRequest()->getPost('alias');
            $cat->title       = $this->getRequest()->getPost('title');
            $cat->keywords    = $this->getRequest()->getPost('keywords');
            $cat->description = $this->getRequest()->getPost('description');
            $cat->save();

            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);
        }

        $this->view->cat = $cat;

    }

    function newAction()
    {
        $this->_helper->auth->requireAdminPerm('cats-edit');
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������� ���������';


        if ( $this->getRequest()->isPost() ) {

            $post = $this->getRequest()->getPost();
            $anCatsTable = new CatsTable();

            $cat['name'] = $post['name'];
            $cat['fullname'] = $post['fullname'];
            $cat['alias'] = $post['alias'];
            $cat['priority'] = $post['priority'];
            $cat['title'] = $this->getRequest()->getPost('title');
            $cat['keywords'] = $this->getRequest()->getPost('keywords');
            $cat['description'] = $this->getRequest()->getPost('description');

            $anCatsTable->insert($cat);

            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);
         }

    }

    function deleteAction()
    {
        $this->_helper->auth->requireAdminPerm('cats-del');
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������� ���������';

        $anCatsTable = new CatsTable();

        if ( $this->getRequest()->isPost() ) {
            $post = $this->getRequest()->getPost();

            $cat = $anCatsTable->find($this->getRequest()->getPost('cat_id'))->current();
            $cat->delete();

            header('Location: /admin/cats/');
            exit;
        }

        $cat = $anCatsTable->find($params['id'])->current();

        $this->view->cat = $cat;
    }

}