<?php

class Admin_MarticlesController extends Zend_Controller_Action
{
    function indexAction()
    {
        
        $this->_helper->auth->requireAdminPerm('marticles');
        
      $params  = $this->getRequest()->getParams();
      $this->view->title = '������';

      $anArticlesTable = new ArticlesTable();
      
      if ( $this->getRequest()->isPost() ) {
          $this->_helper->auth->requireAdminPerm('marticles-approve');
          $post = $this->getRequest()->getPost();
          foreach($post['article_id'] as $article_id) {
              $article = $anArticlesTable->find($article_id)->current();
              $article->approve();
              $article->save();

              $anUsersTable = new UsersTable();		
              $user = $anUsersTable->find($article->user_id)->current();
              $user->rating+= $article->at_id ?
                        Config::get('rating_approved_article_at', 0) :
                        Config::get('rating_approved_article', 0);
              $user->pub_approved_count = $user->countApprovedArticles();
              $user->save();     
          }
      }  
        
      $adapter = new Zend_Paginator_Adapter_DbTableSelect( $anArticlesTable->select()
                                                                        ->where('is_approved=?', '0')
                                                                        ->order('date DESC') );
        
      $page = (isset($params['p'])) ? $params['p'] : 1;
      $paginator = new Zend_Paginator( $adapter );
      $paginator->setCurrentPageNumber($page);
      $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
      $paginator->setPageRange(10);
      $this->view->paginator = $paginator;    
    }

}