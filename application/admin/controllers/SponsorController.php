<?php

class Admin_SponsorController extends Zend_Controller_Action {

    function indexAction() {
        
        $this->_helper->auth->requireAdminPerm('sponsor');
        
        $params = $this->getRequest()->getParams();
        $this->view->title = '������';

        $anSponsorTable = new SponsorView();

        if ($this->getRequest()->isPost()) {
            $this->_helper->auth->requireAdminPerm('sponsor-approved');
            $post = $this->getRequest()->getPost();
            foreach ($post['sponsor_id'] as $sponsor_id) {
                $sponsor = $anSponsorTable->find($sponsor_id)->current();
                $sponsor->is_approved = 1;
                $this->sendAcceptMail($sponsor);
                $sponsor->save();          
            }
        }

        $adapter = new Zend_Paginator_Adapter_DbTableSelect($anSponsorTable->select()
                                ->order('datetime DESC'));

        $page = (isset($params['p'])) ? $params['p'] : 1;
        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }
    
    function editAction()
    {
        
        $this->_helper->auth->requireAdminPerm('sponsor-edit');
        $this->view->title = '�������������� ����������� ������';
        $params  = $this->getRequest()->getParams();

        $anSponsorTable = new SponsorTable();
        $sponsor = $anSponsorTable->find($params['id'])->current();
        if ( $this->getRequest()->isPost() ) {
            $sponsor->text    = $this->getRequest()->getPost('text');
            $sponsor->keyword        = $this->getRequest()->getPost('keyword');
            $sponsor->uri    = $this->getRequest()->getPost('uri');
            $sponsor->save();

            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);
        }

        $this->view->item = $sponsor;

    }
    
    function markPaidAction() {
        $this->_helper->auth->requireAdminPerm('sponsor-approve');
        
        $anSponsorTable = new SponsorTable;

        $item = $anSponsorTable->find($this->getRequest()->getParam('id'))->current();
        
        if ($item && !$item->is_paid) {
            $item->is_paid = 1;
            $item->save();
            if ($item->aff) {
                $anUsersTable = new UsersTable;
                $user = $anUsersTable->findByLogin($item->aff);
                if ($user) {
                    $anPaymentsTable = new PaymentsTable;

                    $payment = $anPaymentsTable->fetchNew();
                    $payment->type = PaymentsTable::TYPE_INTERNAL;
                    $payment->amount = 50;
                    $payment->datetime = date('Y-m-d H:i:s');
                    $payment->user_id = $user->user_id;
                    $payment->typology = PaymentsTable::TYPOLOGY_SPONSOR;
                    $payment->typology_id = $item->sponsor_id;
                    $payment->save();
                }
            }
        }
        header('Location: /admin/sponsor/');
        exit;
    }

    function deleteAction() {
        
        $this->_helper->auth->requireAdminPerm('sponsor-del');
        
        $anSponsorTable = new SponsorTable;

        $item = $anSponsorTable->find($this->getRequest()->getParam('id'))->current();

        if ($item) {
            $this->sendDeclineMail($item, $this->getRequest()->getParam('reason', '������� �� �������'));
            $item->delete();
        }
        header('Location: /admin/sponsor/');
        exit;
    }
    
    function sendAcceptMail($item) {
        $num  = str_pad($item->sponsor_id, 4, '0', STR_PAD_LEFT);
        
        $mail = new Site_Mail('sponsor-accept');
        $mail->setName($item->name);
        $mail->setNum($num);
        $mail->send($item->email);
    }
    
    function sendDeclineMail($item, $reason) {
        $mail = new Site_Mail('sponsor-decline');
        $mail->setName($item->name);
        $mail->setReason($reason);
        $mail->send($item->email);
    }

}