<?php

class Admin_AboutController extends Zend_Controller_Action {

    function indexAction() {

        $this->_helper->auth->requireAdminPerm('users');

        $this->view->title = '������ � ����';

        $anUsersTable = new UsersTable;
        $select = $anUsersTable->select()->where('about<>?', '')
                ->order('date DESC');

        $q = $this->getRequest()->getParam('q', '');
        if ($q) {
            $q = $select->getAdapter()->quote('%' . $q . '%');
            $select = $select->where("about LIKE {$q}");
        }

        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);

        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }
}