<?php

class Admin_ShopModController extends Zend_Controller_Action
{
    function indexAction()
    {
        
        $this->_helper->auth->requireAdminPerm('shop-mod');
        
      $params  = $this->getRequest()->getParams();
      $this->view->title = '������';

      $anShopArticleTable = new ShopArticleTable;
      
      if ( $this->getRequest()->isPost() ) {
          $this->_helper->auth->requireAdminPerm('shop-mod-approve');
          $post = $this->getRequest()->getPost();
          foreach($post['article_id'] as $article_id) {
              $article = $anShopArticleTable->find($article_id)->current();
              $article->is_approved = 1;
              $article->save();
          }
      }  
        
      $adapter = new Zend_Paginator_Adapter_DbTableSelect( $anShopArticleTable->select()
                                                                        ->where('is_approved=?', '0')
                                                                        ->order('date DESC') );
        
      $page = (isset($params['p'])) ? $params['p'] : 1;
      $paginator = new Zend_Paginator( $adapter );
      $paginator->setCurrentPageNumber($page);
      $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
      $paginator->setPageRange(10);
      $this->view->paginator = $paginator;    
    }

}