<?php

class Admin_ShopCatController extends Zend_Controller_Action
{
    function indexAction()
    {
        $this->_helper->auth->requireAdminPerm('shop-cat');
        
      $params  = $this->getRequest()->getParams();
      $this->view->title = '������� ��������';
      $anShopCatTable = new ShopCatTable();
      $this->view->cats = $anShopCatTable->fetchAll();

    }

    function editAction()
    {
        $this->_helper->auth->requireAdminPerm('shop-cat-edit');
        
        $this->view->title = '�������������� ������� ��������';
        $params  = $this->getRequest()->getParams();

        $anShopCatTable = new ShopCatTable();
        $cat = $anShopCatTable->find($params['id'])->current();
        if ( $this->getRequest()->isPost() ) {
            $cat->name        = $this->getRequest()->getPost('name');
            $cat->alias        = $this->getRequest()->getPost('alias');
            $cat->save();

            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);
        }

        $this->view->cat = $cat;

    }

    function newAction()
    {
        $this->_helper->auth->requireAdminPerm('shop-cat-edit');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������� ���������';


        if ( $this->getRequest()->isPost() ) {
            $post = $this->getRequest()->getPost();
            $anShopCatTable = new ShopCatTable();

            $cat['name'] = $post['name'];
            $cat['alias'] = $post['alias'];
            $anShopCatTable->insert($cat);

            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);
         }

    }

    function deleteAction()
    {
        $this->_helper->auth->requireAdminPerm('shop-cat-del');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������� ���������';

        $anShopCatTable = new ShopCatTable();

        if ( $this->getRequest()->isPost() ) {
            $post = $this->getRequest()->getPost();

            $cat = $anShopCatTable->find($this->getRequest()->getPost('cat_id'))->current();
            $cat->delete();

            header('Location: /admin/shop-cat/');
            exit;
        }

        $cat = $anShopCatTable->find($params['id'])->current();

        $this->view->cat = $cat;
    }

}