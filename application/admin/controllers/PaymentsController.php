<?php

class Admin_PaymentsController extends Zend_Controller_Action {

    function indexAction() {
        $this->_helper->auth->requireAdminPerm('pay');
        $this->view->title = '�������';
        $params = $this->getRequest()->getParams();


        $paymentsView = new PaymentsView;
        $select = $paymentsView->select()->order('datetime DESC');
        if (isset($params['filter_q']) && $params['filter_q'] != '') {
            $select = $select->where("u_login LIKE ? OR u_name_f LIKE ? OR u_name_l LIKE ?", '%' . $params['filter_q'] . '%', '%' . $params['filter_q'] . '%', '%' . $params['filter_q'] . '%');
        }
        
        if (isset($params['filter_type']) && $params['filter_type'] != '') {
            $select = $select->where("type=?",$params['filter_type']);
        }

        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);


        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }

}