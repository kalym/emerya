<?php

class Admin_SettingsController extends Zend_Controller_Action {

    function indexAction() {
        $this->_helper->auth->requireAdminPerm('settings');

        $settingsDescription = $this->getSection($this->getRequest()->getParam('section', 'misc'));


        $params = $this->getRequest()->getParams();
        $this->view->title = '���������';
        Zend_Loader::loadClass('SettingsTable');
        $sTable = new SettingsTable;

        if ($this->getRequest()->isPost()) {
            $this->_helper->auth->requireAdminPerm('settings-edit');

            $post = $this->getRequest()->getPost();
            $toUpdate = array_keys($settingsDescription);

            foreach ($toUpdate as $name) {
                $data['name'] = $name;
                $data['value'] = $post[$name];
                $sTable->replace($data);
            }

            $msg['content'] = '���������� ���������';
            $msg['code'] = 0;
            $this->view->messages = array($msg);
        }


        $sRowSet = $sTable->fetchAll();
        $settings = array();
        foreach ($sRowSet as $sRow) {
            $settings[$sRow->name] = $sRow->value;
        }

        $this->view->settings = $settings;
        $this->view->settingsDescription = $settingsDescription;
    }

    function getSection($section) {
        $desc = $this->getDesc();
        return $desc[$section];
    }

    function getDesc() {
        return array(
            'service' => array(
                'premium_price_pay' => array(
                    'title' => '[������� ����������] ��������� (�����)'
                ),
                'premium_price_rating' => array(
                    'title' => '[������� ����������] ��������� (�������)'
                ),
                'smile_price_pay' => array(
                    'title' => '[�������� � ����������] ��������� (�����)'
                ),
                'smile_price_rating' => array(
                    'title' => '[�������� � ����������] ��������� (�������)'
                ),
                'smile_period' => array(
                    'title' => '[�������� � ����������] ������������ (� ����)'
                ),
                'ball_price_pay' => array(
                    'title' => '[�������� ������ �� ������] ��������� (�����)'
                ),
                'ball_price_rating' => array(
                    'title' => '[�������� ������ �� ������] ��������� (�������)'
                ),
                'ball_period' => array(
                    'title' => '[�������� ������ �� ������] ������������ (� ����)'
                ),
                'stat_price_pay' => array(
                    'title' => '[�������� ���������� �����] ��������� (�����)'
                ),
                'stat_price_rating' => array(
                    'title' => '[�������� ���������� �����] ��������� (�������)'
                ),
                'stat_period' => array(
                    'title' => '[�������� ���������� �����] ������������ (� ����)'
                ),
                'security_price_pay' => array(
                    'title' => '[������ ��������] ��������� (�����)'
                ),
                'security_price_rating' => array(
                    'title' => '[������ ��������] ��������� (�������)'
                ),
                'security_period' => array(
                    'title' => '[������ ��������] ������������ (� ����)'
                ),
                'contact_price_pay' => array(
                    'title' => '[��������� � �������� ��� ���������] ��������� (�����)'
                ),
                'contact_price_rating' => array(
                    'title' => '[��������� � �������� ��� ���������] ��������� (�������)'
                ),
                'contact_period' => array(
                    'title' => '[��������� � �������� ��� ���������] ������������ (� ����)'
                ),
                'anonimus_price_pay' => array(
                    'title' => '[������] ��������� (�����)'
                ),
                'anonimus_price_rating' => array(
                    'title' => '[������] ��������� (�������)'
                ),
                'anonimus_period' => array(
                    'title' => '[������] ������������ (� ����)'
                ),
                'ignor_price_pay' => array(
                    'title' => '[������������� ������������] ��������� (�����)'
                ),
                'ignor_price_rating' => array(
                    'title' => '[������������� ������������] ��������� (�������)'
                ),
                'ignor_period' => array(
                    'title' => '[������������� ������������] ������������ (� ����)'
                ),
                'darball_price_pay' => array(
                    'title' => '[������ ����� ����� ������] ��������� (�����/�� ����)'
                ),
                'salebal_price_pay' => array(
                    'title' => '[������ �������] ��������� (�����/�� ����)'
                ),
                'snigenie_price_rating' => array(
                    'title' => '[�������� ������ ������] ��������� (�������/�� ����)'
                )
            ),
            'email' => array(
                'email_admin' => array(
                    'title' => 'E-mail ��������������'
                ),
                'email_from' => array(
                    'title' => 'E-mail ��� ��������� �����'
                ),
                'email_name' => array(
                    'title' => '��� ����������� ��� ��������� �����'
                ),
                'email_signature' => array(
                    'title' => '������� ��� ��������� �����',
                    'type' => 'textarea'
                ),
                'email_signature_html' => array(
                    'title' => '������� ��� ��������� ����� [HTML ������]',
                    'type' => 'textarea'
                ),
            ),
            'block' => array(
                'block_top' => array(
                    'title' => '������� ���� (������)',
                    'type' => 'textarea'
                ),
                'block_bottom' => array(
                    'title' => '������ ���� (�� ������ �������)',
                    'type' => 'textarea'
                ),
                'block_article_bottom' => array(
                    'title' => '���� ��� �������',
                    'type' => 'textarea'
                ),
                'block_article_bottom_2' => array(
                    'title' => '���� ��� ������� [2]',
                    'type' => 'textarea'
                ),
                'block_right' => array(
                    'title' => '������ ���� (������)',
                    'type' => 'textarea'
                ),
                'block_right_top' => array(
                    'title' => '������ ������� ���� (������)',
                    'type' => 'textarea'
                ),
                'block_image' => array(
                    'title' => '���� ��� ��������� � ������',
                    'type' => 'textarea'
                ),
                'block_qna_item' => array(
                    'title' => '���� �� �������� ������� (������� � ������)',
                    'type' => 'textarea'
                ),
                'block_blog_item' => array(
                    'title' => '���� �� �������� ����� (�����)',
                    'type' => 'textarea'
                ),
                'block_index_middle' => array(
                    'title' => '���� �� ������� (����� ������ � ������)',
                    'type' => 'textarea'
                ),
                'block_cat_middle' => array(
                    'title' => '���� � ������� �������� (����� ������ � ������)',
                    'type' => 'textarea'
                ),
                'block_qa_middle' => array(
                    'title' => '���� � �������� (����� ������ � ������)',
                    'type' => 'textarea'
                ),
                'block_popular_middle' => array(
                    'title' => '���� � ���������� (����� ������ � ������)',
                    'type' => 'textarea'
                ),
                'block_blog_middle' => array(
                    'title' => '���� � ������ (����� ������ � ������)',
                    'type' => 'textarea'
                ),
                'block_copyright' => array(
                    'title' => '���� Copyrights',
                    'type' => 'textarea'
                ),
            ),
            'meta' => array(
                'title' => array(
                    'title' => '����� �� ��������� (title)'
                ),
                'keywords' => array(
                    'title' => '�������� ����� �� ��������� (keywords)'
                ),
                'description' => array(
                    'title' => '�������� �� ��������� (description)'
                ),
            ),
            'wm' => array(
                'wmr' => array(
                    'title' => 'WEBMONEY - WMR'
                ),
                'wm-secret-key' => array(
                    'title' => 'WEBMONEY - Secret Key'
                ),
            ),
            'rating' => array(
                'rating_approved_article' => array(
                    'title' => '�������� ������ �� ���������� ���������'
                ),
                'rating_approved_article_at' => array(
                    'title' => '�������� ������ �� ������������� ���������'
                ),
                'rating_comment_for_article' => array(
                    'title' => '����������� �� ����������� ������������ � ������'
                ),
                'rating_click_article' => array(
                    'title' => '�������� ������'
                ),
                'rating_rate_article' => array(
                    'title' => '������ ������ (+/-)'
                ),
            ),
            'pagination' => array (
                'back_item_perpage' => array(
                    'title' => '���������� ������� �� �������� (���������������� �����)'
                ),
                'sidebar_comments_count' => array(
                    'title' => '���������� ������������ � ������� ������'
                ),
                'sidebar_popular_count' => array(
                    'title' => '���������� ���������� ������ � ������� ������'
                ),
                'sidebar_drafts_count' => array(
                    'title' => '���������� ���������� � ������� ������'
                ),
                'popular_item_perpage' => array(
                    'title' => '���������� ���������� ������ �� ��������'
                ),
                'news_item_perpage' => array(
                    'title' => '���������� �������� �� ��������'
                ),
                'posts_item_perpage' => array(
                    'title' => '���������� ������ � ����� �� ��������'
                ),
                'front_user_posts_perpage' => array(
                    'title' => '���������� ������ � ����� �� �������� ������������'
                ),
                'users_item_perpage' => array(
                    'title' => '���������� ������ �� ��������'
                ),
                'feed_item_perpage' => array(
                    'title' => '���������� ��������� � ����� ��������'
                ),
                'index_article_perpage' => array(
                    'title' => '���������� ������ �� �������� (������� ��������)'
                ),
                'cat_article_perpage' => array(
                    'title' => '���������� ������ �� �������� (�������� ���������)'
                ),
                'draft_article_perpage' => array(
                    'title' => '���������� ������ �� �������� (���������)'
                ),
            ),
            'ppv' => array(
                'ppv_price_RU' => array(
                    'title' => '���� �� ���������� �������� (������), ���'
                ),
                'ppv_price_UA' => array(
                    'title' => '���� �� ���������� �������� (�������), ���'
                ),
                'ppv_price_BY' => array(
                    'title' => '���� �� ���������� �������� (��������), ���'
                ),
                'ppv_top' => array(
                    'title' => '������������ �������� ���������� (Top Author), ����'
                ),
                'ppv_mas' => array(
                    'title' => '������������ �������� ���������� (Master), ����'
                ),
                'ppv_pro' => array(
                    'title' => '������������ �������� ���������� (Professional), ����'
                ),
                'ppv_spe' => array(
                    'title' => '������������ �������� ���������� (Specialist), ����'
                ),
                'ppv_deb' => array(
                    'title' => '������������ �������� ���������� (Debutant), ����'
                ),
                'ppv_tra' => array(
                    'title' => '������������ �������� ���������� (Trainee), ����'
                ),
                'ppv_display_stat_days' => array(
                    'title' => '����� ���-�� ���� ���������� ��������� ���������� ����� ��������� �������� ����������'
                ),
                'ppv_truncate' => array(
                    'title' => '����� ����� ���-�� ���� ������� ��������� ���������� ����� ��������� �� ��������'
                ),
            ),
            'ppa' => array(
                'ppa_top' => array(
                    'title' => '���� (Top Author), ���.'
                ),
                'ppa_mas' => array(
                    'title' => '���� (Master), ���.'
                ),
                'ppa_pro' => array(
                    'title' => '���� (Professional), ���.'
                ),
                'ppa_spe' => array(
                    'title' => '���� (Specialist), ���.'
                ),
                'ppa_deb' => array(
                    'title' => '���� (Debutant), ���.'
                ),
                'ppa_tra' => array(
                    'title' => '���� (Trainee), ���.'
                )
            ),
            'misc' => array(
                'article_commission' => array(
                    'title' => '�������� �� ������� ������ (%)'
                ),
                'payout_date' => array(
                    'title' => '���� ��������� ������',
                    'type' => 'date'
                ),
                'at_only' => array(
                    'title' => '��������� ������������ ��������� ��� ������ (������������ ������ ������������ ���������)',
                    'type' => 'checkbox'
                ),
                'book_delay' => array(
                    'title' => '������ ������������ (� �����)'
                ),
                'article_limit' => array(
                    'title' => '����� ������ � ����� (0 - ��� �����������)'
                ),
                'article_limit_increase' => array(
                    'title' => '���������� ����� ����������� ��� ���������� ������ ������ �� 1'
                ),
                'book_limit_increase' => array(
                    'title' => '���������� ����� ����������� ��� ���������� ���������� ������������ ��������������� ������ �� 1'
                ),
                'qna_limit' => array(
                    'title' => '����� �������� � ������ ������-����� (0 - ��� �����������)'
                ),
                'comment_min_length_to_rate' => array(
                  'title' => '����������� ����� ����������� ��� ������ ������ (0 - ��� �����������)'  
                ),
                'cron_last_run' => array(
                    'title' => '��������� ������ ����� (������ ��������)',
                    'attributes' => array('readonly' => 'readonly')
            ))
        );
    }

}