<?php

class Admin_UsersController extends Zend_Controller_Action
{
    function indexAction()
    {
        $this->_helper->auth->requireAdminPerm('users');
        
      $params  = $this->getRequest()->getParams();
      $type = $this->getRequest()->getParam('type', User::TYPE_WRITER);
      
      if ($type == User::TYPE_WRITER) {
        $this->view->title = '������';
      } elseif ($type == User::TYPE_READER) {
         $this->view->title = '��������'; 
      } else {
          $this->view->title = '������������';
      }

      $usersTable = new UsersTable;
      
      $row = $usersTable->fetchRow(
              $usersTable->select()
                  ->from('users', array(
                      'all'=> new Zend_Db_Expr('COUNT(user_id)'),
                      'lock'=> new Zend_Db_Expr('COUNT(IF(is_locked, user_id, NULL))')
                      ))
              ->where('type=?', $type)
          );
      
      $this->view->all = $row->all;
      $this->view->lock = $row->lock;
      
        $select = $usersTable->select()->where('type=?', $type)->order('date DESC');
      if (isset($params['q']) && $params['q']!='') {
          $select = $select->where("email LIKE ? OR login LIKE ? OR wmr LIKE ? OR name_f LIKE ? OR name_l LIKE ?", 
                  '%' . $params['q'] . '%', '%' . $params['q'] . '%', 
                  '%' . $params['q'] . '%', '%' . $params['q'] . '%',
                  '%' . $params['q'] . '%');
      }
        
      $adapter = new Zend_Paginator_Adapter_DbTableSelect( $select );
        

      $paginator = new Zend_Paginator( $adapter );
      $paginator->setCurrentPageNumber(
              $this->getRequest()->getParam('p', 1)
              );
      $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
      $paginator->setPageRange(10);
      $this->view->paginator = $paginator;
        
    }

    function loginAction()
    {
        $this->_helper->auth->requireAdminPerm('users-login');
        
        $anUsersTable = new UsersTable;
        $user = $anUsersTable->find($this->getRequest()->getParam('id'))->current();

        $adapter = new Site_Auth_Adapter_Null($user->login, '');
        Zend_Auth::getInstance()->authenticate($adapter);

        $this->_redirect('/');
    }
    
    function editAction()
    {
        $this->_helper->auth->requireAdminPerm('users-edit');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������������� ������������';
        
        $usersTable = new UsersTable();
        $user = $usersTable->find($params['id'])->current();
        if ( $this->getRequest()->isPost() ) {
            $post = $this->getRequest()->getPost();
            
            if ($user->email != $post['email']) {
                $anCSTable = new CSTable();
                $anCSTable->update(array(
                    'email' => $post['email']
                ), 'user_id=' . $user->user_id);
            }
            
            $user->email  = $post['email'];
            $user->login = $post['login'];
            $user->about = $post['about'];
            $user->wmr = $post['wmr'];
            $user->web = $post['web'];
            $user->icq  = $post['icq'];
            $user->skype = $post['skype'];
            $user->name_f = $post['name_f'];
            $user->name_l = $post['name_l'];
            $user->country = $post['country'];
            $user->city = $post['city'];
            $user->rating = $post['rating'];
            $user->karma_correction = $post['karma_correction'];
            if (isset($post['lock_reason'])) {
                $user->lock_reason = $post['lock_reason'];
            }

            if ($user->country) {
                list($user->x,$user->y) = YMap::getInstance()->getCoordinate(YMap::getInstance()->getAddrLine($user));
            } else {
                $user->x = 0;
                $user->y = 0;
            }
            
            $user->save();
            
            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);
            
        }
        
        $this->view->user = $user;
    }
    
    function mailAction()
    {
        $this->_helper->auth->requireAdminPerm('users-mail');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '��������� ��������� ������������';
        
        $anFeedTable = new FeedTable();
        
        $usersTable = new UsersTable();
        $user = $usersTable->find($params['id'])->current();
        if ( $this->getRequest()->isPost() ) {
            $post = $this->getRequest()->getPost();
            
            $feed = $anFeedTable->fetchNew();
            $feed->user_id = $user->user_id;
            $feed->datetime = date('Y-m-d H:i:s');
            $feed->is_new = 1;
            $feed->type = FeedTable::TYPE_ADMIN;
            $feed->content = $post['content'];
            $feed->save();
            
            $msg['content'] = '��������� ����������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);
            
        }
        
        $this->view->user = $user;
        
        $adapter = new Zend_Paginator_Adapter_DbTableSelect( $anFeedTable->select()
                ->where('type=?', FeedTable::TYPE_ADMIN)
                ->where('user_id=?', $user->user_id)
                ->order('datetime DESC'));
        

          $paginator = new Zend_Paginator( $adapter );
          $paginator->setCurrentPageNumber(
                  $this->getRequest()->getParam('p', 1)
                  );
          $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
          $paginator->setPageRange(10);
          $this->view->paginator = $paginator;
    }
    
    function deleteAction()
    {
        $this->_helper->auth->requireAdminPerm('users-del');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������� ������������';
        
        $usersTable = new UsersTable();
        
        if ( $this->getRequest()->isPost() ) {
            
            $user = $usersTable->find($this->getRequest()->getPost('user_id'))->current();
            
            $user->delete();
            $back = $this->getRequest()->getParam('b', '/admin/users/');
            header('Location: ' . $back);
            exit;

        }
        
        $user = $usersTable->find($params['id'])->current();
        $this->view->user = $user;
    }

    function lockAction()
    {
        $this->_helper->auth->requireAdminPerm('users-edit');
        
        $usersTable = new UsersTable();
        
        $user = $usersTable->find($this->getRequest()->getParam('id'))->current();
        $user->is_locked = $user->is_locked ? 0 : 1;
        $user->lock_reason = $this->getRequest()->getParam('reason', '');
        $user->save();

        $back = $this->getRequest()->getParam('b', '/admin/users/');
        header('Location: ' . $back);
        exit;
    }
    
    function premiumAction()
    {
        $this->_helper->auth->requireAdminPerm('users-edit');
        
        $usersTable = new UsersTable();
        
        $user = $usersTable->find($this->getRequest()->getParam('id'))->current();
        if ($user->isPremium()) {
            $user->premium_since = null;
        } else {
            $user->premium_since = date('Y-m-d H:i:s');
        }
        $user->save();

        $back = $this->getRequest()->getParam('b', '/admin/users/');
        header('Location: ' . $back);
        exit;
    }

}