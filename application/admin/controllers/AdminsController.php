<?php

class Admin_AdminsController extends Zend_Controller_Action {

    function preDispatch() {
        $this->_helper->auth->requireAdminPerm(Admin::PERM_ROOT);
    }
    
    function indexAction() {
        $params = $this->getRequest()->getParams();
        $this->view->title = '��������������';
        $adminsTable = new AdminsTable();


        $select = $adminsTable->select();


        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);

        $page = (isset($params['p'])) ? $params['p'] : 1;
        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(5);
        $paginator->setPageRange(8);
        $this->view->paginator = $paginator;
    }

    function editAction() {
        $params = $this->getRequest()->getParams();
        $this->view->title = '�������������� ������������';
        Zend_Loader::loadClass('AdminsTable');
        $this->view->acl = $this->getResourceList();

        $adminsTable = new AdminsTable();
        $user = $adminsTable->find($params['id'])->current();
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();

            $user->login = $post['login'];
            $user->is_root = $post['is_root'];
            $user->setAcl($post['acl']);

            if ($post['pass']) {
                $user->passhash = md5($post['pass']);
            }

            if ($post['pass'] && $post['pass'] != $post['pass1']) {
                $msg['content'] = '������ �� ���������';
                $msg['code'] = 1;
                $this->view->messages = array($msg);
            } else {
                $user->save();
                $msg['content'] = '���������� ���������';
                $msg['code'] = 0;
                $this->view->messages = array($msg);
            }
        }

        $this->view->user = $user;
    }

    function newAction() {
        $params = $this->getRequest()->getParams();
        $this->view->title = '���������� ��������������';
        $this->view->acl = $this->getResourceList();

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            Zend_Loader::loadClass('AdminsTable');
            $adminsTable = new AdminsTable();
            $admin = $adminsTable->fetchNew();

            $admin->login = $post['login'];
            $admin->passhash = md5($post['pass']);
            $admin->is_root = $post['is_root'];
            $admin->setAcl($post['acl']);

            if (!$post['pass']) {
                $msg['content'] = '������� ������';
                $msg['code'] = 1;
                $this->view->messages = array($msg);
            } elseif ($post['pass'] != $post['pass1']) {
                $msg['content'] = '������ �� ���������';
                $msg['code'] = 1;
                $this->view->messages = array($msg);
            } else {
                $admin->save();
                $msg['content'] = '���������� ���������';
                $msg['code'] = 0;
                $this->view->messages = array($msg);
            }
        }
    }

    function deleteAction() {
        $params = $this->getRequest()->getParams();
        $this->view->title = '�������� ������������';

        $adminsTable = new AdminsTable();

        if ($this->getRequest()->isPost()) {
            $user = $adminsTable->find($this->getRequest()->getPost('id'))->current();
            $user->delete();
            header('Location: /admin/admins/');
            exit;
        }

        $user = $adminsTable->find($params['id'])->current();
        $this->view->user = $user;
    }

    function getResourceList() {
        $acl = array(
            'quick-stat' => '������� ����: ���������� �� ��������� ����� (�����������)',
            'stat-share' => '������� ����: ���������� ������� ������ ����� (�����������)',
            'last-user' => '������� ����: ��������� ����������� ������������ (�����������)',
            'cms' => '����: �������� (��������)',
            'cms-edit' => '����: �������� (��������������)',
            'cms-del' => '����: �������� (��������)',
            'news' => '����: ������� (��������)',
            'news-edit' => '����: ������� (��������������)',
            'news-del' => '����: ������� (��������)',
            'qa' => '����: ������� ������������� (��������)',
            'qa-edit' => '����: ������� ������������� (��������������)',
            'qa-del' => '����: ������� ������������� (��������)',
            'adv' => '����: ��������� ������� (��������)',
            'adv-edit' => '����: ��������� ������� (��������������)',
            'adv-stat' => '����: ��������� ������� (����������)',
            'adv-del' => '����: ��������� ������� (��������)',
            'users' => '������������: ������������ (��������)',
            'users-login' => '������������: ������������ (���� �� ����� ������������)',
            'users-edit' => '������������: ������������ (��������������)',
            'users-del' => '������������: ������������ (��������)',
            'users-mail' => '������������: ������������ (�������� ���������)',
            'msg' => '������������: ������ ��������� (��������)',
            'msg-del' => '������������: ������ ��������� (��������)',
            'photo' => '������������: ���������� (��������)',
            'photo-del' => '������������: ���������� (��������)',
            'job' => '������������: ����� ������� (��������)',
            'job-edit' => '������������: ����� ������� (��������������)',
            'job-del' => '������������: ����� ������� (��������)',
            'comments' => '������������: ����������� (��������)',
            'comments-edit' => '������������: ����������� (��������������)',
            'comments-del' => '������������: ����������� (��������)',
            'mcomments' => '������������: ��������� ������������ (�����������)',
            'shop-cat' => '������� ������: ������� (��������)',
            'shop-cat-edit' => '������� ������: ������� (��������������)',
            'shop-cat-del' => '������� ������: ������� (��������)',
            'shop-article' => '������� ������: ������ (��������)',
            'shop-article-edit' => '������� ������: ������ (��������������)',
            'shop-article-del' => '������� ������: ������ (��������)',
            'shop-mod' => '������� ������: ��������� (��������)',
            'shop-mod-approve' => '������� ������: ��������� (���������)',
            'cats' => '������: ������� �������� (��������)',
            'cats-edit' => '������: ������� �������� (��������������)',
            'cats-del' => '������: ������� �������� (��������)',
            'articles' => '������: ������ (��������)',
            'articles-edit' => '������: ������ (��������������)',
            'articles-del' => '������: ������ (��������)',
            'articles-link' => '������: ������ (������������)',
            'at' => '������: ��������� ��� ������ (��������)',
            'at-edit' => '������: ��������� ��� ������ (��������������)',
            'at-del' => '������: ��������� ��� ������ (��������)',
            'book' => '������: ������������ (��������)',
            'book-del' => '������: ������������ (��������)',
            'sponsor' => '������: ����������� (��������)',
            'sponsor-approve' => '������: ����������� (���������)',
            'sponsor-edit' => '������: ����������� (��������������)',
            'sponsor-del' => '������: ����������� (��������)',
            'marticles' => '������: ��������� (��������)',
            'marticles-approve' => '������: ��������� (���������)',
            'blogs' => '�����: ����� (��������)',
            'blogs-edit' => '�����: ����� (��������������)',
            'blogs-del' => '�����: ����� (��������)',
            'mblogs' => '�����: ��������� (�����������)',
            'cat-qna' => '������� � ������: ������� (��������)',
            'cat-qna-edit' => '������� � ������: ������� (��������������)',
            'cat-qna-del' => '������� � ������: ������� (��������)',
            'qna' => '������� � ������: ������� � ������ (��������)',
            'qna-edit' => '������� � ������: ������� � ������ (��������������)',
            'qna-del' => '������� � ������: ������� � ������ (��������)',
            'mqna' => '������� � ������: ��������� (�����������)',
            'subscribers' => '�������� ������: ���������� (��������)',
            'subscribers-edit' => '�������� ������: ���������� (��������������)',
            'subscribers-del' => '�������� ������: ���������� (��������)',
            'send-report' => '�������� ������: ��������� ������ (�����������)',
            'pay' => '�������: ������ (��������)',
            'pay-edit' => '�������: ������ (��������������)',
            'stat-query' => '����������: ��������� ������� (�����������)',
            'stat-login' => '����������: ����� �� ���� (�����������)',
            'settings' => '���������: ��������� (��������)',
            'settings-edit' => '���������: ��������� (��������������)',
            'template' => '���������: ������� ����� (��������)',
            'template-edit' => '���������: ������� ����� (��������������)',
            'banners' => '���������: ������� (��������)',
            'banners-edit' => '���������: ������� (��������������)',
            'banners-del' => '���������: ������� (��������)',
            'broadcast' => '�����������: �������� (�����������)',
            'ban' => '�����������: ��� (�����������)'
        );
        
        $res = array();
        
        foreach ($acl as $perm => $desc) {
            preg_match('/(.*):\s(.*)\s\((.*)\)/', $desc, $match);
            $res[$match[1]][$match[2]][$perm] = $match[3];
        }
        return $res;
    }

}