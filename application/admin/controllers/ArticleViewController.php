<?php

class Admin_ArticleViewController extends Zend_Controller_Action {

    function indexAction() {

        $this->_helper->auth->requireAdminPerm('article-view');

        $this->view->title = '���������� ���������� ������';

        $anArticlesTable = new ArticlesTable();

        $price_ru = Site_Settings::get('ppv_price_RU');
        $price_ua = Site_Settings::get('ppv_price_UA');
        $price_by = Site_Settings::get('ppv_price_BY');
        
        $select = $anArticlesTable->select()
                ->from('articles')
                ->setIntegrityCheck(false)
                ->joinLeft('article_view', 'articles.article_id=article_view.article_id', array(
                    'view_cnt_unique' => new Zend_Db_Expr('COUNT(IF(article_view.is_unique, view_id, NULL))'),
                    'view_cnt' => new Zend_Db_Expr('COUNT(view_id)'),
                    'income' => new Zend_Db_Expr("SUM(IF(article_view.is_unique, CASE
                        WHEN article_view.country = 'RU' THEN $price_ru
                        WHEN article_view.country = 'UA' THEN $price_ua
                        WHEN article_view.country =  'BY' THEN $price_by
                    END, 0))")
                ))
                ->where('stat_begin IS NOT NULL')
                ->group('articles.article_id')
                ->order('date DESC');

        $filter_q = $this->getRequest()->getParam('q', '');
        if ($filter_q) {
            $filter_q = $select->getAdapter()->quote('%' . $filter_q . '%');
            $select = $select->where("name LIKE {$filter_q}");
        }


        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);

        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }
    
    function statAction() 
    {
        $this->_helper->auth->requireAdminPerm('article-view-stat');
        $this->view->title = '����������';
        
        $id = $this->getRequest()->getParam('id');
        $anArticleTable = new ArticlesTable();
        $item = $anArticleTable->find($id)->current();
        $this->view->item = $item;
        
        $this->view->title = '����������: ' . $item->name;
        
        
        $anArticleViewTable = new ArticleViewTable;
        $select = $anArticleViewTable->select()->from('article_view', array(
                'dat' => new Zend_Db_Expr('DATE(dattm)'),
                'view'=> new Zend_Db_Expr('COUNT(*)'),
                'unique'=> new Zend_Db_Expr('COUNT(IF(is_unique, view_id, NULL))'),
                'RU'=> new Zend_Db_Expr("COUNT(IF(is_unique AND country='RU', view_id, NULL))"),
                'UA'=> new Zend_Db_Expr("COUNT(IF(is_unique AND country='UA', view_id, NULL))"),
                'BY'=> new Zend_Db_Expr("COUNT(IF(is_unique AND country='BY', view_id, NULL))")
            ))
                    ->group(new Zend_Db_Expr('DATE(dattm)'))
                    ->where('article_id=?', $id);
        
        $statRow = $anArticleViewTable->fetchAll($select);
        
        $view = array();
        foreach ($statRow  as $row) {
           $view[$row->dat]['view'] = $row->view; 
           $view[$row->dat]['unique'] = $row->unique;
           $view[$row->dat]['RU'] = $row->RU;
           $view[$row->dat]['UA'] = $row->UA;
           $view[$row->dat]['BY'] = $row->BY;
        }
        
        $dat = array();
        $tm = strtotime($item->stat_begin);
        do {
            $d = date('Y-m-d', $tm);
            $dat[] = $d;
            $tm += 3600 * 24;
        } while ($d < date('Y-m-d', strtotime($item->stat_expire)));

        $stat = array();
        foreach ($dat as $d)
           $stat[$d] = array (
               'view' => isset($view[$d]['view']) ? $view[$d]['view'] : 0,
               'unique' => isset($view[$d]['unique']) ? $view[$d]['unique'] : 0,
               'RU' => isset($view[$d]['RU']) ? $view[$d]['RU'] : 0,
               'UA' => isset($view[$d]['UA']) ? $view[$d]['UA'] : 0,
               'BY' => isset($view[$d]['BY']) ? $view[$d]['BY'] : 0,
           );
        
        $this->view->stat = $stat;
        
               ///
        
        $price_ru = Site_Settings::get('ppv_price_RU');
        $price_ua = Site_Settings::get('ppv_price_UA');
        $price_by = Site_Settings::get('ppv_price_BY');
        
        $statement = "SUM(IF(article_view.is_unique, IF(article_view.country = '%s', %s, 0), 0))";
        
        $select = $anArticleViewTable->select()->from('article_view', array(
                'dat' => new Zend_Db_Expr('DATE(dattm)'),
                'RU'=> new Zend_Db_Expr(sprintf($statement, 'RU', $price_ru)),
                'UA' => new Zend_Db_Expr(sprintf($statement, 'UA', $price_ua)),
                'BY' => new Zend_Db_Expr(sprintf($statement, 'BY', $price_by))))
                    ->group(new Zend_Db_Expr('DATE(dattm)'))
                    ->where('article_id=?', $item->article_id);
        
        $statRow = $anArticleViewTable->fetchAll($select);
        
        $income = array();
        foreach ($statRow  as $row) {
           $income[$row->dat]['RU'] = $row->RU; 
           $income[$row->dat]['UA'] = $row->UA;
           $income[$row->dat]['BY'] = $row->BY;
           $income[$row->dat]['income'] = str_replace(',', '.', (float)$row->RU + (float)$row->UA + (float)$row->BY);
        }
        
        $stat_income = array();
        foreach ($dat as $d)
           $stat_income[$d] = array (
               'RU' => isset($income[$d]['RU']) ? $income[$d]['RU'] : 0,
               'UA' => isset($income[$d]['UA']) ? $income[$d]['UA'] : 0,
               'BY' => isset($income[$d]['BY']) ? $income[$d]['BY'] : 0,
               'income' => isset($income[$d]['income']) ? $income[$d]['income'] : 0
           );

        $this->view->stat_income = $stat_income;
        
    }
    
    function stopAction() {

        $this->_helper->auth->requireAdminPerm('article-view-stop');

        $params = $this->getRequest()->getParams();
        $this->view->title = '��������� ����������';

        $anArticlesTable = new ArticlesTable();

        if ($this->getRequest()->isPost()) {
            $article = $anArticlesTable->find($this->getRequest()->getPost('article_id'))->current();
            $anArticleViewTable = new ArticleViewTable;
            $anArticleViewTable->stop($article, $params['add_payment']);
            header('Location: /admin/article-view/');
            exit;
        }

        $article = $anArticlesTable->find($params['id'])->current();
        $this->view->article = $article;
    }
}