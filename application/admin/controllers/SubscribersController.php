<?php

class Admin_SubscribersController extends Zend_Controller_Action
{
    function indexAction()
    {
        $this->_helper->auth->requireAdminPerm('subscribers');
        
        
      $params  = $this->getRequest()->getParams();
      $this->view->title = '����������';

      $subscribersTable = new SubscribersTable;
      
      $row = $subscribersTable->fetchRow(
              $subscribersTable->select()
                  ->from('subscribers', array(
                      'all'=> new Zend_Db_Expr('COUNT(subscriber_id)'),
                      'verified'=> new Zend_Db_Expr('COUNT(IF(is_verified, subscriber_id, NULL))')
                      ))
          );
      
      $this->view->all = $row->all;
      $this->view->verified = $row->verified;
        
      if (isset($params['q']) && $params['q']!='') {
          $select = $subscribersTable->select()->where("email LIKE ?", '%' . $params['q'] . '%')
                 ->order('datetime DESC');
      } else {
          $select = $subscribersTable->select()->order('datetime DESC');
      }
      
      
      if ( $this->getRequest()->isPost() ) {
          $item = $this->getRequest()->getPost();
          foreach($item['item_id'] as $item_id) {
              $item = $subscribersTable->find($item_id)->current();
              $item->is_verified = 1;
              $item->save();
          }
      }
        
      $adapter = new Zend_Paginator_Adapter_DbTableSelect( $select );
        

      $paginator = new Zend_Paginator( $adapter );
      $paginator->setCurrentPageNumber(
              $this->getRequest()->getParam('p', 1)
              );
      $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
      $paginator->setPageRange(10);
      $this->view->paginator = $paginator;
        
    }
  
    function deleteAction()
    {
        $this->_helper->auth->requireAdminPerm('subscribers-del');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������� ����������';
        
        $subscribersTable = new SubscribersTable();
        
        if ( $this->getRequest()->isPost() ) {
            
            $sub = $subscribersTable->find($this->getRequest()->getPost('subscriber_id'))->current();
            
            $sub->delete();
            header('Location: /admin/subscribers/');
            exit;

        }
        
        $item = $subscribersTable->find($params['id'])->current();
        $this->view->item = $item;
    }
    
     function newAction()
    {
        $this->_helper->auth->requireAdminPerm('subscribers-edit');
        $params  = $this->getRequest()->getParams();
        $this->view->title = '���������� ����������';


        if ( $this->getRequest()->isPost() ) {

            $post = $this->getRequest()->getPost();
            $subscribersTable = new SubscribersTable();

            $subscribersTable->insert(array(
                'email' => $post['email'],
                'datetime' => date('Y-m-d H:i:s'),
                'is_verified' => 1
            ));

            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);
         }

    }
}