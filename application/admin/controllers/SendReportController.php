<?php

class Admin_SendReportController extends Zend_Controller_Action {

    function indexAction() {
        
        $this->_helper->auth->requireAdminPerm('send-report');
        
        $this->view->title = '��������� ������';

        if ($this->getRequest()->isPost()) {
            $settings = Zend_Registry::getInstance()->get('settings');
            $config = Zend_Registry::getInstance()->get('config');

            $date = $this->view->date(null, sprintf("%s %s %s", 
                            Zend_Date::DAY_SHORT, 
                            Zend_Date::MONTH_NAME, 
                            Zend_Date::YEAR)) . '  �.';
            $weekday = $this->view->date(null, Zend_Date::WEEKDAY);
            
            
            $html = $this->getHtml();
            $popularList = $this->getPopularHtml();
             $subscribers = array();
            if ($this->getRequest()->getParam('type') == 'broadcast') {
                $anSubscribersTable = new SubscribersTable;
                $items = $anSubscribersTable->fetchAll(
                        $anSubscribersTable->select()->where('is_verified=1')
                    );
                foreach ($items as $item) {
                    $subscribers[] = array(
                        'email' => $item->email,
                        'sc' => $item->getSC()
                    );
                }
                
            } else {
                $subscribers[] = array(
                    'email' => $this->getRequest()->getParam('email'),
                    'sc' => 'TEST'
                );
            }
            
            foreach ($subscribers as $sub) {
                $et = new Site_Mail('broadcast');
                $et->setDate($date);
                $et->setWeekday($weekday);
                $et->setUnsubscribeLink('http://' . $config->site->domain . '/subscribe/unsubscribe?sc=' . $sub['sc']);
                $et->setArticleList($html);
                $et->setPopularList($popularList);
                try {
                    $et->send($sub['email']);
                } catch (Exception $e) {
                    
                }
            }

            $msg['content'] = '��������� ����������';
            $msg['code'] = 0;
            $this->view->messages = array($msg);
        }
    }
    
    protected function getPopularHtml() {
         $anPopularView = new PopularView();
        $articles = $anPopularView->fetchAll(
            $select = $anPopularView->select()
                ->order('next_sunday DESC')
                ->limit(1)
        );
        
        return $this->getArticleHtml($articles->current(), '#fcf5e3');;
    }
    
    protected function getHtml() {
        $res = array();
        
        $anArticlesView = new ArticlesView;    
        $articles = $anArticlesView->fetchAll(
                $anArticlesView->select()->where('is_approved=1')
                ->order('date DESC')
                ->limit(6)
                );
        
        foreach ($articles as $item) {
            $res[] = $this->getArticleHtml($item);
        }       
        return implode('<br />', $res);
    }
   
    protected function getArticleHtml($item, $color = '#f3f3f0') {
            $config = Zend_Registry::getInstance()->get('config');
            $domain = $config->site->domain;
            $url = $url = $this->view->url(array(
                $item->alias
            ), 'article', true);
            $cat_url = $this->view->url(array(
                'controller'=>'articles', 
                'id'=>$item->cat_id), 
                 'default', true);
            $user_url = $this->view->url(array(
                    $item->u_login), 
                    'user', true);
            
            $title = $this->view->escape($item->name);
            $user_name = $item->u_name;
            $cat_title = $this->view->escape($item->c_name);
            $preview = $item->preview2;
            
            $date = $this->view->date($item->date);
            
            $content = $this->view->strip(strip_tags($item->content));
            
            $comments_cnt = $this->view->commentsCount($item->article_id);
            
            $result = <<<CUT
<div style="font-size:12px;line-height:14px;zoom:1;overflow:hidden;">
    <div style="font:normal 12px/15px Tahoma, Geneva, sans-serif;zoom:1;overflow:hidden;margin:0 0 7px 0;">
        <div style="float:left;color:#000;margin:0 12px 0 0;background:#f5f0d9;">{$date}</div><a href="http://{$domain}{$cat_url}">{$cat_title}</a>
    </div>
    <div style=" height:20%;background: {$color};padding-right:12px;overflow: hidden;">
        <div style="margin: 0 12px 0 0;float: left;"><a href="http://{$domain}{$url}"><img src="http://{$domain}{$preview}" width="198" height="170" alt="{$title}" title="{$title}"></a></div>
        <h4 style="margin-top:1em;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:14px;line-height:14px;"><a href="http://{$domain}{$url}">{$title}</a></h4>
        <div style="height:100%;overflow:hidden;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:12px;line-height:16px;color:#6a6a6c;">{$content}</div>
    </div>
</div>
CUT;
        return $result;
    }
}
