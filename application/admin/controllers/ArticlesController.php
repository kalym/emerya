<?php

class Admin_ArticlesController extends Zend_Controller_Action
{
    protected $images = array();
    protected $bigImage = '';

    function indexAction()
    {

        $this->_helper->auth->requireAdminPerm('articles');

        $this->view->title = '������';

        $anArticlesTable = new ArticlesTable();
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            foreach ($post['item_id'] as $item_id) {
                $item = $anArticlesTable->find($item_id)->current();
                $user_id = $item->user_id;
                $item->delete();

                $anUsersTable = new UsersTable();
                $user = $anUsersTable->find($user_id)->current();
                $user->pub_approved_count = $user->countApprovedArticles();
                $user->save();
            }
        }

        $select = $anArticlesTable->select()->order('date DESC');
        $filter_type = $this->getRequest()->getParam('filter_type', 'all');
        switch ($filter_type) {
            case 'draft' :
                $select = $select->where('is_approved=?', 0);
                break;
            case 'approved' :
                $select = $select->where('is_approved=?', 1);
                break;
            case 'all' :
            default :
                $select = $select;
        }

        $filter_q = $this->getRequest()->getParam('filter_q', '');
        if ($filter_q) {
            $filter_q = $select->getAdapter()->quote('%' . $filter_q . '%');
            $select = $select->where("name LIKE {$filter_q}");
        }


        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);

        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }

    function deletepaymentAction()
    {
        $this->_helper->auth->requireAdminPerm('articles-edit');

        $anPaymentsTable = new PaymentsTable();
        $payment = $anPaymentsTable
            ->find($this->getRequest()->getParam('id'))
            ->current();

        $article_id = $payment->article_id;
        $payment->delete();

        $this->_redirect('/admin/articles/edit?id=' . $article_id);
    }

    function editAction()
    {

        $this->_helper->auth->requireAdminPerm('articles-edit');

        $params = $this->getRequest()->getParams();
        $this->view->title = '�������������� ������';

        $anArticlesTable = new ArticlesTable();
        $article = $anArticlesTable->find($params['id'])->current();

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();

            $prev_approved = $article->is_approved;

            $article->image = $post['image'];
            $article->preview = $post['preview'];
            $article->preview2 = $post['preview2'];
            $article->original = $post['original'];
            $article->widget_img = $post['widget_img'];
            $article->full_image = $post['full_image'];

            $uploadError = $this->uploadImage();
            if (!$uploadError) {
                $article->image = $this->getImage();
                $article->preview = $this->getPreview();
                $article->preview2 = $this->getPreview2();
                $article->original = $this->getOriginalImage();
                $article->widget_img = $this->getWidgetImage();
            }

            $uploadError = $this->uploadImageFull();
            if (!$uploadError) {
                $article->full_image = $this->bigImage;
            }

            $article->podcast = $post['podcast'];

            $article->name = $post['name'];
            $article->content = $post['content'];
            $article->isPremium = $post['isPremium'];
            $article->date = $post['date'];
            $article->is_approved = $post['is_approved'];
            $article->cat_id = $post['cat_id'];
            $article->comment = $post['comment'];
            $article->next_sunday = Article::getNextSunday($article->date);

            $article->adv = $post['adv'];

            $img_gallery = array();
            foreach ($post['img_preview'] as $k => $preview) {
                if (!$preview) continue;
                $img_gallery[] = array(
                    'preview' => $preview,
                    'orig' => $post['img_orig'][$k]
                );
            }

            $article->setImgGallery($img_gallery);

            $video_gallery = array();
            foreach ($post['video_preview'] as $k => $preview) {
                if (!$preview) continue;
                $video_gallery[] = array(
                    'preview' => $preview,
                    'thumbnail' => $post['video_thumbnail'][$k],
                    'video' => $post['video_video'][$k]
                );
            }

            $article->setVideoGallery($video_gallery);

            if (isset($post['rework'])) {
                $article->is_rework = 1;
                $article->is_approved = 0;

                $anUsersTable = new UsersTable();
                $user = $anUsersTable->find($article->user_id)->current();

                $registry = Zend_Registry::getInstance();
                $settings = $registry->get('settings');
                $config = $registry->get('config');

                $uri = 'http://' . $config->site->domain . $this->view->url(array(
                        $article->alias), 'article', true);

                $mail = new Site_Mail('magazine-rework');
                $mail->setArticle($article);
                $mail->setUri($uri);
                $mail->send($user->email);
            }
            $img = $this->view->getFirst($article->full_image, '/assets/img/stamp-holder.jpg');
            $article->ogimage = $this->view->ogImage($article->name, $article->article_id, 'article', substr($img, 1));
            $article->save();

            if ($post['amount']) {
                $anPaymentsTable = new PaymentsTable;

                $payment = $anPaymentsTable->fetchNew();
                $payment->type = PaymentsTable::TYPE_INTERNAL;
                $payment->amount = $post['amount'];
                $payment->datetime = date('Y-m-d H:i:s');
                $payment->user_id = $article->user_id;
                $payment->article_id = $article->article_id;
                $payment->typology_id = $article->article_id;
                $payment->typology = PaymentsTable::TYPOLOGY_ARTICLE;
                $payment->save();
            }

            $anUsersTable = new UsersTable();
            $user = $anUsersTable->find($article->user_id)->current();

            if (!$prev_approved && $article->is_approved) {
                $article->approve();
                $article->save();
                $user->rating += $article->at_id ?
                    Config::get('rating_approved_article_at', 0) :
                    Config::get('rating_approved_article', 0);
            }

            $user->pub_approved_count = $user->countApprovedArticles();
            $user->save();

            $msg['content'] = '���������� ���������';
            $msg['code'] = 0;
            $this->view->messages = array($msg);
        }

        $anCatsTable = new CatsTable();
        $this->view->catOptions = $anCatsTable->getOptions();
        $this->view->article = $article;


        $anArticleLinkView = new ArticleLinkView;
        $this->view->links = $anArticleLinkView->fetchAll($anArticleLinkView->select()->where('article_id=?', $article->article_id));
    }

    function deleteAction()
    {

        $this->_helper->auth->requireAdminPerm('articles-del');

        $params = $this->getRequest()->getParams();
        $this->view->title = '�������� ������';

        $anArticlesTable = new ArticlesTable();

        if ($this->getRequest()->isPost()) {
            $article = $anArticlesTable->find($this->getRequest()->getPost('article_id'))->current();
            $user_id = $article->user_id;
            $article->delete();

            $anUsersTable = new UsersTable();
            $user = $anUsersTable->find($user_id)->current();
            $user->pub_approved_count = $user->countApprovedArticles();
            $user->save();

            header('Location: /admin/articles/');
            exit;
        }

        $article = $anArticlesTable->find($params['id'])->current();
        $this->view->article = $article;
    }


    protected function uploadImage()
    {
        if ($this->getRequest()->isPost() &&
            isset($_FILES['image']['name']) &&
            $_FILES['image']['name']
        ) {
            $imUploader = new ImageUploader(Zend_Registry::getInstance()->get('root_dir'), 'image');

            try {
                $this->images = $imUploader->upload(array(
                    array(80, 80),
                    array(198, 170),
                    array(266, 229),
                    array(110, 110)), true);
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }

        return '';
    }

    protected function uploadImageFull()
    {
        if ($this->getRequest()->isPost() &&
            isset($_FILES['image-full']['name']) &&
            $_FILES['image-full']['name']
        ) {
            $imUploader = new ImageUploader(Zend_Registry::getInstance()->get('root_dir'), 'image-full');

            try {
                $urls = $imUploader->upload(array(
                    array(80, 80),
                    array(198, 170),
                    array(266, 229),
                    array(110, 110)), true);
                $this->bigImage = $urls[4];
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }

        return '';
    }

    protected function getPreview()
    {
        return $this->images[0];
    }

    protected function getPreview2()
    {
        return $this->images[1];
    }

    protected function getImage()
    {
        return $this->images[2];
    }

    protected function getWidgetImage()
    {
        return $this->images[3];
    }

    protected function getOriginalImage()
    {
        return $this->images[4];
    }
}