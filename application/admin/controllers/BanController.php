<?php

class Admin_BanController extends Zend_Controller_Action {

    function indexAction() {
        $this->_helper->auth->requireAdminPerm('ban');

        $params = $this->getRequest()->getParams();
        $this->view->title = '���';

        $anBanTable = new BanTable();

        $adapter = new Zend_Paginator_Adapter_DbTableSelect($anBanTable->select());

        $page = (isset($params['p'])) ? $params['p'] : 1;
        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(30);
        $paginator->setPageRange(8);
        $this->view->paginator = $paginator;
    }

    function editAction() {
        $this->_helper->auth->requireAdminPerm('ban');

        $params = $this->getRequest()->getParams();
        $this->view->title = '�������������� �������';

        $anBanTable = new BanTable();
        $this->view->typeOptions = $anBanTable->getTypeOptions();
        $ban = $anBanTable->find($params['id'])->current();

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();

            $ban->type = $post['type'];
            $ban->value = $post['value'];
            $ban->comment = $post['comment'];
           
            $ban->save();
            $msg['content'] = '���������� ���������';
            $msg['code'] = 0;
            $this->view->messages = array($msg);
        }

        $this->view->ban = $ban;
    }

    function newAction() {
        $this->_helper->auth->requireAdminPerm('ban');

        $params = $this->getRequest()->getParams();
        $this->view->title = '�������� �������';

        $anBanTable = new BanTable();
        $this->view->typeOptions = $anBanTable->getTypeOptions();

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();

            $ban['type'] = $post['type'];
            $ban['value'] = $post['value'];
            $ban['comment'] = $post['comment'];

            $anBanTable->insert($ban);
            $msg['content'] = '���������� ���������';
            $msg['code'] = 0;
            $this->view->messages = array($msg);
        }
    }

    function deleteAction() {
        $this->_helper->auth->requireAdminPerm('ban');

        $params = $this->getRequest()->getParams();
        $this->view->title = '�������� �������';

        $anBanTable = new BanTable();

        if ($this->getRequest()->isPost()) {
            $ban = $anBanTable->find($this->getRequest()->getPost('ban_id'))->current();
            $ban->delete();
            header('Location: /admin/ban/');
            exit;
        }

        $ban = $anBanTable->find($params['id'])->current();
        $this->view->ban = $ban;
    }

}