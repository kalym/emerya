<?php

class Admin_ShopArticleController extends Zend_Controller_Action {

    function indexAction() {

        $this->_helper->auth->requireAdminPerm('shop-article');

        $this->view->title = '������';

        $anShopArticleTable = new ShopArticleTable;
        
        $paid = $anShopArticleTable->fetchAll($anShopArticleTable->select()->where('owner_id>0'));
        
        $this->view->paid = $paid->count();
        
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            foreach ($post['item_id'] as $item_id) {
                $item = $anShopArticleTable->find($item_id)->current();
                $item->delete();
            }
        }

        $select = $anShopArticleTable->select()->order('date DESC');
        $filter_type = $this->getRequest()->getParam('filter_type', 'all');
        switch ($filter_type) {
            case 'draft' :
                $select = $select->where('is_approved=?', 0);
                break;
            case 'approved' :
                $select = $select->where('is_approved=?', 1);
                break;
            case 'all' :
            default :
                $select = $select;
        }

        $filter_q = $this->getRequest()->getParam('filter_q', '');
        if ($filter_q) {
            $filter_q = $select->getAdapter()->quote('%' . $filter_q . '%');
            $select = $select->where("name LIKE {$filter_q}");
        }


        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);

        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }

    function editAction() {

        $this->_helper->auth->requireAdminPerm('shop-article-edit');

        $params = $this->getRequest()->getParams();
        $this->view->title = '�������������� ������';

        $anShopArticleTable = new ShopArticleTable();
        $article = $anShopArticleTable->find($params['id'])->current();

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();

            $prev_approved = $article->is_approved;

            $article->image = $post['image'];
            $article->preview = $post['preview'];
            $article->original = $post['original'];

            $article->desc = $post['desc'];
            $article->price = $post['price'];
            $article->type = $post['type'];
            $article->name = $post['name'];
            $article->content = $post['content'];
            $article->date = $post['date'];
            $article->is_approved = $post['is_approved'];
            $article->cat_id = $post['cat_id'];
            $article->comment = $post['comment'];

            if (isset($post['rework'])) {
                $article->is_rework = 1;
                $article->is_approved = 0;

                $anUsersTable = new UsersTable();
                $user = $anUsersTable->find($article->user_id)->current();

                $registry = Zend_Registry::getInstance();
                $settings = $registry->get('settings');
                $config = $registry->get('config');

                $uri = 'http://' . $config->site->domain . '/shop/add?id=' . $article->article_id;
    
                $mail = new Site_Mail('shop-rework');
                $mail->setArticle($article);
                $mail->setUri($uri);
                $mail->send($user->email);
            }

            $article->save();

            $msg['content'] = '���������� ���������';
            $msg['code'] = 0;
            $this->view->messages = array($msg);
        }

        $anShopCatTable = new ShopCatTable;
        $this->view->typeOptions = array(
            ShopArticle::TYPE_COPYWRITING => '����������',
            ShopArticle::TYPE_REWRITING => '���������',
            ShopArticle::TYPE_TRANSLATION => '��������'
        );

        $this->view->catOptions = $anShopCatTable->getOptions();
        $this->view->article = $article;
    }

    function deleteAction() {

        $this->_helper->auth->requireAdminPerm('shop-article-del');

        $params = $this->getRequest()->getParams();
        $this->view->title = '�������� ������';

        $anShopArticleTable = new ShopArticleTable();

        if ($this->getRequest()->isPost()) {
            $article = $anShopArticleTable->find($this->getRequest()->getPost('article_id'))->current();
            $article->delete();

            header('Location: /admin/shop-article/');
            exit;
        }

        $article = $anShopArticleTable->find($params['id'])->current();
        $this->view->article = $article;
    }

}