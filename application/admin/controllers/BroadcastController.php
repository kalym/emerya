<?php

class Admin_BroadcastController extends Zend_Controller_Action {

    function indexAction() {
        $this->_helper->auth->requireAdminPerm('broadcast');

        $this->view->typeOptions = array(
            '' => '����',
            User::TYPE_WRITER => '�������',
            User::TYPE_READER => '���������',
            User::TYPE_EMPLOYER => '�������������'
        );
        
        $this->view->title = '�������� (������������������ �������������)';

        if ($this->getRequest()->isPost()) {

            $anUsersTable = new UsersTable();

            $select = $anUsersTable->select();
            
            if ($type = $this->getRequest()->getParam('type')) {
                $select = $select->where('type=?', $type);
            }
            
            $subscribers = $anUsersTable->fetchAll($select);

            $registry = Zend_Registry::getInstance();
            $settings = $registry->get('settings');
            $config = $registry->get('config');

            $subject = $this->getRequest()->getParam('subject');

            $body = $this->getRequest()->getParam('content');

            foreach ($subscribers as $sub) {
                if ($sub->email) {
                    $mail = new Zend_Mail('windows-1251');
                    if ($this->getRequest()->getParam('is_html')) {
                        $mail->setBodyHtml($body);
                    } else {
                        $mail->setBodyText($body);
                    }
                    $mail->setFrom($settings['email_from'], $settings['email_name']);
                    $mail->addTo($sub->email, null);
                    $mail->setSubject($subject);
                    try {
                        $mail->send();
                    } catch (Exception $e) {
                        
                    }
                }
            }

            $msg['content'] = '��������� ����������';
            $msg['code'] = 0;
            $this->view->messages = array($msg);
        }
    }

}
