<?php

class Admin_AdvController extends Zend_Controller_Action
{

    function indexAction()
    {
        $this->_helper->auth->requireAdminPerm('adv');
        $this->view->title = '��������� �������';
        $anAdvTable = new AdvTable();
        $select = $anAdvTable->select();
        $select->from('adv', '*')
            ->setIntegrityCheck(false)
            ->columns(array(
                'click' => new Zend_Db_Expr('(SELECT COUNT(adv_click_id) FROM adv_click WHERE adv_id=adv.adv_id)'),
                'view' => new Zend_Db_Expr('(SELECT COUNT(adv_view_id) FROM adv_view WHERE adv_id=adv.adv_id)')
            ));

        $this->view->items = $anAdvTable->fetchAll($select);
    }

    function editAction()
    {
        $this->_helper->auth->requireAdminPerm('adv-edit');
        $this->view->title = '�������������� �������';

        $anAdvTable = new AdvTable();
        $record = $anAdvTable->find($this->getRequest()->getParam('id'))->current();
        $this->doFormAction($record);
    }

    function newAction()
    {
        $this->_helper->auth->requireAdminPerm('adv-edit');
        $this->view->title = '���������� �������';

        $anAdvTable = new AdvTable();
        $record = $anAdvTable->fetchNew();
        $this->doFormAction($record);
    }

    function statAction()
    {
        $this->_helper->auth->requireAdminPerm('adv-stat');
        $this->view->title = '����������';

        $id = $this->getRequest()->getParam('id');
        $anAdvTable = new AdvTable();
        $this->view->item = $anAdvTable->find($id)->current();


        $anAdvClickTable = new AdvClickTable;
        $select = $anAdvClickTable->select()->from('adv_click', array(
            'dat' => new Zend_Db_Expr('DATE(dattm)'),
            'click' => new Zend_Db_Expr('COUNT(*)')))
            ->group(new Zend_Db_Expr('DATE(dattm)'))
            ->where('dattm>?', date('Y-m-d', time() - 3600 * 24 * 14))
            ->where('adv_id=?', $id);

        $clickCnt = $anAdvClickTable->fetchAll($select);

        $anAdvViewTable = new AdvViewTable();
        $select = $anAdvViewTable->select()->from('adv_view', array(
            'dat' => new Zend_Db_Expr('DATE(dattm)'),
            'view' => new Zend_Db_Expr('COUNT(*)')))
            ->group(new Zend_Db_Expr('DATE(dattm)'))
            ->where('dattm>?', date('Y-m-d', time() - 3600 * 24 * 14))
            ->where('adv_id=?', $id);

        $viewCnt = $anAdvViewTable->fetchAll($select);

        $click = array();
        foreach ($clickCnt as $cc) {
            $click[$cc->dat] = $cc->click;
        }

        $view = array();
        foreach ($viewCnt as $vc) {
            $view[$vc->dat] = $vc->view;
        }

        $dat = array();
        for ($i = 14; $i >= 0; $i--)
            $dat[] = date('Y-m-d', strtotime("- $i day"));

        $stat = array();
        foreach ($dat as $d)
            $stat[$d] = array(
                'click' => isset($click[$d]) ? $click[$d] : 0,
                'view' => isset($view[$d]) ? $view[$d] : 0
            );

        $this->view->stat = $stat;
    }

    function doFormAction($record)
    {
        $this->_helper->viewRenderer->setScriptAction('form');
        if ($this->getRequest()->isPost()) {

            $post = $this->getRequest()->getPost();

            $record->title = $post['title'];
            $record->is_disabled = $post['is_disabled'];
            $record->html = $post['html'];
            $record->type = $post['type'] ? $post['type'] : 0;
            $record->category_id = $post['category_id'] ? $post['category_id'] : 0;

            $record->save();

            $msg['content'] = '���������� ���������';
            $msg['code'] = 0;
            $this->view->messages = array($msg);
        }

        $this->view->item = $record;
    }

    function deleteAction()
    {
        $this->_helper->auth->requireAdminPerm('adv-del');
        $params = $this->getRequest()->getParams();
        $this->view->title = '�������� �������';

        $anAdvTable = new AdvTable();

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();

            $adv = $anAdvTable->find($this->getRequest()->getPost('adv_id'))->current();
            $adv->delete();

            header('Location: /admin/adv/');
            exit;
        }
        $this->view->item = $anAdvTable->find($params['id'])->current();
    }
}