<?php

class Admin_McommentsController extends Zend_Controller_Action
{
    function indexAction()
    {
        
      $this->_helper->auth->requireAdminPerm('mcomments');
        
      $params  = $this->getRequest()->getParams();
      $this->view->title = '�����������';

      $anCommentsTable = new CommentsTable;
      
      if ( $this->getRequest()->isPost() ) {
          $comment = $this->getRequest()->getPost();
          foreach($comment['comment_id'] as $comment_id) {
              $comment = $anCommentsTable->find($comment_id)->current();
              $comment->is_approved = 1;
              $comment->save();
              
              $cp = new CommentsProcessor($comment->typology_id, $comment->typology);
              $cp->processApprovedComment($comment);

              $router = Zend_Controller_Front::getInstance()->getRouter();
              
              $url = call_user_func_array(array($router, 'assemble'), $comment->getUrlParams());
              $url = $url . '#comment-' . $comment->comment_id;

              HookManager::getInstance()->call(HookManager::EVENT_APPROVE_COMMENT, array(
                'item' => $cp->getItem(),
                'comment' => $comment,
                'url' => $url
            ));
          }
      }  
        
      $adapter = new Zend_Paginator_Adapter_DbTableSelect( $anCommentsTable->select()
                                                                        ->where('is_approved=?', '0')
                                                                        ->order('datetime DESC') );
        
      $page = (isset($params['p'])) ? $params['p'] : 1;
      $paginator = new Zend_Paginator( $adapter );
      $paginator->setCurrentPageNumber($page);
      $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
      $paginator->setPageRange(10);
      $this->view->paginator = $paginator;    
    }

}