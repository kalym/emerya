<?php

class Admin_PayController extends Zend_Controller_Action
{
    function indexAction()
    {
      $params  = $this->getRequest()->getParams();
      $this->view->title = '������';

      $this->_helper->auth->requireAdminPerm('pay');

      $anDeptsView = new DebtsView();
      
      $this->view->total = $anDeptsView->getTotal();

      $adapter = new Zend_Paginator_Adapter_DbTableSelect(
              $anDeptsView->select()
                  ->where('amount>0')
                  ->order('amount DESC')
              );


      $this->view->warning = $anDeptsView->fetchAll($anDeptsView->select()
                  ->where('amount<0'));
      
      $paginator = new Zend_Paginator( $adapter );
      $paginator->setCurrentPageNumber(
              $this->getRequest()->getParam('p', 1)
              );
      $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
      $paginator->setPageRange(10);
      $this->view->paginator = $paginator;

    }

    function payAction()
    {
        
        $this->_helper->auth->requireAdminPerm('pay-edit');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '������';

        $debtsView = new DebtsView();
        $debt = $debtsView->find($params['id'])->current();
        
        if ( $this->getRequest()->isPost() ) {
            $post = $this->getRequest()->getPost();

            $anPaymentsTable = new PaymentsTable();
            $anUsersTable = new UsersTable();
            $user = $anUsersTable->find($debt->user_id)->current();

            $payment = $anPaymentsTable->fetchNew();
            $payment->type = PaymentsTable::TYPE_EXTERNAL;
            $payment->user_id = $debt->user_id;
            $payment->comment = $post['comment'];
            $payment->amount = $post['amount'];
            $payment->datetime = date('Y-m-d H:i:s');

            $payment->save();
            
            $anPaymentsTable->update(array(
                'is_paid' => 1
            ), sprintf("user_id = %d AND type = %d AND datetime < '%s' AND is_paid=0",
                    $debt->user_id,
                    PaymentsTable::TYPE_INTERNAL,
                    date('Y-m-d H:i:s', $post['tm'])));

            $user->earn += $post['amount'];
            $user->save();
            
            $anFeedTable = new FeedTable();
            
            $feed = $anFeedTable->fetchNew();
            $feed->user_id = $user->user_id;
            $feed->datetime = $payment->datetime;
            $feed->content = sprintf('�� ��� ���� � Webmoney ����������� ������� � ������� %d ������', $post['amount']);
            $feed->is_new = 1;
            $feed->type = FeedTable::TYPE_PAYOUT;
            $feed->save();

            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);

            $nextDebt = $debtsView->fetchAll(
                $debtsView->select()
                  ->where('amount>0')
                  ->order('amount DESC')
                  ->limit(1)
            )->current();

            if ($nextDebt) {
                $action = 'pay';
                $id = $nextDebt->user_id;
            } else {
                $action = 'index';
                $id = null;
            }

            $this->_redirect(
                $this->view->url(
                    array(
                        'module'=>'admin',
                        'controller'=>'pay',
                        'action'=>$action,
                        'id' =>$id
                    ), null, true)
            );

        }

        $this->view->debt = $debt;
        $this->view->tm = time();
    }



}