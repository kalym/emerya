<?php

class Admin_BookController extends Zend_Controller_Action {

    function indexAction() {
        $this->_helper->auth->requireAdminPerm('book');

        $this->view->title = '������������ ����������';

        $anATView = new ATView();

        $adapter = new Zend_Paginator_Adapter_DbTableSelect($anATView->select()->where('is_active=1')->where('book_due>?', date('Y-m-d H:i:s'))->order('name DESC'));

        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber($this->getRequest()->getParam('p', 1));
        $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }

    function deleteAction() {
        $this->_helper->auth->requireAdminPerm('book-del');
        $anATTable = new ATTable();
        $at = $anATTable->find($this->getRequest()->getParam('id'))->current();

        $at->book_due = date('Y-m-d H:i:s', time() - 3600);
        $at->save();

        header("Location: /admin/book");
        exit;
    }

}