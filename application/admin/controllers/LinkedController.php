<?php

class Admin_LinkedController extends Zend_Controller_Action {

    function indexAction() {
        $q = $this->getRequest()->getParam('q', '');


        $this->_helper->auth->requireAdminPerm('articles-link');
        $params = $this->getRequest()->getParams();
        $this->view->title = '��������������� ������';
        $select = new Zend_Db_Select(Zend_Db_Table::getDefaultAdapter());
        $select = $select->from(array('a' => 'articles'))
                        ->joinLeft(array('al' => 'article_link'), 'a.article_id=al.article_id')
                        ->columns(array(
                            'name' => 'a.name',
                            'words' => new Zend_Db_Expr("GROUP_CONCAT(al.word SEPARATOR ', ')")
                        ))
                        ->group('a.article_id')->having("words<>''");

        if ($q)
            $select = $select->where('name LIKE ?', '%' . $q . '%');

        $this->view->items = $select->query(PDO::FETCH_OBJ);
    }

    function deleteAction() {
        $this->_helper->auth->requireAdminPerm('articles-link');
        $params = $this->getRequest()->getParams();

        $anArticleLink = new ArticleLinkTable;
        $anArticleLink->delete(sprintf('article_id=%d', $this->getRequest()->getParam('id')));
        $b = $this->getRequest()->getParam('b');
        header('Location: ' . ($b ? $b : ' /admin/linked/'));
        exit;
    }

}