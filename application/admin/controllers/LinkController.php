<?php

class Admin_LinkController extends Zend_Controller_Action {

    function indexAction() {
        $this->_helper->auth->requireAdminPerm('articles-link');
        $this->view->title = '������������ ������';
        $this->view->id = $this->getRequest()->getParam('id');
        
        $anArticlesTable = new ArticlesTable;
        $article = $anArticlesTable->find($this->view->id)->current();
        $this->view->title .= ' (' . $article->name . ')'; 
        $this->view->name = $article->name;
        
        $anArticleLinkTable = new ArticleLinkView();
        $select = $anArticleLinkTable->select()->where('article_id=?', $this->getRequest()->getParam('id'));
        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);
        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }

    function newAction() {
        $this->_helper->auth->requireAdminPerm('articles-link');
        $params = $this->getRequest()->getParams();
        $this->view->title = '����� �����';
        $this->view->id = $this->getRequest()->getParam('id');
        $this->view->b = $this->getRequest()->getParam('b', '/admin/link/?id=' . $this->view->id);

        if ($this->getRequest()->isPost()) {

            $post = $this->getRequest()->getPost();
            $anArticleLink = new ArticleLinkTable;
            $anArticleLink->insert(array(
                'article_id' => $this->getRequest()->getParam('id'),
                'word' => $post['word'],
                'article_link_id' => $this->getRequest()->getParam('article_link_id')
            ));

            $msg['content'] = '���������� ���������';
            $msg['code'] = 0;
            $this->view->messages = array($msg);
        }
    }
    
    function suggestAction() 
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $id = $this->getRequest()->getParam('id');
        $word = $this->getRequest()->getParam('word');
        
        $anArticlesTable = new ArticlesTable;
//        $this->view->items = $anArticlesTable->fetchAll($anArticlesTable->select()
//                    ->where('article_id<>?', $id)
//                    ->where('name LIKE ?', '%' . $word . '%'));
        $this->view->items = $anArticlesTable->fetchAll($anArticlesTable->select()
                    ->where('article_id<>?', $id)
                    ->where('MATCH(name) AGAINST(?)', iconv('utf8', 'cp1251', $word)));
    }

    function deleteAction() {
        $this->_helper->auth->requireAdminPerm('articles-link');
        $params = $this->getRequest()->getParams();

        $anArticleLink = new ArticleLinkTable;
        $item = $anArticleLink->find($this->getRequest()->getParam('id'))->current()->delete();
        $b = $this->getRequest()->getParam('b');
        header('Location: ' . ($b ? $b : ' /admin/articles/'));
        exit;
    }

}