<?php

class Admin_CountryController extends Site_Controller_Grid {
    
    protected $pk='country';
    protected $canCreate = false;
    protected $title = "������";
    
    function preDispatch() {
        if ($this->getRequest()->isXmlHttpRequest()) {
            $params = $this->getRequest()->getParams();
            $params = $this->decode("UTF-8", "windows-1251", $params);
            $this->getRequest()->setParams($params);
            $_POST = $this->decode("UTF-8", "windows-1251", $_POST);
            $_GET = $this->decode("UTF-8", "windows-1251", $_GET);
        }
    }
    
    function decode($from, $to, $value) {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->decode($from, $to, $v);
            }
        } else {
            $value = iconv($from, $to, $value);
        }
        return $value;
    }
    
    function createTable() {
        return new CountryTable;
    }
    
    function initFields() {
        $this->addField(new Site_Controller_Grid_Field_Icon('globe'));
        $this->addField('alpha2', '���');
        $this->addField('name', '�������� ������');
        $this->addField('name_ru', '�������� ������ (RU)')
                ->setRenderFunc(array($this, 'renderLiveEdit'));
        $this->addField('country', '')
                ->setRenderFunc(array($this, 'renderAction'));
        
    }
    
    function getActions($item) {
        return array();
    }
    
    function getFilter()
    {
        return new Site_Controller_Grid_Filter('', array('alpha2', 'name', 'name_ru'));
    }
    
    function quickUpdateAction()
    {
        Zend_Layout::getMvcInstance()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        
        /* @var $country Zend_Db_Table_Row */      
        $country = $this->getTable()->find($this->getRequest()->getParam('id'))->current();
        $params = $this->getRequest()->getParams();
        unset($params['id']);
        foreach ($params as $k => $v)
            $params[$k] = $v ? $v : null;
        $country->setFromArray($params);
        $country->save();
    }
    
    function renderLiveEdit($value, $record, $field)
    {
        return sprintf('<td data-country="%s" data-value="%s" data-field="%s" class="live-edit-field">%s</td>',
                $this->view->escape($record->country),
                $this->view->escape($value),
                $this->view->escape($field),
                $this->view->escape($value) );
                
    }
    
    function renderAction($value, $record) {
        return sprintf('<td width="%s">
            <a class="live-edit" data-country="%s" href="javascript:void(null)"><img src="/public/images/admin/edit.png" /></a>
            <a class="live-save" data-country="%s" style="display:none" href="javascript:void(null)"><img src="/public/images/admin/save.png" /></a>
</td>',
                '1%',
                $this->view->escape($value), 
                $this->view->escape($value));
    }
    
    function renderStatic()
    {
        return <<<'CUT'
<script type="text/javascript">
$('a.live-edit').click(function(){
    $(this).hide();
    $(this).closest('tr').find('a.live-save').show();
    $(this).closest('tr').find('.live-edit-field').each(function(){
        $(this).empty().append($('<input type="text" />').val($(this).data('value')));
    })
})
$('a.live-save').click(function(){
    $(this).hide();
    $(this).closest('tr').find('a.live-edit').show();
    var request = {};
    $(this).closest('tr').find('.live-edit-field').each(function(){
        $(this).data('value', $(this).find('input').val());
        $(this).empty().html( $(this).data('value'));
        request[$(this).data('field')] = $(this).data('value');
    })
    request['id'] = $(this).data('country');
    $.post('/admin/country/quick-update', request, function(){});
})
</script>        
CUT;
    }
}