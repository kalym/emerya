-- MySQL dump 10.13  Distrib 5.5.30, for Linux (x86_64)
--
-- Host: localhost    Database: emerya
-- ------------------------------------------------------
-- Server version	5.5.30-cll

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary table structure for view `articles_view`
--

DROP TABLE IF EXISTS `articles_view`;
/*!50001 DROP VIEW IF EXISTS `articles_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `articles_view` (
  `article_id` tinyint NOT NULL,
  `user_id` tinyint NOT NULL,
  `image` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `content` tinyint NOT NULL,
  `date` tinyint NOT NULL,
  `is_approved` tinyint NOT NULL,
  `cat_id` tinyint NOT NULL,
  `clicks` tinyint NOT NULL,
  `original` tinyint NOT NULL,
  `preview` tinyint NOT NULL,
  `preview2` tinyint NOT NULL,
  `alias` tinyint NOT NULL,
  `rating` tinyint NOT NULL,
  `comment` tinyint NOT NULL,
  `is_rework` tinyint NOT NULL,
  `next_sunday` tinyint NOT NULL,
  `at_id` tinyint NOT NULL,
  `podcast` tinyint NOT NULL,
  `adv` tinyint NOT NULL,
  `img_gallery` tinyint NOT NULL,
  `video_gallery` tinyint NOT NULL,
  `stat_begin` tinyint NOT NULL,
  `stat_expire` tinyint NOT NULL,
  `stat_stop` tinyint NOT NULL,
  `stat_view` tinyint NOT NULL,
  `stat_view_unique` tinyint NOT NULL,
  `stat_income` tinyint NOT NULL,
  `widget_img` tinyint NOT NULL,
  `u_avatar` tinyint NOT NULL,
  `u_rating` tinyint NOT NULL,
  `u_name_f` tinyint NOT NULL,
  `u_name_l` tinyint NOT NULL,
  `u_login` tinyint NOT NULL,
  `u_name` tinyint NOT NULL,
  `c_name` tinyint NOT NULL,
  `c_alias` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `articles_view`
--

/*!50001 DROP TABLE IF EXISTS `articles_view`*/;
/*!50001 DROP VIEW IF EXISTS `articles_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`emerya`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `articles_view` AS select `a`.`article_id` AS `article_id`,`a`.`user_id` AS `user_id`,`a`.`image` AS `image`,`a`.`name` AS `name`,`a`.`content` AS `content`,`a`.`date` AS `date`,`a`.`is_approved` AS `is_approved`,`a`.`cat_id` AS `cat_id`,`a`.`clicks` AS `clicks`,`a`.`original` AS `original`,`a`.`preview` AS `preview`,`a`.`preview2` AS `preview2`,`a`.`alias` AS `alias`,`a`.`rating` AS `rating`,`a`.`comment` AS `comment`,`a`.`is_rework` AS `is_rework`,`a`.`next_sunday` AS `next_sunday`,`a`.`at_id` AS `at_id`,`a`.`podcast` AS `podcast`,`a`.`adv` AS `adv`,`a`.`img_gallery` AS `img_gallery`,`a`.`video_gallery` AS `video_gallery`,`a`.`stat_begin` AS `stat_begin`,`a`.`stat_expire` AS `stat_expire`,`a`.`stat_stop` AS `stat_stop`,`a`.`stat_view` AS `stat_view`,`a`.`stat_view_unique` AS `stat_view_unique`,`a`.`stat_income` AS `stat_income`,`a`.`widget_img` AS `widget_img`,`u`.`avatar` AS `u_avatar`,`u`.`rating` AS `u_rating`,`u`.`name_f` AS `u_name_f`,`u`.`name_l` AS `u_name_l`,`u`.`login` AS `u_login`,trim(concat(`u`.`name_f`,' ',`u`.`name_l`)) AS `u_name`,`c`.`name` AS `c_name`,`c`.`alias` AS `c_alias` from ((`articles` `a` left join `users` `u` on((`a`.`user_id` = `u`.`user_id`))) left join `categories` `c` on((`a`.`cat_id` = `c`.`cat_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-05 22:24:17
