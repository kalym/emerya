<?php

class Admin_CatQnaController extends Zend_Controller_Action
{
    function indexAction()
    {
        $this->_helper->auth->requireAdminPerm('cat-qna');
        
      $params  = $this->getRequest()->getParams();
      $this->view->title = '������� ��������';
      $anCatQnaTable = new CatQnATable();
      $this->view->cats = $anCatQnaTable->fetchAll();

    }

    function editAction()
    {
        $this->_helper->auth->requireAdminPerm('cat-qna-edit');
        
        $this->view->title = '�������������� ������� ��������';
        $params  = $this->getRequest()->getParams();

        $anCatQnaTable = new CatQnATable();
        $cat = $anCatQnaTable->find($params['id'])->current();
        if ( $this->getRequest()->isPost() ) {
            $cat->name        = $this->getRequest()->getPost('name');
            $cat->alias       = $this->getRequest()->getPost('alias');
            $cat->save();

            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);
        }

        $this->view->cat = $cat;

    }

    function newAction()
    {
        $this->_helper->auth->requireAdminPerm('cat-qna-edit');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������� ���������';


        if ( $this->getRequest()->isPost() ) {
            $post = $this->getRequest()->getPost();
            $anCatQnaTable = new CatQnATable();

            $cat['name'] = $post['name'];
            $cat['alias'] = $post['alias'];
            $anCatQnaTable->insert($cat);

            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);
         }

    }

    function deleteAction()
    {
        $this->_helper->auth->requireAdminPerm('cat-qna-del');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������� ���������';

        $anCatQnATable = new CatQnATable();

        if ( $this->getRequest()->isPost() ) {
            $post = $this->getRequest()->getPost();

            $cat = $anCatQnATable->find($this->getRequest()->getPost('cat_id'))->current();
            $cat->delete();

            header('Location: /admin/cat-qna/');
            exit;
        }

        $cat = $anCatQnATable->find($params['id'])->current();

        $this->view->cat = $cat;
    }

}