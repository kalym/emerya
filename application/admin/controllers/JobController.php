<?php

class Admin_JobController extends Zend_Controller_Action {

    function indexAction() {

        $this->_helper->auth->requireAdminPerm('job');

        $this->view->title = '����� �������';

        $anJobView = new JobView();
        $select = $anJobView->select()->order('datetime DESC');

        $adapter = new Zend_Paginator_Adapter_DbTableSelect($select);

        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber(
                $this->getRequest()->getParam('p', 1)
        );
        $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
        $paginator->setPageRange(10);
        $this->view->paginator = $paginator;
    }

    function editAction() {

        $this->_helper->auth->requireAdminPerm('job-edit');

        $params = $this->getRequest()->getParams();
        $this->view->title = '�������������� ������';

        $anJobTable = new JobTable();
        $job = $anJobTable->find($params['id'])->current();

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            $job->price = $post['price'];
            $job->name = $post['name'];
            $job->content = $post['content'];
            $job->alias = $post['alias'];

            $job->save();

            $msg['content'] = '���������� ���������';
            $msg['code'] = 0;
            $this->view->messages = array($msg);
        }

        $this->view->item = $job;
    }

    function deleteAction() {

        $this->_helper->auth->requireAdminPerm('job-del');

        $params = $this->getRequest()->getParams();
        $this->view->title = '�������� ������';

        $anJobTable = new JobTable();

        if ($this->getRequest()->isPost()) {
            $job = $anJobTable->find($this->getRequest()->getPost('job_id'))->current();
            $job->delete();

            header('Location: /admin/job/');
            exit;
        }

        $job = $anJobTable->find($params['id'])->current();
        $this->view->item = $job;
    }

}