<?php

class Admin_BannersController extends Zend_Controller_Action {

    function indexAction() {
        $this->_helper->auth->requireAdminPerm('banners');

        $params = $this->getRequest()->getParams();
        $this->view->title = '�������';

        $anBannersTable = new BannersTable();

        $adapter = new Zend_Paginator_Adapter_DbTableSelect($anBannersTable->select());

        $page = (isset($params['p'])) ? $params['p'] : 1;
        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(30);
        $paginator->setPageRange(8);
        $this->view->paginator = $paginator;
    }

    function editAction() {
        $this->_helper->auth->requireAdminPerm('banners-edit');

        $params = $this->getRequest()->getParams();
        $this->view->title = '�������������� �������';

        $anBannersTable = new BannersTable();
        $this->view->typeOptions = $anBannersTable->getTypeOptions();
        $this->view->deviceOptions = $anBannersTable->getDeviceOptions();
        $banner = $anBannersTable->find($params['id'])->current();

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();

            $banner->title = $post['title'];
            $banner->type = $post['type'];
            $banner->device = $post['device'];
            $banner->url = $post['url'];
            $banner->picture_url = $post['picture_url'];

            $banner->save();
            $msg['content'] = '���������� ���������';
            $msg['code'] = 0;
            $this->view->messages = array($msg);
        }

        $this->view->banner = $banner;
    }

    function newAction() {
        $this->_helper->auth->requireAdminPerm('banners-edit');

        $params = $this->getRequest()->getParams();
        $this->view->title = '�������� �������';

        $anBannersTable = new BannersTable();
        $this->view->typeOptions = $anBannersTable->getTypeOptions();
        $this->view->deviceOptions = $anBannersTable->getDeviceOptions();

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();

            $banner['title'] = $post['title'];
            $banner['type'] = $post['type'];
            $banner['device'] = $post['device'];
            $banner['url'] = $post['url'];
            $banner['picture_url'] = $post['picture_url'];

            $anBannersTable->insert($banner);
            $msg['content'] = '���������� ���������';
            $msg['code'] = 0;
            $this->view->messages = array($msg);
        }
    }

    function deleteAction() {
        $this->_helper->auth->requireAdminPerm('banners-del');

        $params = $this->getRequest()->getParams();
        $this->view->title = '�������� �������';

        $anBannersTable = new BannersTable();

        if ($this->getRequest()->isPost()) {
            $banner = $anBannersTable->find($this->getRequest()->getPost('banner_id'))->current();
            $banner->delete();
            header('Location: /admin/banners/');
            exit;
        }

        $banner = $anBannersTable->find($params['id'])->current();
        $this->view->banner = $banner;
    }

}