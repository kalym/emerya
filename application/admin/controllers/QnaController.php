<?php

class Admin_QnaController extends Zend_Controller_Action
{
    function indexAction()
    {
        
        $this->_helper->auth->requireAdminPerm('qna');
        
      $this->view->title = '������� � ������';

      $anQnaTable = new QnaTable();
      if ( $this->getRequest()->isPost() ) {
          $post = $this->getRequest()->getPost();
          foreach($post['item_id'] as $item_id) {
              $item = $anQnaTable->find($item_id)->current();
              $item->delete();
          }
      }

      $select = $anQnaTable->select()->order('datetime DESC');
      $adapter = new Zend_Paginator_Adapter_DbTableSelect( $select );

      $paginator = new Zend_Paginator( $adapter );
      $paginator->setCurrentPageNumber(
            $this->getRequest()->getParam('p', 1)
      );
      $paginator->setItemCountPerPage(Site_Settings::get('back_item_perpage', 30));
      $paginator->setPageRange(10);
      $this->view->paginator = $paginator;

    }

    function editAction()
    {
        $this->_helper->auth->requireAdminPerm('qna-edit');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������������� �������';

        $anQnaTable = new QnaTable();
        $qna = $anQnaTable->find($params['id'])->current();

        if ( $this->getRequest()->isPost() ) {
            $__post = $this->getRequest()->getPost();

            $qna->name = $__post['name'];
            $qna->alias = $__post['alias'];
            
            $qna->video = $__post['video'];
            
            $gallery = array();
            foreach ($params['_gallery_preview'] as $k => $v) {
                if (!$v) continue;
                $gallery[] = array(
                    'preview' => $v,
                    'small' => $params['_gallery_small'][$k],
                    'big' => $params['_gallery_big'][$k],
                );
            }
            
            $qna->gallery = serialize($gallery);
            
            $qna->save();

            $msg['content'] = '���������� ���������';
            $msg['code']    = 0;
            $this->view->messages = array($msg);

        }
        $this->view->item = $qna;

    }

    function deleteAction()
    {
        $this->_helper->auth->requireAdminPerm('qna-del');
        
        $params  = $this->getRequest()->getParams();
        $this->view->title = '�������� �������';

        $anQnaTable = new QnaTable();

        if ( $this->getRequest()->isPost() ) {
            $qna = $anQnaTable->find($this->getRequest()->getPost('qna_id'))->current();
            $qna->delete();
            header('Location: /admin/qna/');
            exit;

        }

        $qna = $anQnaTable->find($params['id'])->current();
        $this->view->item = $qna;

    }

}